#!/bin/bash

if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

usage () {
    cat <<EOF
Script to restart docker-compose.yml files.

Options:
  -i    install crontab entry running daily
  -t    install crontab entry running daily at the specified time
  -r    install crontab entry to run at reboot
  -s    show status
  -c    check setup
  -u    uninstall crontab entry
  -l    ensure persistent log files and show logs
  -f    follow log files (tail -f)
  -p    show script process owning the lock
  -k    kill all running script instances
  -h    print (this) help
  -d    enable debug logging
  -v    enable verbose logging

Will stop and start files at:
~/docker/*/docker-compose.yml

The script can install itself as a cron job to run at reboot.

To install use:
${scriptNameRawBam_global} -i

To check install and show last log use:
${scriptNameRawBam_global} -c

To uninstall use:
${scriptNameRawBam_global} -u

If you are not root, the log file will be at /tmp
which does not persist across reboots. If you have
sudo rights, you can persist the log file at /var/log.

To to make log file persist across reboots use:
${scriptNameRawBam_global} -l

EOF
}

use cron

while getopts it:rsculfpkhdv o
do
    case "$o" in
        i )
            cronInstallDaily
            exit
            ;;
        t )
            cronInstallDaily "${OPTARG}"
            exit
            ;;
        r )
            cronInstallAtReboot
            exit
            ;;
        s )
            cronShowStatus
            exit 0
            ;;
        c )
            cronCheckScript
            exit
            ;;
        u )
            cronRemoveScript
            exit
            ;;
        l )
            cronEnsurePersistentLogFiles
            cronShowLogFiles
            exit
            ;;
        f )
            cronTailLogFiles
            exit
            ;;
        p )
            isCronScriptRunning
            exit 0
            ;;
        k )
            killall "${scriptName_global}"
            exit 0
            ;;
        h )
            usage
            exit 0
            ;;
        d )
            logDebugEnable
            ;;
        v )
            logVerboseEnable
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`

# MAIN

cronScriptBootstrap

use docker
dockerComposePullAndRestartAll
sleep 10
dockerPruneEverything

bamUpdate

