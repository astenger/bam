#!/bin/bash

if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            # We are suppressing the echo for cron scripts.
            #echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

usage () {
    local cronBootLogFile=''
    local cronExecLogFile=''

    getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile

    cat <<EOF
Sample cron script that executes in a loop.

Options:
  -i    install crontab entry to keep script running
  -t    install crontab entry running daily at the specified time
  -s    show status
  -c    check setup
  -u    uninstall crontab entry
  -l    ensure persistent log files and show logs
  -f    follow log files (tail -f)
  -p    show script process owning the lock
  -k    kill all running script instances
  -s    perform single run only
  -h    print (this) help
  -d    enable debug logging
  -v    enable verbose logging

cronScriptBootstrap ensures that there is only one instance of the
script running. When cron or something else starts the script again it
will cronScriptBootstrap if another script instance is active.

When cron starts the script you can see traces for that in this log:
${cronBootLogFile}

When cronScriptBootstrap does not exit and the script executes this is
the active log:
${cronExecLogFile}

Both log files will be overwritten by subequent executions.


${scriptNameRawBam_global} -t ''

Will install a crontab entry for the script to be run at a random time
between 02:00 and 05:59. The lazy man's way to spread daily cron task
across different times.

The -t option is a good option for cron scripts that are only supposed
to run once daily and exit after. With this approach -i usually
installs at a random time like -t ''.

This is option not very useful for scripts that are supposed to be
active and running in a loop all the time.


${scriptNameRawBam_global} -t 'xx:xx'

Will install a crontab entry for the script to be run at the specified
time once a day.


${scriptNameRawBam_global} -l

The -l option relocates the log files from /tmp to /var/log to ensure
visibility across reboots.

If the script is run or installed as root /var/log is the default
location.

If the script is run or installed as a user /tmp is the default
location and sudo access is required to relocate the logs. Once the
logs have been relocated user executions will use the /var/log
location as well. It is assumed that the same user will re-execute the
script. If you have multiple different users execute the script you
may need to fine tune access modes on the log files to make it work.

If you don't have a sudo password for the user, but you do have root
access to the host, you can run the script with -l as the user and
stop the script using ctrl-c, and then re-run the script with -l as
root to relocate aka mv the logs. This will preserve the original user
based ownership of the log files.

This also shows the log file contents.

${scriptNameRawBam_global} -v

Verbose mode will not overwrite flash messages. This will also show
additional internal global variables.


${scriptNameRawBam_global} -d

Debug mode will also show verbose messages as described above.


${scriptNameRawBam_global} -s

Performs a single run only and logs to the terminal instead of the log
files. Useful during development or for a quick manual check if there
is no other script instance active.

Useful for scripts that are actively running in a loop all the time.

EOF
}

use cron

singleRun='no'
while getopts it:sculpkshdv o
do
    case "$o" in
        i )
            cronInstallEveryTenMinutes
            exit
            ;;
        t )
            cronInstallDaily "${OPTARG}"
            exit
            ;;
        s )
            cronShowStatus
            exit 0
            ;;
        c )
            cronCheckScript
            exit
            ;;
        u )
            cronRemoveScript
            exit
            ;;
        l )
            cronEnsurePersistentLogFiles
            exit
            ;;
        p )
            isCronScriptRunning
            exit 0
            ;;
        k )
            killall "${scriptName_global}"
            exit 0
            ;;
        s )
            singleRun='yes'
            ;;
        h )
            usage
            exit 0
            ;;
        d )
            logDebugEnable
            ;;
        v )
            logVerboseEnable
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`

run () {
    logInfo `date`

    showGlobalVars

    for i in 1 2 3 4 5 6 7 8 9 10; do
        logFlash   "A flash message. ${i}/10"
        sleep .2
    done
    logFlashClear
    logDebug   "A debug message."
    logVerbose "A verbose message."
    logInfo    "A info message."
    logWarning "A warning message."
    logError   "A error message."
}

loop () {
    while true; do
        run
        sleep 60
    done
}

# MAIN

if [ "${singleRun}" = 'yes' ]; then
    cronScriptBootstrap --noLogFiles
    run
else
    cronScriptBootstrap
    loop
fi
