#!/bin/bash

if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

usage () {
    cat <<EOF
Script that shows global variables during script execution.

Some variables reported by showGlobalVars are empty during interactive
shell sessions and this script shows example values when an actual
script is run.

Also note that additional modules may introduce additional global
variables.

Additional modules can be loaded with one or more -m options.

${scriptNameRawBam_global} -m m  # load additional module
${scriptNameRawBam_global} -h    # show (this) usage information
${scriptNameRawBam_global} -d    # enable debug logging
${scriptNameRawBam_global} -v    # enable verbose logging

${scriptNameRawBam_global} -m pid -m timer

Load additional modules pid and timer before listing global variables.


${scriptNameRawBam_global} -v

This will also show additional internal global variables.


${scriptNameRawBam_global} -d

Debug mode will also show verbose messages as described above.

EOF
}

while getopts m:hdv o
do
    case "$o" in
        m )
            use "${OPTARG}"
            ;;
        h )
            usage
            exit 0
            ;;
        d )
            logDebugEnable
            ;;
        v )
            logVerboseEnable
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`

# MAIN

showGlobalVars
