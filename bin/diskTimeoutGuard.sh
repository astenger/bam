#!/bin/bash

if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

usage () {
    cat <<EOF
Script to relax disk timeouts and to trigger reboots when the root
disk is mounted read-only.

Options:
  -i    install crontab entry running every hour
  -s    show status
  -c    check setup
  -u    uninstall crontab entry
  -l    ensure persistent log files and show logs
  -f    follow log files (tail -f)
  -p    show script process owning the lock
  -k    kill all running script instances
  -h    print (this) help
  -d    enable debug logging
  -v    enable verbose logging

Needs to be run as root.

This is helpful on VMs in virtualized environments where disk IO might
hang when the underlying storage becomes unavailable for some time.

The script installs itself as a cron job and runs every hour.

To install use:
${scriptNameRawBam_global} -i

To check install and show last log use:
${scriptNameRawBam_global} -c

To uninstall use:
${scriptNameRawBam_global} -u

EOF
}

use cron
use pid

while getopts isculfpkhdv o
do
    case "$o" in
        i )
            cronInstallEveryHour
            exit
            ;;
        s )
            cronShowStatus
            exit 0
            ;;
        c )
            cronCheckScript
            exit
            ;;
        u )
            cronRemoveScript
            exit
            ;;
        l )
            cronEnsurePersistentLogFiles
            cronShowLogFiles
            exit
            ;;
        f )
            cronTailLogFiles
            exit
            ;;
        p )
            isCronScriptRunning
            exit 0
            ;;
        k )
            killall "${scriptName_global}"
            exit 0
            ;;
        h )
            usage
            exit 0
            ;;
        d )
            logDebugEnable
            ;;
        v )
            logVerboseEnable
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`

ignoreMountPointPaterns_global="
/var/snap/
"

relaxDiskTimeouts () {
    local timeoutInSeconds="$1"

    setDefault timeoutInSeconds 600

    assertInt "${timeoutInSeconds}"

    local aTimeoutFile=''
    local currentTimeout=''
    local desiredTimeout=600
    local timeoutCount=0
    for aTimeoutFile in `ls /sys/class/scsi_generic/*/device/timeout 2>/dev/null`; do
        timeoutCount+=1
        currentTimeout=`cat ${aTimeoutFile}`
        if [ "${currentTimeout}" -ge "${desiredTimeout}" ]; then
            logVerbose "Timeout already set for: ${aTimeoutFile}"
            continue
        fi
        logInfo "Setting timeout to 5 minutes (600 s): ${aTimeoutFile} (was ${currentTimeout} s)"
        echo ${timeoutInSeconds} > "${aTimeoutFile}"
    done
    if [ "${timeoutCount}" = '0' ]; then
        logErrorAndAbort "No timeout controls at: /sys/class/scsi_generic/*/device/timeout"
    fi
}

installTimeoutsInRcLocal () {
    logErrorAndAbort "Not implemented."
    # Edit /etc/rc.local and add before the final exit 0:
    # for i in /sys/class/scsi_generic/*/device/timeout; do echo 600 > "$i"; done
    # Check settings after reboot:
    # for i in /sys/class/scsi_generic/*/device/timeout; do cat "$i"; done
}

isNfs4MountRespondingR1 () {
    local returnVarYesNo="$1"
    local nfsMountPoint="$2"

    local nfs4MountTest=`mount | grep " on ${nfsMountPoint} type nfs4"`
    if [ ! -d "${nfsMountPoint}" ]; then
        logError "nfs4 mount point is not a directory: ${nfsMountPoint}"
        setVar "${returnVarYesNo}" ''
        return
    fi
    if [ "${nfs4MountTest}" = '' ]; then
        logError "nfs4 mount point is not mounted: ${nfsMountPoint}"
        setVar "${returnVarYesNo}" ''
        return
    fi

    local doneBeforeTimeout
    pidExecuteWithTimeoutR1 -s doneBeforeTimeout ls "${nfsMountPoint}"
    setVarOrLogInfo "${returnVarYesNo}" "${doneBeforeTimeout}"
}

areAllNfs4MountsRespondingR1 () {
    local returnVarYesNo="$1"

    local activeNfs4Mounts=`
              mount \
                  | grep 'type nfs4 ' \
                  | sed 's/ type .*//' \
                  | sed 's/.* on //'`
    local etcFstabNfs4Mounts=`
              cat /etc/fstab \
                  | pipeStripComments \
                  | egrep "[ ${tab_global}]nfs4[ ${tab_global}]" \
                  | sed "s/[ ${tab_global}][ ${tab_global}]*nfs4[ ${tab_global}].*//" \
                  | sed "s/.*[ ${tab_global}]//"`

    if [ "${activeNfs4Mounts}${etcFstabNfs4Mounts}" = '' ]; then
	logDebug "There are no known nfs4 mounts."
        setVarOrLogInfo "${returnVarYesNo}" 'yes'
        return
    fi

    local allResponding='yes'
    local thisOk='no'
    local responding
    local aMount
    local maxRetries=5
    local tryCount=0
    logDebug "Checking mounts:
${etcFstabNfs4Mounts}"
    while read aMount; do
        aMount="${aMount%/}"
        thisOk='no'
        logDebug "Checking mount : ${aMount}"
        tryCount=0
        while [ "${tryCount}" -lt "${maxRetries}" ]; do
            let tryCount+=1
            logDebug "Try ${tryCount}/${maxRetries}"
            activeNfs4Mounts=`
              mount \
                  | grep 'type nfs4 ' \
                  | sed 's/ type .*//' \
                  | sed 's/.* on //'`
            local isMounted=`echo "${activeNfs4Mounts}" | egrep "^${aMount}"'$'`
            if [ "${isMounted}" = '' ]; then
                logWarning "fstab nfs4 mount is NOT mounted: ${aMount}"
                logInfo "Mounting: ${aMount}"
                mount "${aMount}"
                logDebug "Sleeping for 1s"
                sleep 1
                sync
                isMounted=`echo "${activeNfs4Mounts}" | egrep "^${aMount}"'$'`
                if [ "${isMounted}" != '' ]; then
                    logInfo "Mount successful."
                    thisOk='yes'
                    break
                fi
            else
                logInfo "fstab nfs4 mount is     mounted: ${aMount}"
                thisOk='yes'
                break
            fi
        done
        if [ "${thisOk}" != 'yes' ]; then
            allResponding='no'
        fi
    done < <( echo "${etcFstabNfs4Mounts}" | pipeStripStandard )
    activeNfs4Mounts=`
              mount \
                  | grep 'type nfs4 ' \
                  | sed 's/ type .*//' \
                  | sed 's/.* on //'`
    logDebug "Checking responsiveness:
${activeNfs4Mounts}"
    while read aMount; do
        logDebug "Checking response: ${aMount}"
        continue
        tryCount=0
        while [ "${tryCount}" -lt "${maxRetries}" ]; do
            if [ "${tryCount}" != '0' ]; then
                sleep 1
                logDebug "Sleeping for 1s"
            fi
            let tryCount+=1
            logDebug "Try ${tryCount}/${maxRetries}"
            isNfs4MountRespondingR1 responding "${aMount}"
            if [ "${responding}" = 'no' ]; then
                allResponding='no'
                logWarning "active nfs4 mount is NOT responding: ${aMount}"
            else
                logInfo "active nfs4 mount is     responding: ${aMount}"
            fi
        done
    done < <( echo "${activeNfs4Mounts}" | pipeStripStandard )
    setVarOrLogInfo "${returnVarYesNo}" "${allResponding}"
}

rebootIfNfs4MountsAreNotResponding () {
    local allNfs4MountsAreResponding=''
    areAllNfs4MountsRespondingR1 allNfs4MountsAreResponding
    if [ "${allNfs4MountsAreResponding}" = 'no' ]; then
        logWarning "NOT Rebooting ..."
        ## reboot
    fi
}

isDirWriteableR1 () {
    local returnVarYesNo="$1"
    local dirToTest="$2"

    local testFileNameShort=".bam.isDirWriteableR1.${username_global}"
    qualifyPathR1 dirToTest

    assertDir "${dirToTest}"

    local testFileNameLong="${dirToTest}/${testFileNameShort}"
    if [ -e "${testFileNameLong}" ]; then
        logDebug "Removing: ${testFileNameLong}"
        rm -f "${testFileNameLong}" 2>/dev/null
        if [ -e "${testFileNameLong}" ]; then
            logInfo "Unable to remove existing file: ${testFileNameLong}"
            # Unable to remove file = not writable

            # There may be edge cases where root owns the file and a
            # user is trying to remove it. Since we have the username
            # in the file name that should not happen. And if we are
            # testing as root it won't matter anyways. Good enough.
            setVarOrLogInfo "${returnVarYesNo}" 'no'
            return
        fi
    fi
    logDebug "Creating: ${testFileNameLong}"
    touch "${testFileNameLong}"
    if [ ! -f "${testFileNameLong}" ]; then
        logInfo "Unable to create test file: ${testFileNameLong}"
        setVarOrLogInfo "${returnVarYesNo}" 'no'
        return
    fi
    setVarOrLogInfo "${returnVarYesNo}" 'yes'
    logDebug "Removing: ${testFileNameLong}"
    rm -f "${testFileNameLong}" 2>/dev/null
    if [ -f "${testFileNameLong}" ]; then
        logWarning "Unable to remove test file after creating it: ${testFileNameLong}"
    fi
}

rebootIfLocalDisksAreNotWritable () {
    assertIamRoot

    local mounts
    mounts=`mount | egrep '^/dev/sd'`
    #mounts=`mount`
    local mount=''
    local reboot='no'
    while read mount; do
        local device=`echo "${mount}" | sed 's/ on .*//'`
        local mountPoint=`echo "${mount}" | sed 's/.* on //' | sed 's/ type .*//'`
        local isReadOnly=`echo "${mount}" | grep '[\(,]ro[,\)]'`
        local isReadWrite=`echo "${mount}" | grep '[\(,]rw[,\)]'`
        if [ "${isReadOnly}" = '' ]; then
            if [ "${isReadWrite}" = '' ]; then
                logError "This is neither readonly nor readwrite:
${mount}"
                logInfo "Skipping ..."
                continue
            fi
            logInfo "Is mounted readwrite: ${mountPoint}"
            local isWritable=''
            isDirWriteableR1 isWritable "${mountPoint}"
            if [ "${isWritable}" = 'no' ]; then
                reboot=yes
            fi
            continue
        fi
        if [ "${isReadWrite}" != '' ]; then
            logError "This is readonly and readwrite (???):
${mount}"
            logInfo "Skipping ..."
            continue
        fi
        # This is readonly.
        logWarning "Read-only file system at: ${isReadWrite}"
        logInfo "device=${device}"
        logInfo "mountPoint=${mountPoint}"
        local pattern=''
        while read pattern; do
            local ignoreTest=`echo "${mountPoint}" | grep -E "${pattern}"`
            if [ "${ignoreTest}" != '' ]; then
                logInfo "Ignoring mount based on mpunt point pattern: ${pattern}"
                continue 2
            fi
        done < <( echo "${ignoreMountPointPaterns_global}" | pipeStripStandard )
        logWarning "Running fsck on ${device}"
        local trace=''
        trace=`fsck -y "${device}"`
        logInfo "${trace}"
        reboot='yes'
    done < <( echo "${mounts}" )
    if [ "${reboot}" = 'yes' ]; then
        logWarning "Rebooting ..."
        reboot
    fi
}

# MAIN

cronScriptBootstrap

relaxDiskTimeouts
rebootIfLocalDisksAreNotWritable
rebootIfNfs4MountsAreNotResponding
