#!/bin/bash

if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            if [ -t 0 ]; then
                echo "INFO  Sourcing: ${bam}"
            fi
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

usage () {
    cat <<EOF

This is a shell script wrapper for the awsSsmSsh function in the aws
module. Use the source, Luke!

This is helpful for using awsSsmSsh to create shell tunnels, e.g. for
use wirh rsync. To copy this script to an instance where you know the
name aka the instance with the 'Name' tag myInstanceName, you can copy
this script to the home directory of the ec2-user using this:

rsync -e '${scriptNameQualified_global} --name myInstanceName' \\
    --recursive --archive --verbose --progress --partial --compress \\
    '${scriptNameQualified_global}' :~/

EOF
}

# There is no options processing in here since we are passing all
# options through. With one exception:
if [ "$1" = '-h' ]; then
    usage
    exit 0
fi

use aws

awsSsmSsh "$@"
