#!/bin/bash

# run all tests

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

usage () {
    cat <<EOF
Script to run tests in bash mode.

Options:
  -v  verbose; will show successful test names as well
  -h  show (this) help

Will run all tests by default.

You can specify particular modules only, e.g.:

${scriptNameRawBam_global} bam

EOF
}

while getopts vd o
do
    case "$o" in
        v )
            logVerboseEnable
            ;;
        d )
            logDebugEnable
            ;;
        h )
            usage
            exit 0
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`


use test

showGlobalVars

runTests $*
