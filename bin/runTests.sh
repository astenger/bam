#!/bin/sh

# run all tests

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            . "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

usage () {
    cat <<EOF
Script to run tests in sh mode.

Options:
  -v  verbose; will show successful test names as well
  -h  show (this) help

Will run all tests by default.

You can specify particular modules only, e.g.:

${scriptNameRawBam_global} bam

EOF
}

while getopts vd o
do
    case "$o" in
        v )
            logVerboseEnable
            ;;
        d )
            logDebugEnable
            ;;
        h )
            usage
            exit 0
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`


use test

showGlobalVars

# bash simulated sh can handle "${variable:0:1}"
# but dash simulated sh can not
# https://wiki.ubuntu.com/DashAsBinSh

shIsPointingTo=`readlink /bin/sh | sed 's/.*\///'`
if [ "${shIsPointingTo}" = '' ]; then
    logDebug "/bin/sh is not a softlink. All good."
elif [ "${shIsPointingTo}" = 'busybox' ]; then
    logDebug "/bin/sh is pointing to busybox. All good."
elif [ "${shIsPointingTo}" = 'bash' ]; then
    logDebug "/bin/sh is pointing to bash. All good."
elif [ "${shIsPointingTo}" = 'dash' ]; then
    # We'll probably never get here since testStringSlicing fails when
    # bam.sh is sourced.
    echo "ERROR  dash has all sorts of problems."
    abort 2>/dev/null
else
    echo "WARN  /bin/sh is pointing to: ${shIsPointingTo}"
    echo "WARN  This may be a bumpy ride."
fi

runTests $*
