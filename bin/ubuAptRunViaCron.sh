#!/bin/bash

if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

usage () {
    cat <<EOF
Script to update the local package database and install package upgrades
on ubuntu.

Options:
  -i    install crontab entry running daily
  -t    install crontab entry running daily at the specified time
  -r    install crontab entry to run at reboot
  -j    enable  jojo:        reboot after updates if required
  -J    disable jojo: do NOT reboot after updates
  -s    show status
  -c    check setup
  -u    uninstall crontab entry
  -l    ensure persistent log files and show logs
  -f    follow log files (tail -f)
  -p    show script process owning the lock
  -k    kill all running script instances
  -h    print (this) help
  -d    enable debug logging
  -v    enable verbose logging

The script can install itself as a cron job to run nightly. A random
time between 02:00 and 05:59 will be used for execution, unless a time
is specified.

If env var ubuAptRunViaCron_rebootIfRequired=yes, a reboot will be
executed if required aka if /var/run/reboot-required exists.
To enable reboots use:
    ${scriptNameRawBam_global} -j
To disable reboots use:
    ${scriptNameRawBam_global} -J
or uninstall the cron job:
    ${scriptNameRawBam_global} -u

To install use one of these:
${scriptNameRawBam_global} -i        # install for daily run, random time
${scriptNameRawBam_global} -t 03:30  # install for daily run at 03:30
${scriptNameRawBam_global} -i -j     # install and enable jojo/reboots

To check install and show last log use:
${scriptNameRawBam_global} -c

To uninstall use:
${scriptNameRawBam_global} -u

EOF
}

checkRebootStatus () {
    unset ubuAptRunViaCron_rebootIfRequired
    envVarLoad ubuAptRunViaCron_rebootIfRequired
    if [ "${ubuAptRunViaCron_rebootIfRequired}" = 'yes' ]; then
        logInfo "jojo/reboot if required is enabled."
        logInfo "Call script with option -J to disable:"
        logInfo "${scriptNameRawBam_global} -J"
    else
        logInfo "jojo/reboot if required is disabled."
        logInfo "Call script with option -j to enable:"
        logInfo "${scriptNameRawBam_global} -j"
    fi
}

assertIamRoot

use cron

while getopts it:rsculfpkrjJhdv o
do
    case "$o" in
        i )
            cronInstallDaily
            checkRebootStatus
            exit
            ;;
        t )
            cronInstallDaily "${OPTARG}"
            checkRebootStatus
            exit
            ;;
        r )
            cronInstallAtReboot
            checkRebootStatus
            exit
            ;;
        s )
            cronShowStatus
            exit 0
            ;;
        c )
            cronCheckScript
            checkRebootStatus
            exit
            ;;
        u )
            cronRemoveScript
            envVarDelete ubuAptRunViaCron_rebootIfRequired
            exit
            ;;
        l )
            cronEnsurePersistentLogFiles
            cronShowLogFiles
            exit
            ;;
        f )
            cronTailLogFiles
            exit
            ;;
        p )
            isCronScriptRunning
            exit 0
            ;;
        k )
            killall "${scriptName_global}"
            exit 0
            ;;
        j )
            installedAndEnabled=''
            cronCheckScriptR1 installedAndEnabled 1>/dev/null 2>&1
            if [ "${installedAndEnabled}" = 'yes' ]; then
                ubuAptRunViaCron_rebootIfRequired='yes'
                envVarSave ubuAptRunViaCron_rebootIfRequired
                checkRebootStatus
            else
                logErrorAndAbort "Cron job is not enabled."
            fi
            exit
            ;;
        J )
            installedAndEnabled=''
            cronCheckScriptR1 installedAndEnabled 1>/dev/null 2>&1
            if [ "${installedAndEnabled}" = 'yes' ]; then
                ubuAptRunViaCron_rebootIfRequired='no'
                envVarSave ubuAptRunViaCron_rebootIfRequired
                checkRebootStatus
            else
                logErrorAndAbort "Cron job is not enabled."
            fi
            exit
            ;;        h )
            usage
            exit 0
            ;;
        d )
            logDebugEnable
            ;;
        v )
            logVerboseEnable
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`

# MAIN

cronScriptBootstrap

use ubu
ubuAptRun

checkRebootStatus

if [ ! -f /var/run/reboot-required ]; then
    logWarning "Reboot is not required."
else
    if [ "${ubuAptRunViaCron_rebootIfRequired}" = 'yes' ]; then
        logInfo "Reboot is required and enabled. Rebooting."
        /sbin/reboot
    else
        logWarning "Reboot is required, but disabled for automatic execution."
    fi
fi
