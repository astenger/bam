#!/bin/bash

# run all tests

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

usage () {
    cat <<EOF

Script to test the reExecute function.

It reduces remaining maximum re-execution depth with each
call by one.

The script can also demonstrate the safety limit in action.
See examples below.

Options:
  -r <value>  set remaining re-execution depth
  -h          show (this) help

Examples:

# re-execute myself once:
${scriptName_global}

# re-execute myself 4 times, aka I'll be running 5 times:
${scriptName_global} -r 5

# test the re-execute defaut limit:
${scriptName_global} -r 6

# Set a higher depth limit:
( export BAM_RE_EXECUTE_MAX_DEPTH; reExececuteTest.bash -r 6 )

# Test abort for higher limit:
( export BAM_RE_EXECUTE_MAX_DEPTH; reExececuteTest.bash -r 7 )

EOF
}

remainingDepth=''
while getopts r:h o
do
    case "$o" in
        r )
            remainingDepth=${OPTARG}
            ;;
        h )
            usage
            exit 0
            ;;
        * )
            usage
            exit 1
            ;;
    esac
done
shift `expr $OPTIND - 1`

setDefault remainingDepth 2

if [ "${remainingDepth}" -le 1 ]; then
    logInfo "We reached remaining depth=${remainingDepth}"
    logInfo "Done :-)"
    exit 0
fi

let nextDepth=${remainingDepth}-1

reExecute -r ${nextDepth}
