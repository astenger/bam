#!/bin/bash

# bam bootstrap aka install script

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi

bootstrapFetchGit () {
    local isGitInPath=`which git 2>/dev/null`
    if [ "${isGitInPath}" = '' ]; then
        logErrorAndAbort "git not found in PATH"
    fi
    # On Mac, sometimes one need to agree to the xcode license AGAIN ...
    local gitVersionOutput=`git --version`
    local isGitUsable=`echo "${gitVersionOutput}" | grep 'git version'`
    if [ "${isGitUsable}" = '' ]; then
        logErrorAndAbort "git --version output does not look like git:
${gitVersionOutput}"
    fi

    if [ -f ./bam/.git/FETCH_HEAD ]; then
        logInfo "bootstrapFetchGit(): Updating bam using git pull."
        (
            cd ./bam/
            git pull
        )
    else
        logInfo "bootstrapFetchGit(): Running initial git clone."
        git clone https://gitlab.com/astenger/bam.git
    fi
}

bootstrapFetchTar () {
    tarGzUrl='https://gitlab.com/astenger/bam/-/archive/master/bam-master.tar.gz'
    pipeDownloadFile "${tarGzUrl}" | tar xzf -
    assertDir './bam-master'
    assertDoesNotExist './bam'
    mv './bam-master' './bam'
}

bootstrapErrorAndAbort () {
    echo "ERROR $@"
    echo "ERROR bootstrap failed."
    exit 1
}

bootstrap () {
    if [ "${bam_global}" = '' ]; then
        # With bash we can do fancy redirect, but we don't know
        # whether we even have bash on the current machine, which
        # might be sh only. Rather than requiring a local bash, we'll
        # require the ability to write to /tmp.
        if [ -f /tmp/bam.sh.$$ ]; then
            rm -f /tmp/bam.sh.$$
        fi
        if [ -f /tmp/bam.sh.$$ ]; then
            bootstrapErrorAndAbort "Unable to remove /tmp/bam.sh.$$"
        fi
        if [ "`which curl 2>/dev/null`" != '' ]; then
            curl -s 'https://gitlab.com/astenger/bam/-/raw/master/bam.sh' > /tmp/bam.sh.$$
        elif [ "`which wget 2>/dev/null`" != '' ]; then
            wget -qO- 'https://gitlab.com/astenger/bam/-/raw/master/bam.sh' > /tmp/bam.sh.$$
        else
            bootstrapErrorAndAbort "Neither curl nor wget in PATH."
        fi
        if [ ! -f /tmp/bam.sh.$$ ]; then
            bootstrapErrorAndAbort "Unable to download https://gitlab.com/astenger/bam/-/raw/master/bam.sh"
        fi
        export BAM_BOOT=yes
        if [ "`which bash 2>/dev/null`" != '' ]; then
            bash --rcfile /tmp/bam.sh.$$
            return
        elif [ "`which sh 2>/dev/null`" != '' ]; then
            ENV=/tmp/bam.sh.$$
            export ENV
            sh
            return
        elif [ "`which zsh 2>/dev/null`" != '' ]; then
            bootstrapErrorAndAbort "Bootstrap on zsh is not implemented."
        fi
        return
    fi
    logInfo "Running bootstrap."

    # Common edge case scenarios:
    # 1) new Linux box set up with user first login as user, initial
    #    sudo bash does not change the directory to /root, but stays
    #    in the users home directory. This means that the files will
    #    be owned by root and the user can't update the install.
    # 2) on AWS sudo leads to current dir being / instead of /root
    # 3) AWS SSM: Fresh sessions are in /usr/bin.
    local currentDir=`pwd`
    if [ "`id`" = 'uid=0(root) gid=0(root) groups=0(root)' ]; then
        if [ "${currentDir}" = '/' -o "${currentDir:0:6}" = '/home/' ]; then
            if [ -d /root ]; then
                logInfo "Changing directory to: /root"
                cd /root
            fi
        fi
    fi
    if [ "`pwd`" = '/usr/bin' ]; then
        cd
    fi

    currentDir=`pwd`
    if [ "${currentDir}" != '/root' -a "${currentDir:0:6}" != '/home/' ]; then
        logWarning "Installing outside of home directory: ${currentDir}"
    fi

    mkdir -p bam
    assertDir './bam'

    local isGitInPath=`which git 2>/dev/null`

    if [ "${isGitInPath}" != '' ]; then
        bootstrapFetchGit
    else
        logWarning "git is not in PATH. Installing from tar."
        bootstrapFetchTar
    fi

    assertDir './bam'
    assertFile './bam/bam.sh'
    bam_global='./bam/bam.sh'
    qualifyPathR1 bam_global
    logInfo "bam is available at: ${bam_global}"
    logInfo "Source bam to use it right away:
. ${bam_global}"
    exit 0
}

# MAIN

bootstrap
