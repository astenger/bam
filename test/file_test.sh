#!/bin/bash

# test file module functions functions

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

use test
use file

showDir () {
    logInfo "==== $1"
    for f in `ls "$1"`; do
        logInfo -- "---- $f
`cat $1/$f`"
    done
    logInfo "===="
}


fileUpdateBackupRestore_test () {
    local tempdir=`
              mktemp -d /tmp/bam_file_temp_dir_${scriptName_global}.XXXXXX` \
              || logErrorAndAbort "Unable to create temp dir."
    logInfo "Created temp dir: ${tempdir}"
    local testfile="${tempdir}/testfile"
    local backupFile1=''
    logDisable
    fileUpdateR1 backupFile1 "${testfile}" 'v1'
    logEnable
    local fileV1=`cat "${testfile}"`
    testAssertEqualValue "${fileV1}" 'v1' 'v1 update'
    testAssertNotEmpty "${backupFile1}" 'backup file new file'
    local lineCount=`cat "${backupFile1}" | wc -l`
    testAssertEqualValue ${lineCount} '0' 'empty backup for new file'

    local backupFile2=''
    logDisable
    fileUpdateR1 backupFile2 "${testfile}" 'v2'
    logEnable
    local backupV1=`cat "${backupFile2}"`
    testAssertEqualValue "${backupV1}" 'v1' 'v1 backup'
    local fileV2=`cat "${testfile}"`
    testAssertEqualValue "${fileV2}" 'v2' 'v2 update'

    local backupFileFromRestore=''
    logDisable
    fileRestoreR1 backupFileFromRestore "${testfile}"
    logEnable
    testAssertEqualValue "${backupFile2}" "${backupFileFromRestore}" \
                         'restore finds old backup'
    local fileV1=`cat "${testfile}"`
    testAssertEqualValue "${fileV1}" 'v1' 'v1 restore'

    #showDir "${tempdir}"
    rm -rf "${tempdir}"
    assertDoesNotExist "${tempdir}"
    logInfo "Removed temp dir: ${tempdir}"
}
