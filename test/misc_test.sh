#!/bin/bash

# test random box functions functions

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

use test
use misc

sedEscapeR1_test () {
    local value=''
    local result=''
    local expectedResult=''

    # FIXME: sh seems to stumble across backslashes
    # Known condition: The sh version of setVar (using escaped eval) does mess up on backslashes.
    # Since sedEscapeR1 uses setVar it all falls apart.
    value='\\'
    result=''
    expectedResult='\\\\'
    sedEscapeR1 result "${value}"
    if [ "${result}" != "${expectedResult}" ]; then
        printf 'value          %s\n' "${value}"
        printf 'result         %s\n' "${result}"
        printf 'expectedResult %s\n' "${expectedResult}"
    fi
    testAssertEqualValue "${result}" "${expectedResult}" 'tricky double backslashes'

    value='test/with//slashes\back\\slashes.and.dots'
    result=''
    expectedResult='test\/with\/\/slashes\\back\\\\slashes\.and\.dots'
    sedEscapeR1 result "${value}"
    if [ "${result}" != "${expectedResult}" ]; then
        printf 'value          %s\n' "${value}"
        printf 'result         %s\n' "${result}"
        printf 'expectedResult %s\n' "${expectedResult}"
    fi
    testAssertEqualValue "${result}" "${expectedResult}" 'explicit value'

    result="${value}"
    sedEscapeR1 result
    if [ "${result}" != "${expectedResult}" ]; then
        printf 'value          %s\n' "${value}"
        printf 'result         %s\n' "${result}"
        printf 'expectedResult %s\n' "${expectedResult}"
    fi
    testAssertEqualValue "${result}" "${expectedResult}" 'value in return var'

    # we are using this in ssh.sh sshKnownHostKeyDelete
    value='[gogs.example.com]:10022'
    result=''
    expectedResult='\[gogs\.example\.com\]:10022'
    sedEscapeR1 result "${value}"
    if [ "${result}" != "${expectedResult}" ]; then
        printf 'value          %s\n' "${value}"
        printf 'result         %s\n' "${result}"
        printf 'expectedResult %s\n' "${expectedResult}"
    fi
    testAssertEqualValue "${result}" "${expectedResult}" 'brackets'
}
