#!/bin/bash

# test slice module functions

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

use test
use slice

isSliceMarkedCorrectly_test () {
    local testSliceMarked="# SLICE-BEGIN TestSlice

This is a test slice.

There is some more text here.

# SLICE-END TestSlice"

    local yesNoResult=''
    local sliceNameResult=''
    isSliceMarkedCorrectlyR2 \
        yesNoResult sliceNameResult "${testSliceMarked}"
    testAssertEqualValue "${yesNoResult}" 'yes' "R2 Positive yes/no result"
    testAssertEqualValue "${sliceNameResult}" 'TestSlice' "Slice name extraction"

    yesNoResult=''
    sliceNameResult=''
    isSliceMarkedCorrectlyR1 \
        yesNoResult "${testSliceMarked}"
    testAssertEqualValue "${yesNoResult}" 'yes' "R1 Positive yes/no result"

    local testSliceNameMismatch="# SLICE-BEGIN TestSlice
Blahbla blah
# SLICE-END OtherSliceName"
    sliceNameResult='yes'
    isSliceMarkedCorrectlyR2 \
        yesNoResult sliceNameResult "${testSliceNameMismatch}"
    testAssertEqualValue "${yesNoResult}" 'no' "R2 Name mismatch yes/no result"
    testAssertEqualValue "${sliceNameResult}" '' "R2 Mismatched slice name extraction empty"

    sliceNameResult='yes'
    isSliceMarkedCorrectlyR1 \
        yesNoResult "${testSliceNameMismatch}"
    testAssertEqualValue "${yesNoResult}" 'no' "R1 Name mismatch yes/no result"

    local testSliceNested="# SLICE-BEGIN TestSlice
Blah

# SLICE-BEGIN OtherSlice
# SLICE-END OtherSlice

# SLICE-END TestSlice"
    isSliceMarkedCorrectlyR1 \
        yesNoResult "${testSliceNested}"
    testAssertEqualValue "${yesNoResult}" 'no' "Fail nested slices"
}

sliceGetR1_test () {
    local text="# This is a test file.
a=1
b=2

# SLICE-BEGIN TestSlice
c=3
d=4
# SLICE-END TestSlice
e=5
f=6
"
    local sliceExpected="# SLICE-BEGIN TestSlice
c=3
d=4
# SLICE-END TestSlice"
    local sliceExtracted=''
    sliceGetR1 sliceExtracted 'TestSlice' "${text}"
    testAssertEqualValue "${sliceExtracted}" "${sliceExpected}" "Get slice"

    text="No
slice in here
at all"
    sliceExtracted='this_needs_to_be_cleared'
    sliceGetR1 sliceExtracted 'TestSlice' "${text}"
    testAssertEqualValue "${sliceExtracted}" '' "Get non-existent slice"

}

assertSliceIsCorrectlyMarked_test () {
    local slice="# SLICE-BEGIN TestSlice
c=3
d=4
# SLICE-END TestSlice"
    assertSliceIsCorrectlyMarked "${slice}"
    local output=`(SUBSHELL='yes'; assertSliceIsCorrectlyMarked "${slice}" >/dev/null; echo GOOD)`
    testAssertEqualValue "${output}" 'GOOD' "good slice no abort"

    slice="# SLICE-BEGIN TestSlice
dupe begin in here
# SLICE-BEGIN TestSlice
blahblah
# SLICE-END"
    output=`(SUBSHELL='yes'; assertSliceIsCorrectlyMarked "${slice}"  >/dev/null; echo BAD)`
    testAssertDoesNotContainRegex "${output}" 'BAD' "bad slice dupe begin aborts"

    slice="# SLICE-BEGIN TestSlice
dupe end in here
# SLICE-END TestSlice
blahblah
# SLICE-END"
    output=`(SUBSHELL='yes'; assertSliceIsCorrectlyMarked "${slice}"  >/dev/null; echo BAD)`
    testAssertDoesNotContainRegex "${output}" 'BAD' "bad slice dupe end aborts"
}

sliceReplaceR1_test () {
    local text="# This is a test file.
a=1
b=2

# SLICE-BEGIN TestSlice
c=3
d=4
# SLICE-END TestSlice

e=5
f=6
"
    local sliceNew="# SLICE-BEGIN TestSlice
x=100
y=99
z=98
# SLICE-END TestSlice"
    # Note that the trailing empty line gets lost in the new text
    local textNewExpected="# This is a test file.
a=1
b=2

# SLICE-BEGIN TestSlice
x=100
y=99
z=98
# SLICE-END TestSlice

e=5
f=6"
    local textNew=''
    sliceReplaceR1 textNew "${sliceNew}" "${text}"
    testAssertEqualValue "${textNew}" "${textNewExpected}" "replace a slice"

    sliceNew="# SLICE-BEGIN slicePrepend
This is a new slice to be added at the front of the file
# SLICE-END slicePrepend"

    text="This text
has no slice markings
whatsoever"
    textNewExpected="# SLICE-BEGIN slicePrepend
This is a new slice to be added at the front of the file
# SLICE-END slicePrepend

This text
has no slice markings
whatsoever"
    sliceReplaceR1 --prepend textNew "${sliceNew}" "${text}"
    testAssertEqualValue "${textNew}" "${textNewExpected}" "prepend a slice"

    sliceNew="# SLICE-BEGIN sliceAppend
This is a new slice to be added at the end of the file
# SLICE-END sliceAppend"

    text="This text
has no slice markings
whatsoever"
    textNewExpected="This text
has no slice markings
whatsoever

# SLICE-BEGIN sliceAppend
This is a new slice to be added at the end of the file
# SLICE-END sliceAppend"
    sliceReplaceR1 --append textNew "${sliceNew}" "${text}"
    testAssertEqualValue "${textNew}" "${textNewExpected}" "append a slice"

    sliceNew="# SLICE-BEGIN sliceRepeatTest
    test with leading spaces
# SLICE-END sliceRepeatTest"
    text=''
    textNewExpected="${sliceNew}"
    sliceReplaceR1 --prepend textNew "${sliceNew}" "${text}"
    sliceReplaceR1 --prepend textNew "${sliceNew}" "${textNew}"
    testAssertEqualValue "${textNew}" "${textNewExpected}" "repeated slice replace"

}

glgl() {
    local test="# This is a test file.
a=1
b=2

# SLICE-BEGIN TestSlice
c=3
d=4
# SLICE-END TestSlice
e=5
f=6
"

        text="# This is a test file.
a=1
b=2

# SLICE-BEGIN TestSlice
dupe begin in here
# SLICE-BEGIN TestSlice
blahblah
# SLICE-END TestSlice
e=5
f=6
"
}
