#!/bin/sh

# test bam functions

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

use test

bam_test () {
    globalEmptyVariables="
fmt_global
logTimerStart_global
logContext_global
logPendingHeaderNoFmt_global
logPendingHeaderMessage_global
logPendingHeaderWrap_global
"
    local globalVariables=`
        showGlobalVars \
            | grep '=' \
            | sed 's/=.*//' \
            | pipeStripStandard`
    if [ "${isInteractive_global}" = 'yes' ]; then
        # Some globals are not defined in interactive mode.
        globalVariables=`
            echo "${globalVariables}" \
                | egrep -v '^script[A-Z]'`
    fi
    local globalVariable=''
    for globalVariable in ${globalVariables}; do
        local allowedEmptyTest=`echo "${globalEmptyVariables}" | egrep "^${globalVariable}"'$'`
        if [ "${allowedEmptyTest}" != '' ]; then
            logDebug "Skipping test for allowed empty global variable: ${globalVariable}"
            continue
        fi
        testAssertVariable "${globalVariable}" "${globalVariable}"
    done
}

setVar_test () {
    #logWarning "setVarFunctionType_global=${setVarFunctionType_global}"
    local space=' '
    local a="${space}${space}
${space}${space}

${space}value with leading and trailing whitespace${space}

${space}${space}"
    local b=''
    setVar b "${a}"
    testAssertEqualValue "${b}" "${a}" "verbatim copy"

    b=''
    a="'test with quotes \"\" and ticks'"
    setVar b "${a}"
    testAssertEqualValue "${b}" "${a}" "tricky verbatim copy quotes and tics"

    if [ "${setVarFunctionType_global}" = 'evalEsc' ]; then
        logWarning "Known condition: The sh version of setVar (using escaped eval) does loose trailing blank lines."
    fi
    b=''
    a='line
'
    setVar b "${a}"
    testAssertEqualValue "${b}" "${a}" "trailing empty lines may not make it"

    b=''
    a='test with double\\backslash'
    setVar b "${a}"
    testAssertEqualValue "${b}" "${a}" "backslash copy"

}

sourceScriptFromVar_test () {
    local script='
aTestFunction_ () {
    echo "TEST"
}
'
    sourceScriptFromVar script
    local result=`( SUBSHELL='yes'; aTestFunction_ ) 2>/dev/null`
    testAssertEqualValue "${result}" "TEST" 'sourced function is executing'
}


abort_test () {
    local nestedAbortTest=`
        ( SUBSHELL='yes'
abort
          echo "BAD: sub-process after abort"
        )
        echo "GOOD: We are back in the main process."
        `
    local check=''
    check=`echo "${nestedAbortTest}" | grep 'INFO  Aborting.'`
    testAssertNotEmpty "${check}" 'abort executing'
    check=`echo "${nestedAbortTest}" | grep 'BAD'`
    testAssertEmpty "${check}" 'code not executing after abort'
    check=`echo "${nestedAbortTest}" | grep 'GOOD'`
    testAssertNotEmpty "${check}" 'code outside of sub-shell is executing'
}

stripR1_test () {
    local space=' '
    local tab=$'\t'
    local a=''
    local b=''
    local expected=''
    local stripped=''

    a="${space}${space}
${space}${space}

value with leading and trailing whitespace

${space}${space}"
    b=''
    stripR1 b "${a}"
    testAssertEqualValue \
        "NOT a ${b}" \
        'NOT a value with leading and trailing whitespace' \
        'single remaining line value in-place var'


    a="${space}${tab}${space}
${tab}line1${space}

${space}line2${tab}
${space}
line3
${tab}"
    expected='line1

line2

line3'
    stripped=''
    stripR1 stripped "${a}"
    testAssertEqualValue \
        "${stripped}" \
        "${expected}" \
        'multi-line copy to other var'
    stripR1 a
    testAssertEqualValue \
        "${a}" \
        "${expected}" \
        'multi-line in place var'

}

stripCommentsR1_test () {
    local tab=$'\t'
    local a="#!/bin/myPersonalShellExecutable
echo hi # print someting
echo back${tab}# comment after tab
# end of test"
    local expected='echo hi
echo back'
    local b=''
    stripCommentsR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer'
    stripCommentsR1 a
    testAssertEqualValue "${a}" "${expected}" 'in-place'

    a='- using - as a comment symbol
no comment - this is a comment'
    expected='no comment'
    stripCommentsR1 b "${a}" '-'
    testAssertEqualValue "${b}" "${expected}" 'alternate comment symbol'
}

stripLeadingWhitespaceR1_test () {
    local space=' '
    local tab=$'\t'
    local a=''
    local b=''

    a="${tab}${space}
${space}${space}

${tab}${space}hello${tab}${space}

${space}${tab}-"
    expected="


hello${tab}${space}

-"
    stripLeadingWhitespaceR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer without trailing newlines'
    a="${tab}${space}
${space}${space}

${tab}${space}hello${tab}${space}

${space}${tab}"
    expected="


hello${tab}${space}"
    stripLeadingWhitespaceR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer with trailing newlines'
    stripLeadingWhitespaceR1 a
    testAssertEqualValue "${a}" "${expected}" 'in-place with trailing newlines'
}

stripTrailingWhitespaceR1_test () {
    local space=' '
    local tab=$'\t'
    local a=''
    local b=''

    a="${tab}${space}
${space}${space}

${tab}${space}hello${tab}${space}

${space}${tab}-"
    expected="


${tab}${space}hello

${space}${tab}-"
    stripTrailingWhitespaceR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer without trailing newlines'
    stripTrailingWhitespaceR1 a
    testAssertEqualValue "${a}" "${expected}" 'in-place'
}

stripEmptyLinesR1_test () {
    local a='
line1

line2

line3

'
    local expected='line1
line2
line3'
    local b=''
    stripEmptyLinesR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer'
    stripEmptyLinesR1 a
    testAssertEqualValue "${a}" "${expected}" 'in-place'
}

stripStandardR1_test () {
    local space=' '
    local tab=$'\t'
    local a=''
    local b=''
    local a="
myvar=test${space}
${space}${tab}
${tab}anothervar=test2${space}${tab}# lets see ...

"
    local expected='myvar=test
anothervar=test2'
    local b=''
    stripStandardR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer'
    stripStandardR1 a
    testAssertEqualValue "${a}" "${expected}" 'in-place'
}

pipeStripLeadingBlankLines_test () {
    local space=' '
    local tab=$'\t'
    local a=''
    local b=''
    local a="
${space}

${tab}
myvar=test${space}
${space}${tab}
${tab}anothervar=test2

"
    local expected="${space}

${tab}
myvar=test${space}
${space}${tab}
${tab}anothervar=test2"
    local b=''
    stripLeadingBlankLinesR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer'
    stripLeadingBlankLinesR1 a
    testAssertEqualValue "${a}" "${expected}" 'in-place'
}

pipeStripTrailingBlankLines_test () {
    local space=' '
    local tab=$'\t'
    local a=''
    local b=''
    local a="
${space}

${tab}
myvar=test${space}
${space}${tab}
${tab}anothervar=test2

"
    local expected="
${space}

${tab}
myvar=test${space}
${space}${tab}
${tab}anothervar=test2"
    local b=''
    stripTrailingBlankLinesR1 b "${a}"
    testAssertEqualValue "${b}" "${expected}" 'transfer'
    stripTrailingBlankLinesR1 a
    testAssertEqualValue "${a}" "${expected}" 'in-place'
}

assertDir_test () {
    local a=`assertDir .; echo GOOD`
    testAssertContainsRegex "${a}" 'GOOD' 'dir exists'
    a=`(SUBSHELL='yes'; assertDir _this_dir_does_not_exist_ >/dev/null; echo BAD)`
    testAssertDoesNotContainRegex "${a}" 'BAD' 'dir does not exists'
}

assertValidName_test () {
    local a=`assertValidName '_a_09'; echo GOOD`
    testAssertContainsRegex "${a}" 'GOOD' 'name is valid'
    local a=`(SUBSHELL='yes'; assertValidName '0_a_09' >/dev/null; echo BAD)`
    testAssertDoesNotContainRegex "${a}" 'BAD' 'name is invalid'
}

isIntR1_test () {
    local result=''
    isIntR1 result '123'
    testAssertEqualValue "${result}" 'yes' 'is int'
    isIntR1 result '1a2b'
    testAssertEqualValue "${result}" 'no' 'is not int'
    isIntR1 result ''
    testAssertEqualValue "${result}" 'no' 'is not int - empty'
    isIntR1 result ' '
    testAssertEqualValue "${result}" 'no' 'is not int - space only'
    isIntR1 result '12 34'
    testAssertEqualValue "${result}" 'no' 'is not int - space between numbers'
    isIntR1 result ' 1234 '
    testAssertEqualValue "${result}" 'yes' 'is int - space padded number'
    result='012'
    isIntR1 result
    testAssertEqualValue "${result}" 'yes' 'single var is int'
    result='abc'
    isIntR1 result
    testAssertEqualValue "${result}" 'no' 'single var is not int'
}

assertInt_test () {
    local a=`assertInt '123'; echo GOOD`
    testAssertContainsRegex "${a}" 'GOOD' 'is int'
    local a=`(SUBSHELL='yes'; assertInt '0_a_09' >/dev/null; echo BAD)`
    testAssertDoesNotContainRegex "${a}" 'BAD' 'is not int'
}

pipeReverseLines_test () {
    # This looses leading and/or trailing lines. We are not testing for that.
    local forward='line 1
line 2

line 5'
    local reverseExpected='line 5

line 2
line 1'
    local reverse=`printf "${forward}" | pipeReverseLines`
    testAssertEqualValue "${reverse}" "${reverseExpected}" "simple echo to pipeReverseLines"
}

isDirEmptyR1_test () {
    assertDoesNotExist /tmp/emptyDir_isDirEmptyR1_test
    assertDoesNotExist /tmp/nonEmptyDir_isDirEmptyR1_test
    mkdir /tmp/emptyDir_isDirEmptyR1_test
    mkdir /tmp/nonEmptyDir_isDirEmptyR1_test
    touch /tmp/nonEmptyDir_isDirEmptyR1_test/aFile
    assertDir /tmp/emptyDir_isDirEmptyR1_test
    assertFile /tmp/nonEmptyDir_isDirEmptyR1_test/aFile

    local expectedResult=''
    local result=''
    expectedResult='yes'
    isDirEmptyR1 result /tmp/emptyDir_isDirEmptyR1_test
    assertVariableValue result "${expectedResult}"

    expectedResult='no'
    isDirEmptyR1 result /tmp/nonEmptyDir_isDirEmptyR1_test
    assertVariableValue result "${expectedResult}"

    removeFileIfExists /tmp/nonEmptyDir_isDirEmptyR1_test/aFile
    rmdir /tmp/emptyDir_isDirEmptyR1_test
    rmdir /tmp/nonEmptyDir_isDirEmptyR1_test
    assertDoesNotExist /tmp/emptyDir_isDirEmptyR1_test
    assertDoesNotExist /tmp/nonEmptyDir_isDirEmptyR1_test
}

qualifyPathR1_test () {
    assertDoesNotExist /directoryThatDoesNotExist

    local file='/directoryThatDoesNotExist/someNonExistentFile.txt'
    local result=''
    local expectedResult="${file}"

    qualifyPathR1 result "${file}"
    assertVariableValue result "${expectedResult}"
}
