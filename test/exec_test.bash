#!/bin/bash

# test exec module functions

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

use test
use exec

execCaptureStdoutStderrR2_testDummy() {
    # https://stackoverflow.com/questions/11027679/capture-stdout-and-stderr-into-different-variables
    # takes return code to issue as $1
    # message for stdout as $2
    # message for stderr as $3
    echo "$3" >&2
    echo "$2" >&1
    return "$1"
}

execCaptureStdoutStderrR2_test () {
    # https://stackoverflow.com/questions/11027679/capture-stdout-and-stderr-into-different-variables
    local stdoutExpected='This is some stdout data
                          and another line'
    local stderrExpected='ERROR This is some stderr data
                                with multiple lines'
    local returnCodeExpected=3
    local stdoutResult=''
    local stderrResult=''
    local returnCodeResult=''
    execCaptureStdoutStderrR2 stdoutResult stderrResult execCaptureStdoutStderrR2_testDummy "${returnCodeExpected}" "${stdoutExpected}" "${stderrExpected}"; returnCodeResult=$?
    testAssertEqualValue "${stdoutExpected}" "${stdoutResult}" 'Capture stdout'
    testAssertEqualValue "${stderrExpected}" "${stderrResult}" 'Capture stderr'
    testAssertEqualValue "${returnCodeExpected}" "${returnCodeResult}" 'Preserve return code'
}

