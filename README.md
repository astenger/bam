# bam - bash magic

The primary focus of bam is bash, but the core library has limited support for sh and busybox a well.

If you are the daring kind, to bootstrap a local interactive shell
from bash:

```bash
export BAM_BOOT=yes; bash --rcfile <( curl -s https://gitlab.com/astenger/bam/raw/master/bam.sh )
```

This will "git clone" the repo or unpack the tar bundle into the current directory and launch a shell with bam.sh as the rcfile. This requires:
- bash
- curl
- git

If you are on sh with wget you can get started with:
```sh
wget -qO- https://gitlab.com/astenger/bam/raw/master/bam.sh | sh -s - boot
. ./bam/bam.sh
```

Or sh using curl:
```sh
curl -s https://gitlab.com/astenger/bam/raw/master/bam.sh | sh -s - boot
. ./bam/bam.sh
```
