use pid

setCursorStyleSteadyBlock () {
    # https://superuser.com/questions/361335/how-to-change-the-terminal-cursor-from-box-to-line
    printf '\033[2 q'

}

setCursorStyleSteadyUnderline () {
    printf '\033[4 q'

}



terminalColumns_global=''
terminalSpaceLine=''
setTerminalColumnsGlobalVar () {
    local previousValue="${terminalColumns_global}"

    # bash and some others set COLUMNS
    terminalColumns_global="${COLUMNS}"
    if [ "${terminalColumns_global}" = '' ]; then
        terminalColumns_global=`stty size 2>/dev/null | awk ' { print $2 } '`
    fi
    if [ "${terminalColumns_global}" = '' ]; then
        terminalColumns_global="${previousValue}"
    fi

    if [ "${terminalColumns_global}" != '' -a \
         "${terminalColumns_global}" != "${previousValue}" ]; then
        # https://unix.stackexchange.com/questions/215584/whats-the-name-of-the-environment-variable-with-current-terminal-width
        #terminalSpaceLine=`echo "${terminalColumns_global}" |awk '{ ORS=" "; n=$1; for (i=0; i<n; ++i) { print ""; } }'`

        #https://stackoverflow.com/questions/38868665/multiplying-strings-in-bash-script
        printf -v terminalSpaceLine "%${terminalColumns_global}s"
    fi
}
setTerminalColumnsGlobalVar

clearCurrentTerminalLine () {
    printf "\r$(tput el 2>/dev/null)"
    return

    # Old implementation before I found tput
    # User could have resized the terminal.
    setTerminalColumnsGlobalVar
    if [ "${terminalColumns_global}" = '' ]; then
        return
    fi
    echo -e -n '\r'
    echo -n "${terminalSpaceLine}"
    echo -e -n '\r'
}

pipeLogFlash () {
    setTerminalColumnsGlobalVar
    local line_pipeLogFlash
    local count=0
    setCursorStyleSteadyUnderline
    while read line_pipeLogFlash; do
        let count+=1
        logFlash -- "${line_pipeLogFlash}"
    done
    clearCurrentTerminalLine
    setCursorStyleSteadyBlock
    logDebug "pipeLogFlash: lines processed: ${count}"
}

slowLs () {
    local line
    local waitTime=''
    ls -la | while read line; do
        echo "${line}"
        let "waitTime = $RANDOM %5"
        waitTime=".${waitTime}"
        sleep ${waitTime}
    done
}

pipeLogFlashTest () {
    slowLs | pipeLogFlash
}

# logFlash was migrated to bam.sh
# migrate more?

logFlashTest  () {
    logFlash "multi
line
string"
    sleep 1
    for i in `ls`; do
        logFlash $i
        sleep .05
    done
    logFlash "multi
line
string"
    sleep 1
    logInfo 'done'
}

logFlashClear () {
    clearCurrentTerminalLine
}

pipeBusyWheel () {
    local prefix="$1"
    local charSequence='_-\|/-_.oO0Oo.'

    use trap
    registerTrapFunction 'pipeBusyWheelStop' INT

    setCursorStyleSteadyUnderline
    local count=0
    local line_pipeBusyWheel
    while read line_pipeBusyWheel; do
        clearCurrentTerminalLine
        let count=${count}%${#charSequence}
        char=${charSequence:${count}:1}
        echo -e -n '\r'
        echo -n "${prefix}${char}"
        echo -e -n '\r'
        let count+=1
    done
    clearCurrentTerminalLine
    setCursorStyleSteadyBlock
    unregisterTrapFunction 'pipeBusyWheelStop' INT
}

pipeBusyWheelStop () {
    clearCurrentTerminalLine
    setCursorStyleSteadyBlock
}
pipeBusyWheelTest () {
    slowLs | pipeBusyWheel 'wheelie: '
}

busyWheelLoop () {
    local tempfile="$1"
    local prefix="$2"
    local charSequence='_-\|/-_.oO0Oo.'

    setDefault prefix ' '
    assertVar tempfile
    if [ ! -f "${tempfile}" ]; then
        logInfo "Not running loop. tempfile does not exist: ${tempfile}"
        return
    fi

    echo "${BASHPID}" >> "${tempfile}"

    setCursorStyleSteadyUnderline
    logDebug "busyWheelLoop: starting using ${tempfile}"
    local count=0
    local char=''
    clearCurrentTerminalLine
    while [ -f "${tempfile}" ]; do
        let count=${count}%${#charSequence}
        char=${charSequence:${count}:1}
        echo -e -n '\r'
        echo -n "${prefix}${char}"
        echo -e -n '\r'
        sleep .09
        let count+=1
    done
    setCursorStyleSteadyBlock
    logDebug "busyWheelLoop: stopping ${tempfile}"
}

busyWheelTempfile_global="/tmp/bam_temp_file_busy_wheel_loop.$$"
busyWheelpid_global=''
busyWheelStart () {
    use trap
    local prefix="$1"

    if [ "${BASHPID}" = '' ]; then
        logDebug "busyWheelStart(): not on bash"
        return
    fi

    removeFileIfExists "${busyWheelTempfile_global}" 1>/dev/null
    > "${busyWheelTempfile_global}"
    assertFile "${busyWheelTempfile_global}"
    chmod 600 "${busyWheelTempfile_global}"
    > "${busyWheelTempfile_global}"

    registerTrapFunction 'busyWheelStop' INT
    pidBackgroundR1 busyWheelpid_global busyWheelLoop "${busyWheelTempfile_global}" "${prefix}"
}

busyWheelStop () {
    if [ "${busyWheelpid_global}" != '' ]; then
        logDebug "Killing busyWheelLoop pid: ${busyWheelpid_global}"
        kill -TERM ${busyWheelpid_global} 2>/dev/null
        busyWheelpid_global=''
    fi
    if [ ! -f "${busyWheelTempfile_global}" ]; then
        logDebug "Busy wheel is NOT running."
        return
    fi

    local pid=`cat "${busyWheelTempfile_global}"`
    if [ "${pid}" != '' ]; then
        logDebug "Killing busyWheelLoop pid from tmpfile: ${pid}"
       kill -TERM ${pid} 2>/dev/null
    fi

    rm "${busyWheelTempfile_global}"
    setCursorStyleSteadyBlock
    unregisterTrapFunction 'busyWheelStop' INT
}

busyWheelTest () {
    # Note that you can log additional messages while the busy wheel is running.
    logInfo "Starting busy wheel."
    busyWheelStart '      testing: '
    sleep 2
    logInfo "Try to use ctrl-c before the sleep is done."
    sleep 2
    logInfo "Really, hit ctrl-c."
    sleep 5
    busyWheelStop
    logInfo "Done sleeping."
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"
