#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam terraform helpers

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Helpers for working with terraform.

EOF

}

terraformBrewInstall () {
    brew tap hashicorp/tap
    brew install hashicorp/tap/terraform
}

terraformLogReset () {
    unset TF_LOG
}

terraformLogInfo () {
    export TF_LOG=INFO
}

terraformLogDebug () {
    export TF_LOG=DEBUG
}

eval "${showLibraryUsageIfNotSourced}"

# Some terraform aliases to make typing easier
alias tf='terraform'
alias tfp='terraform plan'
alias tfa='terraform apply'
alias tfay='terraform apply -auto-approve'
alias tfi='terraform init'
alias tfd='terraform apply -destroy'
alias tfdy='terraform apply -destroy -auto-approve'

