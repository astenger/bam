#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam zfs module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

use ssh

libraryUsage () {
    cat <<EOF

zfs helpers.


zfsMigrate

For zfs migrations you can create a file at ~/bam_zfs_migrate_list
containing entries like this:

host1:data    host2:ttb/data

In this example data on host1 and ttb on host2 are existing
pool. Please make sure that the host names are what the hostname
command on a shell returns since this is how the function determines
whether a dataset is local or remote.

For remote migrations (push or pull, both are fine), you need
passwordless ssh access in place.


EOF

}

zfsMigrate () {
    local noExec='no'
    local passOptions=''
    local sshIdentity=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '-noexec' ]; then
            noExec='yes'
            passOptions="${passOptions} $1"
            shift
        elif [ "$1" = '-i' -o "$1" = '-identity' ]; then
            sshIdentity="$2"
            passOptions="${passOptions} $1 '$2'"
            assertVariable sshIdentity "-i option used without specifying an identity file"
            assertFile "${sshIdentity}"
            shift
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local zfsSourceFqn="$1" # Optional if ~/bam_zfs_migrate_list exists.
    local zfsTargetFqn="$2" # Optional if ~/bam_zfs_migrate_list exists.

    if [ "${zfsSourceFqn}" = '' ]; then
        assertFile ~/bam_zfs_migrate_list
        logInfo "Reading migrations to execute from: ~/bam_zfs_migrate_list"
        local line=''
        # weird: recursion stops after the first iteration unless we
        # allocate a different file descriptor for piping the data
        # into the while loop.
        # https://stackoverflow.com/questions/1521462/looping-through-the-content-of-a-file-in-bash

        # It's not the loop bpdy itself that's the problem. I am
        # suspecting a side effect with what we are doing is what
        # kills the loop.

        while read -u 10 line; do
        #while read line; do
            local source=`echo "${line}" | awk ' { print $1 } '`
            local target=`echo "${line}" | awk ' { print $2 } '`
            zfsMigrate ${passOptions} "${source}" "${target}"
            echo
        #done < <( cat ~/bam_zfs_migrate_list | pipeStripStandard )
        done 10< <( cat ~/bam_zfs_migrate_list | pipeStripStandard )
        logInfo return
        return
    fi
    logHeader1 zfsMigrate ${passOptions} \"${zfsSourceFqn}\" \"${zfsTargetFqn}\"

    if [ "${noExec}" = 'yes' ]; then
        logInfo "Not executing migration commands as requested."
    fi

    local zfsSourceHost="${zfsSourceFqn%:*}"
    local zfsSourceDS="${zfsSourceFqn#*:}"

    local zfsTargetHost="${zfsTargetFqn%:*}"
    local zfsTargetDS="${zfsTargetFqn#*:}"

    local commonSnap=''
    local commonSnapOptionSource=''
    local snapToReplicate=''
    local rc=''

    local colonCount=`echo "${zfsSourceFqn}" | awk -F':' '{print NF-1}'`
    if [ "${colonCount}" -eq 0 ]; then
        logErrorAndAbort "Source fqn must contain one colon, e.g. host:pool/dataset: ${zfsSourceFqn}"
    elif [ "${colonCount}" -gt 1 ]; then
        logErrorAndAbort "Source fqn must not contain multiple colons: ${zfsSourceFqn}"
    fi

    colonCount=`echo "${zfsTargetFqn}" | awk -F':' '{print NF-1}'`
    if [ "${colonCount}" -eq 0 ]; then
        logErrorAndAbort "Target fqn must contain one colon, e.g. host:pool/dataset: ${zfsTargetFqn}"
    elif [ "${colonCount}" -gt 1 ]; then
        logErrorAndAbort "Target fqn must not contain multiple colons: ${zfsTargetFqn}"
    fi

    local remotePrefixSource=''
    local identityOption=''
    local isPasswordlessWorking=''
    if [ "${zfsSourceHost}" != "${hostname_global}" ]; then
        if [ "${sshIdentity}" != '' ]; then
            identityOption="-i ${sshIdentity}"
        else
            # No user specified identity. Check whether passwordless
            # ssh access is set up via ~/.ssh/config and test known
            # locations if not.
            sshTestIdentitiesR2 isPasswordlessWorking identityOption "${zfsSourceHost}" /data/ssh/replication
            assertVariableValue isPasswordlessWorking 'yes' 'Passwordless access to source host is not working: ${zfsSourceHost}'
        fi
        remotePrefixSource="ssh ${identityOption} ${zfsSourceHost}"
    fi
    logDebugVar remotePrefixSource

    local remotePrefixTarget=''
    if [ "${zfsTargetHost}" != "${hostname_global}" ]; then
        if [ "${sshIdentity}" != '' ]; then
            identityOption="-i ${sshIdentity}"
        else
            # No user specified identity. Check whether passwordless
            # ssh access is set up via ~/.ssh/config and test known
            # locations if not.
            sshTestIdentitiesR2 isPasswordlessWorking identityOption "${zfsTargetHost}" /data/ssh/replication
            assertVariableValue isPasswordlessWorking 'yes' 'Passwordless access to target host is not working: ${zfsTargetHost}'
        fi
        remotePrefixTarget="ssh ${identityOption} ${zfsTargetHost}"
    fi
    logDebugVar remotePrefixTarget

    # Note: / in snap names is bad, but : is ok
    local zfsSourceNoSlashes=`echo "${zfsSourceFqn}" | sed 's/\//\./g'`
    local snapshotPrefix="bam_zfs_migrate"
    local snapshotNameNoIncrement="${snapshotPrefix}_from_${zfsSourceNoSlashes}"

    # check for existing snapshots
    logInfo "Fetching snapshot information. This may take a moment."
    logTimerStart
    local sourceSnapshotNumbers=`${remotePrefixSource} zfs list -t snap | awk ' { print $1 } ' | egrep "^${zfsSourceDS}@${snapshotNameNoIncrement}" | sed 's/.*_//' | sort -n`
    local targetSnapshotNumbers=`${remotePrefixTarget} zfs list -t snap | awk ' { print $1 } ' | egrep "^${zfsTargetDS}@${snapshotNameNoIncrement}" | sed 's/.*_//' | sort -n`
    local latestSourceSnapshotNumber=`echo "${sourceSnapshotNumbers}" | tail -n 1`
    local latestTargetSnapshotNumber=`echo "${targetSnapshotNumbers}" | tail -n 1`
    local latestSourceSnapshot=''
    if [ "${latestSourceSnapshotNumber}" != '' ]; then
        latestSourceSnapshot="${zfsSourceDS}@${snapshotNameNoIncrement}_${latestSourceSnapshotNumber}"
    fi
    local latestTargetSnapshot=''
    if [ "${latestTargetSnapshotNumber}" != '' ];then
        latestTargetSnapshot="${zfsTargetDS}@${snapshotNameNoIncrement}_${latestTargetSnapshotNumber}"
    fi
    if [ "${latestSourceSnapshot}" = '' ]; then
        if [ "${latestTargetSnapshot}" != '' ]; then
            logErrorAndAbort "Source snapshot does not exist, but target does: ${latestTargetSnapshot}"
        fi
    else
        if [ "${latestTargetSnapshot}" = '' ]; then
            logInfo "No relevant shapshots found on target."
        else
            local latestTargetSnapshotName="${latestTargetSnapshot#*@}"
            local rc=''
            local sourceSnapshot=`${remotePrefixSource} zfs list "${zfsSourceDS}@${latestTargetSnapshotName}"`; rc=$?
            if [ "${rc}" != '0' ]; then
                logErrorAndAbort "Latest target snapshot does not exist on source: ${zfsSourceDS}@${latestTargetSnapshotName}"
            fi
            commonSnap="${latestTargetSnapshotName}"
            logInfo "Found matching snapshot names on source and target: ${commonSnap}"
            commonSnapOptionSource="-i ${zfsSourceDS}@${commonSnap}"
        fi
    fi

    logInfo "Searching for snapshots to clean up ..."
    local aSnapshotNumber=''
    local snapshotCleanupCount=0
    for aSnapshotNumber in ${sourceSnapshotNumbers}; do
        if [ "${aSnapshotNumber}" != "${latestTargetSnapshotNumber}" ]; then
            if [ "${noExec}" = 'yes' ]; then
                logInfo "Would execute:
${remotePrefixSource} zfs destroy -r \"${zfsSourceDS}@${snapshotNameNoIncrement}_${aSnapshotNumber}\""
            else
                logInfo ${remotePrefixSource} zfs destroy -r "${zfsSourceDS}@${snapshotNameNoIncrement}_${aSnapshotNumber}"
                ${remotePrefixSource} zfs destroy -r "${zfsSourceDS}@${snapshotNameNoIncrement}_${aSnapshotNumber}"
            fi
            let snapshotCleanupCount+=1
        fi
    done
    for aSnapshotNumber in ${targetSnapshotNumbers}; do
        if [ "${aSnapshotNumber}" != "${latestTargetSnapshotNumber}" ]; then
            if [ "${noExec}" = 'yes' ]; then
                logInfo "Would execute:" ${remotePrefixTarget} zfs destroy -r "${zfsTargetDS}@${snapshotNameNoIncrement}_${aSnapshotNumber}"
            else
                logInfo ${remotePrefixTarget} zfs destroy -r "${zfsTargetDS}@${snapshotNameNoIncrement}_${aSnapshotNumber}"
                ${remotePrefixTarget} zfs destroy -r "${zfsTargetDS}@${snapshotNameNoIncrement}_${aSnapshotNumber}"
            fi
            let snapshotCleanupCount+=1
        fi
    done
    if [ "${snapshotCleanupCount}" = '0' ]; then
        logInfo "... nothing to clean up."
    else
        if [ "${noExec}" = 'yes' ]; then
            logInfo "... would have cleaned up ${snapshotCleanupCount} snapshots."
        else
            logInfo "... cleaned up ${snapshotCleanupCount} snapshots."
        fi
    fi

    logInfo "Fetching snapshot information. This may take a moment."
    logTimerStart
    local commonSnapNumber="${commonSnap##**_}"
    local snapToReplicateNumber=''
    let "snapToReplicateNumber=${commonSnapNumber}+1"
    snapToReplicate="${snapshotNameNoIncrement}_${snapToReplicateNumber}"
    local snapToReplicateSource="${zfsSourceDS}@${snapToReplicate}"
    local snapToReplicateTarget="${zfsTargetDS}@${snapToReplicate}"
    local snapToReplicateSourceTest=`${remotePrefixSource} zfs list -t snap | awk ' { print $1 } ' | egrep "^${snapToReplicateSource}"'$' | sort | tail -n 1`
    if [ "${snapToReplicateSourceTest}" != '' ]; then
        logError "Next snapshot number already exists on source: ${snapToReplicateSourceTest}"
        logError "Mitration running in a nother shell or migration terminated mid stream?"
        logErrorAndAbort "Consider:
${remotePrefixSource} zfs destroy -r ${snapToReplicateSourceTest}"
    fi

    logInfo "Creating new snapshot on source: ${snapToReplicateSource}"
    if [ "${noExec}" = 'yes' ]; then
        logInfo "Would execute:" ${remotePrefixSource} zfs snapshot -r "${snapToReplicateSource}"
    else
        logInfo ${remotePrefixSource} zfs snapshot -r "${snapToReplicateSource}"
        ${remotePrefixSource} zfs snapshot -r "${snapToReplicateSource}"
    fi

    logInfo "Starting replication:"
    if [ "${noExec}" = 'yes' ]; then
        logInfo "Would execute:" ${remotePrefixSource} zfs send --verbose --replicate ${commonSnapOptionSource} "${snapToReplicateSource}" \| ${remotePrefixTarget} zfs receive -F "${zfsTargetDS}"
        # Dryrun requires the snapshot to be created, which we're not doing.
        #logInfo "Executing dryrun instead."
        #logInfo ${remotePrefixSource} zfs send --dryrun --verbose --replicate ${commonSnapOptionSource} "${snapToReplicateSource}"
        #${remotePrefixSource} zfs send --dryrun --verbose --replicate ${commonSnapOptionSource} "${snapToReplicateSource}"
    else
        logInfo ${remotePrefixSource} zfs send --verbose --replicate ${commonSnapOptionSource} "${snapToReplicateSource}" \| ${remotePrefixTarget} zfs receive -F "${zfsTargetDS}"
        logTimerStart
        ${remotePrefixSource} zfs send --verbose --replicate ${commonSnapOptionSource} "${snapToReplicateSource}" | ${remotePrefixTarget} zfs receive -F "${zfsTargetDS}"; rc=$?
        if [ "${rc}" != '0' ]; then
            logErrorAndAbort "rc=${rc}"
        fi
        local snapToReplicateTargetTest=`${remotePrefixTarget} zfs list -t snap | awk ' { print $1 } ' | egrep "^${snapToReplicateTarget}"'$' | sort | tail -n 1`
        if [ "${snapToReplicateTargetTest}" = '' ]; then
            logErrorAndAbort "Replication failed. Target snapshot does not exist: ${snapToReplicateTarget}"
        fi
        logInfo "Replication worked. Targt snap exists: ${snapToReplicateTarget}"

        if [ "${commonSnap}" != '' ]; then
            logInfo "Removing previous shared snapshots."
            logInfo ${remotePrefixSource} zfs destroy -r "${zfsSourceDS}@${commonSnap}"
            ${remotePrefixSource} zfs destroy -r "${zfsSourceDS}@${commonSnap}"
            logInfo ${remotePrefixTarget} zfs destroy -r "${zfsTargetDS}@${commonSnap}"
            ${remotePrefixTarget} zfs destroy -r "${zfsTargetDS}@${commonSnap}"
        fi
    fi
}

zfsMigrateFinishAndCleanup () {
    assertInteractive
    logInfo "This will remove ALL LOCAL migration snapshots."
    logInfo "If you migration has not been finalized yet: hit ctrl-c"
    logInfo "Otherwise, to delete all snapshots type: yes"
    local answer=''
    read answer
    if [ "${answer}" != 'yes' ]; then
        logWarning "Cowardly aborting ..."
        abort
    else
        zfs list -t snap \
            | grep bam_zfs_migrate \
            | awk ' { print $1 } ' \
            | while read snapshot ; do
                logInfo "zfs destroy \"${snapshot}\""
                zfs destroy "${snapshot}"
             done
    fi
}


sysctlStates () {
    local action="$1"
    local targetStatesAndValues="$2"

    setDefault action 'report'
    assertVariable targetStatesAndValues

    # sysctl on FreeBSD shows values with a ': ' instead of a ' = ', e.g.
    # vfs.zfs.scrub_delay: 4
    local delimiter=''
    if [ "${hostKernelName_global}" = 'Linux' ]; then
        delimiter=' = '
    elif [ "${hostKernelName_global}" = 'FreeBSD' ]; then
        delimiter=': '
    else
        logErrorAndAbort "Don't know how to interpret sysctl output on: ${hostKernelName_global}"
    fi

    local targetStateAndValue=''
    local state=''
    local targetValue=''
    local currentStateAndValue=''
    local currentValue=''
    local updatedStateAndValue=''
    local updatedValue=''
    local trace=''
    while read targetStateAndValue; do
        state="${targetStateAndValue%%=*}"
        targetValue="${targetStateAndValue#*=}"
        currentStateAndValue=`sysctl ${state}`
        currentValue="${currentStateAndValue#*${delimiter}}"
        logDebugVar action state currentValue targetValue
        if [ "${action}" = 'report' ]; then
            logInfo "${currentStateAndValue}"
        elif [ "${action}" = 'set' ]; then
            if [ "${targetValue}" = "${currentValue}" ]; then
                logInfo "${state}=${currentValue} is already in place."
            else
                trace=`sysctl "${state}=${targetValue}" 2>&1`
                logInfo "${trace}"
                updatedStateAndValue=`sysctl ${state}`
                updatedValue="${updatedStateAndValue#*${delimiter}}"
                if [ "${targetValue}" != "${currentValue}" ]; then
                    logDebug "${state}=${updatedValue} updated from: ${currentValue}"
                else
                    logInfo "${state}=${updatedValue} failed. target: ${targetValue}"
                fi
            fi
        fi
    done < <( echo "${targetStatesAndValues}" | pipeStripEmptyLines )

}

zfsScrubAndResilverTuning () {
    local action="$1" # Optional. Defaults to report

    setDefault action 'report'

    assertVariableValue hostKernelName_global 'FreeBSD'

    # https://www.ixsystems.com/community/threads/scrub-performance-tuning.51959/

    # checked on 2020-03-13 and confirmed for:
    #     wood FreeNAS-11.2-U7 2020-03-13
    #     nail FreeNAS-11.3-RELEASE
    scrubAndResilverSysctlStatesDefault="
vfs.zfs.scrub_delay=4
vfs.zfs.top_maxinflight=32
vfs.zfs.resilver_min_time_ms=3000
vfs.zfs.resilver_delay=2
"

    scrubAndResilverSysctlStatesModerate="
vfs.zfs.scrub_delay=2
vfs.zfs.top_maxinflight=64
vfs.zfs.resilver_min_time_ms=4000
vfs.zfs.resilver_delay=1
"

    scrubAndResilverSysctlStatesAgressive="
vfs.zfs.scrub_delay=0
vfs.zfs.top_maxinflight=128
vfs.zfs.resilver_min_time_ms=5000
vfs.zfs.resilver_delay=0
"

    if [ "${action}" = 'report' ]; then
        sysctlStates 'report' "${scrubAndResilverSysctlStatesDefault}"
    elif [ "${action}" = 'default' ]; then
        sysctlStates 'set' "${scrubAndResilverSysctlStatesDefault}"
    elif [ "${action}" = 'moderate' ]; then
        sysctlStates 'set' "${scrubAndResilverSysctlStatesModerate}"
    elif [ "${action}" = 'aggressive' ]; then
        sysctlStates 'set' "${scrubAndResilverSysctlStatesAgressive}"
    else
        logErrorAndAbort "zfsScrubAndResilverTuning(): unknown action: ${action}"
    fi
}

# MAIN
eval "${showLibraryUsageIfNotSourced}"
