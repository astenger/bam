#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam module template - copy and adapt

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

FIXME

EOF

}

# MAIN

lib_templateMainRemoteBootstrap () {
    logErrorAndAbort "Rename this function"
}

# FIXME: modify the following line for sh or remove for bash
moduleLoading='lib_template.bash' # Required for sh modules.
eval "${showLibraryUsageIfNotSourced}"

lib_templateMainRemoteBootstrap
