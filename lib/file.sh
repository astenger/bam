#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam file module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

File manipulation functions.

EOF

}

isFileContentIdenticalR1 () {
    local returnVarYesNo_isFileContentIdenticalR1="$1"
    local fileName1="$2"
    local fileName2="$3"

    assertFile "${fileName1}"
    assertFile "${fileName2}"

    local rc=''
    local diffTrace=`diff "${fileName1}" "${fileName2}" 2>&1; echo rc=$?`
    logDebug "diff ${fileName1} ${fileName2}
${diffTrace}"
    diff "${fileName1}" "${fileName2}" 1>/dev/null 2>&1; rc=$?

    if [ "${rc}" = '0' ]; then
        setVarOrLogInfo "${returnVarYesNo_isFileContentIdenticalR1}" 'yes'
        return
    fi
    setVarOrLogInfo "${returnVarYesNo_isFileContentIdenticalR1}" 'no'
}

assertFileContentIsIdentical () {
    local fileName1="$1"
    local fileName2="$2"
    local errorMessage="$3"

    setDefault errorMessage "File: content is not identical:
${fileName1}
${fileName2}"

    local isIdentical=''

    isFileContentIdenticalR1 isIdentical "${fileName1}" "${fileName2}"
    if [ "${isIdentical}" != 'yes' ];then
        logErrorAndAbort "${errorMessage}"
    fi
}

fileBackupR1 () {
    # returns name of backup file
    local returnVarBackupFile_fileBackupR1="$1"
    local fileToBackUp="$2"
    local backupFileName="$3" # Optional.

    assertFile "${fileToBackUp}"

    local timestamp=''
    local isFileUnchanged=''
    if [ "${backupFileName}" = '' ]; then
        # Check whether we already have a valid backup file in place.
        local lastBackupFile=''
        fileGetLatestBackupFileR1 lastBackupFile "${fileToBackUp}"
        if [ "${lastBackupFile}" != '' ]; then
            isFileContentIdenticalR1 isFileUnchanged "${fileToBackUp}" "${lastBackupFile}"
            if [ "${isFileUnchanged}" = 'yes' ]; then
                logDebug "File has not changed since last backup."
                setVarOrLogInfo "${returnVarBackupFile_fileBackupR1}" "${lastBackupFile}"
                return
            fi
        fi

        timestamp=`date '+%Y-%m-%d_%H%M%S'`
        backupFileName="${fileToBackUp}.${timestamp}.bak"
    fi
    if [ -f "${backupFileName}" ]; then
        isFileContentIdenticalR1 isFileUnchanged "${fileToBackUp}" "${backupFileName}"
            if [ "${isFileUnchanged}" = 'yes' ]; then
                logDebug "Backup file already exists and file has not changed."
                setVarOrLogInfo "${returnVarBackupFile_fileBackupR1}" "${lastBackupFile}"
                return
            fi
        local alternateBackupFileName=''
        let count=0
        local countPadded=''
        while [ ${count} -lt 1000 ]; do
            countPadded=`printf '%04d' ${count}`
            alternateBackupFileName="${fileToBackUp}.${timestamp}.bak.${countPadded}"
            if [ ! -f "${alternateBackupFileName}" ]; then
                break
            fi
            let count+=1
        done
        if [ -f "${alternateBackupFileName}" ]; then
            logErrorAndAbort "Too many alternate backup files: ${alternateBackupFileName}"
        fi
        logDebug "Moving: ${backupFileName}
to:     ${alternateBackupFileName}"
        mv "${backupFileName}" "${alternateBackupFileName}"
        assertDoesNotExist "${backupFileName}" "Unable to move old backup out of the way: ${backupFileName}"
    fi
    local rc=''
    cat "${fileToBackUp}" >"${backupFileName}" 2>/dev/null; rc=$?
    if [ "${rc}" != '0' ]; then
        logErrorAndAbort "Unable to create backup file: ${backupFileName}"
    fi
    logDebug "${fileToBackUp} ->${backupFileName}"
    assertFileContentIsIdentical "${fileToBackUp}" "${backupFileName}"
    setVarOrLogInfo "${returnVarBackupFile_fileBackupR1}" "${backupFileName}"
}

fileBackup () {
    local dummy=''

    fileBackupR1 dummy "$@"
}

fileGetLatestBackupFileR1 () {
    local returnVarBackupFile_fileGetLatestBackupFileR1="$1"
    local fileToCheck_fileGetLatestBackupFileR1="$2"

    assertFile "${fileToCheck_fileGetLatestBackupFileR1}"
    local lastBackupFile_fileGetLatestBackupFileR1=`
              ls "${fileToCheck_fileGetLatestBackupFileR1}".[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].bak \
              2>/dev/null | sort | tail -1`
    setVarOrLogInfo "${returnVarBackupFile_fileGetLatestBackupFileR1}" "${lastBackupFile_fileGetLatestBackupFileR1}"
}

fileRestoreR1 () {
    local returnVarFileRestoredFrom_fileRestoreR1="$1"
    local fileToRestore="$2"
    local fileToRestoreFrom="$3" # Optional.

    assertVar fileToRestore

    if [ "${fileToRestoreFrom}" = '' ]; then
        local lastBackupFile=''
        fileGetLatestBackupFileR1 lastBackupFile "${fileToRestore}"
        if [ "${lastBackupFile}" = '' ]; then
            logErrorAndAbort "No suitable backup file to restore from found for: ${fileToRestore}"
        fi
        fileToRestoreFrom="${lastBackupFile}"
    fi
    assertFile "${fileToRestoreFrom}"

    local isAlreayIdentical=''
    isFileContentIdenticalR1 isAlreayIdentical "${fileToRestore}" "${fileToRestoreFrom}"
    if [ "${isAlreayIdentical}" = 'yes' ]; then
        logDebug "Contens for file and file to restore from are already identical."
        return
    fi
    local diff=`diff "${fileToRestore}" "${fileToRestoreFrom}"`
    logInfo "${diff}"

    local rc=''
    cat "${fileToRestoreFrom}" > "${fileToRestore}"; rc=$?
    if [ "${rc}" != '0' ]; then
        logErrorAndAbort "Unable to restore from backup: ${fileToRestoreFrom}"
    fi

    assertFileContentIsIdentical "${fileToRestore}" "${fileToRestoreFrom}"
    setVarOrLogInfo "${returnVarFileRestoredFrom_fileRestoreR1}" "${fileToRestoreFrom}"
}

fileRestore () {
    local dummy=''
    fileRestoreR1 dummy "$@"
}

fileRollBack () {
    fileRestore "$@"
}

fileUpdateR1 () {
    local returnVarBackupFile_fileUpdateR1="$1"
    local fileToUpdate="$2"
    local newFileContent="$3"

    assertVar fileToUpdate
    if [ ! -e "${fileToUpdate}" ]; then
        logInfo "fileUpdateR1: creating new file."
        > "${fileToUpdate}"
    fi
    assertFile "${fileToUpdate}"
    logInfo "Updateing file: ${fileToUpdate}"
    local backup=''
    fileBackupR1 backup "${fileToUpdate}"
    echo "${newFileContent}" > "${fileToUpdate}"

    local diff=`diff "${backup}" "${fileToUpdate}"`
    logInfo "${diff}"
    setVarOrLogInfo "${returnVarBackupFile_fileUpdateR1}" "${backup}"
}

fileUpdate () {
    local dummy=''
    fileUpdateR1 dummy "$@"
}

fileRollForward () {
    local fileToUpdate="$1"
    local fileNew="${fileToUpdate}.new"

    assertFile "${fileNew}"

    local isFileToUpdateNew='no'
    if [ ! -f "${fileToUpdate}" ]; then
        isFileToUpdateNew='yes'
    fi

    fileBackup "${fileToUpdate}"
    assertFile "${fileToUpdate}"

    local rc=''
    if [ "${isFileToUpdateNew}" = 'no' ]; then
        diff "${fileNew}" "${fileToUpdate}" 1>/dev/null 2>&1; rc=$?
        if [ "${rc}" = '0' ]; then
            logInfo "Nothing to do."
            return
        fi
    fi
    local diff=`diff "${fileToUpdate}" "${fileNew}"`
    logInfo "${diff}"
    cat "${fileNew}" >"${fileToUpdate}"
    local wasSuccessful=''
    isFileContentIdenticalR1 wasSuccessful "${fileToUpdate}" "${fileNew}"
    if [ "${wasSuccessful}" != 'yes' ]; then
        logErrorAndAbort "Roll forward failed for: ${fileToUpdate}"
    fi
    logInfo "Roll forward successful for: ${fileToUpdate}"
}

fileRollCheck () {
    local diffContextOpt=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-c' ]; then
            diffContextOpt='-c'
            shift
        elif [ "$1" = '-context' ]; then
            diffContextOpt='-c'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "fileRollCheck(): Ignoring unknown option: $1"
            shift
        fi
    done

    local fileToCheck="$1"
    local fileNew="${fileToCheck}.new"

    local rc=''
    assertFile "${fileToCheck}"

    local lastBackupFile=''
    fileGetLatestBackupFileR1 lastBackupFile "${fileToCheck}"
    logDebugVar lastBackupFile fileToCheck
    if [ "${lastBackupFile}" = '' ]; then
        logInfo "Roll back is not possible."
    else
        diff "${fileToCheck}" "${lastBackupFile}" 1>/dev/null 2>&1; rc=$?
        if [ "${rc}" = '0' ]; then
            logInfo "File has not been modified since last backup."
        else
            local diff=`diff ${diffContextOpt} "${fileToCheck}" "${lastBackupFile}"`
            logInfo "Roll back possible:
${diff}"
        fi
    fi
    if [ -f "${fileNew}" ]; then
        diff "${fileToCheck}" "${fileNew}" 1>/dev/null 2>&1; rc=$?
        if [ "${rc}" = '0' ]; then
            logInfo "Roll forward has already been performed."
        else
            local diff=`diff ${diffContextOpt} "${fileToCheck}" "${fileNew}"`
            logInfo "Roll forward possible:
${diff}"
        fi
    fi
}

fileGetSizeR1 () {
    local returnVar_fileGetSizeR1="$1"
    local file="$2"

    assertFile "${file}"

    local statFormatOption='-c %s'
    local statTest=`stat -h 2>&1 | grep -- '-f format'`
    if [ "${statTest}" != '' ]; then
        # bsd version uses -f for format
        statFormatOption='-f %z'
    fi

    local size_fileGetSizeR1=`stat ${statFormatOption} "${file}"`
    setVarOrLogInfo "${returnVar_fileGetSizeR1}" "${size_fileGetSizeR1}"
}

fileGetUserIdR1 () {
    local returnVar_fileGetUserIdR1="$1"
    local file="$2"

    assertExists "${file}"

    local statFormatOption='-c %u'
    local statTest=`stat -h 2>&1 | grep -- '-f format'`
    if [ "${statTest}" != '' ]; then
        # bsd version uses -f for format
        statFormatOption='-f %u'
    fi

    local uid_fileGetUserIdR1=`stat ${statFormatOption} "${file}"`
    setVarOrLogInfo "${returnVar_fileGetUserIdR1}" "${uid_fileGetUserIdR1}"
}

fileGetGroupIdR1 () {
    local returnVar_fileGetGroupIdR1="$1"
    local file="$2"

    assertExists "${file}"

    local statFormatOption='-c %g'
    local statTest=`stat -h 2>&1 | grep -- '-f format'`
    if [ "${statTest}" != '' ]; then
        # bsd version uses -f for format
        statFormatOption='-f %g'
    fi

    local uid_fileGetGroupIdR1=`stat ${statFormatOption} "${file}"`
    setVarOrLogInfo "${returnVar_fileGetGroupIdR1}" "${uid_fileGetGroupIdR1}"
}

fileMatchOwnership () {
    local source="$1"
    local target="$2" # Optional. Defaults to the dir the file is in.

    assertExists "${source}"
    qualifyPathR1 source
    setDefault target "${source%/*}"

    qualifyPathR1 target
    assertExists "${target}"

    local uidSource=''
    local gidSource=''
    local uidTarget=''
    local gidTarget=''
    fileGetUserIdR1 uidSource "${source}"
    fileGetGroupIdR1 gidSource "${source}"
    fileGetUserIdR1 uidTarget "${target}"
    fileGetGroupIdR1 gidTarget "${target}"
    if [ "${uidSource}:${gidSource}" = "${uidTarget}:${gidTarget}" ]; then
        logDebug "Ownership already matches:
${uidSource}:${gidSource} ${source}
${uidTarget}:${gidTarget} ${target}"
        return
    fi
    assertIamRootOrInteractive --logSudoWarning
    logWarning "Source and target have different owners:
${uidSource}:${gidSource} ${source}
${uidTarget}:${gidTarget} ${target}"
    logInfo "Changing user owner of target to match source:
${uidSource}:${gidSource} ${target}"
    if [ "${userid_global}" = "${uidTarget}" ]; then
        # sudo not required
        chown ${uidSource}:${gidSource} "${target}"
    else
        assertIamRootOrInteractive --logSudoWarning
        logInfo "sudo chown ${uidSource}:${gidSource} '${target}'"
        sudo chown ${uidSource}:${gidSource} "${target}"
    fi

    fileGetUserIdR1 uidTarget "${target}"
    fileGetGroupIdR1 gidTarget "${target}"
    if [ "${uidSource}:${gidSource}" = "${uidTarget}:${gidTarget}" ]; then
        logDebug "Ownerhsip change successful:
${uidTarget}:${target} ${target}"
        return
    fi
    logErrorAndAbort "Unable to change ownership of: ${target}"
}


fileIsSizeChangingR1 () {
    local returnVar_fileIsSizeChangingR1="$1" # Optional.
    local file_fileIsSizeChangingR1="$2"
    local maxWaitInSeconds="$3"               # Optional.

    setDefault maxWaitInSeconds 5
    assertInt "${maxWaitInSeconds}"

    assertFile "${file_fileIsSizeChangingR1}"
    local statFormatOption='-c %s'
    local statTest=`stat -h 2>&1 | grep -- '-f format'`
    if [ "${statTest}" != '' ]; then
        #bsd version uses -f for format
        statFormatOption='-f %z'
    fi

    local initialSize=''
    fileGetSizeR1 initialSize "${file_fileIsSizeChangingR1}"
    local size="${initialSize}"
    local secondsSinceEpoch=`date +'%s'`
    local maxSecondsSinceEpoch=''
    let maxSecondsSinceEpoch=${secondsSinceEpoch}+${maxWaitInSeconds}
    while [ "${size}" = "${initialSize}" -a \
            ${secondsSinceEpoch} -lt ${maxSecondsSinceEpoch} ]; do
        sleep 1
        fileGetSizeR1 size "${file_fileIsSizeChangingR1}"
        secondsSinceEpoch=`date +'%s'`
    done

    if [ "${size}" = "${initialSize}" ]; then
        setVarOrLogInfo "${returnVar_fileIsSizeChangingR1}" 'no' "File size unchanged after ${maxWaitInSeconds}s for: ${file_fileIsSizeChangingR1}"
    else
        setVarOrLogInfo "${returnVar_fileIsSizeChangingR1}" 'yes' "File size changed for: ${file_fileIsSizeChangingR1}"
    fi
}

fileGetAgeInSecondsR1 () {
    # https://unix.stackexchange.com/questions/102691/get-age-of-given-file
    # Aka how old relative to right now is the file.
    # The return value will change as time progresses.

    local returnVar_fileGetAgeInSecondsR1="$1"
    local fileName="$2"

    assertExists "${fileName}"

    local ageInS_fileGetAgeInSecondsR1=''
    if [ "${hostKernelName_global}" = 'FreeBSD' ]; then
        ageInS_fileGetAgeInSecondsR1=$(($(date +%s) - $(stat -t %s -f %m -- "${fileName}")))
    elif [ "${hostKernelName_global}" = 'Darwin' ]; then
        ageInS_fileGetAgeInSecondsR1=$(($(date +%s) - $(stat -t %s -f %m -- "${fileName}")))
    elif [ "${hostKernelName_global}" = 'Linux' ]; then
        ageInS_fileGetAgeInSecondsR1=$(($(date +%s) - $(date +%s -r "${fileName}")))
    else
        errorAbort "Don't know how to determine age of a file on: ${hostKernelName_global}"
    fi
    assertInt "${ageInS_fileGetAgeInSecondsR1}"
    setVarOrLogInfo "${returnVar_fileGetAgeInSecondsR1}" "${ageInS_fileGetAgeInSecondsR1}"
}

fileGetLastModifiedEpochR1 () {
local returnVar_fileGetLastModifiedEpochR1="$1"
    local fileName="$2"

    assertExists "${fileName}"

    local lastModified_fileGetLastModifiedEpochR1=''
    if [ "${hostKernelName_global}" = 'FreeBSD' ]; then
        lastModified_fileGetLastModifiedEpochR1=$(stat -t %s -f %m -- "${fileName}")
    elif [ "${hostKernelName_global}" = 'Darwin' ]; then
        lastModified_fileGetLastModifiedEpochR1=$(stat -t %s -f %m -- "${fileName}")
    elif [ "${hostKernelName_global}" = 'Linux' ]; then
        lastModified_fileGetLastModifiedEpochR1=$(date +%s -r "${fileName}")
    else
        errorAbort "Don't know how to determine age of a file on: ${hostKernelName_global}"
    fi
    assertInt "${lastModified_fileGetLastModifiedEpochR1}"
    setVarOrLogInfo "${returnVar_fileGetLastModifiedEpochR1}" "${lastModified_fileGetLastModifiedEpochR1}"
}

isFileOlderThanSR1 () {
    local returnVar_isFileOlderThanSR1="$1"
    local fileName="$2"
    local ageSpecifiedS="$3"

    assertInt "${ageSpecifiedS}"

    local fileAgeInS=''
    fileGetAgeInSecondsR1 fileAgeInS "${fileName}"

    local isOlder_isFileOlderThanSR1='no'
    if [ "${fileAgeInS}" -gt "${ageSpecifiedS}" ]; then
        isOlder_isFileOlderThanSR1='yes'
    fi
    setVarOrLogInfo "${returnVar_isFileOlderThanSR1}" "${isOlder_isFileOlderThanSR1}"
}

isFileOlderThanHR1 () {
    local returnVar_isFileOlderThanHR1="$1"
    local fileName="$2"
    local ageSpecifiedH="$3"

    assertFile "${fileName}"
    assertVar ageSpecifiedH

    local ageSpecifiedS=''
    use time
    timeDurationHumanToSecondsR1 ageSpecifiedS "${ageSpecifiedH}"

    local isOlder_isFileOlderThanHR1=''
    isFileOlderThanSR1 isOlder_isFileOlderThanHR1 "${fileName}" "${ageSpecifiedS}"
    assertVar isOlder_isFileOlderThanHR1
    setVarOrLogInfo "${returnVar_isFileOlderThanHR1}" "${isOlder_isFileOlderThanHR1}"
}

fileRename () {
    local sourceFileName="$1"
    local targetFileName="$2"

    local sourceFileNameFqn
    qualifyPathR1 sourceFileNameFqn "${sourceFileName}"

    local targetFileNameFqn
    qualifyPathR1 targetFileNameFqn "${targetFileName}"

    if [ "${sourceFileName}" = "${targetFileName}" ]; then
        logInfo "Source and target are the same. Skipping rename:
${sourceFileNameFqn}"
        return
    fi

    if [ ! -e "${sourceFileName}" ]; then
        logErrorAndAbort "Unable to rename non-existing file:
${sourceFileName}"
    fi

    if [ -e "${targetFileName}" ]; then
            logErrorAndAbort "Unable to rename. Target already exists:
${targetFileName}"
    fi

    local targetPath=''
    local targetFileNameShort=''
    splitPathR2 targetPath targetFileNameShort "${targetFileName}"

    if [ ! -d "${targetPath}" ]; then
        logErrorAndAbort "Unable to rename. Target directory does not exist:
${targetPath}"
    fi
    mv "${sourceFileName}" "${targetFileName}"
    if [ -e "${sourceFileName}" ]; then
        logErrorAndAbort "Rename failed. Old file still exists:
${sourceFileName}"
    fi
    if [ ! -e "${targetFileName}" ]; then
        logErrorAndAbort "Rename failed. New file does not exist:
${targetFileName}"
    fi

    logInfo "Renamed: ${sourceFileName} ->
         ${targetFileName}"
}

fileCleanupFileNameR1 () {
    # Note: this does not clean up directory names leading up to the file.
    local returnVar="$1"
    local fileNameWithPath="$2"

    local dirName=''
    local fileName=''
    splitPathR2 dirName fileName "${fileNameWithPath}"

    local cleanDirName=`echo "${dirName}" | sed "s/[^[:alnum:]_.-]/_/g" | sed 's/__*/_/g'`
    if [ "${dirName}" != "${cleanDirName}" ]; then
        logWarning "Path to file is not clean: ${dirName}
This will only clean up the file name, but not the path."
    fi
    local cleanFileName=`echo "${fileName}" | sed "s/[^[:alnum:]_.-]/_/g" | sed 's/__*/_/g'`

    if [ "${fileName}" = "${cleanFileName}" ]; then
        logInfo "Nothing to do, file name is already clean: ${fileName}"
        setVarOrLogInfo "${returnVar}" ''
        return
    fi

    fileRename "${dirName}/${fileName}" "${dirName}/${cleanFileName}"

    setVarOrLogInfo "${returnVar}" "${dirName}/${cleanFileName}"
}

fileCleanupFileName () {
    local filesToProcess="$@"
    if [ "${filesToProcess}" = '' ]; then
        local oldIFS="${IFS}"
        IFS='
'
        filesToProcess=(`ls 2>/dev/null`)
        IFS="${oldIFS}"
    fi
    local fileToProcess=''
    for fileToProcess in "${filesToProcess[@]}" ; do
        fileCleanupFileNameR1 '' "${fileToProcess}"
    done
}

fileGetSectionR1 () {
    local returnVarSection_fileGetSectionR1="$1"
    local file_fileGetSectionR1="$2"
    local startEgrep="$3"
    local endEgrep="$4"

    assertFile "${file_fileGetSectionR1}"

    local section_fileGetSectionR1=`
              cat "${file_fileGetSectionR1}" \
              | pipeGetSection "${startEgrep}" "${endEgrep}"`
    setVarOrLogInfo \
        "${returnVarSection_fileGetSectionR1}" \
        "${section_fileGetSectionR1}"
}

fileGetHumanReadableBytesR1 () {
    local padded=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-p' -o "$1" = '--padded' ]; then
            padded='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            # ${FUNCNAME[0]} is not available in sh
            logWarning "fileGetHumanReadableBytesR1(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVar_fileGetHumanReadableBytesR1="$1"
    local bytes_fileGetHumanReadableBytesR1="$2"
    assertInt "${bytes_fileGetHumanReadableBytesR1}"

    local suffix=''
    local value=${bytes_fileGetHumanReadableBytesR1}
    local count=0
    local result_fileGetHumanReadableBytesR1=''
    for suffix in B kiB MiB GiB TiB PiB EiB ZiB YiB; do
        if [ "${padded}" = 'yes' ]; then
            # 12.3 GiB
            # 12345678
            # longest is 4+1+3 = 8 unless last suffix overflows
            result_fileGetHumanReadableBytesR1=`printf '%4s %-3s' "${value}" "${suffix}"`
        else
            result_fileGetHumanReadableBytesR1="${value} ${suffix}"
        fi
        local digitCount=`echo -n "${value}" | sed 's/[^0-9]*//g' | wc -c`
        stripR1 digitCount
        # logDebug value suffix result_fileGetHumanReadableBytesR1 digitCount
        if [ ${digitCount} -le 3 ]; then
            break
        fi
        let count+=1
        # logDebug "echo 'scale=1;${bytes_fileGetHumanReadableBytesR1}.0/(1024^${count})' | bc"
        value=`echo "scale=1;${bytes_fileGetHumanReadableBytesR1}.0/(1024^${count})" | bc`
        value=`printf '%1.1f' "${value}"`
    done
    if [ "${padded}" = 'yes' ]; then
        # 12.3 GiB
        # 12345678
        # longest is 8 unless last suffix overflows
        result_fileGetHumanReadableBytesR1=`printf '%8s' "${result_fileGetHumanReadableBytesR1}"`
    fi
    setVarOrLogInfo "${returnVar_fileGetHumanReadableBytesR1}" "${result_fileGetHumanReadableBytesR1}"
}

# MAIN

fileMainRemoteBootstrap () {
    diffExec=`which diff 2>/dev/null`
    if [ "${diffExec}" = '' ]; then
        isFileContentIdenticalR1 () { logErrorAndAbort "diff not in PATH"; }
    fi
}

moduleLoading='file.sh' # Required for sh modules.
eval "${showLibraryUsageIfNotSourced}"

fileMainRemoteBootstrap
