#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam use related helpers

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

useReload () {
    # friendly name for use -f
    use -f "$@"
    if [ "$*" = '' ]; then
        logError "Please specify module to reload."
    fi
}

useReloadAll () {
    local loadHistoryShortUniq=''
    getModuleLoadHistoryMostRecentLastR1 --short --uniq loadHistoryShortUniq
    if [ "${loadHistoryShortUniq}" = '' ]; then
        logInfo "No modules have been loaded."
        # This should not happen, since at least the use module should
        # have been recorded as loaded.
        return
    fi
    local module=''
    local modulesLoaded=''
    varValueLineDedupeStripEmpty loadHistoryShortUniq
    for module in ${loadHistoryShortUniq}; do
        use -f ${module}
    done
}

useGetAvailableModulesR1 () {
    local short='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' -o  "$1" = '--short' ]; then
            short='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVar_useGetAvailableModulesR1="$1"
    local moduleList_useGetAvailableModulesR1=''
    for dir in \
        "${bamDir_global}/lib" \
        "${bamDir_global}/../bam_projects/"*"/lib" \
        "~/bam/lib" \
        "/root/bam/lib" \
        ; do

        if [ "${isBash_global}" = 'yes' -o "${isZsh_global}" = 'yes' ]; then
            moduleList_useGetAvailableModulesR1="${moduleList_useGetAvailableModulesR1}
`find \"${dir}\" -type f -name '*.bash' 2>/dev/null`"
        fi
        moduleList_useGetAvailableModulesR1="${moduleList_useGetAvailableModulesR1}
`find \"${dir}\" -type f -name '*.sh' 2>/dev/null`"
    done
    local moduleListClean_useGetAvailableModulesR1=''
    local module=''
    local moduleFqn=''
    local duplicateTest=''
    while read module; do
        qualifyPathR1 moduleFqn "${module}"
        duplicateTest=`echo "${moduleListClean_useGetAvailableModulesR1}" | grep -E '^'"${moduleFqn}"'$'`
        if [ "${duplicateTest}" = '' ]; then
            moduleListClean_useGetAvailableModulesR1="${moduleListClean_useGetAvailableModulesR1}
${moduleFqn}"
        else
            logDebug -f "Ignoring duplicate: ${moduleFqn}"
        fi
    done < <( echo "${moduleList_useGetAvailableModulesR1}" | pipeStripStandard )

    if [ "${short}" = 'yes' ]; then
        moduleListClean_useGetAvailableModulesR1=`
            echo "${moduleListClean_useGetAvailableModulesR1}" \
            | sed 's/.*\///' \
            | sort \
            | uniq`
    fi
    stripLeadingBlankLinesR1 moduleListClean_useGetAvailableModulesR1
    setVarOrLogInfo \
        "${returnVar_useGetAvailableModulesR1}" \
        "${moduleListClean_useGetAvailableModulesR1}"
}

useGetMatchingModulesR1 () {
    local returnVar_useGetMatchingModulesR1="$1"
    local patternToMatch="$2"

    assertVariable patternToMatch

    local modules=''
    useGetAvailableModulesR1 modules

    local matchingModules=''
    local module=''
    while read module; do
        local moduleShort="${module##*/}" # e.g. interactive.sh
        moduleShort="${moduleShort/.*}"   # e.g. interactive
        local isDirectMatch=`echo "${moduleShort}" | grep -E '^'"${patternToMatch}"'$'`
        if [ "${isDirectMatch}" != '' ]; then
            matchingModules="${module}"
            break
        fi
        local isMatch=`echo "${moduleShort}" | egrep "${patternToMatch}"`
        if [ "${isMatch}" != '' ]; then
            matchingModules="${matchingModules}
${module}"
        fi
    done < <( echo "${modules}" )
    setVarOrLogInfo \
        "${returnVar_useGetMatchingModulesR1}" \
        "${matchingModules}"
}

useEdit () {
    local module="$1"
    local moduleFile=''

    assertVar module

    if [ -f "${module}" ]; then
        moduleFile="${module}"
    else
        useGetMatchingModulesR1 moduleFile "${module}"
        if [ "${moduleFile}" = '' ]; then
            local availableModules=''
            useGetAvailableModulesR1 --short availableModules
            logError "Unable to find module: ${module}"
            logErrorAndAbort --noStackTrace "Available modules are:
${availableModules}"
        fi
    fi
    if [ "${BAM_EDITOR}" != '' ]; then
        logDebug "\"${BAM_EDITOR}\" ${BAM_EDITOR_OPTIONS} \"${moduleFile}\""
        "${BAM_EDITOR}" ${BAM_EDITOR_OPTIONS} "${moduleFile}"
    elif [ "${EDITOR}" != '' ]; then
        "${EDITOR}" ${moduleFile}
    else
        vim ${moduleFile}
    fi
}

# MAIN
eval "${showLibraryUsageIfNotSourced}"
