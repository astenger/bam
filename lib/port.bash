#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam Mac Ports module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

Mac Ports helper functions.

EOF

}

portUpdate () {
    local outdated=`port info outdated | grep ' @[0-9]' | sed 's/ @.*//'`
    local port=''

    assertIamRootOrInteractive

    # Update the ports base.
    sudo port selfupdate

    # Mass update. It is rare that this runs through all the way.
    sudo port upgrade outdated

    # Pick up the pieces by upgrading packages individualy. This may
    # still not work as expected and some ports may have to be
    # disabled along the way.
    for port in $outdated; do
        logHeader1 "Upgrading: ${port}"
        # -N  non-interactive aka no questions asked
        sudo port -N upgrade "${port}"
    done
}


# MAIN

eval "${showLibraryUsageIfNotSourced}"

if [ "${isInteractive_global}" = 'yes' ]; then
    portsOutdatedMessage=`port outdated`
    logInfo "${portsOutdatedMessage}"
    logInfo "To update run as root: portUpdate"
fi
