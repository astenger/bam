#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam hash module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

Hash functions. Depends on availability of executables.

EOF

}

avaliableHashFunctions_global=''
avaliableHashFileFunctions_global=''
hashSha256R1 () {
    determineAvailableHashFunctions
    hashSha256R1 "$@"
}
hashSha256FileR1 () {
    determineAvailableHashFunctions
    hashSha256R1 "$@"
}
hashMd5R1 () {
    determineAvailableHashFunctions
    hashMd5R1 "$@"
}
hashMd5FileR1 () {
    determineAvailableHashFunctions
    hashMd5FileR1 "$@"
}
hashSumR1 () {
    determineAvailableHashFunctions
    hashSumR1 "$@"
}
hashSumFileR1 () {
    determineAvailableHashFunctions
    hashSumFileR1 "$@"
}

determineAvailableHashFunctions () {
    local returnVar="$1"
    local execInPathTest=''
    execInPathTest=`which sha256sum 2>/dev/null`
    if [ "${execInPathTest}" != '' ]; then
        hashSha256R1 () {
            local _hashSum_=`echo -n "$2" | sha256sum | awk ' { print $1 } '`
            setVar "$1" "${_hashSum_}"
        }
        hashSha256FileR1 () {
            assertFile "$2"
            local _hashSum_=`cat "$2" | sha256sum | awk ' { print $1 } '`
            setVar "$1" "${_hashSum_}"
        }
        avaliableHashFunctions_global="${avaliableHashFunctions_global} hashSha256R1"
        avaliableHashFileFunctions_global="${avaliableHashFileFunctions_global} hashSha256FileR1"
    else
        execInPathTest=`which shasum 2>/dev/null`
        if [ "${execInPathTest}" != '' ]; then
            hashSha256R1 () {
                local _hashSum_=`echo -n "$2" | shasum -a 256 | awk ' { print $1 } '`
                setVar "$1" "${_hashSum_}"
            }
            hashSha256FileR1 () {
                assertFile "$2"
                local _hashSum_=`cat "$2" | shasum -a 256 | awk ' { print $1 } '`
                setVar "$1" "${_hashSum_}"
            }
            avaliableHashFunctions_global="${avaliableHashFunctions_global} hashSha256R1"
            avaliableHashFileFunctions_global="${avaliableHashFileFunctions_global} hashSha256FileR1"
        else
            hashSha256R1 () {
                logErrorAndAbort "Neither sha256sum nor sha256 in PATH."
            }
            hashSha256FileR1 () {
                logErrorAndAbort "Neither sha256sum nor sha256 in PATH."
            }
        fi
    fi

    execInPathTest=`which md5sum 2>/dev/null`
    if [ "${execInPathTest}" != '' ]; then
        hashMd5R1 () {
            local _hashSum_=`echo -n "$2" | md5sum | awk ' { print $1 } '`
            setVar "$1" "${_hashSum_}"
        }
        hashMd5FileR1 () {
            assertFile "$2"
            local _hashSum_=`cat "$2" | md5sum | awk ' { print $1 } '`
            setVar "$1" "${_hashSum_}"
        }
        avaliableHashFunctions_global="${avaliableHashFunctions_global} hashMd5R1"
        avaliableHashFileFunctions_global="${avaliableHashFileFunctions_global} hashMd5FileR1"
    else
        execInPathTest=`which md5 2>/dev/null`
        if [ "${execInPathTest}" != '' ]; then
            hashMd5R1 () {
                local _hashSum_=`echo -n "$2" | md5`
                setVar "$1" "${_hashSum_}"
            }
            hashMd5FileR1 () {
                assertFile "$2"
                local _hashSum_=`cat "$2" | md5`
                setVar "$1" "${_hashSum_}"
            }
            avaliableHashFunctions_global="${avaliableHashFunctions_global} hashMd5R1"
            avaliableHashFileFunctions_global="${avaliableHashFileFunctions_global} hashMd5FileR1"
        else
            hashMd5R1 () {
                logErrorAndAbort "Neither md5sum nor md5 in PATH."
            }
            hashMd5FileR1 () {
                logErrorAndAbort "Neither md5sum nor md5 in PATH."
            }
        fi
    fi
    if [ "${avaliableHashFunctions_global}" != '' ]; then
        avaliableHashFunctions_global=`echo ${avaliableHashFunctions_global}`
        avaliableHashFileFunctions_global=`echo ${avaliableHashFileFunctions_global}`
        hashSumR1 () {
            "${avaliableHashFunctions_global%% *}" "$@"
        }
        hashSumFileR1 () {
            "${avaliableHashFileFunctions_global%% *}" "$@"
        }
    fi
}

# MAIN

hashMainRemoteBootstrap () {
    determineAvailableHashFunctions
}

moduleLoading='hash.sh' # Required for sh modules.
eval "${showLibraryUsageIfNotSourced}"

hashMainRemoteBootstrap
