#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam lock module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

use trap

libraryUsage () {
    cat <<EOF

Directory based locking functions.

This style of locking can't be guaranteed, but it does work pretty
well even with lock directories on network shares.

See: https://stackoverflow.com/questions/6870221/is-there-any-mutex-semaphore-mechanism-in-shell-scripts

All locks will be released when the shell exits.

Important things to remember with semaphores:
- do not nest locking of semaphores if at all possible
- if you absolutely have to do nesting:
  - release locks in reverse order of acquiring them

Otherwise you may end up in debugging hell ;-)

Example usage:

$ use lock
$ lock 1
INFO  Lock acquired: /Users/astenger/bam_test/1
$ lock 2
INFO  Lock acquired: /Users/astenger/bam_test/2
$ unlock 2
INFO  Released lock at: /Users/astenger/bam_test/2
$ unlock 1
INFO  Released lock at: /Users/astenger/bam_test/1

lock / unlock is short for:

lockAcquireOrAbort
lockReleaseOrAbort

EOF

}

lockDirIndicatorFile_global='THIS_DIR_IS_A_LOCK_CREATED_BY_BAM_MODULE_LOCK'
lockDirReleaseRequestedFile_global='LOCK_RELEASE_HAS_BEEN_REQUESTED'
lockDirReleaseRequestCancelledFile_global='LOCK_RELEASE_REQUEST_HAS_BEEN_CANCELLED'
setDefault ownedLocks_lck000 ''
isLockOwnedOrActiveR3 () {
    local pidCheck='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-g' -o "$1" = '--graceful' ]; then
            # Do not abort when lock release fails or even when a
            # different lock is active.
            graceful='-g'
            shift
        elif [ "$1" = '-p' -o "$1" = '--pidCheck' ]; then
            pidCheck='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVarOwnedYesNo_isLockOwnedOrActiveR3="$1"
    local returnVarActiveYesNo_isLockOwnedOrActiveR3="$2"
    local returnVarPid_isLockOwnedOrActiveR3="$3"
    local lockDir="$4"

    assertVariable lockDir
    qualifyPathR1 lockDir

    local isLockActive_isLockOwnedOrActiveR3='no'
    if [ -d "${lockDir}" ]; then
        isLockActive_isLockOwnedOrActiveR3='yes'
    fi

    local isLockOwned_isLockOwnedOrActiveR3=`
        echo "${ownedLocks_lck000}" | egrep "^${lockDir}\$"`
    if [ "${isLockOwned_isLockOwnedOrActiveR3}" = '' ]; then
        isLockOwned_isLockOwnedOrActiveR3='no'
    else
        isLockOwned_isLockOwnedOrActiveR3='yes'
        if [ "${isLockActive_isLockOwnedOrActiveR3}" = 'no' ]; then
            logErrorAndAbort "Lock is owned by current process, but lock dir does not exist: ${lockDir}"
        fi
    fi

    local lockPid_isLockOwnedOrActiveR3=`cat "${lockDir}/pid" 2>/dev/null`

    if [ "${pidCheck}" = 'yes' -a "${lockPid_isLockOwnedOrActiveR3}" != '' ]; then
        use pid
        if [ "${lockPid_isLockOwnedOrActiveR3}" = "$$" ]; then
            if [ "${isLockOwned_isLockOwnedOrActiveR3}" = 'yes' ]; then
                logInfo "Lock pid confirmed for current process: ${lockPid_isLockOwnedOrActiveR3}"
            else
                logWarning "Lock pid is current pid, but lock was not listed as owned. Considering lock as owned."
                isLockOwned_isLockOwnedOrActiveR3='yes'
            fi
        else
            local isPidAlive_isLockOwnedOrActiveR3=''
            isPidAliveR1 isPidAlive_isLockOwnedOrActiveR3 "${lockPid_isLockOwnedOrActiveR3}"
            if [ "${isPidAlive_isLockOwnedOrActiveR3}" = 'yes' ]; then
                isLockActive_isLockOwnedOrActiveR3='yes'
                logDebug "Lock pid is alive: ${lockPid_isLockOwnedOrActiveR3}"
            else
                logWarning "lockAcquireOrAbortR1: Lock dir exists, but pid is not alive: ${lockPid_isLockOwnedOrActiveR3}"
                logWarning "Considering lock as inactive."
                isLockActive_isLockOwnedOrActiveR3='no'
                if [ "${isLockOwned_isLockOwnedOrActiveR3}" = 'yes' ]; then
                    logErrorAndAbort "Lock is owned by us, but lock pid is not alive."
                fi
            fi
        fi
    fi

    if [ "${returnVarOwnedYesNo_isLockOwnedOrActiveR3}" = '' -a \
         "${returnVarActiveYesNo_isLockOwnedOrActiveR3}" = '' -a \
         "${returnVarPid_isLockOwnedOrActiveR3}" = '' ]; then
        logInfo "Lock ${lockDir} is owned:  ${isLockOwned_isLockOwnedOrActiveR3}"
        logInfo "Lock ${lockDir} is active: ${isLockActive_isLockOwnedOrActiveR3}"
        logInfo "Process id holding lock:   ${lockPid_isLockOwnedOrActiveR3}"
    fi
    if [ "${returnVarOwnedYesNo_isLockOwnedOrActiveR3}" != '' ]; then
        setVar \
            "${returnVarOwnedYesNo_isLockOwnedOrActiveR3}" \
            "${isLockOwned_isLockOwnedOrActiveR3}"
    fi
    if [ "${returnVarActiveYesNo_isLockOwnedOrActiveR3}" != '' ]; then
        setVar \
            "${returnVarActiveYesNo_isLockOwnedOrActiveR3}" \
            "${isLockActive_isLockOwnedOrActiveR3}"
    fi
    if [ "${returnVarPid_isLockOwnedOrActiveR3}" != '' ]; then
        setVar \
            "${returnVarPid_isLockOwnedOrActiveR3}" \
            "${lockPid_isLockOwnedOrActiveR3}"
    fi
}

isLockOwnedR1 () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    local returnVar_isLockOwnedR1="$1"
    shift
    isLockOwnedOrActiveR3 ${passOptions# } "${returnVar_isLockOwnedR1}" '' '' "$@"
}

isLockActiveR1 () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    local returnVar_isLockActiveR1="$1"
    shift
    isLockOwnedOrActiveR3 ${passOptions# } '' "${returnVar_isLockActiveR1}" '' "$@"
}

getLockPidR1 () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    local returnVar_getLockPidR1="$1"
    shift
    isLockOwnedOrActiveR3 ${passOptions# } '' '' "${returnVar_getLockPidR1}" "$@"
}

lockAcquireOrAbortR1 () {
    local graceful=''
    local pidCheck='no'
    local abortStackTraceOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-g' -o "$1" = '--graceful' ]; then
            # Do not abort when acquiring lock fails or even when a
            # different lock is active.
            graceful='-g'
            shift
        elif [ "$1" = '-p' -o "$1" = '--pidCheck' ]; then
            pidCheck='yes'
            shift
        elif [ "$1" = '-n' -o "$1" = '--noStackTrace' ]; then
            abortStackTraceOption='--noStackTrace'
            shift
        elif [ "$1" = '-w' -o "$1" = '--withStackTrace' ]; then
            abortStackTraceOption='--withStackTrace'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVarYesNo_lockAcquireOrAbortR1="$1"
    local lockDir="$2"

    local islockAlreadyActive=''
    isLockOwnedR1 islockAlreadyOwned "${lockDir}"
    if [ "${islockAlreadyOwned}" = 'yes' ]; then
        logWarning "Lock is already owned: ${lockDir}"
        setVarOrLogInfo "${returnVarYesNo_lockAcquireOrAbortR1}" 'yes' "Lock is already owned: ${lockDir}"
        return
    fi
    if [ "${ownedLocks_lck000}"  != '' ]; then
        logDebug "Use of nested locks is strongly discouraged."
    fi

    # FUTURE: determine sensible default lock location.
    # could be "`pwd`/${scriptName_global}.lockdir"
    # or we do "`pwd`/.lockdir"
    #if [ "${scriptName_global}" != '' ]; then
    #    setDefault lockDir "`pwd`/${scriptName_global}.lockdir"
    #    setDefault lockDir "`pwd`/.lockdir"
    #fi
    assertVariable lockDir
    qualifyPathR1 lockDir

    if [ "${pidCheck}" = 'yes' ]; then
        if [ -d "${lockDir}" ]; then
            if [ ! -f "${lockDir}/pid" ]; then
                logErrorAndAbort "lockAcquireOrAbortR1: pid check requested,
lock dir exists but pid file does not:
${lockDir}
`ls -lah ${lockDir}`"
            else
                use pid
                local lockPid=`cat "${lockDir}/pid"`
                assertVar lockPid
                if [ "${lockPid}" = "$$" ]; then
                    logWarning "Lock pid is current pid, but lock was not listed as active. Honouring lock and not doing anything else."
                    return
                fi
                local isPidAlive=''
                isPidAliveR1 isPidAlive "${lockPid}"
                if [ "${isPidAlive}" = 'yes' ]; then
                    logErrorAndAbort ${abortStackTraceOption} ${graceful} "Lock NOT acquired: ${lockDir}
Lock dir exists and lock pid is alive."
                else
                    logWarning "lockAcquireOrAbortR1: Lock dir exists, but pid is not alive: ${lockPid}"
                    logWarning "Forcing release of lock: ${lockDir}"
                    rm -rf "${lockDir}"
                    assertDoesNotExist "${lockDir}" "Unable to force delete of lock dir: ${lockDir}"
                fi
            fi
        fi
    fi

    if mkdir "${lockDir}" 2>/dev/null; then
        logDebug "lockAcquireOrAbortR1: Lock acquired using dir: ${lockDir}"
        ownedLocks_lck000="${ownedLocks_lck000}
${lockDir}"
        logDebugVar ownedLocks_lck000
        touch "${lockDir}/${lockDirIndicatorFile_global}"
        touch "${lockDir}/DATA_IN_HERE_WILL_BE_DELETED"
        touch "${lockDir}/${hostname_global}_${USER}_$$"
        echo "${hostname_global}" >"${lockDir}/hostname"
        # ${USER} is not set in cron execution
        echo "${username_global}" >"${lockDir}/user"
        echo "$$" >"${lockDir}/pid"
        if [ "${scriptName_global}" = '' ]; then
            echo '[interactive shell]' > "${lockDir}/script"
        else
            echo "${scriptNameQualified_global}" > "${lockDir}/script"
        fi
        setVarOrLogInfo "${returnVarYesNo_lockAcquireOrAbortR1}" 'yes' "Lock acquired: ${lockDir}"
        return
    else
        setVarOrLogInfo "${returnVarYesNo_lockAcquireOrAbortR1}" 'no'  "Lock NOT acquired: ${lockDir}"
        # If we have a competing cron job on a different machine, all
        # the lock dir trace files may not have been written yet.
        sleep 1
        sync
        logErrorAndAbort ${graceful} "Lock NOT acquired: ${lockDir}
`ls -lah ${lockDir}`"
        return
    fi
}

lockAcquireOrAbort () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    lockAcquireOrAbortR1 ${passOptions# } '' "$@"
}

lock () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done
    lockAcquireOrAbortR1 ${passOptions# } '' "$@"
}

lockReleaseOrAbort () {
    local graceful=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-g' ]; then
            # Do not abort when lock release fails or even when a
            # different lock is active.
            graceful='-g'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local lockDir="$1"

    assertVariable lockDir
    qualifyPathR1 lockDir

    local islockOwned=''
    isLockOwnedR1 islockOwned "${lockDir}"
    if [ "${islockOwned}" != 'yes' ]; then
        logErrorAndAbort ${graceful} "Lock is not active: ${lockDir}"
        return
    fi

    if [ ! -d "${lockDir}" ]; then
        logErrorAndAbort ${graceful} "Lock does not exist: ${lockDir}"
        return
    fi

    local mostRecentLock=`echo "${ownedLocks_lck000}" | tail -n 1`
    if [ "${lockDir}" != "${mostRecentLock}" ]; then
        logWarning "Lock is NOT most recent: ${lockDir}
            most recent: ${mostRecentLock}"
    fi

    if [ ! -f "${lockDir}/${lockDirIndicatorFile_global}" ]; then
        # This might be some entirely different directory.
        # Play it safe.
        local indicator="${lockDir}/${lockDirIndicatorFile_global}"
        logErrorAndAbort "Lock dir indicator file not found: ${indicator}"
    fi

    logDebug "rm -rf '${lockDir}'"
    rm -rf "${lockDir}"
    if [ -d "${lockDir}" ]; then
        logErrorAndAbort ${graceful} "Unable to release lock: ${lockDir}"
        return
    fi
    logInfo "Released lock at: ${lockDir}"
    ownedLocks_lck000=`echo "${ownedLocks_lck000}" | egrep -v "^${lockDir}\$"`
    logDebugVar ownedLocks_lck000
}

unlock () { lockReleaseOrAbort "$@" ; }

releaseAllActiveLocks () {
    local lock=''
    echo "${ownedLocks_lck000}" \
        | while read lock; do
              if [ "${lock}" = '' ]; then
                  continue
              fi
              lockReleaseOrAbort -g "${lock}"
          done
}

assertLockSingleScriptInstance () {
    assertVar scriptName_global \
        "assertSingleScriptInstance: cannot be used in an interactive shell."
    assertDir '/tmp'
    registerExitFunction lockSingleScriptExitAction
    logInfo "${scriptName_global}: Trying to acquire single script instance lock."
    lock --pidCheck --noStackTrace "/tmp/${scriptName_global}"
}

lockSingleScriptExitAction () {
    if [ "${scriptName_global}" != '' ]; then
        logInfo "${scriptName_global}: Exiting."
    fi
}

assertSingleScriptInstance () {
    assertLockSingleScriptInstance "$@"
}

isLockScriptRunningR1 () {
    local returnVarYesNo_isLockScriptRunningR1="$1"
    local isRunning=''
    isLockActiveR1 --pidCheck isRunning "/tmp/${scriptName_global}"
    setVarOrLogInfo "${returnVarYesNo_isLockScriptRunningR1}" "${isRunning}" "Script is running: ${isRunning}"
    local lockPid=''
    getLockPidR1 lockPid "/tmp/${scriptName_global}"
    if [ "${lockPid}" != '' -a "${logDebugEnabled_global}" = 'yes' ]; then
        use pid
        pidShowProcesses " ${lockPid} "
    fi
}

isLockScriptRunning () {
    isLockScriptRunningR1 '' "$@"
}

lockSingleFunctionInstance () {
    local callingFunction="${FUNCNAME[1]}"
    assertVar callingFunction
    assertDir '/tmp'
    lockAcquireOrAbort "/tmp/bam_function_${callingFunction}"
}

releaseSingleFunctionInstance () {
    local callingFunction="${FUNCNAME[1]}"
    assertVar callingFunction
    assertDir '/tmp'
    lockReleaseOrAbort "/tmp/bam_function_${callingFunction}"
}

lockRegisterGracefulReleaseTraps () {
    local lockDir="$1"

    assertVariable lockDir
    qualifyPathR1 lockDir

    if [ "${sourced_global}" = '' ]; then
        # https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced
        logWarning "Variable sourced_global is not set. On bash, use this in your top level script:
(return 0 2>/dev/null) && sourced_global='yes' || sourced_global='no'"
    fi
    if [ "${sourced_global}" = 'yes' ]; then
        registerInterruptCode "lockReleaseOrAbort -g '${lockDir}'"
    fi
    registerExitCode "lockReleaseOrAbort -g '${lockDir}'"
}

lockUnregisterGracefulReleaseTraps () {
    local lockDir="$1"

    assertVariable lockDir
    qualifyPathR1 lockDir

    if [ "${sourced_global}" = 'yes' ]; then
        unregisterInterruptCode "lockReleaseOrAbort -g '${lockDir}'"
    fi
    unregisterExitCode "lockReleaseOrAbort -g '${lockDir}'"
}

lockRequestRelease () {
    local lockDir="$1"

    assertVariable lockDir
    qualifyPathR1 lockDir

    if [ ! -d "${lockDir}" ]; then
        logError "lockRequestRelease: lock dir does not exist: ${lockDir}"
        return
    fi

    assertVar lockDirIndicatorFile_global
    assertVar lockDirReleaseRequestedFile_global

    if [ ! -f "${lockDir}/${lockDirIndicatorFile_global}" ]; then
        logError "lockRequestRelease: dir is not one of our lock dirs: ${lockDir}
`ls \"${lockDir}\"`"
        return
    fi
    if [ -f "${lockDir}/${lockDirReleaseRequestedFile_global}" ]; then
        logInfo "Lock release has already been requested:
`cat \"${lockDir}/${lockDirReleaseRequestedFile_global}\"`"
        return
    fi
    local ts
    getTimestampR1 ts
    echo "Lock release requested by:
Script: ${scriptName_global:-[interactive shell]}
Host:   ${hostname_global:-[unknown host]}
User:   ${username_global:-[unknown user]}
Time:   ${ts}" > "${lockDir}/${lockDirReleaseRequestedFile_global}"
    if [ ! -f "${lockDir}/${lockDirReleaseRequestedFile_global}" ]; then
        logError "Unable to request release of lock: ${lockDir}/${lockDirReleaseRequestedFile_global}"
    fi
    logInfo "Lock release requested: ${lockDir}/${lockDirReleaseRequestedFile_global}"
}

lockCancelReleaseRequest () {
    local lockDir="$1"

    assertVariable lockDir
    qualifyPathR1 lockDir

    if [ ! -d "${lockDir}" ]; then
        logError "lockCancelReleaseRequest: lock dir does not exist: ${lockDir}"
        return
    fi

    assertVar lockDirIndicatorFile_global
    assertVar lockDirReleaseRequestedFile_global
    assertVar lockDirReleaseRequestCancelledFile_global

    if [ ! -f "${lockDir}/${lockDirIndicatorFile_global}" ]; then
        logError "lockCancelReleaseRequest: dir is not one of our lock dirs: ${lockDir}
`ls \"${lockDir}\"`"
        return
    fi
    if [ ! -f "${lockDir}/${lockDirReleaseRequestedFile_global}" ]; then
        logInfo "There is no active release request for: ${lockDir}"
        return
    fi
    logInfo "Cancelling lock release request:
`cat \"${lockDir}/${lockDirReleaseRequestedFile_global}\"`"
    rm "${lockDir}/${lockDirReleaseRequestedFile_global}"
    if [ -f "${lockDir}/${lockDirReleaseRequestedFile_global}" ]; then
        logError "Unable to cancel release request of lock: ${lockDir}/${lockDirReleaseRequestedFile_global}"
        return
    fi
    local ts
    getTimestampR1 ts
    echo "Lock release request cancelled by:
Script: ${scriptName_global:-[interactive shell]}
Host:   ${hostname_global:-[unknown host]}
User:   ${username_global:-[unknown user]}
Time:   ${ts}" > "${lockDir}/${lockDirReleaseRequestCancelledFile_global}"
    if [ ! -f "${lockDir}/${lockDirReleaseRequestCancelledFile_global}" ]; then
        logWarning "Unable to write file: ${lockDir}/${lockDirReleaseRequestCancelledFile_global}"
    fi
    logInfo "Lock release request cancelled: ${lockDir}/${lockDirReleaseRequestedFile_global}"
}

hasLockReleaseBeenRequestedR1 () {
    local returnVarYesNo_hasLockReleaseBeenRequestedR1="$1"
    local lockDir="$2"

    assertVariable lockDir
    qualifyPathR1 lockDir

    if [ ! -d "${lockDir}" ]; then
        logError "hasLockReleaseBeenRequestedR1: lock dir does not exist: ${lockDir}"
        setVarTolerant "${returnVarYesNo_hasLockReleaseBeenRequestedR1}" ''
        return
    fi

    assertVar lockDirIndicatorFile_global
    assertVar lockDirReleaseRequestedFile_global

    if [ ! -f "${lockDir}/${lockDirIndicatorFile_global}" ]; then
        logError "hasLockReleaseBeenRequestedR1: dir is not one of our lock dirs: ${lockDir}
`ls \"${lockDir}\"`"
        setVarTolerant "${returnVarYesNo_hasLockReleaseBeenRequestedR1}" ''
        return
    fi
    if [ -f "${lockDir}/${lockDirReleaseRequestedFile_global}" ]; then
        setVarOrLogInfo "${returnVarYesNo_hasLockReleaseBeenRequestedR1}" 'yes'
        return
    fi
    setVarOrLogInfo "${returnVarYesNo_hasLockReleaseBeenRequestedR1}" 'no'
}

lockRequestReleaseForSingleScriptInstance () {
    assertVar scriptName_global \
          "lockRequestReleaseForSingleScriptInstance: cannot be used in an interactive shell."
    lockRequestRelease "/tmp/${scriptName_global}"
}

lockCancelReleaseRequestForSingleScriptInstance () {
    assertVar scriptName_global \
          "lockRequestReleaseForSingleScriptInstance: cannot be used in an interactive shell."
    lockCancelReleaseRequest "/tmp/${scriptName_global}"
}

hasLockReleaseForSingleScriptInstanceBeenRequestedR1 () {
    local returnVarYesNo_hasLockReleaseForSingleScriptInstanceBeenRequestedR1="$1"
    assertVar scriptName_global \
        "hasLockReleaseForSingleScriptInstanceBeenRequestedR1: cannot be used in an interactive shell."
    if [ ! -d '/tmp' ]; then
        logError "hasLockReleaseForSingleScriptInstanceBeenRequestedR1: Dir does not exist: /tmp"
        return
    fi
    hasLockReleaseBeenRequestedR1 "${returnVarYesNo_hasLockReleaseForSingleScriptInstanceBeenRequestedR1}" "/tmp/${scriptName_global}"
}

# MAIN

lockMainRemoteBootstrap () {
    # Release all locks we are holding on exit.
    registerExitFunction releaseAllActiveLocks
}

eval "${showLibraryUsageIfNotSourced}"

lockMainRemoteBootstrap
