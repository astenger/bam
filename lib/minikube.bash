#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam Minikube module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Minikube helper functions.

EOF
}

minikubeGetProxyPidsR1 () {
    local returnVar_minikubeGetProxyPidsR1="$1"
    local pids_minikubeGetProxyPidsR1=''
    pids_minikubeGetProxyPidsR1=`
        ps -ef \
            | grep kubectl \
            | grep proxy \
            | grep -E '^'"${username_global}"' ' \
            | awk ' { print $2 } '
            `
    serVarOrLogInfo "${returnVar_minikubeGetProxyPidsR1}" "${pids_minikubeGetProxyPidsR1}"
}

minikubeProxyStart () {
    local pids=''
    logInfo "Starting background process: kubectl proxy"
    kubectl proxy &
}

minikubeProxyKill () {
    assertVar username_global
    local pids=''

    minikubeGetProxyPidsR1 pids

    pidKill ${pids}
}

minikubeBootstrap () {
    # https://minikube.sigs.k8s.io/docs/handbook/untrusted_certs/
    local minikubeCertsDir="$HOME/.minikube/certs"
    local embedCertsOption=''
    if [ -f "${minikubeCertsDir}" ]; then
        embedCertsOption='--embed-certs'
    fi
    minikube start ${embedCertsOption}
    minikube addons enable metrics-server
    minikube addons enable dashboard
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"

alias kubectl='minikube kubectl --'
alias k8s-show-ns="kubectl api-resources --verbs=list --namespaced -o name  | xargs -n 1 kubectl get --show-kind --ignore-not-found  -n"
