#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam nvidia linux module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Helpers for nvidia drivers on linux machines.

EOF

}


nvidiaContainerToolkitTest () {
    # https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/sample-workload.html
    use docker
    assertDockerIsInstalled

    if [ "${username_global}" != ' root' -a \
         "${username_global}" != ' docker' ]; then
        logInfo "Not running as root or docker. This will ask for your sudo password."
        assertInteractive
        sudo docker run --rm --runtime=nvidia --gpus all ubuntu nvidia-smi
        return
    fi
    local nvidiaSmi=`docker run --rm --runtime=nvidia --gpus all ubuntu nvidia-smi`
    local gpuTest=`echo "${nvidiaSmi}" | grep -i 'gpu'`
    if [ "${gpuTest}" = '' ]; then
        logWarning "nvidia-smi inside the container failed:
${nvidiaSmi}"
        return
    fi
    logInfo "nvidia-smi inside the container works:
${nvidiaSmi}"
}

nvidiaContainerTestHardwareTranscoding () {
    # https://docs.tdarr.io/docs/installation/docker/hardware-transcoding
    use docker
    assertDockerIsInstalled
    if [ "${username_global}" != ' root' -a \
         "${username_global}" != ' docker' ]; then
        sudo docker run \
            -e "NVIDIA_DRIVER_CAPABILITIES=all" \
            -e "NVIDIA_VISIBLE_DEVICES=all" \
            --gpus=all \
            ghcr.io/haveagitgat/tdarr_node:latest \
            /bin/bash -e \
                -c 'curl \
                        -o /tmp/sample.mkv \
                        -l https://samples.tdarr.io/api/v1/samples/sample__1080__libx264__aac__30s__video.mkv; \
                    ffmpeg \
                        -i /tmp/sample.mkv \
                        -c:v:0 hevc_nvenc \
                        /tmp/sample-out.mkv'
        return
    fi
    docker run \
        -e "NVIDIA_DRIVER_CAPABILITIES=all" \
        -e "NVIDIA_VISIBLE_DEVICES=all" \
        --gpus=all \
        ghcr.io/haveagitgat/tdarr_node:latest \
        /bin/bash -e \
            -c 'curl \
                    -o /tmp/sample.mkv \
                    -l https://samples.tdarr.io/api/v1/samples/sample__1080__libx264__aac__30s__video.mkv; \
                ffmpeg \
                    -i /tmp/sample.mkv \
                    -c:v:0 hevc_nvenc \
                    /tmp/sample-out.mkv'
}

nvidiaWatch () {
    watch -n 1 nvidia-smi
}

nvidiaContainerToolkitInstall () {
    # https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html
    use ubu
    assertUbu "nvidiaContainerToolkitInstall: only implemented for Ubuntu."

    use docker
    assertDockerIsInstalled

    local alreadyInstalledTest=''
    ubuIsPackageInstalledR1 -v alreadyInstalledTest nvidia-container-toolkit
    if [ "${alreadyInstalledTest}" != '' ]; then
        return
    fi

    assertIamRootOrInteractive

    if [ ! -f /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg ]; then
        logInfo "Fetching repo key."
        curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey \
            | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg
    fi
    assertFile /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg

    if [ ! -f /etc/apt/sources.list.d/nvidia-container-toolkit.list ]; then
        logInfo "Configuring apt repo."
        curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list \
            | sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' \
            | sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
    fi
    assertFile /etc/apt/sources.list.d/nvidia-container-toolkit.list

    sudo apt-get update
    ubuInstallPackage nvidia-container-toolkit
}

nvidiaListInstalledPackages () {
    use ubu
    assertUbu "nvidiaListInstalledPackages: only implemented for Ubuntu."
    apt list --installed nvidia* 2>/dev/null | grep -v -E '^[^n]'
}

getNvidiaDriverVersionR1 () {
    local returnVar_getNvidiaDriverVersionR1="$1"
    local driver_getNvidiaDriverVersionR1=`apt list --installed nvidia-driver-* 2>/dev/null | grep -v -E '^[^n]' | sed 's/\/.*//'`
    setVarOrLogInfo "${returnVar_getNvidiaDriverVersionR1}" "${driver_getNvidiaDriverVersionR1}"
}

nvidiaDriverUninstallEverything () {
    use ubu
    assertUbu "nvidiaDriverUninstallEverything: only implemented for Ubuntu."
    if [ "${username_global}" =! ' root' ]; then
        assertInteractive
    fi
    sudo apt autoremove nvidia* --purge
}

nvidiaGetAvailableServerDriverWithHighestVersionR1 () {
    local returnVar_nvidiaGetAvailableServerDriverWithHighestVersionR1="$1"
    use ubu
    assertUbu "nvidiaGetAvailableServerDriverWithHighestVersionR1: only implemented for Ubuntu."

    local nvidiaPciCard=`lspci | grep -e VGA | grep -i nvidia`
    if [ "${nvidiaPciCard}" = '' ]; then
        logErrorAndAbort "No nvidia PCI GPU card found"
    fi
    logInfo "Found nvidia PCI GPU card
${nvidiaPciCard}"
    logInfo "Getting available drivers ..."
    local availableUbuntuDrivers=`ubuntu-drivers devices | grep 'nvidia-driver'`
    local availableUbuntuDriversClean=`echo "${availableUbuntuDrivers}" | sed 's/.*: //' | sed 's/ - .*//'`
    local availableUbuntuServerDrivers=`echo "${availableUbuntuDriversClean}" | grep server`
    assertVar availableUbuntuServerDrivers "No ubuntu nvidia server driver available."
    local latestAvailableUbuntuServerDriver=''
    local driverVersion=''
    local maxDriverVersion='0'
    while read driverVersion; do
        if [ "${driverVersion}" = '' ]; then
            continue
        fi
        assertInt ${driverVersion}
        if [ ${driverVersion} -gt ${maxDriverVersion} ]; then
            maxDriverVersion=${driverVersion}
        fi
    done < <( echo "${availableUbuntuServerDrivers}" \
                  | sed 's/^[^0-9]*//' \
                  | sed 's/-.*//' \
                  | pipeStripEmptyLines )
    local serveDriverWithHighestVersion_nvidiaGetAvailableServerDriverWithHighestVersionR1=`echo "${availableUbuntuServerDrivers}" | grep -- "-${maxDriverVersion}-"`
    setVarOrLogInfo "${returnVar_nvidiaGetAvailableServerDriverWithHighestVersionR1}" "${serveDriverWithHighestVersion_nvidiaGetAvailableServerDriverWithHighestVersionR1}"
}

nvidiaDriverInstall () {
    local driverToInstall="$1"
    # https://www.linuxcapable.com/install-nvidia-drivers-on-ubuntu-linux/
    use ubu
    assertUbu "nvidiaDriverInstall: only implemented for Ubuntu."

    local nvidiaDriverVersion=''
    local package=''
    # alsa-utils seems to be a dependency that is not coded in the package db
    local additionalPackagesPre='alsa-utils'
    # nvidia-cuda-toolkit is required to make tenserflow and others work
    local additionalPackagesPost='nvidia-cuda-toolkit nvtop'
    getNvidiaDriverVersionR1 nvidiaDriverVersion
    if [ "${driverToInstall}" = '' -a "${nvidiaDriverVersion}" != '' ]; then
        logInfo "Nvidia driver is already installed: ${nvidiaDriverVersion}"
        local installed=''
        local allOtherPackagesInstalled='yes'

        for package in ${additionalPackagesPre} ${additionalPackagesPost}; do
            ubuIsPackageInstalledR1 installed "${package}"
            if [ "${installed}" = 'no' ]; then
                allOtherPackagesInstalled='no'
                logInfo "Package is NOT installed: ${package}"
            else
                logInfo "Package is already installed: ${package}"
            fi
        done
        if [ "${allOtherPackagesInstalled}" = 'yes' ]; then
            return
        fi
    fi
    if [ "${nvidiaDriverVersion}" != "${driverToInstall}" ]; then
        logErrorAndAbort "Requested driver ${driverToInstall} is different from installed driver ${nvidiaDriverVersion}.
Please use: nvidiaDriverUninstallEverything"
    fi
    if [ "${driverToInstall}" != '' ]; then
        logInfo "Fetching available drivers..."
        local availableUbuntuDrivers=`ubuntu-drivers devices | grep 'nvidia-driver'`
        local availableUbuntuDriversClean=`echo "${availableUbuntuDrivers}" | sed 's/.*: //' | sed 's/ - .*//'`
        local isValidDriver=`echo "${availableUbuntuDriversClean}" | grep -E '$'"${driverToInstall}"'$'`
        if [ "${isValidDriver}" = '' ]; then
            logErrorAndAbort "Requested driver ${driverToInstall} is not one of the available drivers:
${availableUbuntuDrivers}"
        fi
    fi
    assertIamRootOrInteractive
    ubuAptRun
    if [ "${driverToInstall}" = '' ]; then
        nvidiaGetAvailableServerDriverWithHighestVersionR1 driverToInstall
    fi
    assertVar driverToInstall
    for package in ${additionalPackagesPre} "${driverToInstall}" ${additionalPackagesPost}; do
        logInfo "Installing Package: ${package}"
        ubuInstallPackage "${package}"
    done
    logWarning --wrap "If you have passed the nvidia graphics card into a VM, you may have to reboot the physical host and not just the VM to make it work."
    logWarning "Rebooting."
    sudo reboot
}

nvidiaInstallTheLot () {
    nvidiaDriverInstall
    nvidiaContainerToolkitInstall
}

nvidiaGetGpuModelR1 () {
    local returnVar_nvidiaGetGpuModelR1="$1"
    local gpu_nvidiaGetGpuModelR1=`nvidia-smi --list-gpus 2>&1 | grep -E '^GPU' |  sed 's/[^:]*: //' | sed 's/ (UUID.*//'`
    setVarOrLogInfo "${returnVar_nvidiaGetGpuModelR1}" "${gpu_nvidiaGetGpuModelR1}"
}

isNvidiaGpuAvailableR1 () {
    local returnVar_isNvidiaGpuAvailableR1="$1"
    local nvidiaPciDevices=`lspci 2>&1 | grep -e VGA | grep NVIDIA`
    if [ "${nvidiaPciDevices}" = '' ]; then
        logWarning "No nvidia PCI devices found."
        setVarOrLogInfo "${returnVar_isNvidiaGpuAvailableR1}" 'no'
        return
    fi
    logDebug "Nvidia PCI devices:
${nvidiaPciDevices}"
    local nvidiaSmiExec=`which nvidia-smi 2>/dev/null`
    if [ "${nvidiaSmiExec}" = '' ]; then
        logWarning "nvidia-smi is not in PATH. Drivers not installed correctly?"
        setVarOrLogInfo "${returnVar_isNvidiaGpuAvailableR1}" 'no'
        return
    fi
    local gpuTest=`nvidia-smi 2>&1`
    local gpuTestRc="$?"
    if [ "${gpuTestRc}" != '0' ]; then
        logWarning "nvidia-smi does not work as expected: rc=${gpuTestRc}
${gpuTest}
Drivers not installed correctly?"
        setVarOrLogInfo "${returnVar_isNvidiaGpuAvailableR1}" 'no'
    fi
    local gpuList=`nvidia-smi --list-gpus 2>&1`
    local gpuListTest=`echo "${gpuList}" | grep -i 'no devices'`
    if [ "${gpuListTest}" != '' ]; then
        logWarning "No nvidia gpu found on this system."
        setVarOrLogInfo "${returnVar_isNvidiaGpuAvailableR1}" 'no'
        return
    fi
    logInfo "Found the following nvidia GPU(s):
${gpuList}"
    setVarOrLogInfo "${returnVar_isNvidiaGpuAvailableR1}" 'yes'
}

isNvidiaGpuAvailable () {
    isNvidiaGpuAvailableR1
}

# MAIN

linuxMainRemoteBootstrap () {
    if [ "${hostKernelName_global}" != 'Linux' ]; then
        logInfoInteractive "This machine is not Linux. Stuff might not work as you expect."
    fi
}

eval "${showLibraryUsageIfNotSourced}"

linuxMainRemoteBootstrap
