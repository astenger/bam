#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam cleanup module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Clean-up helpers.

Registered cleanup functions will be run on EXIT, INT, and TERM.

Note that KILL (kill -9 xxx) does not allow for graceful handling of
anything of course.

Simple usage example:

# If you don't need cleanup nesting:
echo "I need to be cleaned up!" >/tmp/mytempfile
cleanupRegisterCode 'rm -f /tmp/mytempfile'
# do something
exit

# If you have cleanup in different sections and want to clean up
# right away
echo "I need to be cleaned up!" >/tmp/mytempfile
cleanupRegisterCode 'rm -f /tmp/mytempfile'
# do something with the file
cleanupRunAndUnregisterCode 'rm -f /tmp/mytempfile'
# do other stuff without the file

Simple usage with a defined cleanup function.

myCleanup() {
    rm -f /tmp/mytempfile
    echo "cleanup done."
}
echo "I need to be cleaned up!" >/tmp/mytempfile
cleanupRegisterFunction myCleanup
# do something with the file
cleanupRunAndUnregisterFunction myCleanup
# do other stuff without the file

Also note that if the user hits ctrl-c multiple times cleanup might
get interrupted and not do what you intended. C'est la vie.

You can also use this in interactive sessions while programming:

$ myCleanupFunction1 ()
{
    rm /tmp/my_temp_file
}
$ cleanupRegisterFunction myCleanupFunction1
$ cleanupRegisterCode 'echo we are done'
$ showCleanupFunctions
myCleanupFunction1 ()
{
    rm /tmp/my_temp_file
}
trapFunction_96b8a4c2d9c705a65833fee79336b47412a1c235ca504fb4c8f1dc99d08b841f ()
{
    echo we are done
}
$ cleanupUnregisterFunction
$ cleanupRunAndUnregisterCode 'echo we are done'
$ showCleanupFunctions
INFO  No cleanup functions registered.

EOF
}

# inspiration taken from https://unix.stackexchange.com/questions/57940/trap-int-term-exit-really-necessary
cleanupRun () {
    logVerbose "Executing cleanupRun"

    local cleanupFunction=''
    local cleanupFunctionsCopy="${_cleanupFunctions_trp000}"
    while read cleanupFunction; do
        logDebug "Running cleanup function: ${cleanupFunction}"
        ${cleanupFunction}
        cleanupUnregisterFunction "${cleanupFunction}"
    done < <( echo "${cleanupFunctionsCopy}" | pipeStripEmptyLines )

    # Paranoia mode: on
    local isCleanupFunctionListEmpty=`echo -n "${_cleanupFunctions_trp000}" | pipeStripEmptyLines`
    if [ "${isCleanupFunctionListEmpty}" != '' ]; then
        logWarning "There still seem to be some registered cleanup-functions:
${_cleanupFunctions_trp000}"
    fi
}

cleanupEstablishTraps () {
    local signal=''

    for signal in 'EXIT' 'INT' 'TERM'; do
        registerTrapFunction cleanupRun "${signal}"
    done
}

cleanupRegisterCode () {
    local code="$1"

    local generatedFunction=''
    defineTrapFunctionForCodeR1 generatedFunction "${code}"
    cleanupRegisterFunction "${generatedFunction}"
}

cleanupUnregisterCode () {
    local code="$1"

    local generatedFunction=''
    defineTrapFunctionForCodeR1 generatedFunction "${code}"
    cleanupUnregisterFunction "${generatedFunction}"
}

cleanupRunAndUnregisterCode () {
    local code="$1"

    local generatedFunction=''
    defineTrapFunctionForCodeR1 generatedFunction "${code}"
    ${generatedFunction}
    cleanupUnregisterFunction "${generatedFunction}"
}

cleanupRegisterFunction () {
    local functionName="$1"

    assertValidName "${functionName}" "cleanupRegisterFunction: function name is invalid: ${functionName}"
    local alreadyRegisteredTest="`echo "${_cleanupFunctions_trp000}" | egrep \"^${functionName}\$\"`"
    if [ "${alreadyRegisteredTest}" != '' ]; then
        logInfo "Cleanup function is already registered: ${functionName}"
        return
    fi
    _cleanupFunctions_trp000="${_cleanupFunctions_trp000}
${functionName}"
    logDebugVar _cleanupFunctions_trp000
    logDebug "Registered cleanup function: ${functionName}"
}

cleanupUnregisterFunction () {
    local functionName="$1"

    local cleanupFunctionsNew="`echo "${_cleanupFunctions_trp000}" | egrep -v "^${functionName}\$"`"
    if [ "${_cleanupFunctions_trp000}" = "${cleanupFunctionsNew}" ]; then
        logInfo "Cleanup function is not registered: ${functionName}"
    else
        _cleanupFunctions_trp000="${cleanupFunctionsNew}"
        logDebug "Cleanup function has been unregistered: ${functionName}"
    fi
}

cleanupRunAndUnregisterFunction () {
    local cleanupFunction="$1"

    logDebug "Running cleanup function: ${cleanupFunction}"
    ${cleanupFunction}

    cleanupUnregisterFunction "${cleanupFunction}"
}

showCleanupFunctions () {
    if [ "${_cleanupFunctions_trp000}" = '' ]; then
        logInfo "No cleanup functions registered."
    else
        local cleanpFunction
        for cleanpFunction in ${_cleanupFunctions_trp000}; do
            type "${cleanpFunction}" | egrep -v 'is a function$'
        done
    fi
}

getTemporaryFile () {
    local returnVarTempFile="$1"
    assertVariable returnVarTempFile

    local tempfile_returnVarTempFile=`mktemp /tmp/bam_temp_file_${scriptName_global}.XXXXXX` \
        || logErrorAndAbort "Unable to create temp file."
    registerExitCode "rm -f '${tempfile_returnVarTempFile}'"
    setVar "${returnVarTempFile}" "${tempfile_returnVarTempFile}"
}

deleteTemporaryFile () {
    local tempfile="$1"
    assertFile "${tempfile}"
    rm -f "${tempfile}"
    registerExitCode "rm -f '${tempfile}'"
}

# MAIN

cleanupMainRemoteBootstrap () {
    cleanupEstablishTraps
}

eval "${showLibraryUsageIfNotSourced}"

use trap

cleanupMainRemoteBootstrap
