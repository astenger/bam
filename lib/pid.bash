#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam pid module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

This module defines process helper functions.

EOF

}

pidBackgroundR1 () {
    # https://stackoverflow.com/questions/7686989/running-bash-commands-in-the-background-without-printing-job-and-process-ids/51061046#51061046
    local silentCommand='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' ]; then
            silentCommand='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVarPid="$1" # Optional.
    shift

    local backgroundPid_pidBackgroundR1=''
    if [ "${silentCommand}" = 'yes' ]; then
        # Supress stdout and stderr as well ... 2>&1 is probably overkill
        logDebug "pidBackgroundR1: starting" "$@" "1>/dev/null 2>&1"
        { 2>&3 "$@" 1>/dev/null 2>&1 & } 3>&2 2>/dev/null
    else
        # This suppresses the backgrounding trace, but stdout of the command will still be visible
        logDebug "pidBackgroundR1: starting" "$@"
        { 2>&3 "$@" & } 3>&2 2>/dev/null
    fi

    backgroundPid_pidBackgroundR1=$!
    logDebug "pidBackgroundR1: pid=${backgroundPid_pidBackgroundR1}"

    # Prevent whine if job has already completed
    disown 1>/dev/null 2>&1

    setVarOrLogInfo "${returnVarPid}" "${backgroundPid_pidBackgroundR1}"
}

isPidAliveR1 () {
    local returnVarYesNo="$1"
    local pidToTest="$2"

    assertVar pidToTest
    local rc
    kill -0 ${pidToTest} 1>/dev/null 2>&1; rc=$?
    if [ "${rc}" != '0' ]; then
        setVarOrLogInfo "${returnVarYesNo}" 'no'
        return
    fi
    setVarOrLogInfo "${returnVarYesNo}" 'yes'
}

pidExecuteWithTimeoutR1 () {
    # Return variable indicates whether job finished before timeout (yes/no)
    # Usage example:
    # pidExecuteWithTimeoutR1 -t 3 jobFinished sleep 1
    # echo "Job finished: ${jobFinished}"

    # https://stackoverflow.com/questions/687948/timeout-a-command-in-bash-without-unnecessary-delay
    local silentCommandOption=''
    local timeout=3  # timeout in seconds
    local interval=1 # interval between checks
    local delay=1    # delay between term and kill

    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' ]; then
            silentCommandOption='-s'
            shift
        elif [ "$1" = '-t' ]; then
            shift
            timeout="$1"
            assertInt "${timeout}"
            shift
            break
        elif [ "$1" = '-i' ]; then
            shift
            interval="$1"
            assertint "${interval}"
            shift
            break
        elif [ "$1" = '-d' ]; then
            shift
            delay="$1"
            assertint "${delay}"
            shift
            break
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVarFinshedBeforeTimeoutYesNo="$1"
    shift

    local bgPid
    pidBackgroundR1 ${silentCommandOption} bgPid "$@"
    local isAlive='yes'

    while [ ${timeout} -gt 0 ]; do
        isPidAliveR1 isAlive ${bgPid}
        logDebug "Pid ${bgPid} is alive: ${isAlive}"
        if [ "${isAlive}" = 'no' ]; then
            break
        fi
        let timeout-=${interval}
        sleep "${interval}"
    done

    isPidAliveR1 isAlive ${bgPid}
    if [ "${isAlive}" = 'yes' ]; then
        pidKill "${bgPid}"
        setVarOrLogInfo "${returnVarFinshedBeforeTimeoutYesNo}" 'no'
        return
    fi
    setVarOrLogInfo "${returnVarFinshedBeforeTimeoutYesNo}" 'yes'
}

pidRunInBackgroundWithTimeout () {
    local timeout="$1"
    shift

    assertVar timeout

    logInfo "Executing in background (timeout=${timeout}s): $*"
    #logInfo "With timeout: ${timeout}s"

    local jobFinished=''

    local startTime=`date +%s`
    pidExecuteWithTimeoutR1 -t "${timeout}" jobFinished "$@"
    local endTime=`date +%s`
    local runTime=''
    let runtime=${endTime}-${startTime}

    if [ "${jobFinished}" = 'yes' ]; then
        logInfo "Job finished after ${runtime}s before timeout of ${timeout}s: ${jobFinished}
$*"
    else
        logInfo "Job failed to finish before timeout of ${timeout}s: ${jobFinished}
$*"
    fi
}

pidShowProcesses () {
    local egrepFilter="$1"

    setDefault egrepFilter '.*'
    local processList=''
    if [ "${isPidPsDashEFUsable_global}" = 'yes' ]; then
        processList=`ps -ef | grep -E "${egrepFilter}" | grep -v grep`
    elif [ "${isPidPsDashEFUsable_global}" = 'yes' ]; then
        processList=`ps -auxwww | grep -E "${egrepFilter}" | grep -v grep`
    else
        logErrorAndAbort "pidShowProcesses() : neither ps -ef not ps -auxwww are usable"
    fi
    logInfo "Process list matching regex: '${egrepFilter}'
${processList}"
}

setDefault isPidPsDashEFUsable_global ''
setDefault isPidPsDashAUXWWWUsable_global ''
pidDetermineShellCapabilities () {
    local psDashEFTest=`ps -ef 2>/dev/null | grep -- "$0"`
    if [ "${psDashEFTest}" = '' ]; then
        isPidPsDashEFUsable_global='no'
    else
        isPidPsDashEFUsable_global='yes'
    fi

    local psDashAUXWWWTest=`ps -auxwww 2>/dev/null | grep -- "$0"`
    if [ "${psDashAUXWWWTest}" = '' ]; then
        isPidPsDashAUXWWWUsable_global='no'
    else
        isPidPsDashAUXWWWUsable_global='yes'
    fi
}

pidShowShellCapabilities () {
    logInfo "Can 'ps -ef' be used:                          isPidPsDashEFUsable_global=${isPidPsDashEFUsable_global}"
    logInfo "Can 'ps -auxwww' be used:                  isPidPsDashAUXWWWUsable_global=${isPidPsDashAUXWWWUsable_global}"
}

pidKill () {
    local pids="$*"

    local pidCount=`echo "${pids}" | wc -w`
    stripR1 pidCount

    local pid
    if [ "${pidCount}" = '0' ]; then
        logWarning "No pid to kill specified."
        return
    elif [ "${pidCount}" = '1' ]; then
        pid="${pids}"
    else
        for pid in ${pids}; do
            pidKill ${pid}
        done
        return
    fi

    assertVar pid
    local isAlive=''
    local delay=1    # delay between term and kill

    isPidAliveR1 isAlive "${pid}"
    if [ "${isAlive}" = 'no' ]; then
        logInfo "Pid does not exist: ${pid}"
        return
    fi
    logInfo "Sending TERM to pid: ${pid}
`ps -f --pid ${pid}`"
    kill -TERM ${pid} 1>/dev/null 2>&1
    isPidAliveR1 isAlive ${pid}
    if [ "${isAlive}" = 'no' ]; then
        return
    fi
    logDebug "Spleeping for: ${delay} s"
    sleep "${delay}"
    isPidAliveR1 isAlive ${pid}
    if [ "${isAlive}" = 'no' ]; then
        return
    fi
    logInfo "Sending KILL to pid: ${pid}"
    kill -KILL ${pid} 1>/dev/null 2>&1
    isPidAliveR1 isAlive ${pid}
    if [ "${isAlive}" = 'no' ]; then
        return
    fi
    logWarning "kill -KILL was unable to kill pid: ${pid}"
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"

pidDetermineShellCapabilities
