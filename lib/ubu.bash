#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam ubuntu helpers

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

Library with ubuntu helper functions.

EOF

}

assertUbu () {
    use linux
    assertLinuxRelease Ubuntu "$1"
}

ubuSnapsRefresh () {
    local snapExec=`which snap 2>/dev/null`
    if [ "${snapExec}" = '' ]; then
        logInfo "Snap not in PATH. Skipping refreshing snaps."
        return
    fi
    logInfo "Refreshing installed snaps"
    sudo snap refresh
}

ubuAptRun () {
    assertIamRootOrInteractive
    logInfo "Updating package database."
    sudo apt-get update -y

    logInfo "Upgrading packages."
    # https://askubuntu.com/questions/1367139/apt-get-upgrade-auto-restart-services
    # NEEDRESTART_MODE=a DEBIAN_FRONTEND=noninteractive will
    # automatically restart services as required. Should only need one
    # of the two, but better safe than sorry.
    sudo NEEDRESTART_MODE=a DEBIAN_FRONTEND=noninteractive apt-get upgrade -y --with-new-pkgs

    logInfo "Cleaning up left-overs."
    sudo apt-get autoremove -y

    logInfo "Cleaning up apt archives."
    # /var/cache/apt/archives/
    # This can use a lot of space over time. Clean it out.
    # https://www.cyberciti.biz/faq/can-i-delete-var-cache-apt-archives-for-ubuntu-debian-linux/
    sudo apt-get clean -y

    ubuSnapsRefresh

    if [ -d "${bamDir_global}/.git" ]; then
        logInfo "Updating bam."
        if [ "${isInteractiveShell_global}" = 'yes' ]; then
            # https://stackoverflow.com/questions/85880/determine-if-a-function-exists-in-bash
            local haveBamAutoUpdateFunction=`LC_ALL=C type -t bamAutoUpdate`
            if [ "${haveBamAutoUpdateFunction}" = 'function' ]; then
                # This will re-launch the current shell if there are
                # updates so we can use the updates right away.
                bamAutoUpdate -f
            else
                bamUpdate
            fi
        else
            bamUpdate
        fi
    fi
}

ubuIsPackageInstalledR1 () {
    local verbose='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-v' -o "$1" = '--verbose' ]; then
            verbose='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "ubuIsPackageInstalledR1(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVar_ubuIsPackageInstalledR1="$1"
    local package="$2"
    assertVar package

    local alreadyIntalledTest=`apt list --installed "${package}" 2>/dev/null | grep -v -E 'Listing...'`
    if [ "${alreadyIntalledTest}" != '' ]; then
        if [ "${verbose}" = 'yes' ]; then
            logInfo "Package is installed: ${package}"
        fi
        setVarOrLogInfo "${returnVar_ubuIsPackageInstalledR1}" 'yes'
        return
    fi
    if [ "${verbose}" = 'yes' ]; then
        logInfo "Package is NOT installed: ${package}"
    fi
    setVarOrLogInfo "${returnVar_ubuIsPackageInstalledR1}" 'no'
}

ubuAptInstallPackage () {
    assertIamRootOrInteractive
    local package=''
    for package in "$@" ; do
        logInfo "Installing: ${package}"
        sudo apt-get install -y ${package}
    done
}

ubuInstallPackage () {
    ubuAptInstallPackage "$@"
}

ubuInstallDevPkg () {
    ubuInstallStandardPkg
    ubuAptInstallPackage \
        make \
        gcc \
        binutils \
        emacs \

}

ubuInstallSmartPkg () {
    ubuAptInstallPackage gnome-disk-utility
}

ubuInstallStandardPkg () {
    ubuAptInstallPackage \
        openssh-server \
        net-tools \
        htop \
        btop \
        git \
        tmux \
        curl \
        jq \
        glances \
        lolcat \
        figlet
}

ubuInstallWifiDriver_rtl88x2bu () {
    mkdir ~/git
    spushd ~/git
    git clone https://github.com/cilynx/rtl88x2bu
    spushd rtl88x2bu

    ubuAptInstallPackage dkms

    # Straight from the README.md
    VER=$(sed -n 's/\PACKAGE_VERSION="\(.*\)"/\1/p' dkms.conf)
    sudo rsync -rvhP ./ /usr/src/rtl88x2bu-${VER}
    sudo dkms add -m rtl88x2bu -v ${VER}
    sudo dkms build -m rtl88x2bu -v ${VER}
    sudo dkms install -m rtl88x2bu -v ${VER}
    sudo modprobe 88x2bu
    spopd
    spopd
}

ubuScsiRescan () {
    ubuScanForNewDisks
    return

    # https://geekpeek.net/rescan-scsi-bus-on-linux-system/
    assertIamRoot

    local scsiHost
    logInfo "Tailing log file in the background: /var/log/syslog"
    tail -f /var/log/syslog &
    local tailBackgroundPid=$!
    for scsiHost in /sys/class/scsi_host/host*/scan; do
        assertVar scsiHost
        logInfo "Triggering: ${scsiHost}"
        echo "- - -" > "${scsiHost}"
    done
    sleep 1
    kill -TERM ${tailBackgroundPid}
}

ubuUpdateAutoConfigureDeprecated () {
    # https://libre-software.net/ubuntu-automatic-updates/
    use file
    assertIamRoot

    apt-get install -y unattended-upgrades
    assertFile /etc/apt/apt.conf.d/50unattended-upgrades
    if [ ! -d /etc/apt/apt.conf.d/bak ]; then
        mkdir /etc/apt/apt.conf.d/bak
    fi
    assertDir /etc/apt/apt.conf.d/bak
    if [ ! -f /etc/apt/apt.conf.d/bak/50unattended-upgrades ]; then
        cp -p /etc/apt/apt.conf.d/50unattended-upgrades /etc/apt/apt.conf.d/bak/50unattended-upgrades
        assertFileContentIsIdentical /etc/apt/apt.conf.d/50unattended-upgrades /etc/apt/apt.conf.d/bak/50unattended-upgrades
    fi
    cat /etc/apt/apt.conf.d/bak/50unattended-upgrades \
        | sed 's/\/\/[ '"${tab_global}"'][ '"${tab_global}"']*"${distro_id}:${distro_codename}-updates";/  '"${tab_global}"'"${distro_id}:${distro_codename}-updates";/' \
        | sed 's/\/\/Unattended-Upgrade::Remove-Unused-Kernel-Packages "false";/Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";/' \
        | sed 's/\/\/Unattended-Upgrade::Remove-Unused-Dependencies "false";/Unattended-Upgrade::Remove-Unused-Dependencies "true";/' \
        | sed 's/\/\/Unattended-Upgrade::Automatic-Reboot "false";/Unattended-Upgrade::Automatic-Reboot "true";/' \
        | sed 's/\/\/Unattended-Upgrade::Automatic-Reboot-Time "02:00";/Unattended-Upgrade::Automatic-Reboot-Time "03:13";/' \
        > /etc/apt/apt.conf.d/bak/50unattended-upgrades.new
    assertFile /etc/apt/apt.conf.d/bak/50unattended-upgrades.new
    fileRollForward /etc/apt/apt.conf.d/bak/50unattended-upgrades

    assertFile /etc/apt/apt.conf.d/20auto-upgrades
    if [ ! -f /etc/apt/apt.conf.d/bak/20auto-upgrades ]; then
        cp -p /etc/apt/apt.conf.d/20auto-upgrades /etc/apt/apt.conf.d/bak/20auto-upgrades
    fi
    cat << EOF >/etc/apt/apt.conf.d/bak/20auto-upgrades.new
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
EOF
    fileRollForward /etc/apt/apt.conf.d/bak/20auto-upgrades

    cat /etc/apt/apt.conf.d/bak/50unattended-upgrades >/etc/apt/apt.conf.d/50unattended-upgrades
    cat /etc/apt/apt.conf.d/bak/20auto-upgrades >/etc/apt/apt.conf.d/20auto-upgrades

    unattended-upgrades --dry-run --debug
}

ubuDisableMultipathOnEsxi () {
    local esxiTest=`vmware-toolbox-cmd timesync status 2>&1 | grep 'bled'`
    if [ "${esxiTest}" = '' ]; then
        logError "We are not a guest on esxi or vmware tools is not installed."
        return
    fi
    local rc=''
    systemctl is-active --quiet multipathd; rc=$?
    if [ "${rc}" != '0' ]; then
        logInfo "multipathd is not active:
`systemctl status multipathd 2>&1`"
        return
    fi
    logInfo "multipathd is active."
    if [ ! -f /etc/multipath.conf ]; then
        logInfo "Does not exist: /etc/multipath.conf"
        logInfo "Not doing anything."
        return
    fi
    local blacklistTest=`grep blacklist /etc/multipath.conf`
    if [ "${blacklistTest}" != '' ]; then
        logInfo "We already have a blacklist entry in /etc/multipath.conf"
        return
    fi
    if [ ! -f /var/log/syslog ]; then
        logWarning "Unable to check for related errors at: /var/log/syslog"
    else
        local syslogEntryTest=`
                  egrep 'multipathd.*failed to get .* uid: (Invalid argument|No such file or directory)' \
                      /var/log/syslog \
                  | head -n 1`
        if [ "${syslogEntryTest}" = '' ]; then
            logInfo "There are no related errors at: /var/log/syslog"
            return
        fi
        logInfo "Found related errors at: /var/log/syslog"
    fi
    assertIamRoot
    use file
    local currentMultipathConf=`cat /etc/multipath.conf`
    local newMultipathConf="${currentMultipathConf}"
    local hasUserFriendlyNames=`echo "${currentMultipathConf}" | grep 'user_friendly_names'`
    if [ "${hasUserFriendlyNames}" = '' ]; then
        newMultipathConf="${newMultipathConf}
defaults {
    user_friendly_names yes
}"
    fi
    newMultipathConf="${newMultipathConf}
blacklist {
    devnode \"^(ram|raw|loop|fd|md|dm-|sr|scd|st|sd)[0-9]*\"
}"
    fileUpdate /etc/multipath.conf "${newMultipathConf}"
    logInfo "Restarting multipathd"
    systemctl restart multipathd
}

ubuSetup () {
    assertIamRoot
    ubuAptRun
    local cronJobScript=''
    for cronJobScript in \
        "${bamDir_global}/bin/diskGuard.sh" \
        "${bamDir_global}/bin/ubuAptRunViaCron.sh" \
        ; do
        if [ -f "${cronJobScript}" ]; then
            local cronTest=`crontab -l | grep "${cronJobScript}"`
            if [ "${cronTest}" = '' ]; then
                "${cronJobScript}" -i
            else
                "${cronJobScript}" -c
            fi
        fi
    done
    bamAutoUpdateEnable
    bamAutoUpdateEnable -f
    autoLoadAdd ubu
    autoLoadAdd docker
    ubuDisableMultipathOnEsxi
    use docker
    dockerInstall
    dockerComposeInstall
    (
        curl --silent https://bam.lv42.com/ | egrep -v '^<.*>$'
        cat << EOF | su - docker
. ./bam/bam.sh
bamInstall
bamAutoUpdateEnable
bamAutoUpdateEnable -f
./bam/bin/dockerComposeStopStart.sh -i
EOF
    )
    if [ -f /tmp/dockerComposeStopStart.sh.cron.log ]; then
        mv /tmp/dockerComposeStopStart.sh.cron.log /var/log/dockerComposeStopStart.sh.cron.log
    fi
}

ubuScanForNewDisks () {
    local scsiHost=''
    local disksBefore=`ls /dev/sd* | grep -E '[a-z]$'`
    logInfo "This might ask for your password to run sudo."
    for scsiHost in /sys/class/scsi_host/host*/scan; do
        logFlash "Triggering scan for: ${scsiHost}"
        echo "- - -" | sudo tee ${scsiHost} >/dev/null
    done
    logFlashClear
    local disksAfter=`ls /dev/sd* | grep -E '[a-z]$'`
    local newDisks=`diff <( echo "${disksBefore}" ) <( echo "${disksAfter}" ) | grep -E '> ' | sed 's/^> //'`
    if [ "${newDisks}" = '' ]; then
        logInfo "No new disks found."
    else
        logInfo "New disks found:
${newDisks}"
    fi
}

ubuGetAdditionalUnmountedDrivesR1 () {
    local returnVar_ubuGetAdditionalUnmountedDrivesR1="$1"

    local mountedDrives=`mount | grep /dev/sd | awk ' { print $1 } ' | sed 's/[1-9]*$//' | sort | uniq`
    local drives_ubuGetAdditionalUnmountedDrivesR1=`ls /dev/sd* | grep -E '[a-z]$'`
    local mountedDrive=''
    for mountedDrive in ${mountedDrives}; do
        drives_ubuGetAdditionalUnmountedDrivesR1=`
            echo "${drives_ubuGetAdditionalUnmountedDrivesR1}" | grep -E -v '^'"${mountedDrive}"'$'`
    done
    logDebugVar drives_ubuGetAdditionalUnmountedDrivesR1
    setVarOrLogInfo \
        "${returnVar_ubuGetAdditionalUnmountedDrivesR1}" \
        "${drives_ubuGetAdditionalUnmountedDrivesR1}" \
        "Additional unounted drives are:
${drives_ubuGetAdditionalUnmountedDrivesR1}"
}

ubuShredDisk () {
    local drive="$1"

    local knownAdditionalDrives=''
    ubuGetAdditionalUnmountedDrivesR1 knownAdditionalDrives

    if [ "${drive}" = '' ]; then
        assertInteractive
        logInfo "Mounted drives are not listed below."
        echo "Please select a drive:"
        select drive in ${knownAdditionalDrives}; do
            if [ "${drive}" != '' ]; then
                break
            fi
        done
    fi
    assertVar drive
    logInfo sudo shred --verbose --zero "'${drive}'"
}

ubuBadblocksWriteTest () {
    local drive="$1"

    local knownAdditionalDrives=''
    ubuGetAdditionalUnmountedDrivesR1 knownAdditionalDrives

    if [ "${drive}" = '' ]; then
        assertInteractive
        logInfo "Mounted drives are not listed below."
        echo "Please select a drive:"
        select drive in ${knownAdditionalDrives}; do
            if [ "${drive}" != '' ]; then
                break
            fi
        done
    fi
    assertVar drive
    # -s       Show the progress of the scan.
    # -w       Use write-mode test.
    # -b 4096  Specify the size of blocks in bytes.
    logInfo sudo badblocks -b 4096 -ws "'${drive}'"
}

ubuIostatUnmountedDrives () {
    local knownAdditionalDrives=''
    ubuGetAdditionalUnmountedDrivesR1 knownAdditionalDrives
    knownAdditionalDrives=`echo "${knownAdditionalDrives}" | sed 's/\/dev\///'`
    logInfo iostat 600 ${knownAdditionalDrives}
    iostat 600 ${knownAdditionalDrives}
}

ubuUpgradeLts22To24 () {
    local alreadyUpgradedTest=`grep -E '^VERSION_ID="24.04"' /etc/os-release`
    if [ "${alreadyUpgradedTest}" != '' ]; then
        logInfo "Current ubuntu version already is 24.04:
`grep -E '^VERSION' /etc/os-release`"
        logInfo "Nothing to do."
        logInfo "You may want to use ubuAptRun instead."
        return
    fi
    assertIamRootOrInteractive

    sudo apt install ubuntu-release-upgrader-core
    local ltsTest=`grep -E '^Prompt=lts$' /etc/update-manager/release-upgrades`
    assertVar ltsTest

    logInfo "Current ubuntu version:
`grep -E '^VERSION' /etc/os-release`"

    # do-release-upgrade starts its own screen session
    # so this can be done via ssh

    # NOT using -d - this is for dev release
    sudo do-release-upgrade -f DistUpgradeViewNonInteractive

    # Do a reboot to finish it off.
    logInfo "Rebooting."
    sudo reboot
}

ubuDisableDnsCache () {
    # In case we don't want the DNS cache server on port 53.  Helpful
    # if we are running our own DNS server thingy on the same
    # machine. E.g. pihole.

    assertFile /etc/systemd/resolved.conf
    assertFile /run/systemd/resolve/resolv.conf

    systemctl stop systemd-resolved

    if [ ! -d /etc/systemd/resolved.conf.d ]; then
        mkdir /etc/systemd/resolved.conf.d
    fi
    assertDir /etc/systemd/resolved.conf.d
    cat <<EOF > /etc/systemd/resolved.conf.d/dns_servers.conf
[Resolve]
DNSStubListener=no
EOF
    assertFile /etc/systemd/resolved.conf.d/dns_servers.conf

    rm /etc/resolv.conf
    ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf

    logInfo "/etc/resolv.conf
`ls -lah /etc/resolv.conf`
`cat /etc/resolv.conf`"

    systemctl start systemd-resolved
}

ubuInteractiveWarningIfNotLatestLTS () {
    local requiredReleaseNumber='24.04'
    if [ "${isInteractiveShell_global}" != 'yes' ]; then
        return
    fi
    if [ ! -f /etc/lsb-release ]; then
        return
    fi
    local ubuTest=`grep '^DISTRIB_ID=Ubuntu$' /etc/lsb-release`
    if [ "${ubuTest}" = '' ]; then
        logDebugInteractive "This is not ubuntu: ${ubuTest}"
        return
    fi
    local isLts=`grep '^DISTRIB_DESCRIPTION=".*LTS"$' /etc/lsb-release`
    if [ "${isLts}" = '' ]; then
        logWarningInteractive "This is not an LTS release: ${isLts##*=}"
        return
    fi
    local ubuRelease=`grep '^DISTRIB_RELEASE=' /etc/lsb-release`
    local ubuReleaseNumberOnly="${ubuRelease##*=}"
    if [ "${ubuReleaseNumberOnly}" = "${requiredReleaseNumber}" ]; then
        logInfoInteractive "This is the latest ubuntu LTS release: ${ubuReleaseNumberOnly}"
        return
    fi
    logInfoInteractive "This is NOT the latest ubuntu LTS release ${requiredReleaseNumber}, but: ${ubuReleaseNumberOnly}"
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"

ubuInteractiveWarningIfNotLatestLTS
