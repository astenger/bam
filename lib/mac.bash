#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam mac module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Helpers for mac computers.

EOF

}


macOsNamesByVersion="
10.6 Snow_Leopard
10.7 Lion
10.8 Mountain_Lion
10.9 Mavericks
10.10 Yosemite
10.11 El_Capitan
10.12 Sierra
10.13 High_Sierra
10.14 Mojave
10.15 Catalina
11 Big_Sur
12 Monterey
13 Ventura
14 Sonoma
"

getMacOsVersionR3 () {
    local returnVarOsVersion_getMacOsVersionR3="$1"
    local returnVarMajorOsVersion_getMacOsVersionR3="$2"
    local returnVarOsName_getMacOsVersionR3="$3"

    local osVersion_getMacOsVersionR3=`sw_vers | grep 'ProductVersion' | awk ' { print $2 } '`
    if [ "${osVersion_getMacOsVersionR3:0:2}" != '10' ]; then
        # Big Sur and following uses the first number only, i.e. Big Sur = 11, Monterey = 12
        osVersion_getMacOsVersionR3="${osVersion_getMacOsVersionR3:0:2}"
    fi

    # remove the last .x from the version to get the major version.
    local osVersionMajor_getMacOsVersionR3="${osVersion_getMacOsVersionR3%.*}"
    local osName_getMacOsVersionR3=`echo "${macOsNamesByVersion}" | egrep "^${osVersionMajor_getMacOsVersionR3} " | awk ' { print $2 }'`
    setDefault osName_getMacOsVersionR3 'Unknown'
    setVarOrLogInfo "${returnVarOsVersion_getMacOsVersionR3}" \
                    "${osVersion_getMacOsVersionR3}" \
                    "${osVersion_getMacOsVersionR3}"
    setVarOrLogInfo "${returnVarMajorOsVersion_getMacOsVersionR3}" \
                    "${osVersionMajor_getMacOsVersionR3}"
    setVarOrLogInfo "${returnVarOsName_getMacOsVersionR3}" \
                    "${osName_getMacOsVersionR3}"
}

getMacOsVersionR1 () {
    local returnVarOsVersion_getMacOsMajorVersionR1="$1"

    local dummy1=''
    local dummy2=''

    local osVersion_getMacOsMajorVersionR1
    getMacOsVersionR3 osVersion_getMacOsMajorVersionR1 dummy1 dummy2
    setVarOrLogInfo "${returnVarOsVersion_getMacOsMajorVersionR1}" \
                    "${osVersion_getMacOsMajorVersionR1}"
}

getMacOsMajorVersionR1 () {
    local returnVarMajorOsVersion_getMacOsMajorVersionR1="$1"

    local dummy1=''
    local dummy2=''

    local majorOsVersion_getMacOsMajorVersionR1=''
    getMacOsVersionR3 dummy1 majorOsVersion_getMacOsMajorVersionR1 dummy2
    setVarOrLogInfo "${returnVarMajorOsVersion_getMacOsMajorVersionR1}" \
                    "${majorOsVersion_getMacOsMajorVersionR1}"
}

getMacOsNameR1 () {
    local returnVarOsName_getMacOsNameR1="$1"

    local dummy1=''
    local dummy2=''

    local osName_getMacOsNameR1=''
    getMacOsVersionR3 dummy1 dummy2 osName_getMacOsNameR1

    setVarOrLogInfo "${returnVarOsName_getMacOsNameR1}" \
                    "${osName_getMacOsNameR1}"
}

macFlushDnsCache () {
    local osName=''

    getMacOsNameR1 osName

    # https://help.dreamhost.com/hc/en-us/articles/214981288-Flushing-your-DNS-cache-in-Mac-OS-X-and-Linux

    if [ "${osName}" = 'Sierra'      -o \
         "${osName}" = 'High_Sierra' -o \
         "${osName}" = 'Mojave'      -o \
         "${osName}" = 'Catalina'    -o \
         "${osName}" = 'Big_Sur'     -o \
         "${osName}" = 'Monterey'    -o \
         "${osName}" = 'Ventura'     -o \
         "${osName}" = 'Sonoma'         ]; then
        logInfo "Please provide your sudo password."
        sudo killall -HUP mDNSResponder
        sudo killall mDNSResponderHelper
        sudo dscacheutil -flushcache
    elif [ "${osName}" = 'El_Capitan' ]; then
        logInfo "Please provide your sudo password."
        sudo killall -HUP mDNSResponder
    else
        logErrorAndAbort "Don't know how to run on: ${osName}"
    fi
    logInfo "To clear your firefox DNS cache visit:"
    logInfo "about:networking#dns"
    logInfo "For Chrome use:"
    logInfo "chrome://net-internals/#dns"
}

macDnsCacheFlush () {
    macFlushDnsCache
}

macTimeMachineLog () {
    # https://superuser.com/questions/1126990/how-to-view-time-machine-log-in-macos-sierra

    # show the last 12 hours
    start="$(date -j -v-12H +'%Y-%m-%d %H:%M:%S')"
    logInfo "Logs from the last 12 hours:"
    log show --style syslog  --start "${start}" --predicate 'senderImagePath contains[cd] "TimeMachine"' --info

    logInfo "Live log stream:"
    log stream --style syslog  --predicate 'senderImagePath contains[cd] "TimeMachine"' --info
}

macAppStoreCheck () {
    local command="$1"

    # For some reason there were some files dating back 3 years using
    # about 50GB on my machine.

    # Inspiration taken from:
    # https://apple.stackexchange.com/questions/245406/how-to-fix-reset-app-store-app-on-mac-osx-el-capitan
    local foundSomething='no'
    if [ -d ~/Library/Caches/com.apple.appstore ]; then
        du -hs ~/Library/Caches/com.apple.appstore
        foundSomething='yes'
        if [ "${command}" = 'DELETE' ]; then
            logInfo "Executing: rm -rf ~/Library/Caches/com.apple.appstore"
            rm -rf ~/Library/Caches/com.apple.appstore
        fi
    fi
    local privateDir=''
    if [ "${command}" = 'DELETE' ]; then
        logInfo "This will probably ask you for your mac password."
    fi
    while read privateDir; do
        du -hs "${privateDir}"
        foundSomething='yes'
        if [ "${command}" = 'DELETE' ]; then
            logInfo "Executing: sudo rm -rf '${privateDir}'"
            sudo rm -rf "${privateDir}"
        fi
    done < <( find /private/var -name com.apple.appstore 2>/dev/null | grep -v com.apple.appstoreagent )
    if [ "${command}" = 'DELETE' ]; then
        logInfo "Executing: sudo softwareupdate --clear-catalog"
        sudo softwareupdate --clear-catalog
    else
        if [ "${foundSomething}" = 'yes' ]; then
            logInfo "To clean up these files use: macAppStoreCleanup"
        else
            logInfo "Everything looks fine."
        fi
    fi
}

macAppStoreCleanup () {
    macAppStoreCheck 'DELETE'
}

macVerboseBootCheck () {
    local command="$1"

    # https://osxdaily.com/2007/03/25/always-boot-mac-os-x-in-verbose-mode/
    local bootArgs=`nvram -p | grep boot-args`
    logDebug "${bootArgs}"
    local verboseBootTest=`echo "${bootArgs}" | grep -- '-v'`
    if [ "${verboseBootTest}" = '' ]; then
        logInfo "Verbose boot is disabled."
        if [ "${command}" = 'enable' ]; then
            logInfo "This will probably ask you for your mac password."
            logInfo "Executing: sudo nvram boot-args='-v'"
            sudo nvram boot-args='-v'
            macVerboseBootCheck
        fi
    else
        logInfo "Verbose boot is enabled."
        if [ "${command}" = 'disable' ]; then
            logInfo "This will probably ask you for your mac password."
            logInfo "Executing: sudo nvram boot-args=''"
            sudo nvram boot-args=''
            macVerboseBootCheck
        fi
    fi
}

macVerboseBootDisable () {
    macVerboseBootCheck 'disable'
}

macVerboseBootEnable () {
    macVerboseBootCheck 'enable'
}

macNetworkService () {
    # https://apple.stackexchange.com/questions/191879/how-to-find-the-currently-connected-network-service-from-the-command-line
    local services=$(networksetup -listnetworkserviceorder | grep 'Hardware Port')

    local line=''
    while read line; do
        local sname=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $2}')
        local sdev=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $4}')
        #echo "Current service: $sname, $sdev, $currentservice"
        if [ -n "$sdev" ]; then
            local ifout="$(ifconfig $sdev 2>/dev/null)"
            echo "$ifout" | grep 'status: active' > /dev/null 2>&1
            local rc="$?"
            if [ "$rc" -eq 0 ]; then
                local currentservice="$sname"
                local currentdevice="$sdev"
                local currentmac=$(echo "$ifout" | awk '/ether/{print $2}')

                # may have multiple active devices, so echo it here
                echo "$currentservice, $currentdevice, $currentmac"
            fi
        fi
    done <<< "$(echo "$services")"

    if [ -z "$currentservice" ]; then
        logErrorAndAbort "Could not find current service"
    fi
}

macGetFirstActiveInterfaceR1 () {
    local returnVarActiveInterfaceName="$1"

    local activeInterfaces=`macNetworkService | awk -F', ' ' { print $2 } '`
    assertVariable activeInterfaces "No active interfaces found."
    local activeInterfaceCount=`echo "${activeInterfaces}" | wc -l`
    assertVariable activeInterfaceCount
    local activeInterface_macGetFirstActiveInterfaceR1=`echo "${activeInterfaces}" | head -n 1`
    if [ ${activeInterfaceCount} -gt 1 ]; then
        logWarning "Multiple active interfaces found."
        logWarning "Using: ${activeInterface_macGetFirstActiveInterfaceR1}"
    fi
    setVarOrLogInfo "${returnVarActiveInterfaceName}" "${activeInterface_macGetFirstActiveInterfaceR1}"
}

macSleep () {
    # https://osxdaily.com/2012/07/22/sleep-a-mac-from-the-command-line/
    # https://osxdaily.com/2012/07/11/mac-wont-sleep-heres-how-to-find-out-why-and-fix-it/
    logInfo "You can also use: Option–Command–Power"
    logInfo "`pmset -g assertions | grep -i sleep`"
    # pmset sleepnow
    # osascript -e 'tell application "Finder" to sleep'
    # Tt appears talking to "System Events" is the most aggressive version:
    osascript -e 'tell application "System Events" to sleep'

    # Hibernate pbobably not needed, but can be forced if required.
    # https://superuser.com/questions/139354/how-can-i-manually-put-a-macbook-pro-to-hibernate-without-going-to-sleep-mode-fi
}

macReinstallCommandLineTools () {
    assertIamRootOrInteractive

    logWarning "This does not seem to work."
    logWarning "softwareupdate stalled looking for software."
    logInfo "Go to: https://developer.apple.com/download/more/?=command%20line%20tools"
    logInfo "And do a manual install."
    logErrorAndAbort "Safety abort."
    # https://trac.macports.org/wiki/ProblemHotlist#reinstall-clt
    sudo touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
    softwareupdate --verbose --list
    # https://birchtree.me/blog/install-mac-app-store-updates-with-one-line-in-the-terminal/
    sudo softwareupdate --install --all
    sudo rm /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
}

macSpotlightReindex () {
    # https://apple.stackexchange.com/questions/62715/applications-dont-show-up-in-spotlight
    sudo mdutil -a -i off
    sync
    sudo mdutil -a -i on
    sudo mdutil /System/Volumes/Data
}

macMainRemoteBootstrap () {
    if [ "${hostKernelName_global}" != 'Darwin' ]; then
        logInfoInteractive "This machine is not a Mac. Stuff might not work as you expect."
    fi
}

macTimeMachineRemoveAclAfterCopy () {
    # see https://superuser.com/questions/361925/permissions-restoring-from-time-machine-finder-copy-vs-cp-copy#answer-362014
    local dirToProcess="$1"
    setDefault dirToProcess './'

    local dirFqn=''
    qualifyPathR1 dirFqn "${dirToProcess}"
    logInfo "Stripping time machine ACL restrictions from: ${dirFqn}"
    chmod -R -a "group:everyone deny add_file,delete,add_subdirectory,delete_child,writeattr,writeextattr,chown" "${dirFqn}" 2>&1 \
        | grep -E -v '^chmod: No ACL present'

    # Alternatively use tmutil restore [-v] src ... dst
}

tmutilSetBase () {
    local base="$1"

    tmutilBase_global=''
    setDefault base "`pwd`"
    assertDir "${base}"
    assertDir "${base}/System"
    tmutilBase_global="${base}"
}

tmutilGetBaseR1 () {
    local returnVar_tmutilGetBaseR1="$1"
    assertVar tmutilBase_global
    assertDir "${tmutilBase_global}"
    setVarOrLogInfo "${returnVar_tmutilGetBaseR1}" "${tmutilBase_global}"
}

tmutilBasePushd () {
    local base=''
    tmutilGetBaseR1 base
    spushd "${base}"
}

tmutilRestore () {
    local filesToRestore_tmutilRestore=( "$@" )

    local base=''
    tmutilGetBaseR1 base
    local fileToRestore_tmutilRestore=''
    local fileNoBase=''

    for fileToRestore_tmutilRestore in "${filesToRestore_tmutilRestore[@]}"; do
        qualifyPathR1 fileToRestore_tmutilRestore
        fileNoBase="/${fileToRestore_tmutilRestore#${base}}"
        if [ "${fileToRestore_tmutilRestore}" = "${fileNoBase}" ]; then
            logWarning "File is not located at set base: ${fileToRestore_tmutilRestore}"
            logWarning "Skiping ..."
            continue
        fi
        if [ ! -e "${fileToRestore_tmutilRestore}" ]; then
            logWarining "Source does not exist: ${fileToRestore_tmutilRestore}"
            continue
        fi
        if [ -e "${fileNoBase}" ]; then
            logWarning "Target already exists: ${fileNoBase}"
            logWarning "Please remove before restore."
            continue
        fi
        logInfo "tmutil restore \"${fileToRestore_tmutilRestore}\" \"${fileNoBase}\""
        tmutil restore "${fileToRestore_tmutilRestore}" "${fileNoBase}"
    done
}

macShowDeviceEnrolment () {
    logInfo 'sudo profiles show -type enrollment'
    sudo profiles show -type enrollment
}

macDesktopHideIcons () {
    defaults write com.apple.finder CreateDesktop false
    killall Finder
}
macDesktopShowIcons () {
    defaults write com.apple.finder CreateDesktop true
    killall Finder
}

macTimeMachineEnable () {
    # https://www.macworld.com/article/220774/control-time-machine-from-the-command-line.html
    sudo tmutil enable
}
macTimeMachineDisable () {
    sudo tmutil disable
}
macDoNotDisturbOn () {
    fixme "This might work? https://github.com/sindresorhus/do-not-disturb"
}

macDoNotDisturbOff () {
    fixme "This might work? https://github.com/sindresorhus/do-not-disturb"
}

getMacApplicationAndUrlNameR2 () {
    # https://superuser.com/questions/1441270/apps-dont-show-up-in-camera-and-microphone-privacy-settings-in-macbook
    local returnVarAppName_getMacApplicationAndUrlNameR2="$1"
    local returnVarAppUrlName_getMacApplicationAndUrlNameR2="$2"
    local egrepPattern="$3"

    if [ "${egrepPattern}" = '' ]; then
        egrepPattern='.*'
    fi
    #local PS3_OLD="${PS3}"
    #PS3="Enter a number: "

    local infoFileList=`
        ls /Applications/*.app/Contents/Info.plist \
          ~/Applications/*.app/Contents/Info.plist 2>/dev/null \
        | grep -E -i "${egrepPattern}[^\/]*.app/" \
        | sort --ignore-case`
    local infoFile=''
    while read infoFile; do
	logDebugVar infoFile
        local urlName_getMacApplicationAndUrlNameR2=''
        local appNameShort_getMacApplicationAndUrlNameR2=`echo "${infoFile}" | sed 's/.*\/Applications\///' | sed 's/\.app\/.*/.app/'`
        local bundleIdentifier=`
            plutil -convert xml1 -o - "${infoFile}" \
            | grep --after-context=2 '<key>CFBundleIdentifier</key>' \
            | grep '<string>' \
            | sed 's/.*<string>//' \
            | sed 's/<\/string>//' `
        if [ "${bundleIdentifier}" != '' ]; then
            urlName_getMacApplicationAndUrlNameR2="${bundleIdentifier}"
        else
            local bundleUrlName=`
                plutil -convert xml1 -o - "${infoFile}" \
                | grep --after-context=2 '<key>CFBundleURLName</key>' \
                | grep '<string>' \
                | sed 's/.*<string>//' \
                | sed 's/<\/string>//' `
            if [ "${bundleIdentifierbundleUrlName}" != '' ]; then
                urlName_getMacApplicationAndUrlNameR2="${bundleUrlName}"
            fi
        fi
        if [ "${returnVarAppUrlName_getMacApplicationAndUrlNameR2}" = '' ]; then
            local infoLine=`printf '%-30s %s\n' "${appNameShort_getMacApplicationAndUrlNameR2}" "${urlName_getMacApplicationAndUrlNameR2}"`
            logInfo "${infoLine}"
        else
            if [ "${returnVarAppName_getMacApplicationAndUrlNameR2}" != '' ]; then
                setVar "${returnVarAppName_getMacApplicationAndUrlNameR2}" "${appNameShort_getMacApplicationAndUrlNameR2}"
            fi
            setVar "${returnVarAppUrlName_getMacApplicationAndUrlNameR2}" "${urlName_getMacApplicationAndUrlNameR2}"
            break
        fi
    done < <( echo "${infoFileList}" | pipeStripStandard )
}

macAllowAppAccess () {
    # https://superuser.com/questions/1441270/apps-dont-show-up-in-camera-and-microphone-privacy-settings-in-macbook
    local service=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-m' -o "$1" = '--microphone' ]; then
            service='kTCCServiceMicrophone'
            shift
        elif [ "$1" = '-c' -o "$1" = '--camera' ]; then
            service='kTCCServiceCamera'
            shift
        elif [ "$1" = '-s' -o "$1" = '--screencapture' ]; then
            service='kTCCServiceScreenCapture'
            shift
        elif [ "$1" = '-a' -o "$1" = '--accessibility' ]; then
            service='kTCCServiceAccessibility'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local appNameUserSpecified="$1"

    assertVar appNameUserSpecified

    local appName=''
    local bundleUrlIdentifier=''
    getMacApplicationAndUrlNameR2 appName bundleUrlIdentifier "${appNameUserSpecified}"
    logInfo "Using: ${bundleUrlIdentifier} from ${appName}"

    local userTccDbFile=~/Library/Application\ Support/com.apple.TCC/TCC.db
    local systemTccDbFile=/Library/Application\ Support/com.apple.TCC/TCC.db

    # These seem to be kept in the user database:
    # kTCCServiceMicrophone
    # kTCCServiceCamera

    # These seem to be kept in the system database
    # kTCCServiceScreenCapture
    # kTCCServiceAccessibility

    local sql=''
    local secondsSinceEpoch=`date +%s`

    #service='kTCCServiceMicrophone'
    #bundleUrlIdentifier='us.zoom.xos'

    assertVar service
    assertVar bundleUrlIdentifier
    assertInt "${secondsSinceEpoch}"
    sqlInsert="insert into access values ('${service}','${bundleUrlIdentifier}', 0, 2, 2, 1, null, null, null, 'UNUSED', null, null, ${secondsSinceEpoch});"
    sqlSelect="select service, client from access where service = '${service}' and client='${bundleUrlIdentifier}';"
    # sqlite> .schema access
    # CREATE TABLE access (
    #     service        TEXT        NOT NULL,
    #     client         TEXT        NOT NULL,
    #     client_type    INTEGER     NOT NULL,
    #     auth_value     INTEGER     NOT NULL,
    #     auth_reason    INTEGER     NOT NULL,
    #     auth_version   INTEGER     NOT NULL,
    #     csreq          BLOB,
    #     policy_id      INTEGER,
    #     indirect_object_identifier_type    INTEGER,
    #     indirect_object_identifier         TEXT NOT NULL DEFAULT 'UNUSED',
    #     indirect_object_code_identity      BLOB,
    #     flags          INTEGER,
    #     last_modified  INTEGER     NOT NULL DEFAULT (CAST(strftime('%s','now') AS INTEGER)),
    #     PRIMARY KEY (service, client, client_type, indirect_object_identifier),
    #     FOREIGN KEY (policy_id) REFERENCES policies(id) ON DELETE CASCADE ON UPDATE CASCADE);
    # select service, client from access;
    # select * from access where service = 'kTCCServiceMicrophone' and client='us.zoom.xos';

    assertInteractive
    local dbFile=''
    local existingEntry=''
    local haveEntry='no'
    local sudo=''
    for dbFile in "${userTccDbFile}" "${systemTccDbFile}" ; do
        # assertFile "${dbFile}" # fails on the system one because the user does not have access to the dir
        if [ "${dbFile:0:7}" = '/Users/' ]; then
            sudo=''
        else
            sudo='sudo'
            logInfo "This might ask for your sudo password."
        fi
        logDebug "echo \"${sqlSelect}\" | ${sudo} sqlite3 \"${dbFile}\" 2>&1"
        existingEntry=`
            echo "${sqlSelect}" | ${sudo} sqlite3 "${dbFile}" 2>&1`
        if [ "${existingEntry}" = 'Parse error near line 1: no such table: access' ]; then
            logError "File does not exist? ${dbFile}"
        elif [ "${existingEntry}" != '' ]; then
            logInfo "Found match in: ${dbFile}
${existingEntry}"
            haveEntry='yes'
            break
        else
            logInfo "No match in: ${dbFile}"
        fi
    done
    local trace=''
    if [ "${haveEntry}" = 'no' ]; then
        if [ "${service}" = 'kTCCServiceMicrophone' -o "${service}" = 'kTCCServiceCamera' ]; then
            dbFile="${userTccDbFile}"
        else
            dbFile="${systemTccDbFile}"
        fi
        if [ "${dbFile:0:7}" = '/Users/' ]; then
            sudo=''
        else
            sudo='sudo'
            logInfo "This might ask for your sudo password."
        fi
	logInfo "Adding entry to: ${dbFile}"
	logDebug "echo \"${sqlInsert}\" | ${sudo} sqlite3 \"${dbFile}\" 2>&1"
        trace=`echo "${sqlInsert}" | ${sudo} sqlite3 "${dbFile}" 2>&1`
        logInfo "${trace}"
    fi
}

macDsStoreFilesFind () {
    local delete='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--delete' ]; then
            delete='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done
    local dirsToSearch="$@"

    assertExecutableInPath gfind
    assertExecutableInPath gxargs

    if [ "${dirsToSearch}" = '' ]; then
        dirsToSearch=( "`pwd`" )
    fi
    local dirToSearch=''
    for dirToSearch in ${dirsToSearch[@]}; do
        if [ ! -d "${dirToSearch}" ]; then
            logWarning "Dir dows not exist: $dirToSearch}"
            continue
        fi
        spushd "${dirToSearch}"
        local dsFiles=`gfind . -name '.DS_Store' -print0 | gxargs -0 --no-run-if-empty ls`
        if [ "${dsFiles}" = '' ]; then
            logInfo "No .DS_Store files found at: ${dirToSearch}"
        elif [ "${delete}" != 'yes' ]; then
            logInfo "Found the following .DS_Store files at: ${dirToSearch}
${dsFiles}"
        else
            logInfo "Deleting the following files at: ${dirToSearch}
${dsFiles}"
            gfind . -name '.DS_Store' -print0 | gxargs -0 --no-run-if-empty rm
        fi
        spopd
    done
}

macDsStoreFilesDelete () {
    macDsStoreFilesFind --delete "$@"
}

macUpdate () {
    local restart='yes'
    if [ "$1" = '--restart' ]; then
        restart='yes'
        shift
    elif [ "$1" = '--norestart' ]; then
        restart='no'
        shift
    elif [ "$1" = '--help' -o "$1" = '-h' ]; then
        cat <<EOF
macUpdate [ --norestart ] [ --restart ]
EOF
        return
    fi
    # https://www.macrumors.com/how-to/update-macos-terminal-command/
    logInfo "Current version: `sw_vers --productName` `sw_vers --productVersion` `sw_vers --buildVersion`"
    logFlash "Fetching list of available updates."
    if [ "${macUpdateList_global}" = '' ]; then
        macUpdateList_global=`softwareupdate -l | grep -v 'Software Update Tool' | grep -v 'Finding available software' | grep -v 'Software Update found the following new or updated software:'`
    fi
    macUpdateList_global=`echo "${macUpdateList_global}" | grep -v 'Software Update Tool' | grep -v 'Finding available software' | grep -v 'Software Update found the following new or updated software:'`
    if [ "${macUpdateList_global}" = '' ]; then
        logInfo "No updates found."
        return
    fi
    logInfo "Found the following updates:
${macUpdateList_global}"
    local selectionOptions=`echo "${macUpdateList_global}" | grep 'Label:' | sed 's/.*Label: //'`
    local selectionArray=()
    linesToArrayR1 selectionArray "${selectionOptions}"
    local userSelection
    if [ "${#selectionArray[@]}" -eq 0 ]; then
        logInfo: "No updates available"
        return
    elif [ "${#selectionArray[@]}" -eq 1 ]; then
        userSelection="${selectionArray[0]}"
    else
        select userSelection in "${selectionArray[@]}"; do
            if [ "${userSelection}" != '' ]; then
                break
            fi
        done
    fi
    if [ "${restart}" = 'yes' ]; then
        logWarning "This will reboot the machine if a reboot is required."
        logInfo "I will be running:
sudo softwareupdate --no-scan --install --restart '${userSelection}'"
    else
        logWarning "This will NOT reboot the machine if a reboot is required."
        logInfo "I will be running:
sudo softwareupdate --no-scan --install '${userSelection}'"
    fi
    local reallyDoUpdate
    read -p "Really install update '${userSelection}'? [Y/n] " reallyDoUpdate
    setDefault reallyDoUpdate 'yes'
    castYesNoR1 reallyDoUpdate
    if [ "${reallyDoUpdate}" = 'yes' ]; then
        logInfo "Network download is done at 60%"
        logInfo "Unpacking after does not show progress."
        logInfo "Check CPU usage for softwareupdated."
        if [ "${restart}" = 'yes' ]; then
            logInfo "Running:
sudo softwareupdate --no-scan --install --restart '${userSelection}'"
            logInfo "This might ask for your sudo password."
            sudo softwareupdate --no-scan --install --restart "${userSelection}"
        else
            logInfo "Running:
sudo softwareupdate --no-scan --install '${userSelection}'"
            logInfo "This might ask for your sudo password."
            sudo softwareupdate --no-scan --install "${userSelection}"
        fi
    fi
}

macCoreaudiodRestart () {
    local pidLong=`ps -ef | grep /usr/sbin/coreaudiod | grep -v grep`
    local pid=`echo ${pidLong} | awk ' { print $2 } '`
    assertVar pid
    logInfo "Sending TERM to:
${pidLong}"
    logInfo "This might ask for your sudo password."
    sudo kill -TERM ${pid}
    sleep 1
    pidLong=`ps -ef | grep /usr/sbin/coreaudiod | grep -v grep`
    local newPid=`echo ${pidLong} | awk ' { print $2 } '`
    if [ "${pid}" != "${newPid}" ]; then
       logInfo "New process is:
${pidLong}"
    else
        logWarning "Something went wrong:
${pidLong}"
    fi
}

copyTaggedFiles () {
    local matchBaseFileName='no'
    local baseFileNameSeparator='_'
    local noCopy='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--matchBaseFileName' ]; then
            matchBaseFileName='yes'
            shift
        elif [ "$1" = '--baseFileNameSeparator' ]; then
            shift
            baseFileNameSeparator="$1"
            shift
        elif [ "$1" = '--noCopy' -o "$1" = '--dryRun' -o "$1" = '-n' ]; then
            noCopy='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done
    local tagColor="$1"
    local sourceDir="$2"
    local targetDir="$3"

    assertVar tagColor
    assertDir "${sourceDir}"
    assertDir "${targetDir}"
    local testColorName=`echo "${tagColor}" | grep -E '^(Blue|Green|Grey|Orange|Purple|Red|Yellow)$'`
    assertVar testColorName "Color needs to be one of: Blue,Green,Grey,Orange,Purple,Red,Yellow"

    assertExecutableInPath tag "You can install 'tag' using: brew install tag"
    # https://github.com/jdberry/tag/
    local tagSanityCheck=`tag --help | grep 'tag - A tool for manipulating and querying file tags.'`
    assertVar "tagSanityCheck" "The command 'tag' does not seem to be the tag I expected:
tag --help
`tag --help`"
    local file=''
    logInfo "Copying files tagged ${tagColor}"
    local fileCount=0
    local filesCopied=0
    for file in `tag --find "${tagColor}" "${sourceDir}"`; do
        if [ ! -f "${file}" ]; then
            logFlash "Not a file: ${file}"
        fi
        let fileCount+=1
        local fileShort="${file##*/}"
        targetFile="${targetDir}/${fileShort}"
        if [ -f "${targetFile}" ]; then
            logFlash "Target already exists: ${targetFile}"
            continue
        fi
        if [ "${matchBaseFileName}" = 'yes' ]; then
            local baseFileSource="${fileShort%%.*}"
            baseFileSource="${baseFileSource%%${baseFileNameSeparator}*}"
            #logInfo "Checking base: ${baseFileSource}"
            local targetBaseExists=`ls "${targetDir}/${baseFileSource}"* 2>/dev/null`
            if [ "${targetBaseExists}" != '' ]; then
                logDebug "Skipping copy because of base file name match:
file:   ${file}
base:   ${baseFileSource}
target: ${targetBaseExists}"
                continue
            fi
        fi
        if [ "${noCopy}" != 'no' ]; then
            logInfo "Would copy: ${file}
to:         ${targetFile}"
            continue
        else
            logInfo "Copying: ${file}
to:      ${targetFile}"
            cp "${file}" "${targetFile}"
            assertFile "${targetFile}"
            local targetFileColor=`tag --list "${targetFile}"`
            local checkTag=`echo "${targetFileColor}" | grep "${tagColor}"`
            assertVar checkTag "Color tag did not copy across"
            let filesCopied+=1
        fi
    done
    logInfo "Files copied: ${filesCopied}/${fileCount}"
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"

macMainRemoteBootstrap
