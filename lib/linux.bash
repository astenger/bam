#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam linux module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Helpers for linux machines.

EOF

}

doesGroupExistR1 () {
    local returnVar="$1"
    local groupName_doesGroupExistR1="$2"

    assertVar groupName_doesGroupExistR1

    assertExecutableInPath getent
    local getentGroupEntry=`getent group | egrep "^${groupName_doesGroupExistR1}:"`

    if [ "${getentGroupEntry}" = '' ]; then
        setVarOrLogInfo "${returnVar}" 'no' "Group does not exist: ${groupName_doesGroupExistR1}"
    else
        setVarOrLogInfo "${returnVar}" 'yes' "Group does exist: ${groupName_doesGroupExistR1}"
    fi
}

assertGroupExists () {
    local groupName="$1"
    local errorMessage="$2"

    assertVar groupName

    local doesGroupExist=''
    doesGroupExistR1 doesGroupExist "${groupName}"
    if [ "${doesGroupExist}" != 'yes' ]; then
        setDefault errorMessage "Group does not exist: ${groupName}"
        logErrorAndAbort "Group does not exist: ${groupName}"
    fi
}

ensureGroup () {
    local systemOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' -o "$1" = '--system' ]; then
            systemOption='--system'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local groupName="$1"
    assertVar groupName

    local groupExists=''
    doesGroupExistR1 groupExists "${groupName}"

    local groupaddTest=`which groupadd 2>/dev/null`
    assertVar groupaddTest "groupadd is not in PATH"

    if [ "${groupExists}" != yes ]; then
        assertIamRoot
        logInfo "Creating group: ${userName}"
        groupadd ${systemOption} "${groupName}"
    fi
    assertGroupExists "${groupName}" "Unable to create group: ${groupName}"
}

ensureGroup () {
    local systemOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' -o "$1" = '--system' ]; then
            systemOption='--system'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local groupName="$1"
    assertVar groupName

    local groupExists=''
    doesGroupExistR1 groupExists "${groupName}"

    assertExecutableInPath groupadd

    if [ "${groupExists}" != yes ]; then
        assertIamRoot
        logInfo "Creating group: ${userName}"
        groupadd ${systemOption} "${groupName}"
    fi
    assertGroupExists "${groupName}" "Unable to create group: ${groupName}"
}

ensureSystemGroup () {
    ensureGroup -s "$@"
}

doesUserExistR1 () {
    local returnVar="$1"
    local userName_doesUserExistR1="$2"

    assertVar userName_doesUserExistR1

    assertExecutableInPath getent
    local getentGroupEntry=`getent passwd | egrep "^${userName_doesUserExistR1}:"`

    if [ "${getentGroupEntry}" = '' ]; then
        setVarOrLogInfo "${returnVar}" 'no' "User does not exist: ${userName_doesUserExistR1}"
    else
        setVarOrLogInfo "${returnVar}" 'yes' "User does exist: ${userName_doesUserExistR1}"
    fi
}

doesUserExistR1 () {
    local returnVar="$1"
    local userName_doesUserExistR1="$2"

    assertVar userName_doesUserExistR1

    local getentGroupEntry=`getent passwd | egrep "^${userName_doesUserExistR1}:"`

    if [ "${getentGroupEntry}" = '' ]; then
        setVarOrLogInfo "${returnVar}" 'no' "User does not exist: ${userName_doesUserExistR1}"
    else
        setVarOrLogInfo "${returnVar}" 'yes' "User does exist: ${userName_doesUserExistR1}"
    fi
}

assertUserExists () {
    local userName="$1"

    assertVar userName

    local doesUserExist=''
    doesUserExistR1 doesUserExist "${userName}"
    if [ "${doesUserExist}" != 'yes' ]; then
        logErrorAndAbort "User does not exist: ${userName}"
    fi
}

ensureUserAndGroup () {
    local systemOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' -o "$1" = '--system' ]; then
            systemOption='--system'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local userName="$1"
    assertVar userName

    local userExists=''
    doesUserExistR1 userExists "${userName}"

    assertExecutableInPath useradd
    if [ "${userExists}" != yes ]; then
        assertIamRoot
        if [ "${systemOption}" = '--system' ]; then
            ensureSystemGroup "${userName}"
        else
            ensureGroup "${userName}"
        fi
        assertIamRoot
        logInfo "Creating user: ${userName}"
        useradd ${systemOption} \
            --create-home \
            --no-user-group \
            --gid "${userName}" \
            --groups "${userName}" \
            --shell /bin/bash "${userName}"
    fi
    assertUserExists "${userName}" "Unable to create user: ${userName}"
}

ensureSystemUserAndGroup () {
    ensureUserAndGroup -s "$@"
}

getLinuxReleaseName () {
    local returnVarReleaseName="$1"

    assertFile /etc/os-release
    local nameLine=`cat /etc/os-release | egrep '^NAME='`
    assertVar nameLine "NAME= not found in: /etc/os-release"
    local releaseName_getLinuxReleaseName="${nameLine#*=}"
    releaseName_getLinuxReleaseName="${releaseName_getLinuxReleaseName#\"}"
    releaseName_getLinuxReleaseName="${releaseName_getLinuxReleaseName%\"}"
    assertVar releaseName_getLinuxReleaseName "Unable to determine release name"

    setVarOrLogInfo "${returnVarReleaseName}" "${releaseName_getLinuxReleaseName}"
}

assertLinux () {
    if [ "${hostKernelName_global}" != 'Linux' ]; then
        logErrorAndAbort "This is not Linux but: ${hostKernelName_global}"
    fi
}

assertLinuxRelease () {
    local releaseNameRequired="$1"
    local msg="$2"

    local releaseNameRequiredLower=''
    downcaseR1 releaseNameRequiredLower "${releaseNameRequired}"
    local releaseName=''
    getLinuxReleaseName releaseName
    local releaseNameLower=''
    downcaseR1 releaseNameLower "${releaseName}"

    if [ "${releaseNameRequiredLower}" != "${releaseNameLower}" ]; then
        setDefault msg "Linux release name is not ${releaseNameRequired} but: ${releaseName}"
        logErrorAndAbort "${msg}"
    fi
}

linuxSetLocalTimezone () {
    local dryRun='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '--dryRun' ]; then
            dryRun='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    # https://www.howtogeek.com/405088/how-to-get-your-systems-geographic-location-from-a-bash-script/

    assertExecutableInPath timedatectl
    local publicIp=`curl -s https://ipinfo.io/ip`
    local timezone=`curl -s https://ipinfo.io/${publicIp} | grep '"timezone":'`
    timezone=`echo "${timezone}" | sed 's/.*: "//' | sed 's/".*//'`
    assertVariable timezone
    local currentTimezone=`timedatectl | grep 'Time zone:' | sed 's/.*: //' | sed 's/ .*//'`
    assertVariable currentTimezone
    if [ "${currentTimezone}" = "${timezone}" ]; then
        logInfo "Time zone is already set to: ${currentTimezone}"
    else
        logInfo "Changing time zone from ${currentTimezone} to ${timezone}"
        logInfo "This might ask for your sudo password."
        logInfo sudo timedatectl set-timezone \"${timezone}\"
        if [ "${dryRun}" = 'no' ]; then
            sudo timedatectl set-timezone "${timezone}"
        else
            logInfo "Skipping execution in dryRun mode."
        fi
    fi
}

# MAIN

linuxMainRemoteBootstrap () {
    if [ "${hostKernelName_global}" != 'Linux' ]; then
        logInfoInteractive "This machine is not Linux. Stuff might not work as you expect."
    fi
}

eval "${showLibraryUsageIfNotSourced}"

linuxMainRemoteBootstrap
