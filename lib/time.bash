#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam time module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Time and date related helper functions.

EOF

}

timeEpochToTouchTR1 () {
    local returnVar_timeEpochToTouchTR1="$1"
    # YYYYmmddHHMM.SS can be used by touch -t option to e.g. set the modified time of a file.
    local epochTs="$2"

    setDefault epochTs "`date +%s`"

    assertVar hostKernelName_global
    local touchT_timeEpochToTouchTR1=''
    if [ "${hostKernelName_global}" = 'Darwin' ]; then
        touchT_timeEpochToTouchTR1=`date -r "${epochTs}" +%Y%m%d%H%M.%S`
    elif [ "${hostKernelName_global}" = 'Linux' ]; then
        touchT_timeEpochToTouchTR1=`date -d "@${epochTs}" +%Y%m%d%H%M.%S`
    else
        logErrorAndAbort "timeEpochToTouchTR1(): don't know how to run on: ${hostKernelName_global}"
    fi
    setVarOrLogInfo "${returnVar_timeEpochToTouchTR1}" "${touchT_timeEpochToTouchTR1}"
}

timeEpochToTouchT () {
    timeEpochToTouchTR1 '' "$@"
}

timeGetEpochTimestampNowR1 () {
    local returnVar_timeGetEpochTimestampNowR1="$1"
    local epochTs_timeGetEpochTimestampNowR1=`date +%s`
    assertInt "${epochTs_timeGetEpochTimestampNowR1}"
    setVarOrLogInfo "${returnVar_timeGetEpochTimestampNowR1}" "${epochTs_timeGetEpochTimestampNowR1}"
}

timeUpdateFileModifiedTimeUsingEpoch () {
    local fileName="$1"
    local epochTs="$2"

    assertVar fileName
    assertFile "${fileName}"
    assertVar epochTs

    local touchT=''
    timeEpochToTouchTR1 touchT "${epochTs}"
    touch -t "${touchT}" "${fileName}"
}

timeDurationHumanToSecondsR1 () {
    # Convert human readable duration to seconds, e.g.:
    # "3d 7h 5m 10s"
    local returnVar_timeDurationHumanToSecondsR1="$1"
    local durationH="$2"

    setDefaultFromVar durationH "${returnVar_timeDurationHumanToSecondsR1}"

    assertVar durationH

    # https://stackoverflow.com/questions/50433578/how-to-convert-time-period-string-in-sleep-format-to-seconds-in-bash
    local durationInS_timeDurationHumanToSecondsR1=`sed 's/d/*24*3600 +/g; s/h/*3600 +/g; s/m/*60 +/g; s/s/\+/g; s/+[ ]*$//g' <<< "${durationH}" | bc`
    assertInt "${durationInS_timeDurationHumanToSecondsR1}"
    setVarOrLogInfo "${returnVar_timeDurationHumanToSecondsR1}" "${durationInS_timeDurationHumanToSecondsR1}"
}

timeIsoDateToEpochR1 () {
    local doNotUseGdate='no'
    if [ "$1" == '--nogdate' ]; then
        doNotUseGdate='yes'
        shift
    fi

    local returnVar_timeIsoDateToEpochR1="$1"
    local isoDate="$2"

    setDefault isoDate "`date +'%Y-%m-%d'`"

    local epoch_timeIsoDateToEpochR1=''
    local dateExecIsGnuTest=`date --help 2>/dev/null | grep gnu`
    if [ "${dateExecIsGnuTest}" != '' ]; then
        epoch_timeIsoDateToEpochR1=`date --date="${isoDate}" +%s`
    else
        local isGdateAvailableTest=`gdate --help 2>/dev/null | grep gnu`
        if [ "${isGdateAvailableTest}" != '' -a "${doNotUseGdate}" != 'yes' ]; then
            epoch_timeIsoDateToEpochR1=`gdate --date="${isoDate}" +%s`
        else
            local dateExecIsMacOsVariantTest=`date --help 2>&1 | grep -- '-j' | grep -- '-f input_fmt'`
            if [ "${dateExecIsMacOsVariantTest}" != '' ]; then
                local format=''
                local formatTest=`echo "${isoDate}" | grep -E '^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$'`
                if [ "${formatTest}" != '' ]; then
                    format='%Y-%m-%d'
                else
                    formatTest=`echo "${isoDate}" | grep -E '^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]$'`
                    if [ "${formatTest}" != '' ]; then
                        format='%Y-%m-%d %H:%M:%S'
                    else
                        logErrorAndAbort "timeIsoDateToEpochR1(): Unsupported date format on MacOS without gdate installed."
                    fi
                fi
                epoch_timeIsoDateToEpochR1=`date -jf "${format}" "${isoDate}" +%s`
            else
                logErrorAndAbort "I don't know the date variant available on this system."
            fi
        fi
    fi
    assertVar epoch_timeIsoDateToEpochR1
    setVarOrLogInfo "${returnVar_timeIsoDateToEpochR1}" "${epoch_timeIsoDateToEpochR1}"
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"
