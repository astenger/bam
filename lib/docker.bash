#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam docker module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

use linux

libraryUsage () {
    cat <<EOF

Docker related helpers. For Ubuntu machines.

EOF

}


dockerSetupAptRepository () {
    local ubuntuVersionId=`egrep '^VERSION_ID=' /etc/os-release`
    ubuntuVersionId="${ubuntuVersionId##*=}"
    if [ "${ubuntuVersionId}" != '"18.04"' -a \
         "${ubuntuVersionId}" != '"20.04"' -a \
         "${ubuntuVersionId}" != '"21.04"' -a \
         "${ubuntuVersionId}" != '"21.10"' -a \
         "${ubuntuVersionId}" != '"22.04"' ] ;then
        logErrorAndAbort "Unsupported ubuntu version: ${ubuntuVersionId}"
    fi
    local machineHardwareName=`uname -m`
    if [ "${machineHardwareName}" != 'x86_64' -a \
         "${machineHardwareName}" != 'amd64' ]; then
        logErrorAndAbort "Unsupported machine hardware name: ${machineHardwareName}"
    fi
    assertIamRoot

    # Skipping uninstall
    ###logInfo "Uninstalling old docker versions"
    ###apt-get remove docker docker-engine docker.io containerd runc

    logInfo "Setting up repository"
    apt update
    apt install -y \
         apt-transport-https \
         ca-certificates \
         curl \
         software-properties-common \
         gnupg \
         lsb-release
    if [ -f /usr/share/keyrings/docker-archive-keyring.gpg ]; then
        rm /usr/share/keyrings/docker-archive-keyring.gpg
    fi
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
        | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    logInfo "docker repo key fingerprint:"
    apt-key fingerprint 0EBFCD88

    rm -rf  /etc/apt/sources.list.d/docker.list
    echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt update
    apt-cache policy docker-ce
}

assertDockerIsInstalled () {
    local dockerExec=`which docker 2>/dev/null`
    assertVar dockerExec "docker is not in PATH."
}

dockerInstall () {
    # see https://docs.docker.com/install/linux/docker-ce/ubuntu/

    # ??? use https://get.docker.com/ ???

    assertLinux
    assertLinuxRelease 'ubuntu' "dockerInstall: only implemented for Ubuntu."

    linuxSetLocalTimezone

    assertIamRoot

    # FIXME: "restart services without asking?" needs auto-answer yes
    assertInteractive

    if [ -f /etc/apt/sources.list.d/docker.list ]; then
        local lsbRelease=`lsb_release -cs`
        local dockerLsbReleaseMatch=`grep "${lsbRelease}" /etc/apt/sources.list.d/docker.list`
        if [ "${dockerLsbReleaseMatch}" = '' ]; then
            logWarning "Current LSB release ${lsbRelease} does not match entry in apt docker source."
            logWarning "Assuming OS has been upgraded."
            logWarning "Removing file to force re-generation:"
            logWarning "/etc/apt/sources.list.d/docker.list"
            rm -f /etc/apt/sources.list.d/docker.list
            logWarning "You might want to reboot after docker has been upgraded."
        fi
    fi

    local dockerExec=`which docker 2>/dev/null`
    if [ "${dockerExec}" != '' -a -f /etc/apt/sources.list.d/docker.list ]; then
        logInfo "Docker executable is already in PATH: ${dockerExec}"
        logInfo "And file exists: /etc/apt/sources.list.d/docker.list"
        logInfo "Assuming docker repositoriy has been set up already."
        logInfo "To force re-setup of docker apt repository use:"
        logInfo "dockerSetupAptRepository"
    else
        dockerSetupAptRepository
    fi

    apt-get update
    logInfo "Running apt-get upgrade to be on the safe side."
    apt-get upgrade -y
    logInfo "Installing docker engine."
    apt-get install -y docker-ce docker-ce-cli containerd.io jq

    systemctl status docker | cat

    ensureSystemUserAndGroup docker
    if [ -d /home/docker/.docker ]; then
        chown --recursive "docker:docker" /home/docker/.docker
        chmod --recursive g+rwx /home/docker/.docker
    fi

    # This seems to lock up. Disabling for now.
    # su --command 'docker run hello-world' docker

    systemctl enable docker

    if [ -d /root/.ssh -a -d /home/docker -a ! -d /home/docker/.ssh ]; then
        logInfo "Copying .ssh from /root/ to /home/docker/"
        cp -r /root/.ssh /home/docker/
        chown --recursive docker:docker /home/docker/
    fi

    dockerComposeInstall
}

dockerRunPortainer () {
    # run portainer

    assertExecutableInPath 'docker'
    docker volume create portainer_data

    docker run -d -p 9000:9000 \
        --name portainer \
        --restart always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v portainer_data:/data portainer/portainer
}

dockerPruneEverything () {
    local forceOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o  "$1" = '--force' ]; then
            shift
            forceOption='--force'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    if [ "${isInteractiveShell_global}" != 'yes' ]; then
        forceOption='--force'
    fi
    logWarning "docker system prune ${forceOption} --all --volumes"
    if [ "${isInteractiveShell_global}" != 'yes' ]; then
        logWarning "This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all anonymous volumes not used by at least one container
  - all images without at least one container associated to them
  - all build cache"
    fi
    if [ "${isInteractiveShell_global}" != 'yes' ]; then
        local trace=`docker system prune ${forceOption} --all --volumes 2>&1`
        logInfo "${trace}"
    else
        docker system prune ${forceOption} --all --volumes
    fi
}

assertDockerComposeFile () {
    local ymlFile="$1"

    assertFile "${ymlFile}"
    local ymlFileShort="${ymlFile##*/}"
    if [ "${ymlFileShort}" != 'docker-compose.yml' -a "${ymlFileShort}" != 'docker-compose.yaml' ]; then
        logErrorAndAbort "File name is not docker-compose.yml nor .yaml: ${ymlFile}"
    fi
}

dockerComposeDown () {
    local tolerant='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-t' -o  "$1" = '--tolerant' ]; then
            shift
            tolerant='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local ymlFile="$1"
    local rc=''

    assertDockerComposeFile "${ymlFile}"
    cdf "${ymlFile}"

    # This test changes between different docker-compose versions. Disabling for now.
    #local runningTest=`docker-compose ps | grep running`
    #if [ "${runningTest}" = '' ]; then
    #    logInfo "Nothing seems to be running, but I'll continue anyways."
    #fi
    logInfo "Shutting down: ${ymlFile}"
    local trace=''
    trace=`docker-compose down 2>&1`; rc=$?
    cdo
    if [ "${rc}" != '0' ]; then
        logError "Unable to shut down: ${ymlFile}"
        logError "${trace}"
        if [ "${tolerant}" = 'yes' ]; then
            return ${rc}
        fi
        logErrorAndAbort "Please investigate."
    fi
    logInfo "${trace}"
    logInfo "Shut down    : ${ymlFile}"
}

dockerComposeUp () {
    local tolerant='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-t' -o  "$1" = '--tolerant' ]; then
            shift
            tolerant='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local ymlFile="$1"
    local rc=''

    assertDockerComposeFile "${ymlFile}"
    cdf "${ymlFile}"

    # This test changes between different docker-compose versions. Disabling for now.
    #local runningTest=`docker-compose ps | grep running`
    #if [ "${runningTest}" != '' ]; then
    #    logError "Cowardly refusing to start docker-compose with running containers."
    #    return
    #fi
    logInfo "Starting     : ${ymlFile}"
    local trace=''
    trace=`docker-compose up -d`; rc=$?
    cdo
    if [ "${rc}" != '0' ]; then
        logError "Unable to start up: ${ymlFile}"
        logError "${trace}"
        if [ "${tolerant}" = 'yes' ]; then
            return ${rc}
        fi
        logErrorAndAbort "Please investigate."
    fi
    logInfo "${trace}"
    logInfo "Started      : ${ymlFile}"
}

dockerComposeDownUp () {
    dockerComposeDown "$1" && \
    sleep 1 && \
    sync && \
    dockerComposeUp "$1"
}

dockerComposePullAndRestart () {
    local ymlFile="$1"
    local rc=''

    if [ "${ymlFile}" = '' ]; then
        local yamFileCandidate=''
        for yamFileCandidate in \
            ./docker-compose.yaml \
            ./docker-compose.yml \
            ; do
            if [ -f "${yamFileCandidate}" ]; then
                ymlFile="${yamFileCandidate}"
                break
            fi
        done
    fi
    assertDockerComposeFile "${ymlFile}"

    local image=''
    local line
    local downloadedSomething='no'
    while read line; do
        image=`echo "${line}" | awk ' { print $2 }'`
        if [ "${image:0:1}" = "'" -a "${image:${#image}-1:1}" = "'" ]; then
            image="${image:1:-1}"
        elif [ "${image:0:1}" = '"' -a "${image:${#image}-1:1}" = '"' ]; then
            image="${image:1:-1}"
        fi

        logInfo "Pulling image: ${image}"
        local trace=''
        if [ "${isInteractive_global}" = 'yes' ]; then
            trace=`docker pull "${image}" 2>&1 | tee /dev/tty`
        else
            trace=`docker pull "${image}" 2>&1`
            logInfo "${trace}"
        fi
        local imageDownloadTest=`echo "${trace}" | grep 'Status: Downloaded newer image'`
        if [ "${imageDownloadTest}" != '' ]; then
            downloadedSomething='yes'
        fi
    done < <( cat "${ymlFile}" | grep 'image:' | pipeStripStandard )

    if [ "${downloadedSomething}" = 'yes' ]; then
        dockerComposeDownUp "${ymlFile}"
    else
        logInfo "No newer image downloaded. No need to shut down and restart."
    fi
}

dockerComposePullAndRestartAll () {
    # Based on the convention of keeping container definitiones at
    # ~/docker/*/docker-compose.yml or ~/*/docker-compose.yml

    local ymlFile=''
    while read ymlFile; do
        dockerComposePullAndRestart "${ymlFile}"
    done < <( ls ~/docker/*/docker-compose.yml ~/*/docker-compose.yml ~/docker/*/docker-compose.yaml ~/*/docker-compose.yaml 2>/dev/null )
}

dockerComposeDownAll () {
    # Based on the convention of keeping container definitiones at
    # ~/docker/*/docker-compose.yml or ~/*/docker-compose.yml

    local ymlFile=''
    while read ymlFile; do
        dockerComposeDown "${ymlFile}"
    done < <( ls ~/docker/*/docker-compose.yml ~/*/docker-compose.yml ~/docker/*/docker-compose.yaml ~/*/docker-compose.yaml 2>/dev/null )
}

dockerComposeUpAll () {
    # Based on the convention of keeping container definitiones at
    # ~/docker/*/docker-compose.yml or ~/*/docker-compose.yml

    local ymlFile=''
    while read ymlFile; do
        dockerComposeUp "${ymlFile}"
    done < <( ls ~/docker/*/docker-compose.yml ~/*/docker-compose.yml ~/docker/*/docker-compose.yaml ~/*/docker-compose.yaml 2>/dev/null )
}

dockerComposeRemove () {
    if [ -f '/usr/local/bin/docker-compose' ]; then
        logInfo "Removing: /usr/local/bin/docker-compose"
        sudo rm /usr/local/bin/docker-compose
    else
        loginfo "No install at: /usr/local/bin/docker-compose"
    fi
}

dockerComposeInstall () {
    # https://docs.docker.com/compose/compose-file/compose-versioning/
    # https://github.com/docker/compose/releases
    local version="$1" # e.g. v2.2.2
    if [ -f /usr/local/bin/docker-compose ]; then
        logInfo "Already exists: /usr/local/bin/docker-compose"
        logInfo "To reinstall, use dockerComposeRemove first"
        return
    fi
    assertDoesNotExist /usr/local/bin/docker-compose

    if [ "${version}" = '' ]; then
        logInfo "Trying to determine latest version."
        local latestVersion=`
                  wget -qO- https://github.com/docker/compose/releases \
                      | egrep '\.\.\.v[0-9]' \
                      | head -n 1 \
                      | sed 's/.*\.\.\.v/v/' \
                      | sed 's/".*//'`
        local versionTest=`echo "${latestVersion}" | egrep '^v[0-9]+\.[0-9]+\.[0-9]+$'`
        if [ "${versionTest}" = '' ]; then
            logWarning "Unable to determine latest version."
            logInfo "Using last known version."
            setDefault version 'v2.2.3'
        else
            setDefault version "${latestVersion}"
        fi
        logInfo "Installing version: ${version}"
    fi
    sudo curl -L "https://github.com/docker/compose/releases/download/${version}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    if [ ! -f /usr/local/bin/docker-compose ]; then
        logErrorAndAbort "Installation failed"
    fi
    sudo chmod a+x /usr/local/bin/docker-compose
    which docker-compose
    docker-compose -v
    logInfo "You may need to restart this or other shells to pick up the new version."
}

dockerComposeInstallV2.2.2 () {
    dockerComposeRemove
    dockerComposeInstall 'v2.2.2'
}
dockerComposeInstallV2.2.3 () {
    dockerComposeRemove
    dockerComposeInstall 'v2.2.3'
}

dockerShell () {
    #docker ps --format '{{.ID}}\t{{.Image}}\t{{.Names}}'
    local containers=(`docker ps --format '{{.Names}}'`)
    local container="$1"
    assertInteractive
    if [ "${#containers[@]}" -eq 0 ]; then
        logErrorAndAbort "No running container found."
    elif [ "${#containers[@]}" -eq 1 ]; then
        container="${containers[0]}"
    else
        echo "Please select a container:"
        select container in "${containers[@]}"; do
            if [ "${container}" != '' ]; then
                break
            fi
        done
    fi
    local uidEnvVar=`docker inspect "${container}" | jq -r '.[].Config.Env[]' | grep -E '^[A-Za-z_]*UID=' | head -n 1`
    local uid=`echo "${uidEnvVar}" | awk -F'=' ' { print $2 } '`
    local dashuOption=''
    if [ "${uid}" != '' ]; then
        logInfo "Using UID detected in container env var: ${uidEnvVar}"
        dashuOption="-u ${uid}"
    fi
    local slashBin=`docker exec ${dashuOption} "${container}" ls /bin`
    local shells="bash sh zsh"
    local shellCandidate=''
    local shell=''
    for shellCandidate in ${shells}; do
        local shellTest=`echo "${slashBin}" | egrep '^'"${shellCandidate}"'$'`
        if [ "${shellTest}" != '' ]; then
            shell="${shellCandidate}"
            break
        fi
    done
    assertVariable shell "No shell found in container at /bin: ${shells}"
    logInfo "Launching ${shell} in container ${container}"
    logInfo "docker exec ${dashuOption} -it '${container}' '/bin/${shell}'"
    docker exec ${dashuOption} -it "${container}" "/bin/${shell}"
}

dockerComposeBounceAndLog () {
    docker-compose down \
        && docker-compose up -d \
            || logErrorAndAbort "Bounce failed."

    ps -ef \
        | grep -E "^${USER} " \
        | grep 'docker-compose logs -f' \
        | grep -v 'grep' \
        | awk ' { print $2 } ' \
        | xargs --no-run-if-empty kill -TERM

    docker-compose logs -f &
}

dockerContextSshCreate () {
    # test passwordless ssh
    # make sure to fetch remote ssh key
    # assume docker user on remote host
    # allow for context name option
    # shorten hostname for default context name
    docker context create --docker host=ssh://docker@twiki.lv42.com --description 'remote engine on twiki.lv42.com' twiki

    # docker@docker:~ $ docker context list
    # NAME        DESCRIPTION                               DOCKER ENDPOINT                  ERROR
    # audocker    remote engine on audocker.lv42.com        ssh://docker@audocker.lv42.com
    # default *   Current DOCKER_HOST based configuration   unix:///var/run/docker.sock
    # twiki       remote engine on twiki.lv42.com           ssh://docker@twiki.lv42.com
    # usdocker    remote engine on usdocker.lv42.com        ssh://docker@usdocker.lv42.com
}

dockerDaemonEnableExternalTcp () {
    logWarning "This will expose the docker daemon on an unsecured TCP port."
    logWarning "This is not recommended."
    local answer=''
    read -p 'Do you really want to continue? (yN) ' answer
    setDefault answer 'no'
    castYesNoR1 answer
    if [ "${answer}" != 'yes' ]; then
        abort --noStackTrace
    fi
    assertIamRoot
    assertDoesNotExist /etc/docker/daemon.json
    touch /etc/docker/daemon.json
    assertFile /etc/docker/daemon.json "Unable to create: /etc/docker/daemon.json"
    cat <<EOF >/etc/docker/daemon.json
{"hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]}
EOF
    logInfo "Created: /etc/docker/daemon.json"
    createDir /etc/systemd/system/docker.service.d/
    assertDoesNotExist /etc/systemd/system/docker.service.d/override.conf
    touch /etc/systemd/system/docker.service.d/override.conf
    assertFile /etc/systemd/system/docker.service.d/override.conf
    cat <<EOF >/etc/systemd/system/docker.service.d/override.conf
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd
EOF
    logInfo "created: /etc/systemd/system/docker.service.d/override.conf"
    logInfo "Reloading systemd daemon ..."
    systemctl daemon-reload
    logInfo "Restarting docker service ..."
    systemctl restart docker.service
}

# MAIN

dockerMainRemoteBootstrap () {
    alias dc='docker-compose'
    alias dcDown='dockerComposeDownAll'
    alias dcUp='dockerComposeUpAll'
    alias dcPull='dockerComposePullAndRestartAll'
    alias dcBounceAndLog='dockerComposeBounceAndLog'
}

eval "${showLibraryUsageIfNotSourced}"

dockerMainRemoteBootstrap

