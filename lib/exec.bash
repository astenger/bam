#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# exec helpers

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

execCaptureStdoutStderrR2 () {
    # https://stackoverflow.com/questions/11027679/capture-stdout-and-stderr-into-different-variables
    #
    # Usage:
    # execCaptureStdoutStderr sdtoutVar stderrVar; rc=$?
    eval "$({
__2="$(
  { __1="$("${@:3}")"; } 2>&1;
  ret=$?;
  printf '%q=%q\n' "$1" "$__1" >&2;
  exit $ret
  )";
ret="$?";
printf '%s=%q\n' "$2" "$__2" >&2;
printf '( exit %q )' "$ret" >&2;
} 2>&1 )";
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"
