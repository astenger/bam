#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam (open)ssl module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

(Open)SSL helper functions.

EOF

}

sslAssertCertFileIsPemFormat () {
    local isPemFormat='unknown'
    sslIsCertFilePemFormatR1 isPemFormat "$@"
    assertVariableValue isPemFormat 'yes' "Certificate does not appear to be in pem format: $1"
}

sslIsCertFilePemFormatR1 () {
    local returnVar_sslIsCertFilePemFormatR1="$1"
    local certFile="$2"

    assertFile "${certFile}"

    local pemTest=`grep -- '-----BEGIN CERTIFICATE-----' "${certFile}"`
    if [ "${pemTest}" != '' ]; then
        setVarOrLogInfo "${returnVar_sslIsCertFilePemFormatR1}" 'yes'
        return
    fi
    setVarOrLogInfo "${returnVar_sslIsCertFilePemFormatR1}" 'no'
}

sslShowCertDetails () {
    local pemFile="$1"

    assertFile "${pemFile}"
    openssl x509 -noout -text -in "${pemFile}" \
        | sed 's/[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f:]*:/[...]/' \
        | uniq \
        | pipeStripEmptyLines
}

sslCheckKeyCertCsr () {
    local keyFile="$1"  # Optional.
    local certFile="$2" # Optional.
    local csrFile="$3"  # Optional.

    # https://www.sslshopper.com/certificate-key-matcher.html

    local ckecksumKey=''
    local ckecksumCert=''
    local ckecksumCsr=''

    if [ "${keyFile}" != '' ]; then
        assertFile "${keyFile}"
        ckecksumKey=`openssl pkey -in "${keyFile}" -pubout -outform pem | sha256sum`
        logInfo "Key  checksum: ${ckecksumKey}"
    fi

    if [ "${certFile}" != '' ]; then
        assertFile "${certFile}"
        ckecksumCert=`openssl x509 -in "${certFile}" -pubkey -noout -outform pem | sha256sum`
        logInfo "Cert checksum: ${ckecksumCert}"
    fi
    if [ "${csrFile}" != '' ]; then
        assertFile "${csrFile}"
        ckecksumCsr=`openssl req -in "${csrFile}" -pubkey -noout -outform pem | sha256sum`
        logInfo "CSR  checksum: ${ckecksumCsr}"

    fi

    if [ "${ckecksumKey}" != '' -a "${ckecksumCert}" != '' ]; then
        if [ "${ckecksumKey}" = "${ckecksumCert}" ]; then
            logInfo "Key  and cert match."
        else
            logWarning "Key  and cert do NOT match."
        fi
    fi

    if [ "${ckecksumKey}" != '' -a "${ckecksumCsr}" != '' ]; then
        if [ "${ckecksumKey}" = "${ckecksumCsr}" ]; then
            logInfo "Key  and CSR match."
        else
            logWarning "Key  and CSR do NOT match."
        fi
    fi

    if [ "${ckecksumCert}" != '' -a "${ckecksumCsr}" != '' ]; then
        if [ "${ckecksumCert}" = "${ckecksumCsr}" ]; then
            logInfo "Cert and CSR match."
        else
            logWarning "Cert and CSR do NOT match."
        fi
    fi


}

# MAIN

eval "${showLibraryUsageIfNotSourced}"
