#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam net module - network helpers

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

This module defines network helper functions.

EOF

}

netShowActiveConnections () {
    use os
    assertLinux
    # https://www.computerhope.com/issues/ch001079.htm
    netstat -natp | less
}

netCheckDnsIsWorkingR1 () {
    local returnVar_netAssertDnsWorkingR1="$1"
    local hostnameToResolve="$2"
    setDefault hostnameToResolve 'one.one.one.one'
    local dnsServerToQuery="$3"
    setDefault dnsServerToQuery '1.1.1.1'
    local resolution=`dig +short "${hostnameToResolve}" @"${dnsServerToQuery}"`
    if [ "${resolution}" = '' ]; then
        logError "Unable to resolve: dig +short '${hostnameToResolve}' @'${dnsServerToQuery}'"
        setVarOrLogInfo "${returnVar_netAssertDnsWorkingR1}" 'no' \
                        'DNS is not working.'
        return
    fi
    setVarOrLogInfo "${returnVar_netAssertDnsWorkingR1}" 'yes' \
                    'DNS is working.'
}

netAssertDnsWorking () {
    local hostnameToResolve="$1"
    local dnsServerToQuery="$2"
    local dnsIsWorking=''
    netCheckDnsIsWorkingR1 dnsIsWorking "${hostnameToResolve}" "${dnsServerToQuery}"
    if [ "${dnsIsWorking}" = 'no' ]; then
        logErrorAndAbort "DNS is not working"
    fi
    logInfo "DNS is working."
}

netGetPublicIpR1 () {
    local returnVar_netGetPublicIp="$1"
    local publicIP_netGetPublicIp=''
    #publicIP_netGetPublicIp=`dig +short myip.opendns.com @resolver1.opendns.com`
    publicIP_netGetPublicIp=`curl --silent ipinfo.io/ip`
    assertVar publicIP_netGetPublicIp "Unable to determine public IP."
    setVarOrLogInfo "${returnVar_netGetPublicIp}" "${publicIP_netGetPublicIp}" \
                    "Current public IP is: ${publicIP_netGetPublicIp}"

}

netGetPublicIp () {
    local ip=''
    netGetPublicIpR1 ip

    logInfo "Current public IP is: ${ip}"
    assertVar ip
    local ipInfo=`curl --silent "ipinfo.io/${ip}"`
    logInfo "${ipInfo}"
}


netTest () {
    netCheckDnsIsWorkingR1
    local publicIp=''
    netGetPublicIpR1 publicIp
    logInfo "Current public IP is: ${publicIp}"

    local publicDnsNameDefaultDns=`dig +short -x ${publicIp}`
    if [ "${publicDnsNameDefaultDns}" = '' ]; then
        logInfo "Public IP does not resolve. Skipping related checks."
    else
        logInfo "Reverse lookup of public IP from default DNS: ${publicDnsNameDefaultDns}"
        local publicDnsNameDefaultDnsResolved=`dig +short "${publicDnsNameDefaultDns}"`
        if [ "${publicDnsNameDefaultDnsResolved}" = "${publicIp}" ]; then
            logInfo "Resolution of public DNS name matches public IP."
        else
            logWarning "Resolution of public DNS name does not match public IP: ${publicDnsNameDefaultDnsResolved}"
        fi
        local publicDnsNamePublicDnsResolved=`dig @1.1.1.1 +short -x ${publicIp}`
        if [ "${publicDnsNamePublicDnsResolved}" = '' ]; then
            logInfo "Public IP does not resolve on public server. Skipping related checks."
        else
            if [ "${publicDnsNamePublicDnsResolved}" = "${publicDnsNameDefaultDnsResolved}" ]; then
                logWarning "Default and public reverse lookup of public IP do match."
            else
                logWarning "Default and public reverse lookup of public IP do not match: ${publicDnsNamePublicDnsResolved}"
            fi
            local publicDnsNamePublicDnsResolved=`dig +short "${publicDnsNamePublicDnsResolved}"`
            if [ "${publicDnsNameDefaultDnsResolved}" = "${publicDnsNamePublicDnsResolved}" ]; then
                logInfo "Public and default lookup of public names match."
            else
                logWarning "Public and default reverse lookup of public names do not match: ${publicDnsNameResolvedPublic}"
            fi
        fi
    fi
}


# MAIN

eval "${showLibraryUsageIfNotSourced}"
