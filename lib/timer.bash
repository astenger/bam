#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam timer module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Timer functions with resume. Use multiple timers by name.

Examples:

$ # Start default timer:
$ timerStart
INFO  Starting timer: 'default' at Tue 27 Sep 2022 19:44:14 AEST

$ # Start a second timer:
$ timerStart secondTimer
INFO  Starting timer: 'secondTimer' at Tue 27 Sep 2022 19:44:32 AEST

$ timerStop
INFO  Stopping timer: 'default' at Tue 27 Sep 2022 19:44:44 AEST
INFO  Total elapsed time for timer 'default': 30s

$ timerGetElapsedTime secondTimer
INFO  Elapsed time for timer 'secondTimer': 38s

$ timerStop secondTimer
INFO  Stopping timer: 'secondTimer' at Tue 27 Sep 2022 19:45:24 AEST
INFO  Total elapsed time for timer 'secondTimer': 52s

$ timerDelete secondTimer

EOF

}

setDefault timerList_global "# timerName startTime previousElapsedTime"

timerDelete () {
    local timerName="$1"

    setDefault timerName 'default'
    assertVariable timerName
    assertValidName timerName

    timerList_global=`echo "${timerList_global}" | grep -E -v '^'"${timerName} "`
}

timerConvertEpochToHumanReadableR1 () {
    local returnVar_timerConvertEpochToHumanReadableR1="$1"
    local epoch="$2"

    if [ "${timerDateCommandVariant_global}" = '' -o \
         "${timerDateCommandVariant_global}" = 'unknown' ]; then
        setVarOrLogInfo "${returnVar_timerConvertEpochToHumanReadableR1}" "${epoch}"
    fi
    assertInt "${epoch}"
    local time_timerConvertEpochToHumanReadableR1=''
    if [ "${timerDateCommandVariant_global}" = 'gnu' ]; then
        time_timerConvertEpochToHumanReadableR1=`date -d @${epoch} --iso-8601=seconds`
    elif [ "${timerDateCommandVariant_global}" = 'bsd' ]; then
        time_timerConvertEpochToHumanReadableR1=`date -r ${epoch}`
    else
        logErrorAndAbort "timerConvertEpochToHumanReadableR1(): don't know how to execute with date variant ${timerDateCommandVariant_global}"
    fi
    setVarOrLogInfo "${returnVar_timerConvertEpochToHumanReadableR1}" "${time_timerConvertEpochToHumanReadableR1}"
}

timerStart () {
    local resumeRequested='no'
    if [ "$1" = '-r' ]; then
        resumeRequested='yes'
        shift
    fi

    local timerName="$1"

    setDefault timerName 'default'
    assertVariable timerName
    assertValidName timerName

    local startTime=`date +%s`
    local startTimeH=''
    timerConvertEpochToHumanReadableR1 startTimeH "${startTime}"
    local existingTimer=`echo "${timerList_global}" | pipeStripStandard | grep -E '^'"${timerName} "`
    local newTimerEntry=''
    if [ "${existingTimer}" = '' ]; then
        # This is a new timer
        logInfo "Starting timer: '${timerName}' at ${startTimeH}"
        newTimerEntry="${timerName} ${startTime} 0"
        timerList_global="${timerList_global}
${newTimerEntry}"
        return
    fi
    # Get second last field:
    # awk ' { print ( $(NF-1) ) }'
    # matters for timers with spaces
    local previousStartTime=`echo "${existingTimer}" | awk ' { print ( $(NF-1) ) }'`
    local elapsedTime=`echo "${existingTimer}" | awk ' { print ( $(NF) ) }'`
    if [ "${previousStartTime}" = '-' ]; then
        if [ "${resumeRequested}" = 'yes' ]; then
            logInfo "Resuming timer: ${timerName} at ${startTimeH}"
            newTimerEntry="${timerName} ${startTime} ${elapsedTime}"
        else
            logInfo "Restarting timer: '${timerName}' at ${startTimeH}"
            newTimerEntry="${timerName} ${startTime} 0"
        fi
        timerDelete "${timerName}"
        timerList_global="${timerList_global}
${newTimerEntry}"
        return
    fi
    logWarning "Timer is already running: ${timerName}"
}

timerResume () {
    timerStart -r "$@"
}

timerStopR1 () {
    local returnVar_timerStopR1="$1"
    local timerName="$2"

    setDefault timerName 'default'
    assertVariable timerName
    assertValidName timerName

    local stopTime=`date +%s`
    local stopTimeH=''
    timerConvertEpochToHumanReadableR1 stopTimeH "${stopTime}"

    local existingTimer=`echo "${timerList_global}" | pipeStripStandard | grep -E '^'"${timerName} "`
    if [ "${existingTimer}" = '' ]; then
        logError "Timer is not known: ${timerName}"
        return
    fi

    local dhms=''
    local startTime=`echo "${existingTimer}" | awk ' { print ( $(NF-1) ) }'`
    local previousElapsedTime=`echo "${existingTimer}" | awk ' { print ( $(NF) ) }'`
    #logInfo "startTime=${startTime}"
    #logInfo "previousElapsedTime=${previousElapsedTime}"
    if [ "${startTime}" = '-' ]; then
        logWarning "Timer has been stopped already: ${timerName}"
        timerConvertSecondsToDHMSR1 dhms "${previousElapsedTime}"
        setVarOrLogInfo "${returnVar_timerStopR1}" "${totalElapsedTime}" "Total elapsed time for timer '${timerName}': ${dhms}"
        return
    fi
    local currentElapsedTime=''
    local totalElapsedTime=''
    let currentElapsedTime=${stopTime}-${startTime}
    let totalElapsedTime=${previousElapsedTime}+${currentElapsedTime}
    logInfo "Stopping timer: ${timerName} at ${stopTimeH}"
    timerDelete "${timerName}"
    local newTimerEntry="${timerName} - ${totalElapsedTime}"
    timerList_global="${timerList_global}
${newTimerEntry}"
    timerConvertSecondsToDHMSR1 dhms "${totalElapsedTime}"
    setVarOrLogInfo "${returnVar_timerStopR1}" "${totalElapsedTime}" "Total elapsed time for timer '${timerName}': ${dhms}"
}

timerStop () {
    timerStopR1 '' "$@"
}

timerGetElapsedTimeR1 () {
    # This is since last resume by default
    local getTotalElapsedTime='no'
    if [ "$1" = '-t' ]; then
        getTotalElapsedTime='yes'
        shift
    fi
    local returnVar_timerGetEplapsedR1="$1"
    local timerName="$2"

    setDefault timerName 'default'
    assertVariable timerName
    assertValidName timerName

    local existingTimer=`echo "${timerList_global}" | pipeStripStandard | grep -E '^'"${timerName} "`
    if [ "${existingTimer}" = '' ]; then
        logError "Timer is not known: ${timerName}"
        return
    fi
    local dhms=''
    local startTime=`echo "${existingTimer}" | awk ' { print ( $(NF-1) ) }'`
    local previousElapsedTime=`echo "${existingTimer}" | awk ' { print ( $(NF) ) }'`
    #logInfo startTime=${startTime}
    #logInfo previousElapsedTime=${previousElapsedTime}
    if [ "${startTime}" = '-' ]; then
        timerConvertSecondsToDHMSR1 dhms "${previousElapsedTime}"
        setVarOrLogInfo "${returnVar_timerGetEplapsedR1}" "${previousElapsedTime}" "Previous elapsed time: ${dhms}"
        return
    fi
    local currentTime=`date +%s`
    local elapsedTime_timerGetElapsedTimeR1=''
    let elapsedTime_timerGetElapsedTimeR1=${currentTime}-${startTime}
    if [ "${getTotalElapsedTime}" = 'yes' ]; then
        let elapsedTime_timerGetElapsedTimeR1+=${previousElapsedTime}
        timerConvertSecondsToDHMSR1 dhms "${elapsedTime_timerGetElapsedTimeR1}"
        setVarOrLogInfo "${returnVar_timerGetEplapsedR1}" "${elapsedTime_timerGetElapsedTimeR1}" "Total elapsed time for timer '${timerName}': ${dhms}"
    else
        timerConvertSecondsToDHMSR1 dhms "${elapsedTime_timerGetElapsedTimeR1}"
        setVarOrLogInfo "${returnVar_timerGetEplapsedR1}" "${elapsedTime_timerGetElapsedTimeR1}" "Elapsed time for timer '${timerName}': ${dhms}"
    fi
}

timerGetElapsedTime () {
    timerGetElapsedTimeR1 '' "$@"
}

timerGetElapsedTimeTotalR1 () {
    timerGetElapsedTimeR1 -t "$@"
}

timerGetElapsedTimeTotal () {
    timerGetElapsedTimeR1 -t '' "$@"
}

timerList () {
    local emptyTest=`echo "${timerList_global}" | pipeStripStandard`
    if [ "${emptyTest}" = '' ]; then
        logInfo "There are no timers."
        return
    fi
    logDebug "Known timers:
${timerList_global}"
    local line=''
    local timer=''
    local elapsed=''
    local elapsedTotal=''
    local startTime=''
    local previousElapsed=''
    local status=''
    while read line; do
        timer=`echo "${line}" | awk ' { print $1} '`
        startTime=`echo "${line}" | awk ' { print $2} '`
        previousElapsed=`echo "${line}" | awk ' { print $3} '`
        status='unknown'
        if [ "${startTime}" = '-' ]; then
            status='stopped'
        elif [ "${startTime}" != '' ]; then
            status='running'
        fi
        timerGetElapsedTimeR1 elapsed "${timer}"
        timerConvertSecondsToDHMSR1 elapsed "${elapsed}"
        timerGetElapsedTimeR1 -t elapsedTotal "${timer}"
        timerConvertSecondsToDHMSR1 elapsedTotal "${elapsedTotal}"
        if [ "${status}" = 'stopped' ]; then
            logInfo "Timer ${timer}: stopped ${elapsed}"
        elif [ "${elapsed}" = "${elapsedTotal}" ]; then
            logInfo "Timer ${timer}: running ${elapsed}"
        else
            logInfo "Timer ${timer}: lap ${elapsed} - total ${elapsedTotal}"
        fi
    done < <( echo "${timerList_global}" | pipeStripStandard )
}

timerConvertSecondsToDHMSR1 () {
    local returnVar_timerConvertSecondsToDHMSR1="$1"
    local timeInSecondsToConvert="$2"

    assertInt "${timeInSecondsToConvert}"

    local sMod=''
    let sMod=${timeInSecondsToConvert}%60
    local mTotal=''
    let mTotal=${timeInSecondsToConvert}/60
    local mMod=''
    let mMod=${mTotal}%60
    local hTotal=''
    let hTotal=${mTotal}/60
    local hMod=''
    let hMod=${hTotal}%24
    local dTotal=''
    let dTotal=${hTotal}/24

    local returnValue_timerConvertSecondsToDHMSR1=''
    if [ "${dTotal}" -ne 0 ]; then
        returnValue_timerConvertSecondsToDHMSR1=`
            printf '%dd %02dh %02dm %02ds' ${dTotal} ${hMod} ${mMod} ${sMod}`
    elif [ "${hMod}" -ne 0 ]; then
        returnValue_timerConvertSecondsToDHMSR1=`
            printf '%02dh %02dm %02ds' ${hMod} ${mMod} ${sMod}`
    elif [ "${mMod}" -ne 0 ]; then
        returnValue_timerConvertSecondsToDHMSR1=`
            printf '%02dm %02ds' ${mMod} ${sMod}`
    else
        returnValue_timerConvertSecondsToDHMSR1=`
            printf '%02ds' ${sMod}`
    fi
    setVarOrLogInfo \
        "${returnVar_timerConvertSecondsToDHMSR1}" \
        "${returnValue_timerConvertSecondsToDHMSR1}" \
        "${timeInSecondsToConvert}s = ${returnValue_timerConvertSecondsToDHMSR1}"
}

timerConvertDHMSToSecondsR1 () {
    # We are assuming seconds are always there, and the longer timeframes might not be.
    # We are ignoring fractions of a second
    # 0:22:58.218000

    local returnVar_timerConvertDHMSToSecondsR1="$1"
    local dhms="$2"

    assertVar dhms

    local s=''
    local m=''
    local h=''
    local d=''

    # something like "1d 2h 3m 4s" or "1d3m" - all fields are optional
    local spelledOutTest=`echo "${dhms}" | grep -E '^([0-9]*d[ ]*)?([0-9]*h[ ]*)?([0-9]*m[ ]*)?([0-9]*s)?$'`
    # someting like "1:2:3:4" or ":2::4" or "3:4.5" - has to have at least seconds
    local colonNotaionTest=`echo "${dhms}" | grep -E '^([0-9]*[:])?([0-9]*[:])?([0-9]*[:])?[0-9]+(.[0-9]*)?$'`
    if [ "${spelledOutTest}" != '' ]; then
        # something like "1d 2h 3m 4s" or "1d 3m"
        #logInfoVar d h m s
        s=`echo "${dhms}" |  sed -r 's/^([0-9]*d[ ]*)?([0-9]*h[ ]*)?([0-9]*m[ ]*)?([0-9]+s)?$/\4/'`
        m=`echo "${dhms}" |  sed -r 's/^([0-9]*d[ ]*)?([0-9]*h[ ]*)?([0-9]*m[ ]*)?([0-9]+s)?$/\3/'`
        h=`echo "${dhms}" |  sed -r 's/^([0-9]*d[ ]*)?([0-9]*h[ ]*)?([0-9]*m[ ]*)?([0-9]+s)?$/\2/'`
        d=`echo "${dhms}" |  sed -r 's/^([0-9]*d[ ]*)?([0-9]*h[ ]*)?([0-9]*m[ ]*)?([0-9]+s)?$/\1/'`
        #logInfoVar d h m s
        s=`echo "${s}" | sed 's/[dhms ]*$//'`
        m=`echo "${m}" | sed 's/[dhms ]*$//'`
        h=`echo "${h}" | sed 's/[dhms ]*$//'`
        d=`echo "${d}" | sed 's/[dhms ]*$//'`
    elif [ "${colonNotaionTest}" != '' ]; then
        s="${dhms##*:}"
        local lenSeconds="${#s}"
        local lenDHMS="${#dhms}"
        if [ ${lenDHMS} -gt ${lenSeconds} ]; then
            local lenNoSeconds=''
            let lenNoSeconds=${lenDHMS}-${lenSeconds}-1
            local dhm="${dhms:0:${lenNoSeconds}}"
            m="${dhm##*:}"
            local lenMinutes="${#m}"
            local lenDHM="${#dhm}"
            if [ ${lenDHM} -gt ${lenMinutes} ]; then
                local lenNoMinutes=''
                let lenNoMinutes=${lenDHM}-${lenMinutes}-1
                local dh="${dhm:0:${lenNoMinutes}}"
                h="${dh##*:}"
                local lenHours="${#h}"
                local lenDH="${#dh}"
                if [ ${lenDH} -gt ${lenHours} ]; then
                    d=${dh%:*}
                fi
            fi
        fi
        # Strip fractions of seconds
        s="${s%.*}"
    else
        logErrorAndAbort "timerConvertDHMSToSecondsR1: unknown format: ${dhms}"
    fi

    # Remove leading zeroes or let will interpret as octal and complain
    s=`echo "${s}" | sed 's/^0[0]*//g'`
    m=`echo "${m}" | sed 's/^0[0]*//g'`
    h=`echo "${h}" | sed 's/^0[0]*//g'`
    d=`echo "${d}" | sed 's/^0[0]*//g'`
    setDefault s 0
    setDefault m 0
    setDefault h 0
    setDefault d 0
    #logDebugVar d h m s
    assertInt "${s}"
    assertInt "${m}"
    assertInt "${h}"
    assertInt "${d}"
    local totalSeconds_timerConvertDHMSToSecondsR1=''
    let totalSeconds_timerConvertDHMSToSecondsR1="${d}*24*60*60+${h}*60*60+${m}*60+${s}"
    assertInt "${totalSeconds_timerConvertDHMSToSecondsR1}"
    setVarOrLogInfo "${returnVar_timerConvertDHMSToSecondsR1}" "${totalSeconds_timerConvertDHMSToSecondsR1}"
}

timerWait () {
    local waitTime="$1"
    local messageSuffix="$2"
    local timerName="$3"

    setDefault waitTime 60
    setDefault messageSuffix 'until timer is done'
    setDefault timerName 'waitTimer'
    assertVariable timerName
    assertValidName timerName

    timerStart "${timerName}"

    local elapsedTime=0
    local secondsRemaining=0
    local timeRemainingDHMS=''

    if [ "${logFlashEnabled_global}" != 'yes' ]; then
        timerGetElapsedTimeR1 elapsedTime "${timerName}"
        let secondsRemaining=${waitTime}-${elapsedTime}
        timerConvertSecondsToDHMSR1 timeRemainingDHMS ${secondsRemaining}
        logInfo "`date` - sleeping for ${timeRemainingDHMS} until timer is done"
	sleep ${secondsRemaining}
    else
	local waitTimeDHMS=''
	timerConvertSecondsToDHMSR1 waitTimeDHMS "${waitTime}"
	while [ ${elapsedTime} -lt ${waitTime} ]; do
            timerGetElapsedTimeR1 elapsedTime "${timerName}"
            let secondsRemaining=${waitTime}-${elapsedTime}
            timerConvertSecondsToDHMSR1 timeRemainingDHMS ${secondsRemaining}
            logFlash "`date` - ${timeRemainingDHMS}/${waitTimeDHMS} ${messageSuffix}"
	    if [ ${secondsRemaining} -le 2 ]; then
		sleep .5
	    elif [ ${secondsRemaining} -le 61 ]; then
		sleep 1
	    elif [ ${secondsRemaining} -le 610 ]; then
		sleep 10
	    else
		sleep 60
	    fi
	done
    fi
    logFlashClear
    timerStop "${timerName}"
    timerDelete "${timerName}"
}

timerShow () {
    local timerName="$1"
    while true; do
        local elapsed=''
        local elapsedH=''
        timerGetElapsedTimeR1 elapsed "${timerName}"
        timerConvertSecondsToDHMSR1 elapsedH "${elapsed}"
        clear
        echo "${elapsedH}"
        sleep 5
    done
}

timerRunAndShow () {
    timerDelete
    timerStart
    timerShow
}

timerMainRemoteBootstrap () {
    timerDateCommandVariant_global=''
    if [ "`date --help 2>&1 | grep GNU`" != '' ]; then
        timerDateCommandVariant_global='gnu'
    elif [ "`date -u -r 0 2>&1`" = 'Thu  1 Jan 1970 00:00:00 UTC' ]; then
        timerDateCommandVariant_global='bsd'
    else
        timerDateCommandVariant_global='unknown'
    fi
}

timerGetElapsedVsProcessedRatioR1() {
    local returnVar_timerGetProcessedVsElapsedRatioR1="$1"
    local elapsedTimeInS="$2"
    local processedTimeInS="$3"

    assertFloat "${elapsedTimeInS}" "Variable elapsedTimeInS is not a float."
    assertFloat "${processedTimeInS}" "Variable processedTimeInS is not a float."

    assertExecutableInPath bc
    local ratio_timerGetProcessedVsElapsedRatioR1=`echo "scale=2; ${elapsedTimeInS}/${processedTimeInS}" | /usr/bin/bc`
    setVarOrLogInfo "${returnVar_timerGetProcessedVsElapsedRatioR1}" "${ratio_timerGetProcessedVsElapsedRatioR1}"
}

timerReportElapsedVsProcessedRatio () {
    local elapsed="$1"
    local processed="$2"
    local messagePrefix="$3"

    assertVar elapsed
    assertVar processed
    setDefault messagePrefix 'ratio:'

    local elapsedInS=''
    local processedInS=''
    timerConvertDHMSToSecondsR1 elapsedInS "${elapsed}"
    timerConvertDHMSToSecondsR1 processedInS "${processed}"

    local elapsedH=''
    local processedH=''
    timerConvertSecondsToDHMSR1 elapsedH "${elapsedInS}"
    timerConvertSecondsToDHMSR1 processedH "${processedInS}"

    local ratio=''
    timerGetElapsedVsProcessedRatioR1 ratio "${elapsedInS}" "${processedInS}"
    logInfo "${messagePrefix} ${ratio} = ${elapsedH} elapsed / ${processedH} processed"
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"
timerMainRemoteBootstrap
