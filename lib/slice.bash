#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam slice module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Slice and dice text files.

EOF

}

getFirstLineR1 () {
    local returnVarLine_getFirstLineR1="$1"
    local text="$2"

    local line=`echo "${text}" | sed -n '1p'`
    setVarOrLogInfo "${returnVarLine_getFirstLineR1}" "${line}"
}

getLastLineR1 () {
    local returnVarLine_getLastLineR1="$1"
    local text="$2"

    local line=`echo "${text}" | sed -n '$p'`
    setVarOrLogInfo "${returnVarLine_getLastLineR1}" "${line}"
}

isSliceMarkedCorrectlyR2 () {
    local returnVarYesNo_isSliceMarkedCorrectlyR2="$1"
    local returnVarSliceName_isSliceMarkedCorrectlyR2="$2"
    local slice="$3"

    local allOk='yes'

    local firstLine=''
    local lastLine=''
    getFirstLineR1 firstLine "${slice}"
    getLastLineR1  lastLine  "${slice}"

    local firstLineTestMarker=`echo "$firstLine" | sed 's/^\(# SLICE-BEGIN\) \(.*\)$/\1/'`
    local lastLineTestMarker=`echo "$lastLine" | sed 's/^\(# SLICE-END\) \(.*\)$/\1/'`

    if [ "${firstLineTestMarker}" != "# SLICE-BEGIN" ]; then
        logDebug "BAD  First line marker not in place: # SLICE-BEGIN"
        allOk='no'
    else
        logDebug "GOOD First line marker is in place: # SLICE-BEGIN"
    fi
    if [ "${lastLineTestMarker}" != "# SLICE-END" ]; then
        logDebug "BAD  Last line marker not in place: # SLICE-END"
        allOk='no'
    else
        logDebug "GOOD Last line marker is in place: # SLICE-END"
    fi

    local firstLineMarkerSliceName=''
    local lastLineMarkerSliceName=''
    if [ "${allOk}" = 'yes' ]; then
        firstLineMarkerSliceName=`
                  echo "$firstLine" | sed 's/^\(# SLICE-BEGIN\) \(.*\)$/\2/'`
        lastLineMarkerSliceName=`
                  echo "$lastLine" | sed 's/^\(# SLICE-END\) \(.*\)$/\2/'`
        if [ "${firstLineMarkerSliceName}" = '' ]; then
            logDebug "BAD  First line marker does not specify a slice name."
            allOk='no'
        fi
        if [ "${lastLineMarkerSliceName}" = '' ]; then
            logDebug "BAD  Last line marker does not specify a slice name."
            allOk='no'
        fi
        if [ "${firstLineMarkerSliceName}" != "${lastLineMarkerSliceName}" ]; then
            logDebug "BAD  First and last line slice names are not the same."
            logDebug "${firstLineMarkerSliceName} != ${lastLineMarkerSliceName}"
            allOk='no'
        else
            logDebug "GOOD First and last line slice names are the same"
            logDebug "${firstLineMarkerSliceName}"
        fi
        local sliceMarkerCountBegin=`
              echo "${slice}" | egrep '^# SLICE-BEGIN ' | wc -l`
        local sliceMarkerCountEnd=`
              echo "${slice}" | egrep '^# SLICE-END ' | wc -l`

        if [ ${sliceMarkerCountBegin} != '1' ] ; then
            logDebug "BAD  Additional begin markers found."
            local allOk='no'
        else
            logDebug "GOOD No additional begin markers found."
        fi
        if [ ${sliceMarkerCountEnd} != '1' ] ; then
            logDebug "BAD  Additional end markers found."
            local allOk='no'
        else
            logDebug "GOOD No additional end markers found."
        fi
    fi
    if [ "${allOk}" != 'yes' ]; then
        setVar "${returnVarSliceName_isSliceMarkedCorrectlyR2}" ''
        setVarOrLogInfo "${returnVarYesNo_isSliceMarkedCorrectlyR2}" 'no' \
                        'Slice is NOT marked correctly.'
        return
    fi
    setVar "${returnVarSliceName_isSliceMarkedCorrectlyR2}" \
           "${firstLineMarkerSliceName}"
    setVarOrLogInfo "${returnVarYesNo_isSliceMarkedCorrectlyR2}" 'yes' \
                    "Slice is marked correctly with name: ${firstLineMarkerSliceName}"
}

isSliceMarkedCorrectlyR1 () {
    local dummySliceName=''
    isSliceMarkedCorrectlyR2 "$1" dummySliceName "$2"
}

assertSliceIsCorrectlyMarkedR1 () {
    local yesNoResult=''
    local sliceName_assertSliceIsCorrectlyMarkedR1=''

    isSliceMarkedCorrectlyR2 yesNoResult sliceName_assertSliceIsCorrectlyMarkedR1 "$2"
    if [ "${yesNoResult}" != 'yes' ]; then
        logErrorAndAbort "Slice is not correctly marked:
$2"
    fi
    setVarOrLogInfo \
        "${1}" \
        "${sliceName_assertSliceIsCorrectlyMarkedR1}" \
        "Slice is correctly marked: ${sliceName_assertSliceIsCorrectlyMarkedR1}"
}

assertSliceIsCorrectlyMarked () {
    local sliceNameDummy=''
    assertSliceIsCorrectlyMarkedR1 sliceNameDummy "$1"
}

sliceGetBeginMarkerForNameR1 () {
    local returnVarMarker_sliceGetBeginMarkerForNameR1="$1"
    local sliceName="$2"

    assertValidName "${sliceName}"
    local marker="# SLICE-BEGIN ${sliceName}"
    setVarOrLogInfo "${returnVarMarker_sliceGetBeginMarkerForNameR1}" \
                    "${marker}"
}

sliceGetEndMarkerForNameR1 () {
    local returnVarMarker_sliceGetBeginMarkerForNameR1="$1"
    local sliceName="$2"

    assertValidName "${sliceName}"
    local marker="# SLICE-END ${sliceName}"
    setVarOrLogInfo "${returnVarMarker_sliceGetBeginMarkerForNameR1}" \
                    "${marker}"
}

assertSliceCleanMarkers () {
    local text="$1"

    local allMarkers=`echo "${text}" | egrep '^# SLICE-(BEGIN|END) .*$'`
    local sortedMarkers=`echo "${allMarkers}" | sort`
    local uniqeMarkers=`echo "${allMarkers}" | uniq`

    if [ "${sortedMarkers}" != "${uniqeMarkers}" ]; then
        logErrorAndAbort "There are duplicate markers:
${allMarkers}"
    fi

    local sliceNames=`echo "${uniqeMarkers}" | sed 's/^# SLICE-BEGIN //' | sed 's/^# SLICE-END //'`
    local sliceName=''
    for sliceName in ${sliceNames}; do
        assertSliceIsCorrectlyMarked "${}"
    done
}

sliceGetR1 () {
    # Returns empty result if slice does not exist.
    local returnVarSliceContent_sliceGetR1="$1"
    local sliceName="$2"
    local content="$3"

    assertValidName "${sliceName}"

    local markerBegin=''
    local markerEnd=''
    sliceGetBeginMarkerForNameR1 markerBegin "${sliceName}"
    sliceGetEndMarkerForNameR1   markerEnd   "${sliceName}"
    local sliceContent=`echo "${content}" | sed -n "/${markerBegin}/,/${markerEnd}/p"`

    if [ "${sliceContent}" != '' ]; then
        assertSliceIsCorrectlyMarked "${sliceContent}"
    fi

    setVarOrLogInfo "${returnVarSliceContent_sliceGetR1}" \
                    "${sliceContent}"
}

sliceReplaceR1 () {
    local prependIfNotPresent='no'
    local appendIfNotPresent='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-p' -o "$1" = '--prepend' ]; then
            prependIfNotPresent='yes'
            shift
        elif [ "$1" = '-a' -o "$1" = '--append' ]; then
            appendIfNotPresent='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    if [ "${prependIfNotPresent}" = 'yes' -a "${appendIfNotPresent}" = 'yes' ]; then
        logErrorAndAbort "sliceReplaceR1: optons -a/-append and -p/-prepend are mutually exclusive"
    fi
    local returnVarText_sliceReplaceR1="$1"
    local slice="$2"
    local textToBeProcessed="$3"

    local sliceName=''
    assertSliceIsCorrectlyMarkedR1 sliceName "${slice}"

    local textNew_sliceReplaceR1=''
    if [ "${textToBeProcessed}" = '' ]; then
	textNew_sliceReplaceR1="${slice}"
        setVarOrLogInfo "${returnVarText_sliceReplaceR1}" \
                        "${textNew_sliceReplaceR1}"
        return
    fi
    local sliceOld=''
    sliceGetR1 sliceOld "${sliceName}" "${textToBeProcessed}"


    if [ "${sliceOld}" = '' ]; then
        # Slice not currently present.
        if [ "${prependIfNotPresent}" = 'yes' ]; then
            textNew_sliceReplaceR1="${slice}

${textToBeProcessed}"
        elif [ "${appendIfNotPresent}" = 'yes' ]; then
            textNew_sliceReplaceR1="${textToBeProcessed}

${slice}"
        else
            logErrorAndAbort "sliceReplaceR1: slice ${sliceName} not present and no -p or -a option specified."
        fi
        setVarOrLogInfo "${returnVarText_sliceReplaceR1}" \
                        "${textNew_sliceReplaceR1}"
        return
    fi

    local beginMarker=''
    local endMarker=''
    getFirstLineR1 beginMarker "${slice}"
    getLastLineR1  endMarker  "${slice}"
    logInfoVar beginMarker endMarker

    textNew_sliceReplaceR1=$(
        # up to the begin marker
        echo "${textToBeProcessed}" \
            | awk '1;/'"${beginMarker}[ ${tab_global}]*"'$/{exit}'
        # the new slice without the markers
        echo "${slice}" | pipeStripFirstLine | pipeStripLastLine
        # from the end marker to the end
        echo "${textToBeProcessed}" \
            | awk '/'"${endMarker}[ ${tab_global}]*"'$/,EOF'
        )
    setVarOrLogInfo "${returnVarText_sliceReplaceR1}" "${textNew_sliceReplaceR1}"
}

sliceReplaceInFile () {
    local sliceReplaceOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-p' -o "$1" = '--prepend' ]; then
            sliceReplaceOption='--prepend'
            shift
        elif [ "$1" = '-a' -o "$1" = '--append' ]; then
            sliceReplaceOption='--append'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local slice="$1"
    local fileName="$2"

    assertVar fileName
    qualifyPathR1 fileName

    if [ ! -f "${fileName}" -a -e "${fileName}" ]; then
        logErrorAndAbort "Exists, but is not a file: ${fileName}"
    fi
    if [ ! -f "${fileName}" ]; then
        logInfo "Creating file: ${fileName}"
	> "${fileName}"
    fi
    assertFile "${fileName}"

    local currentContent=`cat "${fileName}"`
    local newContent=''
    sliceReplaceR1 ${sliceReplaceOption} newContent "${slice}" "${currentContent}"

    use file
    fileBackup "${fileName}"
    local fileNew="${fileName}.new"
    removeFileIfExists "${fileNew}"
    > "${fileNew}"
    assertFile "${fileNew}"
    echo "${newContent}" > "${fileNew}"
    fileRollForward "${fileName}"
}

sliceAssertNoMarkers () {
    local text="$1"

    local markerTest=`echo "${text}" | grep -E "^[ ${tab_global}]*#[ ${tab_global}]*SLICE-"`
    if [ "${markerTest}" != '' ]; then
        logErrorAndAbort "Text has slice markers:
${markerTest}"
    fi
}

sliceAddMarkersToTextR1 () {
    local returnVar_sliceAddMarkersToTextR1="$1"
    local sliceName="$2"
    local textWithoutMarkers="$3"

    assertValidName "${sliceName}"

    setDefaultFromVar textWithoutMarkers "${returnVar_sliceAddMarkersToTextR1}"

    sliceAssertNoMarkers "${textWithoutMarkers}"

    textWithMarkers_sliceAddMarkersToTextR1="# SLICE-BEGIN ${sliceName}
${textWithoutMarkers}
# SLICE-END ${sliceName}"
    assertSliceCleanMarkers textWithMarkers_sliceAddMarkersToTextR1

    setVarOrLogInfo "${returnVar_sliceAddMarkersToTextR1}" \
                    "${textWithMarkers_sliceAddMarkersToTextR1}" \
                    "Text with markers:
${textWithMarkers_sliceAddMarkersToTextR1}"
}

sliceNameReplaceR1 () {
    local sliceReplaceOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-p' -o "$1" = '--prepend' ]; then
            sliceReplaceOption='--prepend'
            shift
        elif [ "$1" = '-a' -o "$1" = '--append' ]; then
            sliceReplaceOption='--append'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVarText_sliceNameReplaceR1="$1"
    local sliceName="$2"
    local sliceText="$3"
    local textToBeProcessed="$4"

    assertValidName "${sliceName}"
    sliceAddMarkersToTextR1 sliceText "${sliceName}"
    sliceReplaceR1 ${returnVarText_sliceNameReplaceR1} "${sliceText}" "${fileNametextToBeProcessed}"
}

sliceNameReplaceInFile () {
    local sliceReplaceOption=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-p' -o "$1" = '--prepend' ]; then
            sliceReplaceOption='--prepend'
            shift
        elif [ "$1" = '-a' -o "$1" = '--append' ]; then
            sliceReplaceOption='--append'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local sliceName="$1"
    local sliceText="$2"
    local fileName="$3"

    assertValidName "${sliceName}"
    sliceAddMarkersToTextR1 sliceText "${sliceName}"
    sliceReplaceInFile ${sliceReplaceOption} "${sliceText}" "${fileName}"
}


# MAIN

eval "${showLibraryUsageIfNotSourced}"
