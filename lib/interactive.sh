#!/bin/sh
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam interactive module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

Bootstrap creature comforts for interactive shells.

EOF

}

showLoadedModules () {
    local modulesLoaded=`echo "${moduleLoadHistory_bam000}" | awk ' { print $1 } ' | sort | uniq`
    modulesLoaded=`echo ${modulesLoaded}`
    logInfo "The following modules have been loaded:

${modulesLoaded}"
}

autoLoadDisable () {
    export BAM_AUTO_LOAD_MODULES_SKIP='yes'
    logInfoInteractive "Module auto-load disabled for this shell session and children:
BAM_AUTO_LOAD_MODULES_SKIP=${BAM_AUTO_LOAD_MODULES_SKIP}"
}

autoLoadEnable () {
    unset BAM_AUTO_LOAD_MODULES_SKIP
    logInfologInfoInteractive "Module auto-load is now enabled again for this session."
}

autoLoadModules () {
    if [ "${BAM_AUTO_LOAD_MODULES_SKIP}" = 'yes' ]; then
        return
    fi
    if [ ! -f ~/.bam/autoLoadModuleList ]; then
        logDebug "does not exist: ~/.bam/autoLoadModuleList"
    else
        logDebug "Loading modules listed at ~/.bam/autoLoadModuleList"
        local moduleToLoad=''
        for moduleToLoad in \
            `cat ~/.bam/autoLoadModuleList | pipeStripStandard` \
            ; do
            use -g "${moduleToLoad}"
        done
    fi

    use -g "${USER}"
}

autoLoadAdd () {
    local moduleToAdd="$1"

    assertVariable moduleToAdd

    local modulePath
    useGetModulePathR1 modulePath tmux
    assertVar modulePath

    if [ ! -d ~/.bam ]; then
        mkdir ~/.bam
    fi
    assertDir ~/.bam

    if [ ! -f ~/.bam/autoLoadModuleList ]; then
        touch ~/.bam/autoLoadModuleList
    fi
    assertFile ~/.bam/autoLoadModuleList
    local autoLoadModuleList=`cat ~/.bam/autoLoadModuleList`
    local entryExistsTest=`echo "${autoLoadModuleList}" | grep -E '^'"${moduleToAdd}"'$'`
    autoLoadModuleList="${autoLoadModuleList}
${moduleToAdd}"
    echo "${autoLoadModuleList}" | pipeStripEmptyLines > ~/.bam/autoLoadModuleList

    use "${moduleToAdd}"
}

autoLoadRemove () {
    local moduleToRemove="$1"

    assertVariable moduleToRemove

    if [ ! -d ~/.bam ]; then
        logInfo "Auto load list does not exist yet."
        return
    fi
    assertDir ~/.bam

    if [ ! -f ~/.bam/autoLoadModuleList ]; then
        logInfo "Auto load list does not exist yet."
        return
    fi
    assertFile ~/.bam/autoLoadModuleList
    local isInLIstTest=`cat ~/.bam/autoLoadModuleList | grep -E "^${moduleToRemove}"'$'`
    if [ "${isInLIstTest}" = '' ]; then
        logInfo "Module is not in auto load list: ${moduleToRemove}"
        return
    fi
    local autoLoadModuleList=`cat ~/.bam/autoLoadModuleList | grep -E -v "^${moduleToRemove}"'$'`
    echo "${autoLoadModuleList}" | pipeStripEmptyLines > ~/.bam/autoLoadModuleList
    isInLIstTest=`cat ~/.bam/autoLoadModuleList | grep -E "^${moduleToRemove}"'$'`
    if [ "${isInLIstTest}" = '' ]; then
        logInfo "Module has been removed from auto load list: ${moduleToRemove}"
        logInfo "The module will not be automatically loaded for new shells."
    else
        logErrorAndAbort "Failed to remove module from auto load list: ${moduleToRemove}"
    fi
}

autoLoadList () {
        if [ ! -d ~/.bam ]; then
        logInfo "Auto load list does not exist yet."
        return
    fi
    assertDir ~/.bam

    if [ ! -f ~/.bam/autoLoadModuleList ]; then
        logInfo "Auto load list does not exist yet."
        return
    fi
    assertFile ~/.bam/autoLoadModuleList
    local autoLoadModuleList=''
    autoLoadModuleList=`cat ~/.bam/autoLoadModuleList | pipeStripStandard`
    logInfo "Current module auto load list:
${autoLoadModuleList}"
}

showColorsTrue () {
    # https://gist.github.com/XVilka/8346728

    if [ "${COLORTERM}" != 'truecolor' -a \
         "${COLORTERM}" != '24bit' ]; then
        logWarning "\$COLORTERM is neither 'truecolor' nor '24bit'."
    fi
    #         s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
    awk 'BEGIN{
             s="          "; s=s s s s s s s s;
             for (colnum = 0; colnum<77; colnum++) {
                 r = 255-(colnum*255/76);
                 g = (colnum*510/76);
                 b = (colnum*255/76);
                 if (g>255) g = 510-g;
                 printf "\033[48;2;%d;%d;%dm", r,g,b;
                 printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
                 printf "%s\033[0m", substr(s,colnum+1,1);
             }
             printf "\n";
         }'
}

showColors272 () {
    # Terminal capability detection is a mess. E.g iterm on my Mac:
    # $ tput colors
    # 8
    # but in the envrionment variables:
    # TERM=xterm
    # COLORTERM=truecolor

    # http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html#16-colors
    # https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences

    local colorOff="\033[0m"

    local i
    local j
    local code
    local wrap=16
    for i in $(seq 0 16); do
        for j in $(seq 1 ${wrap}); do
            let code=i*${wrap}+j-1
            local colorCode="\033[0;38;5;${code}m" # fg
            #local colorCode="\033[0;48;5;${code}m" # bg
            # Does work:
            printf "${colorCode} %3d" "${code}"
            # Does NOT work:
            #printf "%s %3d" "${colorCode}" "${code}"
        done
        printf "${colorOff}\n"
    done
}

showColors256 () {
    # This is a little more organized in output.
    local colorOff="\033[0m"

    local code=0
    local count=0
    local wide='yes'
    for wrapAt in 8 16 52 88 124 160 196 232 256 264 272; do
        if [ "${code}" -ge 256 ]; then
            break
        fi
        while true; do
            #local colorCode="\033[0;38;5;${code}m" # fg
            local colorCode="\033[0;48;5;${code}m" # bg
            # Does work:
            printf "${colorCode} %3d" "${code}"
            # Does NOT work:
            #printf "%s %3d" "${colorCode}" "${code}"
            let code+=1
            let count+=1
            if [ "${code}" = "${wrapAt}" ]; then
                printf "${colorOff}\n"
                count=0
                break
            fi
            if [ "${wide}" != 'yes' -a \
                 "${code}" -gt 16 -a \
                 "${code}" -lt 232 -a \
                 "${count}" -ge 6 ]; then
                printf "${colorOff}\n"
                count=0
            fi
        done
    done
    printf "${colorOff}\n"
}

bashPromptPretty () {
    local promptColor="$1" # Optional

    if [ "${SUDO_COMMAND}" != '' -o "${username_global}" = 'root' ]; then
        setDefault promptColor magenta
    elif [ "${SSH_CONNECTION}" != '' ]; then
        setDefault promptColor green
    else
        if [ "${isBash_global}" = 'yes' ]; then
            setDefault promptColor yellow
        else
            setDefault promptColor cyan
        fi
    fi

    local colorList="
0 black
1 red
2 green
3 yellow
4 blue
5 magenta
6 cyan
7 white
"

    #dirStyle='\w'
    dirStyle='\W'

    local colorHit=`echo "${colorList}" | grep "${promptColor}" | head -n 1`

    assertVariable colorHit "Please specify one of the following numbers or names:
${colorList}"

    local colorNumber="${colorHit%% *}"
    local colorName="${colorHit##* }"
    logDebug "Setting prompt color to: ${colorName}"

    local colorCode="\033[0;3${colorNumber}m"
    local colorOff="\033[0m"

    local promptChar='$'
    if [ "${USER}" = 'root' -o "${userid_global}" = '0' ]; then
        promptChar='#'
    fi
    local awsProfileSection=''
    if [ "${AWS_PROFILE}" != '' ]; then
        local awsText="\033[0;37m" # white
        local awsBackground="\033[0;44m" # blue
        awsProfileSection="\[${awsText}\]\[${awsBackground}\]\${AWS_PROFILE}@\${AWS_DEFAULT_REGION}\[${colorOff}\] "
    fi
    local labelSection=''
    if [ "${promptLabel_global}" != '' ]; then
        local labelText="\033[0;37m" # white
        local labelBackground="\033[0;44m" # blue
        labelSection="\[${labelText}\]\[${labelBackground}\]\${promptLabel_global}\[${colorOff}\] "
    fi
    local windowTitle="\[\033]0;\u@\h:${dirStyle}\007\]"
    local isTty=`tty | egrep '/tty[0-9]'`
    if [ "${isTty}" != '' ]; then
        # Console. There is no title to set. Use dir basename \W instead of \w
        PS1="${awsProfileSection} \[${colorCode}\]\u@\h \W ${labelSection}${promptChar} \[${colorOff}\]"
    else
        PS1='$(logFlashClear)'"${windowTitle}${awsProfileSection}\[${colorCode}\]\u@\h:${dirStyle} ${labelSection}${promptChar} \[${colorOff}\]"
    fi
    export PS1
}

promptLabel_global=''
setInteractivePromptLabel () {
    promptLabel_global="$1"
    bashPromptPretty
}

# FIXME: migrate to somewhere more central?
setExecVariables () {
    local command=''
    for command in \
        find \
        xargs \
        ; do
        local binaryToCheck=''
        for binaryToCheck in \
            `which ${command} 2>/dev/null` \
            `which g${command} 2>/dev/null`\
            ; do
            local gnuTest=`${binaryToCheck} --version 2>/dev/null | grep GNU`
            if [ "${gnuTest}" != '' ]; then
                local globalVariableNameToSet="g${command}Exec_global"
                setVar "${globalVariableNameToSet}" "${binaryToCheck}"
                break
            fi
        done
    done
}

seek () {
    local followOpt=''
        while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o  "$1" = '-follow' ]; then
            followOpt='-follow'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "Ignoring unknown option: $1"
            shift
        fi
    done

    assertVar gfindExec_global
    assertVar gxargsExec_global

    ${gfindExec_global} . -type f ${followOpt} -print0 | ${gxargsExec_global} -0 grep -i -- "$@"
}

seekf () {
    seek -follow "$@"
}

hunt () {
    local followOpt=''
        while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o  "$1" = '-follow' ]; then
            followOpt='-follow'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "Ignoring unknown option: $1"
            shift
        fi
    done

    local nameFragment="$1"

    assertVar gfindExec_global

    if [ "${nameFragment}" = '' ]; then
        ${gfindExec_global} . ${followOpt} | egrep -v '(CVS|.svn|.git)'
    else
        ${gfindExec_global} . ${followOpt} -iname '*'"${nameFragment}"'*' | egrep -v '(CVS|.svn|.git)'
    fi
}

huntf () {
    hunt -follow "$@"
}

awsCompletionEnable () {
    local awsCompleter_exec=`which aws_completer 2>/dev/null`
    if [ "${awsCompleter_exec}" = '' ]; then
        logErrorInteractive "Excutable not found in PATH: aws_completer"
        return
    fi

    # FIXME: zsh
    # see: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-completion.html
    complete -C "${awsCompleter_exec}" aws
}
awsCompletionEnableIfAvailable () {
    local awsCompleter_exec=`which aws_completer 2>/dev/null`
    if [ "${awsCompleter_exec}" = '' ]; then
        return
    fi
    logVerboseInteractive "Enabling aws completion."
}

brewCompletionEnable () {
    # https://sourabhbajaj.com/mac-setup/BashCompletion/
    if [ -f /usr/local/etc/bash_completion ]; then
        . /usr/local/etc/bash_completion
    else
        logErrorInteractive "Does not exist: /usr/local/etc/bash_completion"
        logerrorInteractive "see: https://sourabhbajaj.com/mac-setup/BashCompletion/"
    fi
}
brewCompletionEnableIfAvailable () {
    if [ ! -f /usr/local/etc/bash_completion ]; then
        return
    fi
    logVerboseInteractive "Enabling brew completion."
}

bamAutoUpdateEnable () {
    BAM_AUTO_UPDATE='yes'
    envVarSave BAM_AUTO_UPDATE
}

bamAutoUpdateEnableAlways () {
    BAM_AUTO_UPDATE='always'
    envVarSave BAM_AUTO_UPDATE
}

bamAutoUpdateDisable () {
    BAM_AUTO_UPDATE='no'
    envVarSave BAM_AUTO_UPDATE
}

bamAutoUpdateStatus () {
    envVarShowStatus BAM_AUTO_UPDATE
}

bamAutoUpdate () {
    local force='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' ]; then
            force='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "bamAutoUpdate(): Ignoring unknown option: $1"
            shift
        fi
    done

    if [ ! -t 0 ]; then
        logDebug "stdin is not a terminal"
        return
    fi
    if [ "${isInteractiveShell_global}" != 'yes' -a "${force}" != 'yes' ]; then
        return
    fi
    if [ "${BAM_AUTO_UPDATE}" = '' ]; then
        logWarningInteractive "Auto update is not set. Use one of:
bamAutoUpdateEnable         - to update once a week
bamAutoUpdateEnableAlways   - to always update
bamAutoUpdateDisable        - to never update automatically"
        return
    fi
    if [ "${BAM_AUTO_UPDATE}" = 'no' -a "${force}" != 'yes' ]; then
        logDebugInteractive "Auto update is disabled."
        return
    fi
    if [ "${BAM_AUTO_UPDATE}" = 'always' ]; then
        logInfoInteractive "Auto update always is enabled."
        force='yes'
    fi
    setDefault envVarDir_global ~/.bam/envVar
    qualifyPathR1 envVarDir_global
    # We are using last modified time of the vairable save file
    # to store when we updated last.
    # For testing, we can set the modified time back in time:
    # touch -d '2020-01-01T01:01:01' ~/.bam/envVar/BAM_AUTO_UPDATE
    local envVarFileName="${envVarDir_global}/BAM_AUTO_UPDATE"
    if [ "${force}" != 'yes' ]; then
        if [ ! -f "${envVarFileName}" ]; then
            return
        fi

        use file
        local ageInS=''
        fileGetAgeInSecondsR1 ageInS "${envVarFileName}"

        local autoUpdateIntervalInS="${BAM_AUTO_UPDATE_INTERVAL_IN_S}"
        #let autoUpdateIntervalInS=60*60*24 # one day in seconds
        let autoUpdateIntervalInSDefault=60*60*24*7 # seven days in seconds
        setDefault autoUpdateIntervalInS "${autoUpdateIntervalInSDefault}"

        if [ ${ageInS} -lt ${autoUpdateIntervalInS} ]; then
            logDebugInteractive "Skipping auto update. Last update was recent enough: ${ageInS} s"
            return
        fi
        logInfoInteractive "It's time for auto update."
    fi
    spushd "${bamDir_global}"
    local isRemoteRepoSsh=`git remote -v | grep '@' | grep -v http`
    spopd
    if [ "${isRemoteRepoSsh}" != '' ]; then
        if [ "${hostKernelName_global}" = 'Darwin' -a "${SSH_CLIENT}" = '' ]; then
            logDebugInteractive "Skipping agent load on local Darwin shell."
        else
            if [ "${isInteractive_global}" = 'yes' ]; then
                use ssh
                sshAgent
            else
                return
            fi
        fi
    fi
    local hadUpdates=''
    bamUpdateR1 hadUpdates
    if [ "${BAM_AUTO_UPDATE}" != '' ]; then
        envVarSave BAM_AUTO_UPDATE
    fi
    if [ "${hadUpdates}" = 'yes' ]; then
        reExecute
    fi
}

bamEdit () {
    assertFile "${bam_global}"
    if [ "${BAM_EDITOR}" != '' ]; then
        logDebug "\"${BAM_EDITOR}\" ${BAM_EDITOR_OPTIONS} \"${m}\""
        "${BAM_EDITOR}" ${BAM_EDITOR_OPTIONS} "${bam_global}"
    elif [ "${EDITOR}" != '' ]; then
        "${EDITOR}" "${bam_global}"
    else
        vim "${bam_global}"
    fi
}

interactiveSrcHighlightEnableIfAvailable () {
    local hiliteTest=`which src-hilite-lesspipe.sh 2>/dev/null`
    if [ "${hiliteTest}" != '' ]; then
        export LESSOPEN="| ${hiliteTest} %s"
        export LESS='-FXR'
        # From the less man page:
        # -F or --quit-if-one-screen
        #      Causes less to automatically exit if the entire file can be displayed on the first screen.
        # -X or --no-init
        #      Disables sending the termcap initialization and
        #      deinitialization strings to the terminal.  This is
        #      sometimes desirable if the deinitialization string does
        #      something unnecessary, like clearing the screen.
        # -R or --RAW-CONTROL-CHARS
        #      Like -r, but only ANSI "color" escape sequences and OSC
        #      8 hyperlink sequences are output in "raw" form.  Unlike
        #      -r, the screen appearance is maintained correctly,
        #      provided that there are no escape sequences in the file
        #      other than these types of escape sequences.  Color
        #      escape sequences are only supported when the color is
        #      changed within one line, not across lines.  In other
        #      words, the beginning of each line is assumed to be
        #      normal (non-colored), regardless of any escape
        #      sequences in previous lines.  For the purpose of
        #      keeping track of screen appearance, these escape
        #      sequences are assumed to not move the cursor.
    fi
}

# MAIN

interactiveMainRemoteBootstrap () {

    moduleLoading='interactive.sh' # Required for sh modules.
    eval "${showLibraryUsageIfNotSourced}"

    if [ "`which fortune 2>/dev/null`" != '' ]; then
        echo
        fortune -s
        echo
    fi

    bashPromptPretty

    setExecVariables

    # 2020-10-30 Both of these do not seem to work correctly.
    # Not sure whether this has to do with some of the environment
    # variables that we are setting in BAM?
    awsCompletionEnableIfAvailable
    brewCompletionEnableIfAvailable

    interactiveSrcHighlightEnableIfAvailable
}

eval "${showLibraryUsageIfNotSourced}"

# Outside of main since we don't want to execute this on remote bootstrap:
autoLoadModules
showLoadedModules

interactiveMainRemoteBootstrap

# Outside of main since we don't want to execute this on remote bootstrap:
bamAutoUpdate
