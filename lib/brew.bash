#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam Mac homebrew module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

Homebrew helper functions.

EOF

}

brewInstallDownstream () {
    local pythonLatestVersion=`
              pyenv install --list \
              | egrep '^ [ ]*[1-9][0-9]*\.[1-9][0-9]*\.[1-9][0-9]*$' \
              | tail -n 1`
    pyenv install "${pythonLatestVersion}"
    pyenv versions
    pyenv local "${pythonLatestVersion}"
}

brewInstallDefaultPackages () {
    local disabledPackages="
openssh # does not support ssh/config option usekeychain
"
    local packages="
# Core utilities
coreutils
findutils

bash
bash-completion
binutils
diffutils
ed
findutils
file-formula
gawk
git
gnu-getopt
gnu-indent
gnu-sed
gnu-tar
gnu-which
gnutls
gpg
gpatch
grep
gzip
jq
less
m4
make
nano
python
rsync
screen
# source-highlight
svn
unzip
vim
watch
wdiff
wget

# Making shell life nicer
ffmpeg
htop
imagemagick
mmv
prettyping
pyenv
lolcat
figlet
fortune
zoxide
fzf
"
    local package=''
    while read package; do
        brewInstallPackage "${package}"
    done < <( echo "${packages}" | pipeStripStandard )
    logInfo "Done"
}

brewIsPackageInstalledR1 () {
    local returnVar_brewIsPackageInstalledR1="$1"
    local package="$2"

    assertVar package

    local rc=''
    local isPackageInstalledTest=''
    isPackageInstalledTest=`brew list "${package}" 2>&1`; rc=$?
    if [ "${rc}" = '0' ]; then
        setVarOrLogInfo "${returnVar_brewIsPackageInstalledR1}" 'yes'
        return
    fi
    setVarOrLogInfo "${returnVar_brewIsPackageInstalledR1}" 'no'

}

brewAssertPackageIsInstalled () {
    local package="$1"
    local errorMessage="$2" # optional

    assertVar package

    local isPackageInstalled=''
    brewIsPackageInstalledR1 isPackageInstalled "${package}"
    if [ "${isPackageInstalled}" != 'yes' ]; then
        setDefault errorMessage "Package is not installed: ${package}"
        logErrorAndAbort "${errorMessage}"
    fi
}

brewInstallPackage () {
    local package="$1"
    local rc=''
    local isPackageInstalledTest=''
    isPackageInstalledTest=`brew list "${package}" 2>&1`; rc=$?
    if [ "${rc}" = '0' ]; then
        logFlash "Already installed: ${package}"
        continue
    fi
    logInfo "Installing: ${package}"
    brew install "${package}" </dev/null
    brewAssertPackageIsInstalled "${package}" "Unable to install package: ${package}"
}

brewInstallBrew () {
    local isBrewInstalled=`which brew 2>/dev/null`
    if [ "${isBrewInstalled}" != '' ]; then
        logInfo "Brew is already installed at: ${isBrewInstalled}"
    else
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    fi
    pathAddOverride
}

brewInstall () {
    brewInstallBrew
    brewInstallDefaultPackages
    brewInstallDownstream
}

brewFixPerms () {
    assertDir "${brewRootDir_global}"

    # https://stackoverflow.com/questions/26647412/homebrew-could-not-symlink-usr-local-bin-is-not-writable
    assertDir "${brewRootDir_global}/bin"
    logInfo "This might ask for your sudo password."
    sudo chown -R `whoami`:admin "${brewRootDir_global}/bin"
    if [ ! -d "${brewRootDir_global}/share" ]; then
        sudo mkdir "${brewRootDir_global}/share"
    fi
    if [ ! -d "${brewRootDir_global}/opt" ]; then
        sudo mkdir "${brewRootDir_global}/opt"
    fi
    assertDir "${brewRootDir_global}/share"
    assertDir "${brewRootDir_global}/opt"
    sudo chown -R `whoami`:admin "${brewRootDir_global}/share"
    sudo chown -R `whoami`:admin "${brewRootDir_global}/opt"
}

brewEnableGnuBinaries () {
    assertDir "${brewRootDir_global}"

    local binPath=''
    for binPath in \
        "${brewRootDir_global}/opt/findutils/libexec/gnubin" \
        "${brewRootDir_global}/opt/file-formula/bin" \
        "${brewRootDir_global}/opt/unzip/bin" \
        ; do
        if [ -d "${binPath}" ]; then
            PATH="${binPath}:$PATH"
        fi
    done
    local manPath=''
    for manPath in \
        "${brewRootDir_global}/opt/coreutils/libexec/gnuman" \
        ; do
        if [ -d "${manPath}" ]; then
            MANPATH="${manPath}:$MANPATH"
        fi
    done
}

brewUpdate () {
    # https://docs.brew.sh/FAQ
    logInfo "First we will update homebrew itself."
    logInfo "Afterwards we will upgrade outdated packages."
    logInfo "Updating homebrew itself."
    brew update
    local outdated=`brew outdated 2>&1`
    if [ "${outdated}" = '' ]; then
        logInfo "All packages are up to date."
    else
        logInfo "These packages are outdated:
${outdated}"
        logInfo "Upgrading outdated pakages."
        brew upgrade
    fi
}

brewUpgrade () {
    brewUpdate
}

brewCaskListInstalled () {
    brew list --cask
}

brewCaskSaveListAddInstalled () {
    if [ ! -d ~/.bam ]; then
        mkdir ~/.bam
    fi
    assertDir ~/.bam
    local brewCaskSavedList=''

    if [ ! -f ~/.bam/brewCaskSavedList ]; then
        touch ~/.bam/brewCaskSavedList
    fi
    assertFile ~/.bam/brewCaskSavedList
    local brewCaskSavedList=`cat ~/.bam/brewCaskSavedList | pipeStripStandard`
    if [ "${brewCaskSavedList}" = '' ]; then
        logInfo "No casks have been saved yet."
    else
        logDebug "Currently saved casks:
${brewCaskSavedList}"
    fi
    local currentlyInstalledList=`
        brew list --cask \
            | sed 's/[ '"${tab_global}"']/\n/g' \
            | pipeStripStandard`
    if [ "${currentlyInstalledList}" = '' ]; then
        logInfo "No casks have been installed yet."
        return
    else
        logDebug "Currently installed casks:
${currentlyInstalledList}"
    fi
    local brewCaskMergedList="${brewCaskSavedList}
${currentlyInstalledList}"
    brewCaskMergedList=`
        echo "${brewCaskMergedList}" | sort | uniq | pipeStripStandard`
    if [ "${brewCaskMergedList}" = '' ]; then
        logInfo "Noting saved and nothing installed yet."
    elif [ "${brewCaskMergedList}" = "${brewCaskSavedList}" ]; then
        logInfo "Saved list is up to date already."
    else
        logInfo "Saving merged list:
${brewCaskMergedList}"
        echo "${brewCaskMergedList}" > ~/.bam/brewCaskSavedList
    fi
}

brewCaskListSaved () {
    if [ ! -f ~/.bam/brewCaskSavedList ]; then
        logInfo "There is no saved list."
        return
    fi
    local brewCaskSavedList=`cat ~/.bam/brewCaskSavedList | pipeStripStandard`
    if [ "${brewCaskSavedList}" = '' ]; then
        logInfo "The saved list is empty"
        return
    fi
    logInfo "Saved casks:
${brewCaskSavedList}"
}

brewCaskListInstalled () {
    local currentlyInstalledList=`
        brew list --cask \
            | sed 's/[ '"${tab_global}"']/\n/g' \
            | pipeStripStandard`
    if [ "${currentlyInstalledList}" = '' ]; then
        logInfo "There are no installed casks."
        return
    fi
    logInfo "Installed casks:
${currentlyInstalledList}"
}

brewCaskDiffSavedVsInstalled () {
    if [ ! -f ~/.bam/brewCaskSavedList ]; then
        logInfo "There is no saved list."
        return
    fi
    local brewCaskSavedList=`cat ~/.bam/brewCaskSavedList | pipeStripStandard`
    local currentlyInstalledList=`
        brew list --cask \
            | sed 's/[ '"${tab_global}"']/\n/g' \
            | pipeStripStandard`
    local diffs=`diff ~/.bam/brewCaskSavedList <( echo "${currentlyInstalledList}" ) | grep -E "^[<>]"`
    if [ "${diffs}" = '' ]; then
        logInfo "Saved and installed casks are the same."
    else
        logInfo "Items will show as follows:
> Installed, but not saved yet
< Saved, but not installed yet"
        logInfo "${diffs}"
    fi
}

brewCaskInstallSaved () {
    assertFile ~/.bam/brewCaskSavedList "No casks have been saved."

    local caskToInstall=''
    local rc=''
    while read caskToInstall; do
        if [ "${caskToInstall}" = '' ]; then
            continue
        fi
        brewCaskInstall "${caskToInstall}"
    done < <( cat ~/.bam/brewCaskSavedList | pipeStripStandard )
}

brewCaskInstall () {
    local caskToInstall="$1"
    local rc=''

    assertVar caskToInstall
    brew list --cask "${caskToInstall}" 1>/dev/null 2>&1; rc=$?
    if [ "${rc}" = '0' ]; then
        logInfo "Already installed: ${caskToInstall}"
        continue
    fi
    logInfo "Installing: ${caskToInstall}"
    # --[no-]quarantine Disable/enable quarantining of downloads (default: enabled).
    # This allows installation of the cask even when the software package has not been signed.
    brew install --no-quarantine --cask "${caskToInstall}"
}

# MAIN

brewMainRemoteBootstrap () {
    machineHardware=`uname -m`
    if [ "${machineHardware}" = 'arm64' ]; then
        # Brew install location for Apple silicon
        brewRootDir_global=/opt/homebrew
    elif [ "${machineHardware}" = 'x86_64' ]; then
        # Brew install location for Intel silicon
        brewRootDir_global=/usr/local
    else
        logInfoInteractive "Don't know how to set homebrew path on: ${machineHardware}"
        return
    fi

    pathAddOverride "${brewRootDir_global}/bin"

    ## brewEnableGnuBinaries
}

brewIsTapInstalledR1 () {
    local returnVar_brewIsTapInstalledR1="$1"
    local tap_brewIsTapInstalledR1="$2"

    assertVar tap_brewIsTapInstalledR1

    local isTabKnownTest=`brew tap | grep -E "^${tap_brewIsTapInstalledR1}"'$'`
    if [ "${isTabKnownTest}" != '' ]; then
        setVarOrLogInfo "${returnVar_brewIsTapInstalledR1}" 'yes'
        return
    fi
    setVarOrLogInfo "${returnVar_brewIsTapInstalledR1}" 'no'
}

brewAssertTapIsInstalled () {
    local tap="$1"
    local errorMessage="$2" # Optional
    assertVar tap

    local isTapInstalled=''
    brewIsTapInstalledR1 isTapInstalled "${tap}"

    if [ "${isTapInstalled}" != 'yes' ]; then
        setDefault errorMessage "Tap is not installed: ${tap}"
        logErrorAndAbort "${errorMessage}"
    fi
}

brewTapInstall () {
    local tap="$1"

    assertVar tap

    local isTapInstalled=''
    brewIsTapInstalledR1 isTapInstalled "${tap}"
    if [ "${isTapInstalled}" = 'yes' ]; then
        logInfo "Tap is already installed: ${tap}"
        return
    fi
    logInfo "Installing tap: ${tap}"
    brew tap "${tap}"
    brewAssertTapIsInstalled "Unable to install tap: ${tap}"
}

eval "${showLibraryUsageIfNotSourced}"

brewMainRemoteBootstrap

