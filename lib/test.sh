#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam test module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

testConfirm () {
    local testDescription="$1"
    local callerIndex="$2" # Optional. Defaults to 0

    if [ "${isBash_global}" = 'yes' -o "${isSimulatedSh_global}" = 'yes' ]; then
        if [ "${callerIndex}" = '' ]; then
            local callerFunction=`caller 0`
            callerFunction="${callerFunction% *}"
            callerFunction="${callerFunction#* }"
            if [ "${callerFunction#testAssert}" != "${callerFunction}" ]; then
                callerIndex=1
            fi
        fi
    fi
    setDefault callerIndex 0
    #logInfo "SUCCESS line `caller ${callerIndex} 2>/dev/null` - ${testDescription}"
    logFlash "SUCCESS line `caller ${callerIndex} 2>/dev/null` - ${testDescription}"
    let testConfirmedCount_global+=1
}

testFail () {
    local testDescription="$1"
    local callerIndex="$2" # Optional. Defaults to 0

    if [ "${isBash_global}" = 'yes' -o "${isSimulatedSh_global}" = 'yes' ]; then
        if [ "${callerIndex}" = '' ]; then
            local callerFunction=`caller 0`
            callerFunction="${callerFunction% *}"
            callerFunction="${callerFunction#* }"
            if [ "${callerFunction#testAssert}" != "${callerFunction}" ]; then
                callerIndex=1
            fi
        fi
    fi
    setDefault callerIndex 0
    logError "FAIL    line `caller ${callerIndex} 2>/dev/null`  - ${testDescription}"
    let testFailed_global+=1

    local skip=''
    let skip=${callerIndex}
    if [ "${isBash_global}" = 'yes' ]; then
        getStackTraceR1 '' ${skip}
    fi
}

testAssertEqualValue () {
    local value1="$1"
    local value2="$2"
    local testName="$3"
    if [ "$1" = "$2" ]; then
        testConfirm "${testName}"
    else
        logError "'$1' !=
'$2'"
        if [ "`which xxd 2>/dev/null`" != '' ]; then
            logError "`echo "$1" | xxd`"
            logError vs
            logError "`echo "$2" | xxd`"
        fi
        testFail "${testName}"
        #rm /tmp/testAssertEqualValue.1 /tmp/testAssertEqualValue.2
        printf '%s\n' "$1" >/tmp/testAssertEqualValue.1
        printf '%s\n' "$2" >/tmp/testAssertEqualValue.2
        #ksdiff /tmp/testAssertEqualValue.1 /tmp/testAssertEqualValue.2
        #read
        #rm /tmp/testAssertEqualValue.1 /tmp/testAssertEqualValue.2
    fi
}

testAssertNotEmpty () {
    local value="$1"
    local testName="$2"
    if [ "$1" != '' ]; then
        testConfirm "${testName}"
    else
        testFail "${testName}"
    fi
}

testAssertEmpty () {
    local value="$1"
    local testName="$2"
    if [ "${value}" = '' ]; then
        testConfirm "${testName}"
    else
        testFail "${testName}"
    fi
}

testAssertVariable () {
    local varName="$1"
    local testName="$2"

    local value=''
    getVarR1 value "${varName}"
    if [ "${value}" != '' ]; then
        testConfirm "${testName}"
    else
        logError "Variable is not defined: ${varName}"
        testFail "${testName}"
    fi
}

testAssertContainsRegex () {
    local value="$1"
    local regex="$2"
    local testName="$3"

    local regexTest=`echo "${value}" | egrep "${regex}"`
    if [ "${regexTest}" != '' ]; then
        testConfirm "${testName}"
    else
        logError "Value does not contain regex ${regex}: ${value}"
        testFail "${testName}"
    fi
}

testAssertDoesNotContainRegex () {
    local value="$1"
    local regex="$2"
    local testName="$3"
    local regexTest=`echo "${value}" | egrep "${regex}"`
    if [ "${regexTest}" = '' ]; then
        testConfirm "${testName}"
    else
        logError "Value does contain regex ${regex}: ${value}"
        testFail "${testName}"
    fi
}

runTestScript () {
    local testScript="$1"

    assertFile "${testScript}"

    logInfo "Sourcing test script: ${testScript}"
    source "${testScript}"
    let testScriptCount_global+=1

    local testFunctions=`
        cat "${testScript}" \
            | egrep '^[a-zA-Z][a-zA-Z0-9_]* \(\) \{' \
            | sed 's/ (.*$//' \
            | egrep '_test$' \
            `

    local testFunction=''
    for testFunction in ${testFunctions}; do
        ${testFunction}
        let testFunctionCount_global+=1
    done
}

resetTestCounters () {
    testScriptCount_global=0
    testFunctionCount_global=0
    testConfirmedCount_global=0
    testFailed_global=0
}
showTestStats () {
    logInfo "Test scripts:    `printf '%4d' ${testScriptCount_global}`
     functions:  `printf '%4d' ${testFunctionCount_global}`
     confirmed:  `printf '%4d' ${testConfirmedCount_global}`
     failed:     `printf '%4d' ${testFailed_global}`"
}

runTests () {
    local userSpecifiedTestsToRun="$*"

    resetTestCounters
    local testScript=''
    local testScripts=''
    if [ "${userSpecifiedTestsToRun}" != '' ]; then
        logInfo "Running user specified tests only."
        local userSpecifiedTest=''
        for userSpecifiedTest in ${userSpecifiedTestsToRun}; do
            local found='no'
            local expandedTests=${bamDir_global}/test/${userSpecifiedTest}_test.*sh
            for testScript in ${userSpecifiedTest} ${expandedTests}; do
                if [ -f "${testScript}" ]; then
                    found='yes'
                    logInfo "Selecting test: ${userSpecifiedTest}"
                    testScripts="${testScripts} ${testScript}"
                fi
            done
            if [ "${found}" = 'no' ]; then
                logWarning "No test found for: ${userSpecifiedTest}"
            fi
        done
    else
        logInfo "Running all relevant tests."
        testScripts=`ls ${bamDir_global}/test/*_test.*sh 2>/dev/null`
    fi
    for testScript in ${testScripts}; do
        local testScriptType="${testScript##*.}"
        if [ "${testScriptType}" = 'sh' ]; then
            # Run .sh scripts in sh and bash.
            runTestScript "${testScript}"
        elif [ "${testScriptType}" = 'bash' ]; then
            if [ "${isBash_global}" = 'yes' -o "${isZsh_global}" = 'yes' ]; then
                runTestScript "${testScript}"
            else
                logInfo "Skipping bash test script on sh: ${testScript}"
            fi
        else
            logInfo "Ignoring script type ${testScriptType}: ${testScript}"
        fi
    done
    showTestStats
}

# MAIN

moduleLoading='test.sh' # Required for sh modules.
eval "${showLibraryUsageIfNotSourced}"
