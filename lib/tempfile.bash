#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam module template - copy and adapt

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

FIXME

EOF

}

use trap

getTempfileR1 () {
    # This overrides the function of the same name in bam.sh.

    local returnVarTempfile="$1"

    assertVariable returnVarTempfile

    local tempfile_getTempfileR1=`mktemp /tmp/bam_temp_file_${scriptName_global}.XXXXXX` \
                || logErrorAndAbort "Unable to create temp file."

    registerExitCode "rm '${tempfile_getTempfileR1}'"
    setVar "${returnVarTempfile}" "${tempfile_getTempfileR1}"
}

deleteTempfile () {
    local tempfile="$1"
    if [ -f "${tempfile}" ]; then
        rm "${tempfile}"
    else
        logInfo "Temporary file has already been deleted: ${tempfile}"
    fi
    if [ -f "${tempfile}" ]; then
        logErrorAndAbort "Unable to delete temporary file: ${tempfile}"
    fi
    unregisterExitCode "rm '${tempfile}'"
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"
