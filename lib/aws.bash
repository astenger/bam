#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam aws cli helpers

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

Various aws cli helper functions.

EOF

}

awsSetDefaultProfile () {
    awsSsoSetDefaultProfile "$@"
}

awsSsoSetDefaultProfile () {
    local grepFilter="$1"
    assertFile ~/.aws/config "No Profiles configured."
    local availableProfiles=''
    awsSsoGetKnownProfilesR1 availableProfiles
    assertVar availableProfiles "No Profiles configured."

    if [ "${grepFilter}" != '' ]; then
        availableProfiles=`echo "${availableProfiles}" | grep -E "${grepFilter}"`
        assertVar availableProfiles "No profiles matching '${grepFilter}' found."
    fi
    local PS3_OLD="${PS3}"
    PS3="Enter a number: "

    local selectedProfile=''
    local selectedNumber=''
    local availableProfileCount=`echo "${availableProfiles}" | wc -l`
    if [ ${availableProfileCount} -eq 1 ]; then
        selectedProfile="${availableProfiles}"
    else
        select selectedProfile in ${availableProfiles}; do
            logDebug "User selection: $REPLY"
            if [ "${selectedProfile}" != '' ]; then
                logInfo "Selected profile: ${selectedProfile}"
                break
            fi
        done
    fi

    PS3="${PS3_OLD}"
    AWS_DEFAULT_PROFILE="${selectedProfile}"
    export AWS_DEFAULT_PROFILE
    logInfoVar AWS_DEFAULT_PROFILE
}

awsGetAvailableRegionsR1 () {
    local returnVar_awsGetAvailableRegionsR1="$1"
    local grepFilter="$2"

    # AWS service list by region in json":
    # https://api.regional-table.region-services.aws.a2z.com/index.json
    local availableRegions_awsGetAvailableRegionsR1=`
        curl -s https://api.regional-table.region-services.aws.a2z.com/index.json \
            | jq -r '.prices[].attributes|select(."aws:serviceName" == "Amazon Route 53") | ."aws:region"' \
            | sort
        `
    assertVar availableRegions_awsGetAvailableRegionsR1 "Unable to fetch regions."
    if [ "${grepFilter}" != '' ]; then
        availableRegions_awsGetAvailableRegionsR1=`echo "${availableRegions_awsGetAvailableRegionsR1}" | grep -E "${grepFilter}"`
        assertVar availableRegions_awsGetAvailableRegionsR1 "No regions matching '${grepFilter}' found."
    fi
    setVarOrLogInfo "${returnVar_awsGetAvailableRegionsR1}" "${availableRegions_awsGetAvailableRegionsR1}"
}

awsSetDefaultRegion () {
    local grepFilter="$1"

    local availableRegions
    awsGetAvailableRegionsR1 availableRegions "${grepFilter}"

    local PS3_OLD="${PS3}"
    PS3="Enter a number: "

    local selectedRegion=''
    local selectedNumber=''
    local availableRegionCount=`echo "${availableRegions}" | wc -l`
    if [ ${availableRegionCount} -eq 1 ]; then
        selectedRegion="${availableRegions}"
    else
        select selectedRegion in ${availableRegions}; do
            logDebug "User selection: $REPLY"
            if [ "${selectedRegion}" != '' ]; then
                logInfo "Selected region: ${selectedRegion}"
                break
            fi
        done
    fi

    PS3="${PS3_OLD}"
    AWS_DEFAULT_REGION="${selectedRegion}"
    export AWS_DEFAULT_REGION
    logInfoVar AWS_DEFAULT_REGION
}

awsSetAndSaveDefaultProfileAndRegion () {
    awsSetAndSaveDefaultSsoProfileAndRegion "$@"
}

awsSetAndSaveDefaultSsoProfileAndRegion () {
    if [ "${AWS_DEFAULT_PROFILE}" = '' ]; then
        awsSsoSetDefaultProfile
    fi
    if [ "${AWS_DEFAULT_REGION}" = '' ]; then
        awsSetDefaultRegion
    fi
    export AWS_DEFAULT_PROFILE
    envVarSave AWS_DEFAULT_PROFILE

    export AWS_DEFAULT_REGION
    envVarSave AWS_DEFAULT_REGION
}

awsAssertDefaultRegion () {
    assertEnvVarWithValue AWS_DEFAULT_REGION
}

awsShowEnv () {
    logInfo "`env | grep AWS`"
    if [ "${AWS_DEFAULT_PROFILE}" = '' ]; then
        logWarning "Env var not set: AWS_DEFAULT_PROFILE"
        return
    fi
    if [ -f ~/.aws/config ]; then
        use file
        local profileSection=''
        fileGetSectionR1 profileSection ~/.aws/config "^\[profile[ ${tab_global}]+${AWS_DEFAULT_PROFILE}\]" "^\[.*\]"
        logInfo "Profile section in ~/.aws/config
${profileSection}"
        local ssoSessionName=`
                  echo "${profileSection}" \
                  | pipeStripStandard \
                  | grep -E 'sso_session[ ${tab_global}]+=' \
                  | sed 's/.*=//'`
        stripR1 ssoSessionName
        if [ "${ssoSessionName}" != '' ]; then
            local ssoSessionSection=''
            fileGetSectionR1 ssoSessionSection ~/.aws/config "^\[sso-session[ ${tab_global}]+${ssoSessionName}\]" "^\[.*\]"
            logInfo "SSO session section in ~/.aws/config
${ssoSessionSection}"
        fi
    else
        logWarning "Does not exist: ~/.aws/config"
    fi
}

awsInstallAwsCliViaBrew () {
    brew install awscli
}

awsInstallAwsNukeViaBrew () {
    brew install aws-nuke
}


awsInstallSessionManagerViaBrew () {
    brew install session-manager-plugin
}

awsInstallAwsCliAllUsersMac () {
    local force='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--foce' ]; then
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    if [ "${force}" != 'yes' ]; then
        logErrorAndAbort "This is the package based install.
Please use the brew based install instead: awsInstallAwsCliViaBrew
If you really want to do the packge based install use option --force."
    fi

    spushd ~/Downloads
    curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
    assertFile AWSCLIV2.pkg
    logInfo "Installing AWSCLIV2.pkg"
    logInfo "This might ask for your sudo password."
    sudo installer -pkg AWSCLIV2.pkg -target /
    removeFileIfExists AWSCLIV2.pkg
    local isAwsInPath=`which aws 2>/dev/null`
    if [ "${isAwsInPath}" = '' ]; then
        logError "Not in PATH: aws"
    else
        logInfo "aws version:
`aws --version`"
    fi
}

awsUninstallAwsCliAllUsersMac () {
    assertInteractive
    local isPackageInstalledTest=`pkgutil --pkgs | grep -E '^com.amazon.aws.cli2$'`
    if [ "${isPackageInstalledTest}" = '' ]; then
        logWarning "Package does not seem to be installed: com.amazon.aws.cli2"
    fi
    if [ -d /usr/local/aws-cli ]; then
        logInfo "Removing: /usr/local/aws-cli"
        logInfo "This might ask for your sudo password."
        sudo rm -rf /usr/local/aws-cli
    else
        logWarning "Does not exist: /usr/local/aws-cli"
    fi
    if [ "${isPackageInstalledTest}" != '' ]; then
        logInfo "Forgetting package: com.amazon.aws.cli2"
        logInfo "This might ask for your sudo password."
        sudo pkgutil --forget com.amazon.aws.cli2
    fi
}
awsUninstallSessionManagerMac () {
    assertInteractive
    local isPackageInstalledTest=`pkgutil --pkgs | grep -E '^session-manager-plugin$'`
    if [ "${isPackageInstalledTest}" = '' ]; then
        logWarning "Package does not seem to be installed: session-manager-plugin"
    fi
    if [ -f /Library/LaunchDaemons/SessionManagerPlugin.plist ]; then
        logInfo "Removing: /Library/LaunchDaemons/SessionManagerPlugin.plist"
        logInfo "This might ask for your sudo password."
        sudo rm -rf /Library/LaunchDaemons/SessionManagerPlugin.plist
    else
        logWarning "Does not exist: /Library/LaunchDaemons/SessionManagerPlugin.plist"
    fi
    if [ -d /usr/local/sessionmanagerplugin ]; then
        logInfo "Removing: /usr/local/sessionmanagerplugin"
        logInfo "This might ask for your sudo password."
        sudo rm -rf /usr/local/sessionmanagerplugin
    else
        logWarning "Does not exist: /usr/local/sessionmanagerplugin"
    fi
    if [ "${isPackageInstalledTest}" != '' ]; then
        logInfo "Forgetting package: session-manager-plugin"
        logInfo "This might ask for your sudo password."
        sudo pkgutil --forget session-manager-plugin
    fi
}
awsInstallSessionManagerMac () {
    local force='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--foce' ]; then
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    if [ "${force}" != 'yes' ]; then
        logErrorAndAbort "This is the package based install.
Please use the brew based install instead: awsInstallSessionManagerViaBrew
If you really want to do the packge based install use option --force."
    fi

    # https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html
    local isInstalled=`pkgutil --pkgs | grep -i session-manager-plugin`
    if [ "${isInstalled}" != '' ]; then
        local availableVersion=`curl https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html 2>/dev/null | egrep '<td>[0-9]+.[0-9]+.[0-9]+.[0-0]+</td>' | head -n 1 | sed 's/<[^>]*>//g' | pipeStripStandard`
        if [ "${availableVersion}" = '' ]; then
            logInfo "Unable to determine available version."
            logInfo "Cowardly refusing to upgrade."
        else
            local installedVersion=`session-manager-plugin --version 2>/dev/null`
            if [ "${availableVersion}" = "${installedVersion}" ]; then
                logInfo "We have the latest version installed."
            else
                logInfo "There is a newer version available."
                logInfo "Installed: ${installedVersion}"
                logInfo "Available: ${availableVersion}"
            fi
        fi
    fi

    if [ "${isInstalled}" = '' ]; then
        local macPackages=`curl 2>/dev/null https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html | grep 'session-manager-downloads' | grep latest | grep mac | grep pkg | sed 's/.*curl "//' | sed 's/".*//'`
        local downloadUrl=''
        if [ "`uname -m`" = 'arm64' ]; then
            downloadUrl=`echo "${macPackages}" | grep '/mac_arm64/'`
        else
            downloadUrl=`echo "${macPackages}" | grep '/mac/'`
        fi
        assertVar downloadUrl "Unable to determine download URL for package"
        logInfo "Downloading session manager plugin from:
${downloadUrl}"
        curl "${downloadUrl}" -o ~/"session-manager-plugin.pkg"
        logInfo "Installing session manager plugin."
        logInfo "This will ask for your sudo password."
        sudo installer -pkg ~/session-manager-plugin.pkg -target /
    else
        logInfo "Session manager plugin is already installed."
    fi
    if [ -L /usr/local/bin/session-manager-plugin ]; then
        logInfo "Link already exists:
`ls -lah /usr/local/bin/session-manager-plugin`"
    elif [ -e /usr/local/bin/session-manager-plugin ]; then
        logWarning "Exists but is not a link: /usr/local/bin/session-manager-plugin"
    else
        sudo ln -s /usr/local/sessionmanagerplugin/bin/session-manager-plugin /usr/local/bin/session-manager-plugin

        if [ ! -L /usr/local/bin/session-manager-plugin ]; then
            logError "Failed to create link:
/usr/local/bin/session-manager-plugin ->
/usr/local/sessionmanagerplugin/bin/session-manager-plugin"
        else
            logInfo "Created link:
`ls -lah /usr/local/bin/session-manager-plugin`"
        fi
    fi
    local isSessionManagerPluginInPath=`which session-manager-plugin 2>/dev/null`
    if [ "${isSessionManagerPluginInPath}" = '' ]; then
        logError "Not in PATH: session-manager-plugin"
    else
        logInfo "Session manager version:
`session-manager-plugin --version`"
    fi
    removeFileIfExists ~/session-manager-plugin.pkg
}

awsAssertSessionManagerPluginIsInstalled () {
    if [ "`which session-manager-plugin 2>/dev/null`" = '' ]; then
        logErrorAndAbort "Not in PATH: session-manager-plugin"
    fi
}

awsSsmDirect () {
    local instanceId="$1"
    assertVar instanceId

    awsAssertSessionManagerPluginIsInstalled
    aws ssm start-session --target "${instanceId}"
}

awsSsmSsh () {
    local passOptions=''
    local selectByName='no'
    local selectName=''
    local fetchHostKey='no'
    local waitForInstance='no'
    while [ "${1:0:1}" = "-" ]; do
        if [ "$1" = "--name" ]; then
            selectByName="yes"
            shift
            selectName="$1"
            shift
        elif [ "$1" = "--fetchHostKey" ]; then
            fetchHostKey="yes"
            shift
        elif [ "$1" = "--wait" ]; then
            waitForInstance="yes"
            # It makes sense to fetch the host key for a new instance.
            fetchHostKey="yes"
            shift
        elif [ "$1" = "-o" ]; then
            shift
            passOptions="${passOptions} -o $1"
            shift
        elif [ "$1" = "-R" ]; then
            shift
            passOptions="${passOptions} -R $1"
            shift
        elif [ "$1" = "-L" ]; then
            shift
            passOptions="${passOptions} -L $1"
            shift
        elif [ "$1" = "--" ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    # https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-getting-started-enable-ssh-connections.html

    # You still need to point it to the correct key and specify the
    # correct user.  You can hard code these in ~/.ssh/config if you
    # wish, but with ephemeral cloud deployments this is manual
    # maintenance.

    # Example usage:
    # awsSsmSsh -i ~/.ssh/id_ed25519 ec2-user@i-096372163ae482f3c

    proxyCommand='sh -c "aws ssm start-session --target %h --document-name AWS-StartSSHSession --parameters '"'"'portNumber=%p'"'"'"'

    local targetUser=''
    local targetUserOption=''
    local sshKey=''
    local sshKeyOption=''
    local additionalOptions=''
    local targetInstance=''
    if [ "${BAM_AWS_SSH_OPTIONS}" ]; then
        additionalOptions="${BAM_AWS_SSH_OPTIONS}"
    fi

    if [ "$1" = '' -o "${selectByName}" = 'yes' ]; then
        local instances=''
        awsGetRunningInstancesR1 instances

        assertVar instances "There seem to be no running instances."
        if [ "${selectName}" != '' ]; then
            local fullMatches=`echo "${instances}" | grep -E '^[^ '"$tab_global}"']*['"$tab_global}"']'"${selectName}"'['"$tab_global}"']'`
            if [ "${fullMatches}" != '' ]; then
                logInfoInteractive "Found full matches for: ${selectName}"
                instances="${fullMatches}"
            else
                local partialMatches=`echo "${instances}" | grep -E '^[^ '"$tab_global}"']*['"$tab_global}"'][^ '"$tab_global}"']*'"${selectName}"'[^ '"$tab_global}"']*['"$tab_global}"']'`
                if [ "${partialMatches}" != '' ]; then
                    logInfoInteractive "Found partial matches for: ${selectName}"
                    instances="${partialMatches}"
                fi
            fi
        fi
        # https://www.baeldung.com/linux/reading-output-into-array
        local instanceArray=''
        IFS=$'\n' read -r -d '' -a instanceArray < <( echo "${instances}" | pipeStripStandard && printf '\0' )

        local instanceLine=''
        local userSelection=''

        if [ "${#instanceArray[@]}" -gt 1 ]; then
            assertInteractive "More than one eligible instance and not on an interactive shell."
            local oldPs3="${PS3}"
            PS3="Select instance by number: "
            select instanceLine in "${instanceArray[@]}"; do
                userSelection="${REPLY}"
                if [ "${instanceLine}" != '' ]; then
                    break
                fi
            done
            PS3="${oldPs3}"
        else
            instanceLine="${instances}"
            logInfoInteractive "Using:
${instanceLine}"
        fi

        local instanceId=`  echo "${instanceLine}" | awk ' { print $1 } '`
        local instanceName=`echo "${instanceLine}" | awk ' { print $2 } '`
        local sshKey=`      echo "${instanceLine}" | awk ' { print $3 } '`
        local publicIp=`    echo "${instanceLine}" | awk ' { print $4 } '`

        targetInstance="${instanceId}"

        setDefault targetUser "${BAM_AWS_SSH_USER}"
        setDefault targetUser 'ec2-user'

        if [ "${sshKey}" = 'None' ]; then
            logWarningInteractive "There is no ssh key pair listed for ${instanceId}. This may not work unless you installed keys after provisioning"
            sshKey=''
        fi
        setDefault sshKey "${BAM_AWS_SSH_KEYPAIR}"
        if [ "${sshKey}" != '' ]; then
            if [ ! -f "${sshKey}" ]; then
                local matchingKeyFileCandidate=''
                for matchingKeyFileCandidate in \
                    ~/.ssh/aws/"${sshKey}" \
                    ~/.ssh/aws/"${sshKey}.pem" \
                    ; do
                    if [ -f "${matchingKeyFileCandidate}" ]; then
                        sshKey="${matchingKeyFileCandidate}"
                        break
                    fi
                done
                if [ ! -f "${sshKey}" ]; then
                    logErrorAndAbort "ssh key file does not exist: ${sshKey}"
                fi
            fi
        fi
    fi

    local dashLOptionTest=`echo $* ${additionalOptions} | grep -- '-l'`
    local atSymbolTest=`echo $* | grep -- '@'`
    if [ "${dashLOptionTest}" = '' -a "${atSymbolTest}" = '' ]; then
        setDefault targetUser "${BAM_AWS_SSH_USER}"
        setDefault targetUser 'ec2-user'
    fi
    setDefault sshKey "${BAM_AWS_SSH_KEYPAIR}"
    if [ "${sshKey}" != '' ]; then
        assertFile "${sshKey}"
        logInfoInteractive "Using ssh key file: ${sshKey}"
        sshKeyOption="-i ${sshKey}"
    fi
    if [ "${additionalOptions}" ]; then
        logInfoInteractive "Using additional options: ${additionalOptions}"
    fi
    setDefault targetUser "${BAM_AWS_SSH_USER}"
    if [ "${targetUser}" ]; then
        logInfoInteractive "Using user: ${targetUser}"
        targetUserOption="-l ${targetUser}"
    fi
    if [ ! -t 0 ]; then
        # Suppress warning:
        # Pseudo-terminal will not be allocated because stdin is not a terminal.
        # To keep connection clean for rsync tunnels.
        additionalOptions="${additionalOptions} -T"
    fi

    assertVar targetInstance
    if [ "${waitForInstance}" = 'yes' ]; then
        awsWaitForInstanceToBecomeReady "${targetInstance}"
    fi

    if [ "${fetchHostKey}" = 'yes' ]; then
        use ssh
        # -o StrictHostKeyChecking=no
        logInfoInteractive "Fetching host key for: ${targetInstance}"
        # This will throw a "Permission denied", but that's fine.
        ssh \
            -o ProxyCommand="${proxyCommand}" \
            -o StrictHostKeyChecking=no \
            -o BatchMode=yes \
            ${targetUserOption} \
            ${sshDisableSessionMultiplexingOpt_global} \
            ${sshConnectTimeoutOpt_global} \
            ${sshDisableUserLevelAuthOpt_global} \
            "${targetInstance}" exit \
            1>/dev/null 2>&1
    fi
    # FIXME: there is some additional escaping for ProxyCommand and $@
    # required to allow simple copy paste from the log into a shell.
    logInfoInteractive "ssh -o ProxyCommand='${proxyCommand}' ${keypairOption} ${additionalOptions} ${targetUserOption} ${passOptions} ${targetInstance} $@"
    ssh -o ProxyCommand="${proxyCommand}" ${sshKeyOption} ${additionalOptions} ${targetUserOption} ${passOptions} ${targetInstance} "$@"
}

awsSsmSshPushToRemote () {
    local localFile="$1"
    local remoteFile="$2"
    local instanceNameEgrep="$3"

    assertFile "${localFile}"
    assertVar instanceNameEgrep

    local instances
    awsGetRunningInstancesR1 instances
    local matchingInstances=`echo "${instances}" | grep -E "${instanceNameEgrep}"`
    assertVar matchingInstances "No Matching instances found for regex: ${instanceNameEgrep}"
    while read line; do
        logInfo "Copying myself to: ${line}"
        local matchingInstance=`echo "${line}" | awk ' { print $1 } '`
        cat "${localFile}" | awsSsmSsh -l ec2-user "${matchingInstance}" "cat >'${remoteFile}'; ls -lah '${remoteFile}'"
    done < <( echo "${matchingInstances}" | pipeStripStandard )

}

awsGetRunningInstancesR1 () {
    local returnVar_awsGetRunningInstancesR1="$1"
    # https://www.middlewareinventory.com/blog/aws-cli-ec2/#example2

    # Note: output columns are sorted alphabetically by column header
    # and not in the order specified. Hence a_ b_ ... prefix use.
    local list_awsGetRunningInstancesR1=`
              aws ec2 describe-instances  \
                  --query "Reservations[*].Instances[*].{a_InstanceId:InstanceId,b_Name:Tags[?Key=='Name']|[0].Value,c_SshKeyPairName:KeyName,d_PublicIP:PublicIpAddress}" \
                  --filters Name=instance-state-name,Values=running --output text
              `
    setVarOrLogInfo "${returnVar_awsGetRunningInstancesR1}" "${list_awsGetRunningInstancesR1}"
}

awsGetRunningInstances () {
    awsGetRunningInstancesR1 ''
}

awsConsoleReadmeMd () {
    cat <<EOF

2023-04-30:

Opening the AWS console with CLI SSO credentials is not yet implemented by the AWS tools themselves.


A discussion around opening a console with SSO from the commandline is here: [https://github.com/aws/aws-cli/issues/4642](https://github.com/aws/aws-cli/issues/4642)

Which has a link to [https://gist.github.com/ottokruse/1c0f79d51cdaf82a3885f9b532df1ce5](https://gist.github.com/ottokruse/1c0f79d51cdaf82a3885f9b532df1ce5)

Which I installed using:

```bash
cd
cd bin
curl https://gist.githubusercontent.com/ottokruse/1c0f79d51cdaf82a3885f9b532df1ce5/raw/f1cc7858eb151ccc083b67198d0a500932ff3256/aws-console >aws-console
chmod a+x aws-console
```

Note that aws-console is written in python which requires boto3.

```
python3 -m pip install --upgrade pip
pip3 install boto3
pip3 install awscli
```

And it seems to work :-)
EOF
}

awsInstallConsoleCliTool () {
    assertInteractive
    if [ -f ~/bin/aws-console ]; then
        logInfo "Already exists: ~/bin/aws-console"
        return
    fi
    assertDir ~/bin
    spushd ~/bin
    curl https://gist.githubusercontent.com/ottokruse/1c0f79d51cdaf82a3885f9b532df1ce5/raw/f1cc7858eb151ccc083b67198d0a500932ff3256/aws-console >aws-console
    chmod a+x aws-console
    which aws-console
}

awsInstallPythonTools () {
    assertInteractive

    # Upgrade pip, since it always complains
    python3 -m pip install --upgrade pip
    # 
    pip3 install boto3
    brew install awscli

    local notes="
==> awscli
The \"examples\" directory has been installed to:
  /opt/homebrew/share/awscli/examples

Bash completion has been installed to:
  /opt/homebrew/etc/bash_completion.d
"
}

awsConsole () {
    if [ "`which aws-console 2>/dev/null`" != '' ]; then
        logInfoVar AWS_DEFAULT_PROFILE
        aws-console
    else
        awsConsoleReadmeMd
        logErrorAndAbort "Please install aws-console manually or use awsInstallConsoleCliTool"
    fi
}

awsSsoConsoleSelector () {
    assertVar BAM_AWS_SSO_ORG_START_URL "Please save the account selection start URL in environment variable BAM_AWS_SSO_ORG_START_URL first."
    open "${BAM_AWS_SSO_ORG_START_URL}"
}

awsInstallToolsMac () {
    awsInstallAwsCliViaBrew
    awsInstallSessionManagerViaBrew
    awsInstallPythonTools
    awsInstallConsoleCliTool
}

awsSsoGetCurrentProfileR1 () {
    local returnVar_awsSsoGetCurrentProfileR1="$1"

    local refreshLogin='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-l' -o "$1" = '--login' ]; then
            refreshLogin='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    logFlash 'awsSsoGetCurrentProfileR1 ...'
    local stsId=`aws sts get-caller-identity 2>&1`
    local refreshFailedTest=`echo "${stsId}" | grep 'Token has expired and refresh failed'`
    if [ "${refreshFailedTest}" != '' ]; then
        assertVar AWS_DEFAULT_PROFILE
        setVarOrLogInfo "${returnVar_awsSsoGetCurrentProfileR1}" "${AWS_DEFAULT_PROFILE}"
        return
    fi
    local stsAccount=`echo "${stsId}" | jq -r '.Account' 2>/dev/null`
    assertVar stsAccount
    local profile_awsSsoGetCurrentProfileR1=''
    awsSsoConfigGetProfileNameForAccountIdR1 profile_awsSsoGetCurrentProfileR1 "${stsAccount}"
    setVarOrLogInfo "${returnVar_awsSsoGetCurrentProfileR1}" "${profile_awsSsoGetCurrentProfileR1}"
}

awsSsoGetKnownProfilesR1 () {
    local returnVar_awsSsoGetKnownProfilesR1="$1"
    assertFile ~/.aws/config
    local knownProfiles_awsSsoGetKnownProfilesR1=`
        cat ~/.aws/config \
            | pipeStripStandard \
            | grep -E '^\[profile ' \
            | sed 's/\[profile //' \
            | sed 's/\]//' \
            | sort
    `
    setVarOrLogInfo "${returnVar_awsSsoGetKnownProfilesR1}" "${knownProfiles_awsSsoGetKnownProfilesR1}"
}

assertAwsSsoProfileIsKnown () {
    local profile="$1"
    local knownProfiles=''
    awsSsoGetKnownProfilesR1 knownProfiles
    local profileIsKnownTest=`echo "${knownProfiles}" | grep -E '^'"${profile}"'$'`
    assertVar profileIsKnownTest "AWS SSO profile has not been configured: ${profile}
Known profiles are:
${knownProfiles}"
}

awsSsoEnsureCurrentProfile () {
    local desiredProfile="$1"
    assertVar desiredProfile

    awsSsoSetDefaultProfile "${desiredProfile}"

    local currentProfile=''
    awsSsoGetCurrentProfileR1 currentProfile

    if [ "${desiredProfile}" = "${currentProfile}" ]; then
        logDebug "Current profile is already set to desired profile: ${currentProfile}"
        return
    fi
    awsSsoSetDefaultProfile "${desiredProfile}"
}

awsSsoStatus () {
    local stsId=`aws sts get-caller-identity 2>&1`
    logInfo "${stsId}"
    local refreshFailedTest=`echo "${stsId}" | grep 'Token has expired and refresh failed'`
    if [ "${refreshFailedTest}" != '' ]; then
        logWarning "Use awsSsoLogin to login again."
        return
    fi

    local stsAccount=`echo "${stsId}" | jq -r '.Account' 2>/dev/null`
    assertVar stsAccount
    local userArn=`echo "${stsId}" | jq -r '.Arn'  2>/dev/null`
    local userName="${userArn##*/}"
    local profile=''
    awsSsoConfigGetProfileNameForAccountIdR1 profile "${stsAccount}"
    logInfo "User name:        ${userName}"
    logInfo "Account:          ${stsAccount}"
    logInfo "Matching profile: ${profile}"
    logInfo "Default profile:  ${AWS_DEFAULT_PROFILE}"
    logInfo "Default region:   ${AWS_DEFAULT_REGION}"

}

awsSsoConfigure () {
    # https://docs.aws.amazon.com/cli/latest/userguide/sso-configure-profile-token.html
    assertInteractive
    cat <<EOF
# Here is an example of what the configuration looks like:

andy@spark:~ $ aws configure sso
SSO session name (Recommended): andy4wiz-sso-admin-prod
SSO start URL [None]: https://d-xxxxxxxxxx.awsapps.com/start/
SSO region [None]: ap-southeast-2
SSO registration scopes [sso:account:access]:
Attempting to automatically open the SSO authorization page in your default browser.
If the browser does not open or you wish to use a different device to authorize this request, open the following URL:

https://device.sso.ap-southeast-2.amazonaws.com/

Then enter the code:

XXXX-XXXX
There are 3 AWS accounts available to you.
Using the account ID xxxxxxxxxxxx
The only role available to you is: AdministratorAccess
Using the role name "AdministratorAccess"
CLI default client Region [None]: ap-southeast-2
CLI default output format [None]:

To use this profile, specify the profile name using --profile, as shown:

aws s3 ls --profile andy4wiz-sso-admin-prod
andy@spark:~ $ cat .aws/config
[profile andy4wiz-sso-admin-prod]
sso_session = andy4wiz-sso-admin-prod
sso_account_id = XXXXXXXXXXXX
sso_role_name = AdministratorAccess
region = ap-southeast-2
[sso-session andy4wiz-sso-admin-prod]
sso_start_url = https://d-XXXXXXXXXX.awsapps.com/start/
sso_region = ap-southeast-2
sso_registration_scopes = sso:account:access
andy@spark:~ $

EOF
    aws configure sso
    logInfo "Contents of: .aws/config
`cat .aws/config`"
    logInfo "Running: awsSsoLogin"
    awsSsoLogin
    logInfo "Running: awsShowEnv"
    awsShowEnv
}

awsSsoConfigGetProfileNameForAccountIdR1 () {
    local returnVar_awsSsoConfigGetProfileNamesForAccountIdR1="$1"
    local accountId="$2"

    assertVar accountId

    assertFile ~/.aws/config
    logFlash 'awsSsoConfigGetProfileNameForAccountIdR1 ...'
    local line=''
    local state='none'
    local profile_awsSsoConfigGetProfileNameForAccountIdR1=''
    local matchingProfileNames=()
    local profileAccountId=''
    while read line; do
        if [ "${line:0:9}" = '[profile ' ]; then
            state='profile'
            profile_awsSsoConfigGetProfileNameForAccountIdR1="${line:9}"
            profile_awsSsoConfigGetProfileNameForAccountIdR1="${profile_awsSsoConfigGetProfileNameForAccountIdR1%%]}"
            continue
        fi
        if [ "${state}" != 'profile' ]; then
            continue
        fi
        if [ "${line:0:17}" = 'sso_account_id = ' ]; then
            profileAccountId="${line:17}"
            if [ "${profileAccountId}" = "${accountId}" ]; then
                setVarOrLogInfo \
                    "${returnVar_awsSsoConfigGetProfileNamesForAccountIdR1}" \
                    "${profile_awsSsoConfigGetProfileNameForAccountIdR1}"
                return
            fi
        fi
    done < <( cat ~/.aws/config )
}

awsSsoLogin () {
    local force='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o "$1" = '--force' ]; then
            force='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    if [ "${AWS_DEFAULT_PROFILE}" = '' ]; then
        awsSsoSetDefaultProfile
    fi
    if [ "${AWS_DEFAULT_REGION}" = '' ]; then
        awsSetDefaultRegion
    fi

    if [ "${force}" != 'yes' ]; then
        local stsId=`aws sts get-caller-identity 2>&1`
        local stsAccount=`echo "${stsId}" | jq -r '.Account' 2>/dev/null`
        if [ "${stsAccount}" != '' ]; then
            logDebug "awsSsoLogin: current STS token is still valid"
            return
        fi
    fi

    aws sso login
    awsSsoStatus
}

awsGetSsoSessionUserR1 () {
    local returnVar_awsGetSsoSessionUser="$1"

    local ssoSessionUser_awsGetSsoSessionUserR1=`aws --output=json sts get-caller-identity 2>/dev/null | grep '"UserId"' | awk ' { print $2 } ' | sed 's/^"//' | sed 's/".*//' | sed 's/.*://'`
    setVarOrLogInfo "${returnVar_awsGetSsoSessionUser}" "${ssoSessionUser_awsGetSsoSessionUserR1}"
}

awsStsDecode () {
    local message="$1"

    aws sts decode-authorization-message \
        --encoded-message "${message}" \
        --query DecodedMessage --output text | jq '.'
}

awsAssertSsoLogin () {
    local ssoSessionUser=''
    awsGetSsoSessionUserR1 ssoSessionUser
    if [ "${ssoSessionUser}" = '' ]; then
        logErrorAndAbort "No active sso session. Use:
aws sso login"
    fi
    logInfo "Current aws sso cli session logged in as: ${ssoSessionUser}"
}

awsShowDefaultRegion () {
    logInfo "Current default region is: `aws configure get region`"
    logInfo "Use awsSetDefaultRegion to change default region."
}

awsGetDefaultRegionR1 () {
    local returnVar_awsGetDefaultRegionR1="$1"
    local currentDefaultRegion_awsGetDefaultRegionR1=`aws configure get region`
    setVarOrLogInfo "${returnVar_awsGetDefaultRegionR1}" "${currentDefaultRegion_awsGetDefaultRegionR1}"
}

awsEnsureDefaultRegion () {
    local desiredRegion="$1"
    assertVar desiredRegion

    local currentRegion=''

    awsGetDefaultRegionR1 currentRegion
    if [ "${currentRegion}" = "${desiredRegion}" ]; then
        return
    fi
    awsSetDefaultRegion "${desiredRegion}"
}

awsEnsureSsoLogin () {
    local ssoSessionUser=''
    awsGetSsoSessionUserR1 ssoSessionUser
    if [ "${ssoSessionUser}" = '' ]; then
        logInfo "No active session. Starting a new one. Please approve in your browser."
        aws sso login
    else
        logInfo "Current aws sso cli session logged in as: ${ssoSessionUser}"
        awsShowDefaultRegion
        return
    fi
    awsAssertSsoLogin
    awsShowDefaultRegion
}

awsIsEc2HostR1 () {
    local returnVar_awsIsEc2HostR1="$1"
    isEc2Host_awsIsEc2HostR1='no'
    if [ "${hostname_global#*.}" = 'ec2.internal' ]; then
        isEc2Host_awsIsEc2HostR1='yes'
    fi
    setVarOrLogInfo "${returnVar_awsIsEc2HostR1}" "${isEc2Host_awsIsEc2HostR1}"
}

awsInstanceGetStatusR1 () {
    local returnVar_awsInstanceGetStatusR1="$1"
    local instanceId="$2"

    assertVar instanceId

    local statusFull=`aws --output=json ec2 describe-instance-status --instance-ids ${instanceId} 2>&1`
    assertVar statusFull "Unable to get status for instance: ${instanceId}
command used:aws --output=json ec2 describe-instance-status --instance-ids ${instanceId}"
    local status_awsInstanceGetStatusR1=`echo "${statusFull}" | jq -r '.InstanceStatuses[].InstanceStatus.Status'`
    assertVar status_awsInstanceGetStatusR1 \
              "Unable to parse full status with:  jq -r '.InstanceStatuses[].InstanceStatus.Status'
${statusFull}"
    setVarOrLogInfo "${returnVar_awsInstanceGetStatusR1}" "${status_awsInstanceGetStatusR1}"
}

awsWaitForInstanceToBecomeReady () {
    local instanceId="$1"

    local status=''

    local loopCount=0
    local loopCountMax=120 # max count of 120 at 5 seconds sleep is 10 minutes ish
    while [ "${status}" != 'ok' ]; do
        let loopCount+=1
        awsInstanceGetStatusR1 status "${instanceId}"
        if [ "${status}" = 'ok' ]; then
            logInfoInteractive "Instance is ready: ${instanceId}"
            break
        fi
        if [ ${loopCount} -gt ${loopCountMax} ]; then
            logErrorAndAbort "Instance is still not ready after max checks: ${loopCountMax}"
        fi
        if [ "${status}" = 'initializing' ]; then
            logFlashInteractive "Status of instance ${instanceId}: ${status} (${loopCount}/${loopCountMax})"
            sleep 5
        else
            logErrorAndAbort "Status of instance ${instanceId} is neither 'ok' nor 'initializing' but: ${status}"
        fi
    done
}

awsSecretStringGetR1 () {
    local returnVar_awsSecretStringGetR1="$1"
    local secretId="$2"

    assertVar secretId "awsSecretStringGetR1(): please specify the name/id of the secret"
    local secretString_awsSecretStringGetR1=`
              aws --output=json secretsmanager get-secret-value --secret-id "${secretId}" \
              | jq -r '.SecretString'`
    setVarOrLogInfo "${returnVar_awsSecretStringGetR1}" "${secretString_awsSecretStringGetR1}"
}

awsSecretStringGetUsernamePasswordR2 () {
    local returnVarUsername_awsSecretStringGetUsernamePasswordR2="$1"
    local returnVarPassword_awsSecretStringGetUsernamePasswordR2="$2"
    local secretId="$3"

    local secretJson=''
    awsSecretStringGetR1 secretJson "${secretId}"
    local username=`echo "${secretJson}" | jq -r '.username'`
    local password=`echo "${secretJson}" | jq -r '.password'`

    setVarOrLogInfo "${returnVarUsername_awsSecretStringGetUsernamePasswordR2}" "${username}"
    if [ "${returnVarUsername_awsSecretStringGetUsernamePasswordR2}" = '' ]; then
        setVarOrLogInfo "${returnVarPassword_awsSecretStringGetUsernamePasswordR2}" "${password}"
    else
        if [ "${returnVarPassword_awsSecretStringGetUsernamePasswordR2}" != '' ]; then
            setVar "${returnVarPassword_awsSecretStringGetUsernamePasswordR2}" "${password}"
        fi
    fi
}

awsEc2SetRegionFromMetadata () {
    ownAvailabilityZone=`curl --silent http://169.254.169.254/latest/meta-data/placement/availability-zone`
    # e.g. us-east-1a
    assertVar ownAvailabilityZone
    ownRegion=`echo "${ownAvailabilityZone}" | sed 's/[a-z]$//'`
    assertVar ownRegion
    export AWS_DEFAULT_REGION="${ownRegion}"
    assertEnvVarWithValue AWS_DEFAULT_REGION
}

awsEc2EnsureDefaultRegionIsSet () {
    if [ "${AWS_DEFAULT_REGION}" != '' ]; then
        export AWS_DEFAULT_REGION
        return
    fi
    awsEc2SetRegionFromMetadata
}

awsSourceFiles () {
    if [ -f /opt/homebrew/bin/aws_completer ]; then
        complete -C '/opt/homebrew/bin/aws_completer' aws
    fi
}

awsEksUpdateKubeConfig () {
    local clusterName="$1"
    assertVar AWS_DEFAULT_REGION
    setDefault clusterName "${BAM_AWS_EKS_CLUSTER_NAME}"
    assertVar clusterName

    aws eks update-kubeconfig --region "${AWS_DEFAULT_REGION}" --name "${clusterName}"
}

awsEksListClusterNames () {
    aws eks list-clusters
}

awsSsoExportCreds () {
    eval `aws configure export-credentials --format env`
}

awsSsoShowCreds () {
    aws configure export-credentials --format env
}

awsSsoUnsetCreds () {
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    unset AWS_CREDENTIAL_EXPIRATION
}

alias go='awsSsmSsh --name'

# MAIN

eval "${showLibraryUsageIfNotSourced}"

awsSourceFiles
