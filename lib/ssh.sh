#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam ssh module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

use misc
use file

libraryUsage () {
    cat <<EOF

Helpers to make ssh life easier.

This module assumes that non-standard ssh ports are handled by means
of ~/.ssh/config.

While we are not actively trying to compromise security, as with all
things ssh some functions may have security implications. E.g. when
you are using session multiplexing or the sshAgent function a
malicious administrator on your host could hijack a connection. This
may or may not be acceptable in your particular case.

sshFix user@host
    To set up passwordless ssh access to a host.
    This will also update the ssh host key in case it changed.

sshAgent
    Launch an agent and write parameters to a temp file so other
    shells can use it as well.


EOF

}

sshHostKeyUpdate () {
    # We are not doing a port option. If a different port needs to be
    # used please create an entry in: ~/.ssh/config
    local host="$1"

    assertVar host

    local host="${host}"

    # We could use ssh-keyscan, but using ssh directly will make the
    # updates in ~/.ssh/known_hosts for us.

    # We could use ssh-keygen -F host:port to check for existing host
    # keys, but this seems to behave differently than ssh directly:
    # ssh -p 2222 localhost
    # will generate an entry with
    # [localhost]:2222 ...
    # in ~/.ssh/known_hosts, but
    # ssh-keygen -F localhost:2244
    # will not find this key, but
    # ssh-keygen -F [localhost]:2244
    # will. This is too confusing. Using just ssh is more direct.

    # Since we want to work with the host keys only and not actually
    # execute anything we we'll off all user level auth methods.
    assertVariable sshDisableUserLevelAuthOpt_global

    # StrictHostKeyChecking=yes - check whether we have a conflict or not
    local trace=''
    trace=`ssh -v \
        -o StrictHostKeyChecking=yes \
        -o BatchMode=yes \
        ${sshDisableSessionMultiplexingOpt_global} \
        ${sshConnectTimeoutOpt_global} \
        ${sshDisableUserLevelAuthOpt_global} \
        "${host}" exit \
        2>&1`

    # debug1: Host 'host' is known and matches the host key.
    local hostKeyIsGoodTest=`echo "${trace}" | egrep 'Host .*is known and matches the .*host key.'`
    if [ "${hostKeyIsGoodTest}" != '' ]; then
        logInfo "Host key is fine for: ${host}"
        return
    fi

    # Test for:
    # Offending key in /home/user/.ssh/known_hosts:11
    local offendingKeyFoundTest=`echo "${trace}" | egrep 'Offending .*key in .*known_hosts:'`
    if [ "${offendingKeyFoundTest}" != '' ]; then
        logInfo "Host key changed for: ${host}"
        local knownHostsFileAndLineNumber=`echo "${offendingKeyFoundTest}" | sed 's/.* //'`
        local knownHostsFile="${knownHostsFileAndLineNumber%%:*}"
        local lineNumber="${knownHostsFileAndLineNumber##*:}"
        logInfo "Removing offending host key at: ${knownHostsFileAndLineNumber}"
        local newEtcHostsContent=`sed -e "${lineNumber}d" "${knownHostsFile}"`
        fileUpdate "${knownHostsFile}" "${newEtcHostsContent}"
        # We still need to fetch the key.
    else
        # Test for:
        # No host key is known for localhost and you have requested strict checking.
        local hostNotKnownTest=`echo "${trace}" | egrep 'No .*host key is known for *. and you have requested strict checking.'`
        if [ "${hostNotKnownTest}" != '' ]; then
            logInfo "Host key is not known yet."
            # We still need to fetch the key.
        else
            logInfo "Not sure what to do with this trace:
${trace}"
        fi
    fi

    logDebug "Fetching current key for: ${host}"
    # Example error:
    # StrictHostKeyChecking=no  - this will update the host key if it changed
    trace=`ssh \
        -o StrictHostKeyChecking=no \
        -o BatchMode=yes \
        ${sshDisableSessionMultiplexingOpt_global} \
        ${sshConnectTimeoutOpt_global} \
        ${sshDisableUserLevelAuthOpt_global} \
        "${host}" exit \
        2>&1`
    local addedTest=`echo "${trace}" | egrep 'Permanently added .* to the list of known hosts.'`
    if [ "${addedTest}" != '' ]; then
        logInfo "Ssh host key has been added."
        return
    else
        logErrorAndAbort "Something went wrong:
${trace}"
    fi
}

sshKnownHostKeyDelete () {
    local hostKeyToDelete="$1" # host, host:port, [host]:port, or key

    assertFile ~/.ssh/known_hosts

    local knownHostsContent="`cat ~/.ssh/known_hosts`"
    local knownHostSedEscaped=''
    sedEscapeR1 knownHostSedEscaped "${hostKeyToDelete}"
    logDebugVar knownHostSedEscaped
    # match the host or [host]:port at the front
    local matchingHostKeys=`echo "${knownHostsContent}" | egrep "(^|,)${knownHostSedEscaped}( |,)"`
    if [ "${matchingHostKeys}" != '' ]; then
        logInfo "Deleting matching entries for host:
${matchingHostKeys}"
        knownHostsContent=`echo "${knownHostsContent}" | egrep -v "(^|,)${knownHostSedEscaped}( |,)"`
    fi

    # match the key itself
    local matchingHostKeys=`echo "${knownHostsContent}" | egrep " ${hostKeyToDelete}"'$'`
    if [ "${matchingHostKeys}" != '' ]; then
        logInfo "Deleting matching entries for key:
${matchingHostKeys}"
        knownHostsContent=`echo "${knownHostsContent}" | egrep -v " ${hostKeyToDelete}"'$'`
    fi
    fileUpdate ~/.ssh/known_hosts "${knownHostsContent}"
}

sshGetKnownHostsR1 () {
    local returnVarKnownHosts_sshGetKnownHostsR1="$1"

    local knownHosts_sshGetKnownHostsR1=`cat ~/.ssh/known_hosts | awk ' { print $1 } ' | pipeStripEmptyLines | sort`

    setVarOrLogInfo "${returnVarKnownHosts_sshGetKnownHostsR1}" "${knownHosts_sshGetKnownHostsR1}"
}

sshGetKnownHostKeyR1 () {
    local returnVarHostKey_sshGetHostKeyR1="$1"
    local knownHostName_sshGetHostKeyR1="$2"

    local knownHostSedEscaped=''
    sedEscapeR1 knownHostSedEscaped "${knownHostName_sshGetHostKeyR1}"

    local matchingHostKey=`egrep "(^|,)${knownHostSedEscaped}( |,)" ~/.ssh/known_hosts`

    setVarOrLogInfo "${returnVarHostKey_sshGetHostKeyR1}" "${matchingHostKey}"
}

sshMultiplexMasterFilesDelete () {
    local multiplexMasterFiles=`ls ~/.ssh/master-* 2>/dev/null`

    if [ "${multiplexMasterFiles}" = '' ]; then
        logInfo "No ssh multiplex master files found at: ~/.ssh/master-*"
        return
    fi
    logInfo "Deleting multiplex master files:
${multiplexMasterFiles}"
    rm -f ~/.ssh/master-*

    multiplexMasterFiles=`ls ~/.ssh/master-* 2>/dev/null`
    if [ "${multiplexMasterFiles}" != '' ]; then
        logError "Unable to remove some multiplex master files:
${multiplexMasterFiles}"
    fi
}

sshCheckPasswordlessConnectivityR1 () {
    local passOptions=''
    local sshIdentityOption
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-i' -o "$1" = '-identity' ]; then
            assertValue "$2" "-i option used without specifying an identity file"
            assertFile "$2"
            sshIdentityOption_sshCheckPasswordlessConnectivityR1="-i $2"
            shift
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "sshCheckPasswordlessConnectivityR1(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVarPasswordlessConnectivityWorks="$1" # Optional.
    local targetHost="$2"

    assertVariable targetHost
    logDebug ssh -v ${sshIdentityOption_sshCheckPasswordlessConnectivityR1} \
             ${sshPasswordlessOpt_global} \
             ${sshConnectTimeoutOpt_global} \
             "${targetHost}" hostname
    local remoteTest=`
              ssh -v ${sshIdentityOption_sshCheckPasswordlessConnectivityR1} \
                  ${sshPasswordlessOpt_global} \
                  ${sshConnectTimeoutOpt_global} \
                  "${targetHost}" hostname 2>/dev/null`
    logDebugVar remoteTest
    if [ "${remoteTest}" = '' ]; then
        logDebug "${message}"
        setVarOrLogInfo \
            "${returnVarPasswordlessConnectivityWorks}" \
            'no' \
            "Passwordless connectivity to ${targetHost} is NOT working."
        return
    fi
    setVarOrLogInfo \
        "${returnVarPasswordlessConnectivityWorks}" \
        'yes' \
        "Passwordless connectivity to ${targetHost} is working."
}

sshTestIdentitiesR2 () {
    local returnVarIsPasswordlessWorking="$1"
    local returnVarWorkingIdentitySshOption="$2"
    local targetHost="$3"
    shift
    shift
    shift

    assertVariable targetHost
    local identity
    local identityOption

    # identity '' will test connectivity without any -i option, which
    # will work e.g. if there are good options in ~/.ssh/config or if
    # there is an ssh agent running or if multiplexing is configured
    # and there is another active connection.
    for identity in '' "$@"; do
        logDebug "Testing identity ${identity} for target host ${targetHost}"
        local isWorking=''
        if [ "${identity}" = '' ]; then
            identityOption=''
        else
            if [ ! -r "${identity}" ]; then
                logDebug "Skipping identity: ${identity}"
                continue
            fi
            identityOption="-i ${identity}"
        fi
        logDebug sshCheckPasswordlessConnectivityR1 ${identityOption} isWorking "${targetHost}"
        sshCheckPasswordlessConnectivityR1 ${identityOption} isWorking "${targetHost}"
        if [ "${isWorking}" = 'yes' ]; then
            setVarOrLogInfo "${returnVarIsPasswordlessWorking}" 'yes'
            if [ "${identity}" = '' ]; then
                setVarOrLogInfo "${returnVarWorkingIdentitySshOption}" ''
            else
                setVarOrLogInfo "${returnVarWorkingIdentitySshOption}" "-i ${identity}"
            fi
            return
        fi
    done
    setVarOrLogInfo "${returnVarIsPasswordlessWorking}" 'no'
    setVarOrLogInfo "${returnVarWorkingIdentitySshOption}" ''
}

sshTest () {
    sshTestIdentitiesR2 '' '' "$1"
}

sshAgent () {
    local minimal='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-m' -o "$1" = '--minimal' ]; then
            minimal='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "sshAgent(): Ignoring unknown option: $1"
            shift
        fi
    done

    local agentFile="/tmp/bam_ssh_agent_file_${username_global}_${userid_global}.sh"

    # Agent file looks something like this:
    # SSH_AUTH_SOCK=/var/folders/yk/grm23ycd0qlgpp7y9h7rdj3m0000gn/T//ssh-Id4emtEnXxO8/agent.56972; export SSH_AUTH_SOCK;
    # SSH_AGENT_PID=56973; export SSH_AGENT_PID;
    # echo Agent pid 56973;

    local agentIsRunning=''
    if [ -f "${agentFile}" -a "${SSH_AGENT_PID}" = '' ]; then
        logFlashInteractive "Loading existing ssh agent file: ${agentFile}"
        . "${agentFile}" >/dev/null
    fi
    if [ "${SSH_AGENT_PID}" != '' ]; then
        local agentIsRunning=''
        isProcessIdRunningR1 agentIsRunning "${SSH_AGENT_PID}"
        if [ "${agentIsRunning}" = 'yes' ]; then
            logInfoInteractive "Ssh agent is alive: ${SSH_AGENT_PID}"
            return
        fi
    fi

    if [ -f "${agentFile}" ]; then
        logInteractive "Removing stale file: ${agentFile}"
        removeFileIfExists "${agentFile}"
        logFlashClear
    fi

    if [ "${minimal}" = 'yes' ]; then
        return
    fi

    assertInteractive

    logInfo "Starting agent."
    local tempfile=''
    getTempfileR1 tempfile
    ssh-agent > "${tempfile}"
    mv "${tempfile}" "${agentFile}"
    assertFile "${agentFile}"
    logInfo "Loading new ssh agent file: ${agentFile}"
    . "${agentFile}"  >/dev/null
    logInfo "Adding keys."
    ssh-add
}

sshPushUserKey () {
    local sshIdentity
    local sshIdentityOption
    # We are not handling -p or -l here on purpose to keep code
    # complexity down. In real life, ports should be specified in
    # ~/.ssh and ssh does support user@host if a different user is
    # required.
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-i' -o "$1" = '-identitypubkeyfile' ]; then
            assertFile "$2"
            sshIdentity="$2"
            sshIdentityOption="-i $2"
            shift
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "sshPushUserKey(): Ignoring unknown option: $1"
            shift
        fi
    done

    local host="$1"

    if [ "${sshIdentity}" = '' ]; then
        local sshIdentityCandidate=''
        for sshIdentityCandidate in \
            ~/.ssh/id_ecdsa.pub \
            ~/.ssh/id_rsa.pub \
            ; do
            if [ -r "${sshIdentityCandidate}" ]; then
                sshIdentity="${sshIdentityCandidate}"
                sshIdentityOption="-i ${sshIdentity}"
                break
            fi
        done
    fi
    assertFile "${sshIdentity}"
    local lineCountTest=`cat "${sshIdentity}" | wc -l`
    if [ ${lineCountTest} != '1' ]; then
        logErrorAndAbort "File does not look like a public user key file: ${sshIdentity}"
    fi
    logInfo "Using public user key at: ${sshIdentity}"
    local identityData=`cat "${sshIdentity}"`
    assertVariable identityData

    local passwordlessIsWorking=''
    local passwordlessIdentityOption=''
    sshTestIdentitiesR2 passwordlessIsWorking passwordlessIdentityOption \
                        "${host}" "${sshIdentity}" ~/.ssh/id_ecdsa.pub ~/.ssh/id_rsa.pub
    if [ "${passwordlessIsWorking}" = 'yes' ]; then
        logInfo "Passwordless access is working at present. Nice."
        logDebug "Identity option that worked: ${passwordlessIdentityOption}"
    else
        # User will need to type the password to get access.
        assertInteractive
        passwordlessIdentityOption="${sshIdentityOption}"
    fi

    # We can't be sure what the shell on the other side is. So we'll
    # pipe the code into /bin/sh.

    assertCatEofIsUsable
    (
        cat <<EOF
mkdir -p "\${HOME}/.ssh"
chmod 700 "\${HOME}/.ssh"
if [ ! -f "\${HOME}/.ssh/authorized_keys" ]; then
    echo "INFO  Installing: \${HOME}/.ssh/authorized_keys"
    touch "\${HOME}/.ssh/authorized_keys"
    echo "${identityData}" > "\${HOME}/.ssh/authorized_keys"
elif [ "\`grep '${identityData}' "\${HOME}/.ssh/authorized_keys"\`" = '' ]; then
    echo "INFO  Adding key to: \${HOME}/.ssh/authorized_keys"
    echo "${identityData}" >> "\${HOME}/.ssh/authorized_keys"
else
    echo 'INFO  Identity is already installed.'
fi
chmod 644 "\${HOME}/.ssh/authorized_keys"


EOF
    ) | ssh ${passwordlessIdentityOption} ${sshDisablePseudoTerminalOption_global} ${host}

}

sshFix () {
    local host="$1"

    assertVar host

    sshHostKeyUpdate "${host}"
    sshPushUserKey "${host}"
}

sshSetGlobalVars () {
    setDefault sshConnectTimeout_global 5 # seconds
    sshConnectTimeoutOpt_global="-o ConnectTimeout=${sshConnectTimeout_global}"

    # https://superuser.com/questions/894608/ssh-o-preferredauthentications-whats-the-difference-between-password-and-k
    # """The keyboard-interactive authentication is more complex
    #    request for arbitrary number of pieces of information."""
    sshPasswordOnlyOpt_global='
        -o ChallengeResponseAuthentication=no
        -o KbdInteractiveAuthentication=no
        -o KbdInteractiveDevices=no
        -o PasswordAuthentication=yes
        -o PreferredAuthentications=password
        -o PubkeyAuthentication=no
        '

    sshPasswordlessOpt_global='
        -o ChallengeResponseAuthentication=no
        -o KbdInteractiveAuthentication=no
        -o KbdInteractiveDevices=no
        -o PasswordAuthentication=no
        -o PreferredAuthentications=publickey
        -o PubkeyAuthentication=yes
        '

    # https://linuxcommando.blogspot.com.au/2008/10/how-to-disable-ssh-host-key-checking.html
    sshNoHostKeyCheckOpt_global='
        -o UserKnownHostsFile=/dev/null
        -o StrictHostKeyChecking=no
        '

    # Useful for host key checking only.
    sshDisableUserLevelAuthOpt_global='
        -o ChallengeResponseAuthentication=no
        -o GSSAPIAuthentication=no
        -o HostbasedAuthentication=no
        -o KbdInteractiveAuthentication=no
        -o KbdInteractiveDevices=no
        -o PasswordAuthentication=no
        -o PubkeyAuthentication=no
        '


    # When we use a pipe into ssh it gives a warning:
    # Pseudo-terminal will not be allocated because stdin is not a terminal.
    # The -T option suppresses that.
    sshDisablePseudoTerminalOption_global="-T"

    # When we want to make sure NOT to use session multiplexing if
    # there already is an active session, we disable it this way:
    sshDisableSessionMultiplexingOpt_global='-o ControlPath=none'
}

sshKeygen () {
    local keyType=''
    local showHelp='no'
    local group=''
    local name=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--aws' -o "$1" = '--amazon' ]; then
            keyType='ed25519'
            group='aws'
            shift
        elif [ "$1" = '--azure' -o "$1" = '--microsoft' ]; then
            keyType='rsa'
            group='azure'
            shift
        elif [ "$1" = '--gcp' -o "$1" = '--google' ]; then
            keyType='rsa'
            group='gcp'
            shift
        elif [ "$1" = '-g' -o "$1" = '--group' ]; then
            shift
            group="${1}"
            shift
        elif [ "$1" = '-n' -o "$1" = '--name' ]; then
            shift
            name="${1}"
            shift
        elif [ "$1" = '-h' -o "$1" = '--help' ]; then
            showHelp='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            # ${FUNCNAME[0]} is not available in sh
            logWarning "sshKeygen(): Ignoring unknown option: $1"
            shift
        fi
    done

    setDefault keyType 'ed25519'

    if [ "${showHelp}" = 'yes' ]; then
        # ${FUNCNAME[0]} is not available in sh
        cat <<EOF
sshKeygen(): Generate ssh key pair

Options:
   --aws   --amazon:    Generate ed25519 key in group aws
   --azure --microsoft: Generate rsa key     in group azure
   --gcp   --google:    Generate rsa key     in group gcp
   -g      --group:     Specify a group name
   -n      --name:      Specify a key name

A group name creates a sub-directory in ~/.ssh to store the key.

EOF
        return
    fi

    # https://goteleport.com/blog/comparing-ssh-keys/
    # https://security.stackexchange.com/questions/50878/ecdsa-vs-ecdh-vs-ed25519-vs-curve25519
    # https://security.stackexchange.com/questions/240991/what-is-the-sk-ending-for-ssh-key-types

    # -t options for ssh-keygen:
    # dsa | ecdsa | ecdsa-sk | ed25519 | ed25519-sk | rsa
    # -sk is used for YubiKeys with FIDO2 support (firmware 5.2.3 or higher)

    # dsa is considered weak and has been disabled in OpenSSH
    # http://www.openssh.com/legacy.html

    # Amazon EC2 supports RSA and ed25519, but not ecdsa.
    # Azure only supports RSA.
    # GCP seems to only sue RSA as well.

    local comment=''
    if [ "${name}" != '' ]; then
        assertValidName "${name}"
        comment="name=${name}"
    fi
    if [ "${group}" != '' ]; then
        assertVar name "Please specify a key name using --name."
        assertValidName "${name}"
        assertValidName "${group}"
        comment="${comment} group=${group}"
    fi
    stripR1 comment
    local commentOption=''
    if [ "${comment}" != '' ]; then
        # ${FUNCNAME[0]} is not available in sh
        comment="sshKeygen ${comment}"
        commentOption='-C'
    fi

    if [ ! -d ~/.ssh ]; then
        mkdir ~/.ssh
        assertDir ~/.ssh
        chmod 700 ~/.ssh
    fi
    local dirname=''
    local filename=''
    if [ "${group}" != '' ]; then
        dirname=~/.ssh/"${group}"
        if [ ! -d "${dirname}" ]; then
            mkdir "${dirname}"
            assertDir "${dirname}"
            chmod 700 "${dirname}"
        fi
        filename="${dirname}/${name}"
    fi
    if [ "${name}" != '' ]; then
        setDefault filename ~/.ssh/"${name}"
    fi
    local filenameOption=''
    if [ "${filename}" != '' ]; then
        filenameOption='-f'
    fi
    assertInteractive
    ssh-keygen -t "${keyType}" ${commentOption} "${comment}" ${filenameOption} "${filename}" "$@"
}

# MAIN

sshMainRemoteBootstrap () {
    sshSetGlobalVars

    alias sshPasswordOnly="ssh `echo ${sshPasswordOnlyOpt_global}`"
    alias sshPasswordless="ssh `echo ${sshPasswordlessOpt_global}`"
    alias sshPasswordOnlyIgnoreHostKey="ssh `echo ${sshPasswordOnlyOpt_global} ${sshNoHostKeyCheckOpt_global}`"
    alias sshPasswordlessIgnoreHostKey="ssh `echo ${sshPasswordlessOpt_global} ${sshNoHostKeyCheckOpt_global}`"
}

sshConfigBootstrap () {
    # Long amazon host names lead to this error:
    # unix_listener: path "/Users/andy/.ssh/master-ec2-user@ec2-13-210-24-34.ap-southeast-2.compute.amazonaws.com:22.os4KZrE6tiwUSHfl" too long for Unix domain socket
    configData="
# When asking for passwords for a key, add the key to the agent.
AddKeysToAgent yes

# Enable keepalive
ServerAliveInterval 60

# Use multiplexing, but avoid long amazon hostnames.
Host * !*.amazonaws.com
   ControlMaster auto
   ControlPath ~/.ssh/master-%r@%h:%p
   ControlPersist 30m
"
    use slice
    sliceNameReplaceInFile --prepend bamSshConfigBootstrap "${configData}" ~/.ssh/config
}

moduleLoading='ssh.sh' # Required for sh modules.
eval "${showLibraryUsageIfNotSourced}"

sshMainRemoteBootstrap
if [ "${isInteractive_global}" = 'yes' ]; then
    sshAgent --minimal
fi
