#!/bin/sh
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam misc module

usage () {
    cat <<EOF
These procedures should eventually be relocated to somewhing more appropriate.

EOF
}

sedEscapeVarR1 () {
    # Escape / \ and . in a variable value
    local returnVar_sedEscapeVarR1="$1"
    local varToEscape_sedEscapeVarR1="$2" # Optional. Defaults to return variable.

    if [ "${returnVar_sedEscapeVarR1}" != '' -a \
         "${varToEscape_sedEscapeVarR1}" = '' ]; then
        getVarR1 varToEscape_sedEscapeVarR1 "${returnVar_sedEscapeVarR1}"
    else
        assertVar varToEscape_sedEscapeVarR1
    fi

    if [ "${isBash_global}" = 'no' -a "${isZsh_global}" = 'no' ]; then
        # Note: sh seems to require the double amount of backslaches compared to bash
        local backslashTest=`echo "${varToEscape_sedEscapeVarR1}" | grep '\\\\'`
        if [ "${backslashTest}" != '' ]; then
            logErrorAndAbort "sedEscapeVarR1: unable to handle backslashes on sh"
        fi
    fi
    # Note: Escape backslashes first.
    local valueSedEscaped="`
              echo \"${varToEscape_sedEscapeVarR1}\" \
              | sed 's/\\\\/\\\\\\\/g' \
              | sed 's/\//\\\\\//g' \
              | sed 's/\\./\\\\./g'`"
    setVarOrLogInfo \
        "${returnVar_sedEscapeVarR1}" \
        "${valueSedEscaped}" \
        "Value:       ${varToEscape_sedEscapeVarR1}
sed escaped: ${valueSedEscaped}"
}

sedEscapeR1 () {
    # Escape / \ and . for usage in sed patterns. There are problably
    # other charaters that need escaping as well.
    local returnVar_sedEscapeR1="$1"
    local valueToEscape_sedEscapeR1="$2" # Optional. Defaults to return variable.

    if [ "${returnVar_sedEscapeR1}" != '' -a \
         "${valueToEscape_sedEscapeR1}" = '' ]; then
        logDebugVar returnVar_sedEscapeR1
        getVarR1 valueToEscape_sedEscapeR1 "${returnVar_sedEscapeR1}"
    fi
    logDebugVar valueToEscape_sedEscapeR1
    #if [ "${setVarFunctionType_global}" = 'evalEsc' ]; then
    #    # Known condition: The sh version of setVar (using escaped eval) does mess up on backslashes.
    #    local backslashTest=`echo "${valueToEscape_sedEscapeR1}" | grep '\\\\'`
    #    if [ "${backslashTest}" != '' ]; then
    #        logErrorAndAbort "sedEscapeR1: unable to handle backslashes on sh"
    #    fi
    #fi
    # Note: Escape backslashes first.
    #               | sed 's/\\\\/\\\\\\\/g' \
    #               | sed 's/\\\\/\\\\\\\\\\\\\\\/g' \

    local valueSedEscaped="`
              printf '%s' \"${valueToEscape_sedEscapeR1}\" \
              | sed 's/\\\\/\\\\\\\/g' \
              | sed 's/\//\\\\\//g' \
              | sed 's/\\./\\\\./g' \
              | sed 's/\\[/\\\\[/g' \
              | sed 's/\\]/\\\\]/g' \
          `"
    #printf 'valueToEscape_sedEscapeR1=%s\n' "${valueToEscape_sedEscapeR1}"
    #printf 'valueSedEscaped=%s\n' "${valueSedEscaped}"
    setVarOrLogInfo \
        "${returnVar_sedEscapeR1}" \
        "${valueSedEscaped}" \
        "Value:       ${valueToEscape_sedEscapeR1}
sed escaped: ${valueSedEscaped}"
}

# MAIN

moduleLoading='misc.sh' # Required for sh modules.
eval "${showLibraryUsageIfNotSourced}"
