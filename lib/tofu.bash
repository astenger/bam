#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam opentofu helpers

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

Helpers for working with opentofu.

EOF

}

tofuBrewInstall () {
    brew install opentofu
}

eval "${showLibraryUsageIfNotSourced}"

# Some terraform aliases to make typing easier
alias tf='tofu'
alias tfp='tofu plan'
alias tfa='tofu apply'
alias tfay='tofu apply -auto-approve'
alias tfi='tofu init'
alias tfd='tofu apply -destroy'
alias tfdy='tofu apply -destroy -auto-approve'

