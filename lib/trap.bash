#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam trap module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

This library helps with hanlding of unix signals in general and with
managing exit functions and trap handlers that need to run when a
shell ends to facilitate clean-up.

For convenience you probably want to use the cleanup module instead of
the lower level trap module.

The aim here is to ensure handling of code before the script exits.
The aim is NOT to keep the script running after handling of the code,
with the exeption of the bash ERR trapping facility.

KILL (kill -9 xxx) does not allow for graceful handling of
anything of course.

While not a signal as such, bash does allow traps on EXIT.

You can register functions you have already defined using:
- registerExitFunction <function>
- unregisterExitFunction <function>
- registerTrapFunction <function> <signal>
- unregisterTrapFunction <function> <signal>

... or have the module generate a function wrapper for your code on
the fly using the the code functions:
- registerExitCode <bash code>
- unregisterExitCode <bash code>
- registerTrapCode <bash code> <signal>
- unregisterTrapCode <bash code> <signal>

EOF
}

# This uses global variables like:
# _trapFunctions_EXIT_trp000
# _trapFunctions_INT_trp000
# _trapFunctions_TERM_trp000

trapHandler () {
    local signal="$1"

    logDebug "Executing trap handler: ${signal}"

    local trapFunctionListName="_trapFunctions_${signal}_trp000"
    local trapFunctions=''

    getVarR1 trapFunctions "${trapFunctionListName}"
    logDebugVar trapFunctionListName trapFunctions

    local trapFunction=''
    while read trapFunction; do
        logDebug "Running ${signal} function: ${trapFunction}"
        ${trapFunction}
        unregisterTrapFunction "${trapFunction}" "${signal}"
    done < <( echo "${trapFunctions}" | pipeStripEmptyLines )

    # We are done handling the functions. Remove the trap ...
    logDebug "Removing trap handler for: ${signal}"
    trap - "${signal}"
    # ... and re-send the signal.
    if [ "${signal}" != 'ERR' ]; then
        logDebug "Sending signal ${signal} to myself ($$)."
        kill -"${signal}" $$
    fi
}

showTrapFunctions () {
    local signal="$1"

    setDefault signal 'EXIT'
    assertValidTrapSignal "${signal}"

    local trapFunctionList="_trapFunctions_${signal}_trp000"
    local trapFunctions=''
    getVarR1 trapFunctions "${trapFunctionList}"
    if [ "${trapFunctions}" = '' ]; then
        logInfo "No trap functions registered for signal: ${signal}"
    else
        logInfo "Registered trap functions for signal: ${signal}"
        local trapFunction
        for trapFunction in ${trapFunctions}; do
            type "${trapFunction}" | egrep -v 'is a function$'
        done
    fi
}

showTrapInfo () {
    logVerbose "Known traps: trap -l
`trap -l`"
    logVerbose "Note that the SIG prefix is optional."
    logVerbose "Current trap configuration: trap -p
`trap -p`"

    local trapFunctionLists=`set | egrep '^_trapFunctions_[A-Z][A-Z]*_trp000=' | sed 's/=.*//'`
    local signals=`echo "${trapFunctionLists}" | sed 's/_trapFunctions_//g;s/_trp000//g'`
    local signal=''
    for signal in ${signals}; do
        showTrapFunctions "${signal}"
    done
}

isValidTrapSignalR1 () {
    # Enforce signal names all upper case and without a SIG
    # prefix. Otherwise our lists might get messed up.
    local returnVar="$1"
    local signal="$2"

    assertVariable signal

    local validSignals=`trap -l | sed 's/[0-9][0-9]*)//g' | sed 's/ SIG//g'`
    validSignals=`printf '%s\n' ${validSignals} | sort`
    local validSignalTest=`echo "${validSignals}" | egrep "^${signal}\$"`
    if [ "${validSignalTest}" = '' -a "${signal}" != 'EXIT' ]; then
        setVarOrLogInfo "${returnVar}" 'no' "Not a valid trap signal name: ${signal}"
    else
        setVarOrLogInfo "${returnVar}" 'yes' "Is valid trap signal name: ${signal}"
    fi
}

assertValidTrapSignal () {
    local signal="$1"

    local isValid=''
    isValidTrapSignalR1 isValid "${signal}"
    assertVariableValue isValid 'yes' "Invalid trap signal: ${signal}"
}

registerTrapFunction () {
    local functionName="$1"
    local signal="$2" # EXIT, HUP or TERM or similar. Defaults to EXIT.

    # EXIT is a bash pseudo-signal which indicates that the shell is ending.
    setDefault signal 'EXIT'
    assertValidTrapSignal "${signal}"

    # Create a function on the fly so we can pass the signal name into
    # the handler.
    eval "trapHandler${signal} () { trapHandler '${signal}'; }"
    trap "trapHandler${signal}" "${signal}"

    logDebug "`type trapHandler${signal}`"
    assertValidName "${functionName}" "registerTrapFunction: function name is invalid: ${functionName}"

    local trapFunctionListName="_trapFunctions_${signal}_trp000"
    local trapFunctions=''
    getVarR1 trapFunctions "${trapFunctionListName}"
    local alreadyRegisteredTest="`echo "${trapFunctions}" | egrep "^${functionName}\$"`"
    if [ "${alreadyRegisteredTest}" != '' ]; then
        logDebug "Trap function is already registered for ${signal}: ${functionName}"
        return
    fi
    trapFunctions="${trapFunctions}
${functionName}"
    logDebugVar trapFunctionListName
    setVar "${trapFunctionListName}" "${trapFunctions}"
    logDebug "Registered ${signal} trap function: ${functionName}"
}

unregisterTrapFunction () {
    local functionName="$1"
    local signal="$2" # EXIT, HUP or TERM or similar. Defaults to EXIT.

    # EXIT is a bash pseudo-signal which indicates that the shell is ending.
    setDefault signal 'EXIT'

    assertValidTrapSignal "${signal}"

    local trapFunctionList="_trapFunctions_${signal}_trp000"
    local trapFunctions=''
    getVarR1 trapFunctions "${trapFunctionList}"
    local trapFunctionsNew="`echo "${trapFunctions}" | egrep -v "^${functionName}\$"`"
    if [ "${trapFunctions}" = "${trapFunctionsNew}" ]; then
        logInfo "Function is not registered for signal ${signal}: ${functionName}"
    else
        setVar "${trapFunctionList}" "${trapFunctionsNew}"
        logDebug "Function has been unregistered for signal ${signal}: ${functionName}"
    fi
}

getTrapFunctionNameForCodeR1 () {
    local returnVar_trapGetFunctionNameForCodeR1="$1"
    local code="$2"

    use hash

    local hashForCode=''
    hashSumR1 hashForCode "${code}"
    assertVariable hashForCode
    setVar "${returnVar_trapGetFunctionNameForCodeR1}" "trapFunction_${hashForCode}"
}

defineTrapFunctionForCodeR1 () {
    local returnVar_getTrapFunctionFunctionForCodeR1="$1"
    local code="$2"

    local functionName=''
    getTrapFunctionNameForCodeR1 functionName "${code}"
    eval "${functionName} () {
${code}
}"
    setVar "${returnVar_getTrapFunctionFunctionForCodeR1}" "${functionName}"
}

registerTrapCode () {
    local code="$1"
    local signal="$2"

    local generatedFunction=''
    defineTrapFunctionForCodeR1 generatedFunction "${code}"
    registerTrapFunction "${generatedFunction}" "${signal}"
}

unregisterTrapCode () {
    local code="$1"
    local signal="$2"

    local generatedFunction=''
    getTrapFunctionNameForCodeR1 generatedFunction "${code}"
    unregisterTrapFunction "${generatedFunction}" "${signal}"
}

registerExitCode () { registerTrapCode "$1" 'EXIT'; }
unregisterExitCode () { unregisterTrapCode "$1" 'EXIT'; }
registerExitFunction () { registerTrapFunction "$1" 'EXIT'; }
unregisterExitFunction () { unregisterTrapFunction "$1" 'EXIT'; }
showExitFunctions () { showTrapFunctions 'EXIT'; }

registerInterruptCode () { registerTrapCode "$1" 'INT'; }
unregisterInterruptCode () { unregisterTrapCode "$1" 'INT'; }
registerInterruptFunction () { registerTrapFunction "$1" 'INT'; }
unregisterInterruptFunction () { unregisterTrapFunction "$1" 'INT'; }
showInterruptFunctions () { showTrapFunctions 'INT'; }


thankYouExitFunction () {
    logDebugInteractive "Thank you for using bam and the trap module."
}

# MAIN

trapMainRemoteBootstrap () {
    registerExitFunction thankYouExitFunction
}

eval "${showLibraryUsageIfNotSourced}"

trapMainRemoteBootstrap
