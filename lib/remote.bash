#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam remote module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi
# relevant for sh only
if [ "${bam_global}" = '?' ]; then
    qualifyPathR1 bam
    bam_global="${bam}"
fi

libraryUsage () {
    cat <<EOF

BAM remote module for BAM capabilities without installation
on a remote machine

EOF

}

remotePipeBootstrap () {
    # echo "logDebugEnable"
    # FIXME: not sure whether the trap mopdule does define additonal functions that we need to strip.

    local functionNames=`declare -F | sed 's/^declare -f //' | grep -v 'trapFunction_'`
    local functionName=''
    for functionName in ${functionNames}; do
        type ${functionName} | tail -n +2
    done

    echo "main1"
    echo "main2"
    local module=''
    local moluleLoadHistoryMostRecentLast=''
    getModuleLoadHistoryMostRecentLastR1 --short --uniq moluleLoadHistoryMostRecentLast
    echo "moduleLoadHistory_bam000='${moduleLoadHistory_bam000}'"

    while read module; do
        local moduleMainFunction="${module}MainRemoteBootstrap"
        # https://stackoverflow.com/questions/85880/determine-if-a-function-exists-in-bash
        [[ $(type -t "${moduleMainFunction}") == function ]] && echo "${moduleMainFunction}"
    done < <( echo "${moluleLoadHistoryMostRecentLast}" | pipeReverseLines )

    if [ "${isInteractiveShell_global}" = 'yes' ]; then
        echo "logWarning \"remotePipeBootstrap is experimental: ${hostname_global} -> \${hostname_global}\""
    fi
    echo
}

remotePipeRunFunctionOnHost () {
    local functionName="$1"
    local targetHost="$2"

    assertValidName "${functionName}"
    (
        remotePipeBootstrap
        echo "${functionName}"
    ) | ssh -T "${targetHost}" /bin/bash 2>&1 | grep -v 'mesg: ttyname failed: Inappropriate ioctl for device'
    # -T  Disable pseudo-tty allocation.
}

remotePipeRunShellOnHost () {
    # expect approach: https://stackoverflow.com/questions/9301917/can-i-ssh-somewhere-run-some-commands-and-then-leave-myself-a-prompt
    local targetHost="$1"
    assertVariable targetHost

    use tempfile

    local initCmdTmpFile=''
    getTempfileR1 initCmdTmpFile
    # write init commands to a file (or advanced: redirect fd)
    remotePipeBootstrap >"${initCmdTmpFile}"

    local expect_exec=`which expect 2>/dev/null`
    assertVariable expect_exec

    # expect send file: https://serverfault.com/questions/390448/sending-content-of-a-file-in-expect
    # expect supress output: https://stackoverflow.com/questions/26803188/how-to-suppress-expect-send-output
    # - not it ...
    logWarning "The bootstrap is very noisy."
    expectScript="
spawn ssh -t ${targetHost}
stty -echo
send [exec cat ${initCmdTmpFile}]
stty echo
interact
"
    expect -c "${expectScript}"
    deleteTempfile "${initCmdTmpFile}"
}

remotePipeTest () {
    (
        remotePipeBootstrap
        echo libraryUsage
    ) | ssh -T localhost /bin/bash 2>&1 | grep -v 'mesg: ttyname failed: Inappropriate ioctl for device'
    # -T  Disable pseudo-tty allocation.
}

# MAIN

remoteMainRemoteBootstrap () {
    logDebug "remoteMainRemoteBootstrap"
}

remoteExampleUsage () {
    cat <<EOF

remoteExampleUsage

define your usage function before using remoteSaveEnvAsScript().

You can call your usage function from your main fuction that
you specify when calling remoteSaveEnvAsScript().

EOF

}

remoteExampleMainFunction () {
    logInfo "remoteExampleMainFunction(): $@"

    while getopts hdv o
    do
        case "$o" in
            h )
                remoteExampleUsage
                if [ "${isInteractiveShell_global}" = 'yes' ]; then
                    exit 0
                else
                    return
                fi
                ;;
            d )
                logDebugEnable
                ;;
            v )
                logVerboseEnable
                ;;
            * )
                remoteExampleUsage
                if [ "${isInteractiveShell_global}" = 'yes' ]; then
                    exit 1
                else
                    return 1
                fi
                ;;
        esac
    done
    shift `expr $OPTIND - 1`

}

remoteSaveEnvAsScript () {
    # FIXME
    # Option --automodule is blissfully unaware of order dependencies for module loading.
    # Use --module options in the correct order to work around that.
    # Future work could use moduleLoadHistory_bam000 to determine order.
    logDebug "remoteSaveEnvAsScript $@"
    local modules=''
    local automodule=''
    local save='no'
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-m' -o "$1" = '--module' ]; then
            shift
            modules="${modules} $1"
            passOptions="${passOptions} --module '$1'"
            shift
        elif [ "$1" = '-a' -o "$1" = '--automodule' ]; then
            shift
            automodule="$1"
            passOptions="${passOptions} --automodule '$1'"
            shift
        elif [ "$1" = '-s' -o "$1" = '--save' ]; then
            save='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done
    logInfo "remoteSaveEnvAsScript $@"

    local scriptFile="$1"
    local mainFunction="$2" # Optional. Defaults to remoteExampleMainFunction

    assertVar scriptFile
    assertDoesNotExist "${scriptFile}"

    if [ "${mainFunction}" = '' ]; then
        logWarning "mainFunction not specified. Using:
`declare -f remoteExampleMainFunction`"
    fi
    setDefault mainFunction remoteExampleMainFunction
    export BAM_SUPPRESS_INFO_MESSAGE_IF_SOURCED='yes'
    if [ "${save}" = 'no' ]; then
        local automoduleNames="${automodule}"
        local automoduleNamesPrevious=''
        if [ "${automodule}" != '' ]; then
            while [ "${automoduleNames}" != "${automoduleNamesPrevious}" ]; do
                automoduleNamesPrevious="${automoduleNames}"
                automoduleNames=$(
                    (
                        echo export BAM_AUTO_LOAD_MODULES_SKIP='yes'
                        echo . ${bam_global}
                        echo setGlobalVarsBam
                        local module
                        for module in ${modules}; do
                            echo use ${module}
                        done
                        for module in ${automoduleNamesPrevious}; do
                            echo use ${module}
                            echo echo MODULE ${module}
                        done
                        echo 'set | grep -E "^[ ${tab_global}]*use [a-z]" | sed "s/.*use /MODULE /" | sed "s/;.*//"'
                        echo exit
                    ) | bash
                )
                automoduleNames=`echo "${automoduleNames}" | grep -E '^MODULE ' | sed 's/MODULE //' | sort | uniq`
                logDebugVar automoduleNames
            done
            logInfo "Auto-determined dependency modules:" ${automoduleNames}
        fi
        (
            echo export BAM_AUTO_LOAD_MODULES_SKIP='yes'
            echo . ${bam_global}
            cd "`pwd`"
            echo use remote
            local module
            for module in ${modules} ${automoduleNames}; do
                echo use ${module}
            done
            echo remoteSaveEnvAsScript --save ${passOptions} "${scriptFile}" "${mainFunction}"
            echo exit
        ) | bash
    else
        local mainFunctionExistsTest=`declare -F | sed 's/^declare -f //' | grep -E '^'"${mainFunction}"'$'`
        assertVar mainFunctionExistsTest "Main function is not defined: ${mainFunction}"

        touch "${scriptFile}"
        assertFile "${scriptFile}"
        echo '#!/usr/bin/env bash' > "${scriptFile}"
        echo >> "${scriptFile}"
        remotePipeBootstrap >> "${scriptFile}"
        cat <<EOF >> "${scriptFile}"

determineIsSourcedCode='${determineIsSourcedCode}'

### MAIN

eval "\${determineIsSourcedCode}"
runFunctionIfNotSourced ${mainFunction} "\$@"

EOF
        chmod a+x "${scriptFile}"
        logInfo "Wrote script: ${scriptFile}
`ls -lah \"${scriptFile}\"`"
    fi
    unset BAM_SUPPRESS_INFO_MESSAGE_IF_SOURCED
}

eval "${showLibraryUsageIfNotSourced}"

remoteMainRemoteBootstrap
