#!/bin/bash
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam cron module

# source bam.sh
if [ "${bam_global}" = '' ]; then
    for bam in \
        "`dirname -- $0`/bam.sh" \
        "`dirname -- $0`/../bam.sh" \
        ~/bam/bam.sh \
        /root/bam/bam.sh \
        ; do
        if [ -f "${bam}" ]; then
            echo "INFO  Sourcing: ${bam}"
            source "${bam}"
            break
        fi
    done
fi
if [ "${bam_global}" = '' ]; then
    echo "ERROR bam.sh not found."
    exit 1
fi

libraryUsage () {
    cat <<EOF

This module defines development helper functions.

EOF

}

cronHints () {
    crontabHintText="# Crontab timing:
+----------------minute (0-59)
|  +------------ hour (0 - 23)
|  |  +--------- day of the month (1 - 31)
|  |  |  +------ month (1 - 12)
|  |  |  |  +--- day of the week (0-6, 0=Sunday)
|  |  |  |  |
*  *  *  *  *
"
    logInfo "${crontabHintText}"
}

cronExamples () {
    crontabExamplesText="Crontab examples:
00 23 * * *    every day at 23:00
15 6,18 * * *  every day at 06:15 and 18:15
*/5 * * * *    every 5 minutes
20 */2 * * *   every 2 hours, 20 minutes past the hour
0 22 * * 1-5   at 22:00 on every day-of-week from Monday through Friday
https://crontab.guru/
https://crontab.guru/#20_*/2_*_*_*
"
    logInfo "${crontabExamplesText}"
}

cronShow () {
    cronHints
    logInfo "Crontab for: ${username_global}
`crontab -l 2>&1`"
}

getCronLogFileForScriptR1 () {
    local logFileSuffix=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' -o "$1" = '--suffix' ]; then
            shift
            logFileSuffix="$1"
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    setDefault logFileSuffix '.cron.log'
    local returnVarLogFile_getCronLogFileForScriptR1="$1" # Optional
    local script="$2"              # Optional. Defaults to current script.
    setDefault script "${scriptNameQualified_global}"

    local scriptDir=''
    local scriptShort=''
    splitPathR2 scriptDir scriptShort "${script}"

    local logDir=''
    local logDirCandidate=''

    # If we are root, use /var/log.
    # If we are not, we can't write to /var/log. Unless the log file exiss and is writable.
    local logFileShort="${scriptShort:-unknownScriptName}${logFileSuffix}"
    local persistentLogFile="/var/log/${logFileShort}"
    local qualifiedLogFile_getCronLogFileForScriptR1=''
    if [ -f "${persistentLogFile}" -a -w "${persistentLogFile}" ]; then
        qualifiedLogFile_getCronLogFileForScriptR1="${persistentLogFile}"
    else
        if [ "${userid_global}" = '0' -a -d /var/log ]; then
            logDir=/var/log
        else
            for logDirCandidate in \
                '/tmp' \
                "${scriptDir}/log" \
                "${scriptDir}/../log" \
                ; do
                if [ -d "${logDirCandidate}" ]; then
                    logDir="${logDirCandidate}"
                    break
                fi
            done
        fi
        qualifyPathR1 logDir
        qualifiedLogFile_getCronLogFileForScriptR1="${logDir}/${logFileShort}"
    fi
    setVarOrLogInfo "${returnVarLogFile_getCronLogFileForScriptR1}" "${qualifiedLogFile_getCronLogFileForScriptR1}"
}

getCronLogFilesForScriptR2 () {
    local returnVarCronBootLogFile_getCronLogFilesForScriptR2="$1"
    local returnVarCronExecLogFile_getCronLogFilesForScriptR2="$2"

    getCronLogFileForScriptR1 --suffix '.cronboot.log' "${returnVarCronBootLogFile_getCronLogFilesForScriptR2}"
    getCronLogFileForScriptR1 --suffix '.cronexec.log' "${returnVarCronExecLogFile_getCronLogFilesForScriptR2}"
}

cronEnsurePersistentLogFile () {
    # Ensure we have a log file. This may still be at /tmp

    local noRelocate='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '--noRelocate' ]; then
            noRelocate='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local logFile="$1"
    if [ "${logFile}" = '' ]; then
        getCronLogFileForScriptR1 logFile
    fi

    assertVar logFile
    local logFileShort="${logFile##*/}"
    local persistentLogFile="/var/log/${logFileShort}"

    if [ "${userid_global}" = '0' ]; then
        # If we are root, we got a /var/log location for logFile back.
        # Double check for a /tmp version and relocate if that one exists.
        if [ "${logFile%/*}" = '/var/log' ]; then
            if [ ! -f "${logFile}" ]; then
                local logFileCandidate="/tmp/${logFileShort}"
                if [ -f "${logFileCandidate}" ]; then
                    logFile="${logFileCandidate}"
                fi
            fi
        fi
    fi
    cronEnsureLogFile "${logFile}"
    assertFile "${logFile}"
    if [ "${logFile%/*}" = '/var/log' ]; then
        logInfo "Log file is already in persistent location: ${logFile}"
        return
    fi

    if [ "${logFile}" = "${persistentLogFile}" ]; then
        logErrorAndAbort "cronEnsurePersistentLogFile(): We should not be here. logFile and persistentLogFile are the same."
    fi
    assertFile "${logFile}"
    if [ -f "${persistentLogFile}" ]; then
        logWarning "Double log file:
`ls -lah ${logFile}`
`ls -lah ${persistentLogFile}`"
    fi
    assertDoesNotExist "${persistentLogFile}" "Please investigate manually."
    if [ "${noRelocate}" = 'yes' ]; then
        logInfo "Log file should be relocated: mv ${logFile} ${persistentLogFile}"
    else
        logInfo "Relocating log file: mv ${logFile} ${persistentLogFile}"
        if [ "${userid_global}" != '0' ]; then
            # We are not root, so we need sudo, which works in interactive mode only.
            assertInteractive
            logInfo "This might ask for yor sudo password."
        fi
        sudo mv "${logFile}" "${persistentLogFile}"
        assertFile "${persistentLogFile}"
        assertDoesNotExist "${logFile}"
        logInfo --timestamp "Log file relocated from ${logFile} to ${persistentLogFile}" >>"${persistentLogFile}"
    fi
}

cronEnsureLogFile () {
    local logFileToTest="$1"
    if [ "${logFileToTest}" = '' ]; then
        getCronLogFileForScriptR1 logFileToTest
    fi
    assertVariable logFileToTest
    if [ ! -f "${logFileToTest}" ]; then
        logInfo --timestamp "cronEnsureLogFile: created new log file: ${logFileToTest}" >"${logFileToTest}"
    fi
    assertFile "${logFileToTest}" "Unable to create log file: ${logFileToTest}"
}

cronEnsurePersistentLogFiles () {
    local cronBootLogFile=''
    local cronExecLogFile=''

    getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile
    cronEnsurePersistentLogFile --noRelocate "${cronBootLogFile}"
    cronEnsurePersistentLogFile --noRelocate "${cronExecLogFile}"
    cronEnsurePersistentLogFile "${cronBootLogFile}"
    cronEnsurePersistentLogFile "${cronExecLogFile}"
}

cronAssertLogFileWriteAccess () {
    local logFileToTest="$1"
    if [ "${logFileToTest}" = '' ]; then
        getCronLogFileForScriptR1 logFileToTest
    fi
    assertVariable logFileToTest
    assertFile "${logFileToTest}" "Log file does not exist: ${logFileToTest}"
    if [ ! -w "${logFileToTest}" ]; then
        logErrorAndAbort "I don't have write access to log file: ${logFileToTest}"
    fi
    logInfo "I have write access to log file: ${logFileToTest}"
}

cronCheckLogFileR1 () {
    local returnVarAllOkYesNo_cronCheckLogFileR1="$1"
    local logFile="$2"
    local statusSuffix="$3" # Optional

    local resultAllOk_cronCheckLogFileR1='no'
    assertVar logFile
    if [ -f "${logFile}" ]; then
        if [ -s  "${logFile}" ]; then
            logVerbose "Log file: ${statusSuffix}
${logFile}
`cat \"${logFile}\"`"
        else
            logInfo "Log file is empty: ${statusSuffix}
${logFile}"
        fi
        if [ ! -w "${logFile}" ]; then
            logWarning "Log file is not writable by current user:
${logFile}"
        else
            logDebug "Log file does exist and is writable:
${logFile}"
            resultAllOk_cronCheckLogFileR1='yes'
        fi
    else
        logInfo "Log file does not exist: ${statusSuffix}
${logFile}"
    fi
    setVar "${returnVarAllOkYesNo_cronCheckLogFileR1}" \
           "${resultAllOk_cronCheckLogFileR1}"
}

cronCheckLogFile () {
    cronCheckLogFileR1 '' "$@"
}

cronShowLogFiles () {
    # Old logging style used a single file
    local singleLog
    getCronLogFileForScriptR1 singleLog "${script}"

    # New logging style uses a boot and exec log file
    local cronBootLogFile=''
    local cronExecLogFile=''
    getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile

    local logFile=''
    for logFile in "${singleLog}" "${cronBootLogFile}" "${cronExecLogFile}"; do
        if [ "${logFile}" != '' -a  -f "${logFile}" ]; then
            if [ -s  "${logFile}" ]; then
                logInfo "Log file: ${logFile}
`cat \"${logFile}\"`"
            else
                logInfo "Log file is empty: ${logFile}"
            fi
        fi
    done
}

cronTailLogFiles () {
    local follow='yes'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '--nofollow' ]; then
            follow='no'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local followOption=''
    if [ "${follow}" = 'yes' ]; then
        assertInteractive
    fi

    # Old logging style used a single file
    local singleLog
    getCronLogFileForScriptR1 singleLog "${script}"

    # New logging style uses a boot and exec log file
    local cronBootLogFile=''
    local cronExecLogFile=''
    getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile

    if [ ! -f "${singleLog}" -a \
         ! -f "${cronBootLogFile}" -a \
         ! -f "${cronExecLogFile}" ]; then
        logInfo "None of the following log files exists:
old style: ${singleLog}
new style: ${cronBootLogFile}
new style: ${cronExecLogFile}"
        return
    fi

    if [ ! -f "${singleLog}" ]; then
        singleLog=''
    fi
    if [ ! -f "${cronBootLogFile}" ]; then
        cronBootLogFile=''
    fi
    if [ ! -f "${cronExecLogFile}" ]; then
        cronExecLogFile=''
    fi
    if [ "${follow}" = 'no' ]; then
        local tailData="`tail ${singleLog} ${cronBootLogFile} ${cronExecLogFile}`"
        logInfo "${tailData}"
    else
        tail -f ${singleLog} ${cronBootLogFile} ${cronExecLogFile}
    fi
}

cronTailAllExecLogs () {
   tail -f /var/log/*cronexec*log /tmp/*cronexec*log 2>/dev/null
}

cronLog () {
    assertInteractive
    local cronbootLogs=`crontab -l | grep cronboot | sed "s/.*1>'//" | sed "s/'.*//"`
    local cronexecLogs=`echo "${cronbootLogs}" | sed 's/cronboot/cronexec/'`
    local logs=''
    local log
    for log in ${cronexecLogs} ${cronbootLogs}; do
        if [ -f "${log}" ]; then
            logs="${logs}
${log}"
        fi
    done
    echo "Please select a log file:"
    select log in ${logs}; do
        if [ "${log}" != '' ]; then
            break
        fi
    done
    tail -f "${log}"
}

cronTailExecLog () {
    local follow='yes'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '--nofollow' ]; then
            follow='no'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local followOption=''
    if [ "${follow}" = 'yes' ]; then
        assertInteractive
    fi


    # New logging style uses a boot and exec log file
    local cronBootLogFile=''
    local cronExecLogFile=''
    getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile

    if [ ! -f "${cronExecLogFile}" ]; then
	logInfo "Log file does not exist: ${cronExecLogFile}"
	return
    fi
    if [ "${follow}" = 'no' ]; then
        local tailData="`tail ${cronExecLogFile}`"
        logInfo "Tail of exec log: ${cronExecLogFile}
${tailData}"
    else
        logInfo "Starting follow tail of exec log: ${cronExecLogFile}"
        tail -f ${cronExecLogFile}
    fi
}

cronCheckScriptR1 () {
    local returnVarScriptIsOk_cronCheckScriptR1="$1" # Optional.
    local script="$2"              # Optional. Defaults to current script.

    local allOk='yes'
    setDefault script "${scriptNameQualified_global}"
    assertVariable script

    local entry=`crontab -l | grep "${script}"`
    if [ "${entry}" = '' ]; then
        allOk='no'
        logInfo "No crontab entry for: ${script}"
    else
        logVerbose "Crontab entry exists:
${entry}"
    fi
    if [ ! -x "${script}" ]; then
        allOk='no'
        logInfo "Script is not executable:
`ls -lah ${script}`"
    else
        logVerbose "Script is executable:
`ls -lah ${script}`"
    fi

    # Old logging style used a single file
    local singleLog
    getCronLogFileForScriptR1 singleLog "${script}"

    # New logging style uses a boot and exec log file
    local cronBootLogFile=''
    local cronExecLogFile=''
    getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile

    local newStyleBootOk=''
    local newStyleExecOk=''
    local newStyleAllOk='no'
    local oldStyleOk=''
    cronCheckLogFileR1 newStyleBootOk "${cronBootLogFile}" "new style bootstrap log"
    cronCheckLogFileR1 newStyleExecOk "${cronExecLogFile}" "new style execution log"
    if [ "${newStyleBootOk}" = 'yes' -a "${newStyleExecOk}" = 'yes' ]; then
        newStyleAllOk='yes'
    fi
    logDebugVar newStyleBootOk newStyleExecOk newStyleAllOk

    if [ "${newStyleAllOk}" = 'yes' ]; then
        if [ -f "${singleLog}" ]; then
            cronCheckLogFileR1 oldStyleOk "${singleLog}" "old style single log"
            logWarning "New and old style log files do exist."
            logWarning "You might want to delete: ${singleLog}"
        else
            logVerbose "New style logging is in place."
        fi
    else
        cronCheckLogFileR1 oldStyleOk "${singleLog}" "old style single log"
        if [ -f "${cronBootLogFile}" -o -f "${cronExecLogFile}" ]; then
            if [ -f "${singleLog}" ]; then
                logWarning "You might want to clean up the log files to either new or old style."
                logWarning "Remember to check the crontab entry and reinstall if required."
            else
                logWarning "New style log files need to be set up the correct way."
            fi
        fi
    fi

    setVarOrLogInfo "${returnVarScriptIsOk_cronCheckScriptR1}" "${allOk}" "Script is ready for cron execution: ${allOk}"
    isCronScriptRunning
}

cronCheckScript () { cronCheckScriptR1 '' "$@"; }

cronRemoveScriptR1 () {
    local returnVarCrontabEntry="$1" # Optional.
    local script="$2"               # Optional. Defaults to current script.

    setDefault script "${scriptNameQualified_global}"
    qualifyPathR1 script

    local entry_cronRemoveScriptR1=`crontab -l 2>/dev/null | grep "${script}"`
    if [ "${entry_cronRemoveScriptR1}" = '' ]; then
        logErrorAndAbort "Script is not in crontab: ${script}"
    fi

    local tempfile=`mktemp /tmp/bam_temp_file_${script##*/}.XXXXXX` \
              || logErrorAndAbort "Unable to create temp file."
    crontab -l 2>/dev/null | grep -v "${script}" > "${tempfile}"
    crontab "${tempfile}"
    rm -f "${tempfile}"
    assertDoesNotExist "${tempfile}"
    setVarOrLogInfo "${returnVarCrontabEntry}" "${entry_cronRemoveScriptR1}" "Removed crontab entry:
${entry_cronRemoveScriptR1}"
}
cronRemoveScript () { cronRemoveScriptR1 '' "$@"; }
cronUninstallScript () { cronRemoveScriptR1 '' "$@"; }

cronInstallScriptR1 () {
    local returnVarCrontabEntry="$1" # Optional.
    local timing="$2"
    local script="$3"               # Optional. Defaults to current script.
    local scriptArgs="$4"           # Optional.

    setDefault script "${scriptNameQualified_global}"
    assertVariable script
    qualifyPathR1 script

    if [ ! -x "${script}" ]; then
        errorAbort "Script is not executable: ${script}"
    fi

    local cronBootLogFile=''
    local cronExecLogFile=''
    getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile

    cronEnsureLogFile "${cronBootLogFile}"
    cronEnsureLogFile "${cronExecLogFile}"
    cronAssertLogFileWriteAccess "${cronBootLogFile}"
    cronAssertLogFileWriteAccess "${cronExecLogFile}"

    local existingEntry=`crontab -l 2>/dev/null | grep "${script}"`
    if [ "${existingEntry}" != '' ]; then
        cronRemoveScript "${script}"
    fi
    local log
    getCronLogFileForScriptR1 --suffix '.cronboot.log' log "${script}"

    local entry_cronInstallScriptR1="${timing} [ -x '${script}' ] && '${script}' ${scriptArgs} 1>'${log}' 2>&1"
    logDebug "Installing crontab entry:
${entry_cronInstallScriptR1}"

    local tempfile=`mktemp /tmp/bam_temp_file_${script##*/}.XXXXXX` \
              || logErrorAndAbort "Unable to create temp file."
    crontab -l 2>/dev/null > "${tempfile}"
    echo "${entry_cronInstallScriptR1}" >> "${tempfile}"
    crontab "${tempfile}"
    setVarOrLogInfo "${returnVarCrontabEntry}" "${entry_cronInstallScriptR1}" "Installed crontab entry:
${entry_cronInstallScriptR1}"
    cronCheckScript "${script}"
}
cronInstallScript () { cronInstallScriptR1 '' "$@"; }

cronInstallAtRebootR1 () {
    local returnVarCrontabEntry="$1" # Optional.
    local script="$2"               # Optional. Defaults to current script.
    local scriptArgs="$3"           # Optional.

    cronInstallScriptR1 "${returnVarCrontabEntry}" '@reboot' "${script}" "${scriptArgs}"
}
cronInstallAtReboot () { cronInstallAtRebootR1 '' "$@"; }

cronInstallEveryMinuteR1 () {
    local returnVarCrontabEntry="$1" # Optional.
    local script="$2"               # Optional. Defaults to current script.
    local scriptArgs="$3"           # Optional.

    cronInstallScriptR1 "${returnVarCrontabEntry}" '*/1 * * * *' "${script}" "${scriptArgs}"
}
cronInstallEveryMinute () { cronInstallEveryMinuteR1 '' "$@"; }

cronInstallEveryFiveMinutesR1 () {
    local returnVarCrontabEntry="$1" # Optional.
    local script="$2"               # Optional. Defaults to current script.
    local scriptArgs="$3"           # Optional.

    cronInstallScriptR1 "${returnVarCrontabEntry}" '*/5 * * * *' "${script}" "${scriptArgs}"
}
cronInstallEveryFiveMinutes () { cronInstallEveryFiveMinutesR1 '' "$@"; }

cronInstallEveryTenMinutesR1 () {
    local returnVarCrontabEntry="$1" # Optional.
    local script="$2"               # Optional. Defaults to current script.
    local scriptArgs="$3"           # Optional.

    cronInstallScriptR1 "${returnVarCrontabEntry}" '*/10 * * * *' "${script}" "${scriptArgs}"
}
cronInstallEveryTenMinutes () { cronInstallEveryTenMinutesR1 '' "$@"; }

cronInstallEveryHourR1 () {
    local offset=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-o' -o "$1" = '-offset' -o "$1" = '--offset' ]; then
            # Offset in minutes.
            shift
            offset="$1"
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVarCrontabEntry="$1" # Optional.
    local script="$2"               # Optional. Defaults to current script.
    local scriptArgs="$3"           # Optional.

    if [ "${offset}" = '' ]; then
        getRandomIntR1 offset 0 59
    fi
    assertInt "${offset}" "Offset is not an integer: ${offset}"
    assertIntInRange ${offset} 0 59 "Not a valid minute: ${offset}"

    cronInstallScriptR1 "${returnVarCrontabEntry}" "${offset} * * * *" "${script}" "${scriptArgs}"
}
cronInstallEveryHour () { cronInstallEveryHourR1 '' "$@"; }

cronInstallDailyR1 () {
    local timeBracket='default'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--daytime' ]; then
            timeBracket='daytime'
            shift
        elif [ "$1" = '--morning' ]; then
            timeBracket='morning'
            shift
        elif [ "$1" = '--afternoon' ]; then
            timeBracket='afternoon'
            shift
        elif [ "$1" = '--evening' ]; then
            timeBracket='evening'
            shift
        elif [ "$1" = '--nighttime' ]; then
            timeBracket='nighttime'
            shift
        elif [ "$1" = '--latenight' ]; then
            timeBracket='latenight'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done
    local returnVarCrontabEntry="$1" # Optional.
    local hourAndMinute="$2"        # E.g. 23:15
    local script="$3"               # Optional. Defaults to current script.
    local scriptArgs="$4"           # Optional.

    local hour=''
    local minute=''
    local startHour='2'
    local window='4'
    local endHour=''
    if [ "${hourAndMinute}" = '' ]; then
        if [ "${timeBracket}" = 'daytime' ]; then
            startHour=6
            window=12
        elif [ "${timeBracket}" = 'morning' ]; then
            startHour=6
            window=6
        elif [ "${timeBracket}" = 'afternoon' ]; then
            startHour=12
            window=6
        elif [ "${timeBracket}" = 'evening' ]; then
            startHour=18
            window=6
        elif [ "${timeBracket}" = 'nighttime' ]; then
            startHour=18
            window=12
        elif [ "${timeBracket}" = 'latenight' ]; then
            startHour=0
            window=6
        else
            startHour=2
            window=4
        fi
        let endHour=${startHour}+${window}-1
        let endHour="${endHour} % 24"
        let hour="$RANDOM % ${window}"
        let hour+=${startHour}
        let hour="${hour} % 24"
        let minute="$RANDOM % 60"
        hour=`printf '%02d' "${hour}"`
        minute=`printf '%02d' "${minute}"`
        hourAndMinute="${hour}:${minute}"
        logInfo "Generated random time between ${startHour}:00 and ${endHour}:59: ${hour}:${minute}"
    else
        hour="${hourAndMinute%%:*}"
        minute="${hourAndMinute##*:}"
    fi
    assertInt "${hour}"
    if [ "${hourAndMinute}" = "${hour}" ] ;then
        # There was no : aka no minutes.
        minute=0
    fi
    assertInt "${minute}"
    cronInstallScriptR1 "${returnVarCrontabEntry}" "${minute} ${hour} * * *" "${script}" "${scriptArgs}"
}

cronInstallDaily () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    cronInstallDailyR1 ${passOptions} '' "$@"
}

cronScriptBootstrap () {
    local noLogFiles='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '--noLogFiles' ]; then
            noLogFiles='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    logTimestampEnable
    use lock

    local header="$1"

    local cronBootLogFile=''
    local cronExecLogFile=''

    local useCopyMode='no'
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        useCopyMode='yes'
    fi
    if [ "${noLogFiles}" != 'yes' ]; then
        getCronLogFilesForScriptR2 cronBootLogFile cronExecLogFile
        logInfoInteractive "Redirecting all output to:
${cronBootLogFile}
${cronExecLogFile}"
        if [ "${useCopyMode}" = 'yes' ]; then
            copyStdoutStderrToFile "${cronBootLogFile}"
        else
            redirectStdoutStderrToFile "${cronBootLogFile}"
        fi
    fi

    assertLockSingleScriptInstance
    if [ "${noLogFiles}" != 'yes' ]; then
        if [ "${useCopyMode}" = 'yes' ]; then
            copyStdoutStderrToFile "${cronExecLogFile}"
        else
            redirectStdoutStderrToFile "${cronExecLogFile}"
        fi
    fi
    logHeader1 "${header}"
    logCurrentScript
}

isCronScriptRunningR1 () {
    use lock
    isLockScriptRunningR1 "$@"
}

isCronScriptRunning () {
    isCronScriptRunningR1 '' "$@"
}

cronShowStatus () {
    cronTailExecLog --nofollow
    cronCheckScriptR1
    isCronScriptRunning
}

cronStatusAll () {
    cronCheckAll --logfile
}

cronCheckAll () {
    local withLogFileTail='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-l' -o "$1" = '--logfile' ]; then
            withLogFileTail='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "${FUNCNAME[0]}(): Ignoring unknown option: $1"
            shift
        fi
    done

    local scripts=`
              crontab -l \
              | grep ' ] && '"'" \
              | sed 's/.*\] && '"'"'//' \
              | sed 's/'"'"' .*//'`
    local script=''
    while read script; do
        if [ ! -f "${script}" ]; then
            lowWarning "Script does not exist: ${script}"
            continue
        fi
        local isScriptUsingCronModuleTest=`grep 'cronCheckScript' "${script}"`
        if [ "${isScriptUsingCronModuleTest}" = '' ]; then
            logWarning "Skipping: ${script}
Script does not appear to be using the bam cron module."
            continue
        fi
        logHeader1 -f "${script}"
        if [ "${withLogFileTail}" = 'yes' ]; then
            "${script}" -s
        else
            "${script}" -c
        fi
    done < <( echo "${scripts}" | pipeStripStandard )
}

# MAIN

eval "${showLibraryUsageIfNotSourced}"
