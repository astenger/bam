#!/bin/sh
# This is a library that should be sourced, but it will show you
# defined functions and global variables if you run it.

# bam core library

if [ "${BASH_VERSION}" != '' ]; then
    # This yields syntax error on busybox sh - so we can't have that in the code:
    # scriptArguments_global=("$@")
    # So we'll wrap and eval it:
    scriptArguments_global_wrapper='scriptArguments_global=("$@")'
    eval "${scriptArguments_global_wrapper}"
else
    # Known limitation: This will break on arguments with spaces.
    scriptArguments_global="$*"
fi

reExecute () {
    # This only has been tested on bash so far.
    assertVariableValue isBash_global 'yes'

    local additionalArgs=''
    if [ "${isBash_global}" = 'yes' ]; then
        additionalArgs_wrapper='additionalArgs=("$@")'
        eval "${additionalArgs_wrapper}"
    else
        # Known limitation: This will break on arguments with spaces.
        additionalArgs="$*"
    fi

    # Safety net: only allow 5 reExecute deep
    setDefault BAM_RE_EXECUTE_MAX_DEPTH 5 # This is the orignal script plus 4 reExecute invocations
    setDefault BAM_RE_EXECUTE_DEPTH 0
    let BAM_RE_EXECUTE_DEPTH+=1
    if [ ${BAM_RE_EXECUTE_DEPTH} -ge ${BAM_RE_EXECUTE_MAX_DEPTH} ]; then
        logErrorAndAbort "reExecute(): max depth reached: ${BAM_RE_EXECUTE_DEPTH} >= ${BAM_RE_EXECUTE_MAX_DEPTH}"
    else
        logDebug "reExecute(): max depth not reached: ${BAM_RE_EXECUTE_DEPTH} < ${BAM_RE_EXECUTE_MAX_DEPTH}"
    fi
    export BAM_RE_EXECUTE_MAX_DEPTH
    export BAM_RE_EXECUTE_DEPTH

    local command=''
    if [ "${scriptNameQualified_global}" != '' ]; then
        command="${scriptNameQualified_global}"
    else
        if [ "${isInteractiveShell_global}" = 'yes' ]; then
            if [ "${scriptNameRawBam_global:0:1}" = '-' ]; then
                command="${scriptNameRawBam_global:1}"
            else
                command="${scriptNameRawBam_global}"
            fi
        fi
    fi
    assertVariable command
    if [ "${isBash_global}" = 'yes' ]; then
        local reExecuteBash='
                logInfoInteractive "Re-executing myself: ${command} ${scriptArguments_global[@]} ${additionalArgs[@]}"
                exec "${command}" "${scriptArguments_global[@]}" "${additionalArgs[@]}"
            '
        eval "${reExecuteBash}"
        return
    else
        logInfoInteractive "Re-executing myself: ${command} ${scriptArguments_global} ${additionalArgs}"
        exec "${command}" ${scriptArguments_global}  ${additionalArgsreExecute}
    fi
}

assertBash () {
    if [ "${isBash_global}" != 'yes' ]; then
        logErrorAndAbort "This is not a bash."
    fi
}

assertTrippleRedirectIsUsable () {
    if [ "${isTrippleRedirectUsable_global}" != 'yes' ]; then
        logErrorAndAbort "Tripple redirect is not usable on this shell."
    fi
}

assertSubshellRedirectIsUsable () {
    if [ "${isSubshellRedirectUsable_global}" != 'yes' ]; then
        logErrorAndAbort "Subshell redirect is not usable on this shell."
    fi
}

assertCatEofIsUsable () {
    # This may generate a trace that some temporary file is not
    # accessible on shells that have an issue with <<EOF.
    if [ "${isCatEofUsable_global}" != 'yes' ]; then
        logErrorAndAbort "cat <<EOF is not usable on this shell."
    fi
}

determineShellCapabilities () {
    # Some systems, e.g. debian and ubuntu, use dash to simulate sh, which
    # has multiple concerns for what we are trying to do here. Luckily
    # these systems usually have bash installed as well, so that should
    # not be too much of a drama ;-) And we are not shooting for a posix
    # baseline.
    # This is also an issue for sh on FreeBSD, e.g. pfSense, when bash is
    # not on the system either.

    testStringSlicing () {
        testVar='yesNO'
        (
            echo "${testVar:0:3}"
        ) 2>/dev/null
    }
    stringSlicingTest=`testStringSlicing`
    if [ "${stringSlicingTest}" != 'yes' ]; then
        echo "ERROR This shell does not support string slicing."
        echo "ERROR This will go wrong."
        if [ "`which bash 2>/dev/null`" != '' ]; then
            echo "INFO  This system does have bash."
            echo "INFO  For the best bam experience please use bash."
        fi
        abort 2>/dev/null
    fi

    local rc=''

    # We are using the variable isInteractive_global to determine
    # whether we are in an interactive mode or not. This is important
    # especially if we are using functions during shell
    # initialization, which may result in log messages making the
    # shell "unclean" for ssh or rsync usage.

    isInteractive_global='no'

    # If we have the prompt variable set we'll consider this
    # being interactive.
    if [ "${PS1}" != '' ]; then
        isInteractive_global='yes'
    else
        # Test for an underlying tty.
        tty 1>/dev/null 2>&1; rc=$?
        if [ "${rc}" = '0' ]; then
            isInteractive_global='yes'
        fi
    fi

    # scriptNameRawBam_global may already be set when scripts are nested.
    # scriptNameRawBam_global yields the top level script.  Note that this
    # might be something like '-bash' on an interactive shell.
    # Note: We don't have setDefault yet.
    if [ "${scriptNameRawBam_global}" = '' ]; then
        scriptNameRawBam_global="$0"
    fi
    isInteractiveShell_global='no'
    if [ "${isInteractive_global}" = 'yes' ]; then
        # https://stackoverflow.com/questions/911168/how-to-detect-if-my-shell-script-is-running-through-a-pipe
        # The -t 1 might be all we need. For now, adding it as one of many though.
        #
        # You can start a login bash with /bin/bash -l in which case
        # we do have a fully interactive shell, but
        # ${scriptNameRawBam_global} is '/bin/bash' and not '-bash'
        #
        if [ -t 1 ]; then
            local scriptNameRawShort="${scriptNameRawBam_global##*/}"
            if [ "${scriptNameRawShort}" = 'bash' -o \
                 "${scriptNameRawShort}" = 'sh' -o \
                 "${scriptNameRawShort}" = 'zsh' ]; then
                isInteractiveShell_global='yes'
            fi
        fi
        if [ "${scriptNameRawBam_global}" = 'bash' -o \
             "${scriptNameRawBam_global}" = '-bash' -o \
             "${scriptNameRawBam_global}" = 'sh' -o \
             "${scriptNameRawBam_global}" = '-sh' -o \
             "${scriptNameRawBam_global}" = 'zsh' -o \
             "${scriptNameRawBam_global}" = '-zsh' ]; then
            isInteractiveShell_global='yes'
        fi
    fi

    # For a script launched from an interactive shell:
    # isInteractive_global is 'yes'
    # isInteractiveShell_global is 'no'
    #
    # If we want to know e.g. if the user can type a sudo password
    # when required while running a script,
    # [ isInteractive_global = 'yes' ]
    # is a good check. If we want to modify the command promp
    # this only makes sense when we are on an interactive session that
    # displays command prompts in the first place and in this case
    # [ isInteractiveShell_global = 'yes' ]
    # is the one to use.
    #
    # In any case, if isInteractive_global is 'no', so is
    # isInteractiveShell_global 'no as well.


    # test for while ... done < <( ... )
    #
    # This allows for loop constructs where the loop body does not
    # execute in a sub-shell.
    #
    # Subshell redirects will fail in sh (but work in bash):
    #
    # while read line; do echo "$line"; done < <( echo "test line" )
    # sh: syntax error near unexpected token `<'
    #
    # exec > >(tee -a "${logfile}")
    # sh: syntax error near unexpected token `<'

    isSubshellRedirectUsableTest () {
        local line=''
        (     eval 2>/dev/null '
              while read line; do
                  echo yes
              done 2>/dev/null < <( echo "testLine" ); exit 0'
        )
    }
    isSubshellRedirectUsable_global=`isSubshellRedirectUsableTest`
    if [ "${isSubshellRedirectUsable_global}" != 'yes' ]; then
        isSubshellRedirectUsable_global='no'
    fi

    # Are we on bash?
    isBash_global='no'
    isSimulatedSh_global='no'
    if [ "${BASH_VERSION}" != '' ]; then
        # For bash simulating sh this will be true as well.
        # Bash simulating sh also knows: =~ <<<
        # but does not know: < <( ... )
        if [ "${isSubshellRedirectUsable_global}" = 'no' ]; then
            # This is bash simulating sh
            isBash_global='no'
            isSimulatedSh_global='yes'
        else
            isBash_global='yes'
        fi
    fi

    if [ "${ZSH_VERSION}" != '' ]; then
        isZsh_global='yes'
        # https://scriptingosx.com/2019/08/moving-to-zsh-part-8-scripting-zsh/
        # emulate bash
        # see: https://zsh.sourceforge.io/FAQ/zshfaq03.html
        # We need this for compatibility.
        setopt shwordsplit

        # see: https://unix.stackexchange.com/questions/310540/how-to-get-rid-of-no-match-found-when-running-rm
        # use bash style globbing
        setopt +o nomatch
    else
        isZsh_global='no'
    fi

    # There are certain environments where the <<< does not work,
    # e.g. on busybox sh (but works on bash simulating sh):
    #
    # read -d '' -r isReadUsableTest <<< 'testValue'
    # -sh: syntax error: unexpected redirection

    # Testing for syntax that a shell does not support is tricky.  To
    # avoid bad code on load we'll be using eval, so the script
    # actually does load and gets executed.

    # However, when the eval with unsupported code is executed the
    # script will die as well, unless we put it into a
    # sub-process. And we can test the return code from there to see
    # whether it worked. (We can't test the variable set in the
    # sub-process outside of the sub-process.)

    local rc=''

    # test for <<<

    # We'll use this for setVar if available.
    local isTrippleRedirectUsableTest=''
    ( eval 2>/dev/null "
          read -d '' -r isTrippleRedirectUsableTest <<< 'testValue'
          exit 0"
    ); rc=$?
    if [ "${rc}" = '0' ]; then
        isTrippleRedirectUsable_global='yes'
    else
        isTrippleRedirectUsable_global='no'
    fi

    # test for cat <<EOF

    # This is usually not so much an issue of the shell, but of the
    # environment the shell is running on, typically where user access
    # to /tmp is restricted.
    local catEofTestCode="cat << EOF 2>&1
testOutput
EOF"
    local isCatEofUsableTest=`eval "${catEofTestCode}" 2>/dev/null`
    if [ "${isCatEofUsableTest}" = 'testOutput' ]; then
        isCatEofUsable_global='yes'
    else
        isCatEofUsable_global='no'
    fi

    # test indirect expansion

    local pointer='variable'
    local variable='value'
    isIndirectExpansionUsable_global='no'
    local indirectExpansionTestCode='
        if [ "${!pointer}" = "${variable}" ]; then
            isIndirectExpansionUsable_global="yes"
        fi'
    ( eval "${indirectExpansionTestCode}; exit 0" 2>/dev/null ); rc=$?
    if [ "${rc}" = '0' ]; then
        isIndirectExpansionUsable_global='yes'
    fi
    #if [ "${!pointer}" = "${variable}" ]; then
    #    isIndirectExpansionUsable_global='yes'
    #fi
}
# determineShellCapabilities needs to happen right away - before we
# establish better getVarR1 and setVar functions.
determineShellCapabilities

showShellCapabilities () {
    logInfo "Is the current shell interactive:                    isInteractive_global=${isInteractive_global}"
    logInfo "Is the current shell a bash shell:                          isBash_global=${isBash_global}"
    logInfo "Is the current shell bash simulating sh:             isSimulatedSh_global=${isSimulatedSh_global}"
    logInfo "Is the current shell a zsh shell:                            isZsh_global=${isZsh_global}"
    logInfo "Can 'read <<<' be used:                    isTrippleRedirectUsable_global=${isTrippleRedirectUsable_global}"
    logInfo "Can 'while ... done < <( ... )' be used:  isSubshellRedirectUsable_global=${isSubshellRedirectUsable_global}"
    logInfo "Can cat <<EOF be used:                              isCatEofUsable_global=${isCatEofUsable_global}"
    logInfo "Can indirect expansion be used:          isIndirectExpansionUsable_global=${isIndirectExpansionUsable_global}"
    logInfo "setVar function type is:                        setVarFunctionType_global=${setVarFunctionType_global}"
    logInfo "getVarR1 function type is:                    getVarR1FunctionType_global=${getVarR1FunctionType_global}"

    local moduleCapabilityFunctions=`declare -F | awk ' { print $3 } ' | grep 'ShowShellCapabilities'`
    for moduleCapabilityFunction in ${moduleCapabilityFunctions}; do
        ${moduleCapabilityFunction}
    done
}

setVarTolerant () {
    if [ "$1" = '' ]; then
        return
    fi
    setVar "$1" "$2"
}

defineShellFeatureDependentFunctions () {
    # setVar - define that one first.
    if [ "${isSubshellRedirectUsable_global}" = 'yes' ]; then
        # setVar - more solid implementation.
        # $1 - variable name
        # $2 - value to be set
        # Overlapping local and global variable name spaces are
        # tricky, so we'll stick to using $1 and $2.

        # We need to define this using eval or the script will stop
        # loading with an error on shells that do not support <<<

        # read -d '' should terminate at end of file only, but without
        # IFS='' it will loose trailing whitespace.

        # This is not good enough, since <<< does add a single traling
        # newline for whatever reason:
        # IFS='"''"' read -d '"''"' -r "$1" <<<"$2"
        # However, < <( echo -n "$2" ) does seem to do the trick
        # NOTE: echo does escapes like \b which we want to avoid
        #       printf does not do this
        setVarFunctionWithSubshellRedirect='
            setVar () {
                if [ "$1" = "" ]; then
                    logErrorAndAbort "setVar(): no variable name specified."
                fi
                IFS='"''"' read -d '"''"' -r "$1" < <( printf "%s" "$2" )
            }
            '
        eval "${setVarFunctionWithSubshellRedirect}"
        setVarFunctionType_global='subshellRedirect'
    else
        # We have two alternatives. One looses trailing blank lines,
        # the other cannot handle tick chars in values. Choosing
        # lesser evil of vanishing trailing blank lines.
        setVar () {
            if [ "$1" = "" ]; then
                logErrorAndAbort "setVar(): no variable name specified."
            fi
            # NOTE: the shell looses trailing blank lines.
            local valueEsc_setVar=`printf '%s' "${2}" | sed "s/'/'\"'\"'/g"`
            # NOTE: this does not work in bash simulating sh
            eval "${1}"="'${valueEsc_setVar}'"
        }
        setVarFunctionType_global='evalEsc'

        local setvarTest=''
        setVar setvarTest 'b c'
        if [ "${setvarTest}" != 'b c' ]; then
            setVar () {
                # simple setVar implementation with tic limitation

                # $1 - variable name
                # $2 - value to be set
                # Overlapping local and global variable name spaces are
                # tricky, so we'll stick to using $1 and $2.

                # If there are single quotes aka ticks in the value the
                # eval function will break. Guard against that.  We are
                # not using assertNoTics to avoid circular dependencies.
                if [ "$1" = "" ]; then
                    logErrorAndAbort "setVar(): no variable name specified."
                fi
                local tickTest_setVar="${2#*\'}"
                # comment to fix gitlab syntax highlighting "${2#*\'}"
                if [ "$2" != "${tickTest_setVar}" ]; then
                    logErrorAndAbort \
                        "Tick found in value to be set for variable $1: $2"
                fi
                eval "${1}"="'${2}'"
            }
            setVarFunctionType_global='evalNoTic'
        fi
    fi

    # getVar, setDefault, and assertVar
    if [ "${isIndirectExpansionUsable_global}" = 'yes' ];then
        getVarR1WithIndirectExpansion='
            getVarR1 () {
                # $1 - return variable name
                # $2 - variable name that holds the value

                if [ "${1}" = "" ]; then
                    logErrorAndAbort "getVarR1 (): no return variable specified."
                fi
                if [ "${2}" = "" ]; then
                    logErrorAndAbort "getVarR1 (): no variable name specified."
                fi

                setVar "$1" "${!2}"
            }

            setDefault () {
                # $1 - variable name
                # $2 - default value to set if current value is empty

                if [ "${!1}" = "" ]; then
                    setVar "${1}" "${2}"
                fi
            }

            setDefaultFromVar () {
                # $1 - variable name
                # $2 - default variable to copy if current value is empty

                if [ "${!1}" = "" ]; then
                    local defaultValue_setDefaultFromVar=''
                    getVarR1 defaultValue_setDefaultFromVar "${2}"
                    setVar "${1}" "${defaultValue_setDefaultFromVar}"
                fi
            }

            assertVar () {
                local passOptions=''
                while [ "${1:0:1}" = '-' ]; do
                    if [ "$1" = '--' ]; then
                        shift
                        break
                    else
                        passOptions="${passOptions} $1"
                        shift
                    fi
                done

                # $1 - variable name
                # $2 - error message if variable is not set

                if [ "${!1}" = "" ]; then
                    local errorMessage="$2"
                    setDefault errorMessage "Variable not set or empty: ${1}"
                    logErrorAndAbort ${passOptions# } "${errorMessage}"
                fi
            }
            '
        eval "${getVarR1WithIndirectExpansion}"
        getVarR1FunctionType_global='indirectExpansion'
    else
        getVarR1 () {
            # $1 - return variable name
            # $2 - variable name that holds the value

            if [ "${1}" = '' ]; then
                logErrorAndAbort "getVarR1 (): no return variable specified."
            fi
            if [ "${2}" = '' ]; then
                logErrorAndAbort "getVarR1 (): no variable name specified."
            fi

            local value_getVarR1=''
            eval value_getVarR1="\"\$$2\""

            setVar "$1" "${value_getVarR1}"
        }
        setDefault () {
            # $1 - variable name
            # $2 - default value to set if current value is empty

            local value_setDefault=''
            getVarR1 value_setDefault "${1}"
            if [ "${value_setDefault}" = "" ]; then
                setVar "${1}" "${2}"
            fi
        }
        setDefaultFromVar () {
            # $1 - variable name
            # $2 - default variable to copy if current value is empty

            local value_setDefaultFromVar=''
            getVarR1 value_setDefaultFromVar "${1}"
            if [ "${value_setDefault}" = "" ]; then
                local defaultValue_setDefaultFromVar=''
                getVarR1 defaultValue_setDefaultFromVar "${2}"
                setVar "${1}" "${defaultValue_setDefaultFromVar}"
            fi
        }
        getVarR1FunctionType_global='eval'
        assertVar () {
            local passOptions=''
            while [ "${1:0:1}" = '-' ]; do
                if [ "$1" = '--' ]; then
                    shift
                    break
                else
                    passOptions="${passOptions} $1"
                    shift
                fi
            done
            # $1 - variable name
            # $2 - error message if variable is not set

            local value_assertVariable=''

            getVarR1 value_assertVariable "$1"
            if [ "${value_assertVariable}" = '' ]; then
                local errorMessage="$2"
                setDefault errorMessage "Variable not set or empty: ${1}"
                logErrorAndAbort ${passOptions# } "${errorMessage}"
            fi
        }

    fi

    # sourceScriptFromVar_
    # Note: isInteractiveShell_global is not available yet.

    if [ "${isSubshellRedirectUsable_global}" = 'yes' -a \
         "${isInteractiveShell_global}" = 'yes' ]; then
        # Turns out this function works on an interactive shell, but
        # not while a script is running.
        sourceScriptFromVarWithSubshellRedirect='
            sourceScriptFromVar_ () {
                local varWithScript="$1"
                local script_sourceScriptFromVar_=""
                getVarR1 script_sourceScriptFromVar_ "${varWithScript}"
                source <( echo "${script_sourceScriptFromVar_}" )
            }
            '
        eval "${sourceScriptFromVarWithSubshellRedirect}"
        sourceScriptFromVar_FunctionType_global='redirect'
    else
        sourceScriptFromVar_ () {
            local varWithScript="$1"
            local script_sourceScriptFromVar_=""
            getVarR1 script_sourceScriptFromVar_ "${varWithScript}"
            local tempfile=`mktemp /tmp/bam_temp_file_${scriptName_global}.XXXXXX` \
                || logErrorAndAbort "Unable to create temp file."
            echo "${script_sourceScriptFromVar_}" > "${tempfile}"
            source "${tempfile}"
            rm "${tempfile}"
        }
        sourceScriptFromVar_FunctionType_global='tmpfile'
    fi

    if [ "${isSubshellRedirectUsable_global}" = 'yes' ]; then
        varValueLineDedupeStripEmptyWithSubshellRedirect='
            varValueLineDedupeStripEmpty () {
                # removes duplicate lines even if they are not consecutive.
                local varName_varValueDedupe="$1"

                assertVariableValue isSubshellRedirectUsable_global yes

                local oldValue_varValueDedupe=''
                getVarR1 oldValue_varValueDedupe "${varName_varValueDedupe}"
                local newValue_varValueDedupe=''

                local line=''
                local copiedLineBefore=''
                while read line; do
                    copiedLineBefore=`echo "${newValue_varValueDedupe}" | grep -E '"'"'^'"'"'"${line}"'"'"'$'"'"'`
                    if [ "${copiedLineBefore}" != '"''"' ]; then
                        continue
                    fi
                    newValue_varValueDedupe="${newValue_varValueDedupe}
${line}"
                done < <( echo "${oldValue_varValueDedupe}" | pipeStripEmptyLines )
                newValue_varValueDedupe=`echo "${newValue_varValueDedupe}" | pipeStripEmptyLines`
                setVar "${varName_varValueDedupe}" "${newValue_varValueDedupe}"
            }
            '
        eval "${varValueLineDedupeStripEmptyWithSubshellRedirect}"
        varValueLineDedupeStripEmptyFunctionType_global='subshellRedirect'
    else
        varValueLineDedupeStripEmptyWithoutSubshellRedirect='
            varValueLineDedupeStripEmpty () {
                logErrorAndAbort "varValueLineDedupeStripEmpty() : not implemented for shells without subshellRedirect"
            }
            '
        eval "${varValueLineDedupeStripEmptyWithoutSubshellRedirect}"
        varValueLineDedupeStripEmptyFunctionType_global='notImplemented'
    fi

    if [ "${isSubshellRedirectUsable_global}" = 'yes' ]; then
        copyStdoutStderrToFileWithSubshellRedirect='
            copyStdoutStderrToFile () {
                local append="no"
                while [ "${1:0:1}" = "-" ]; do
                    if [ "$1" = "-a" -o "$1" = "--append" ]; then
                        append="yes"
                        shift
                    elif [ "$1" = "--" ]; then
                        shift
                        break
                    else
                        logWarning "copyStdoutStderrToFile(): Ignoring unknown option: $1"
                        shift
                    fi
                done

                local logFile="$1"
                assertVar logFile
                if [ "${append}" = "yes" ]; then
                    >>"${logFile}"
                    assertFile "${logFile}"
                    exec > >(tee -a "${logFile}") 2>&1
                else
                    >"${logFile}"
                    assertFile "${logFile}"
                    exec > >(tee "${logFile}") 2>&1
                fi
            }
            '
        eval "${copyStdoutStderrToFileWithSubshellRedirect}"
        copyStdoutStderrToFileFunctionType_global='subshellRedirect'
    else
        copyStdoutStderrToFileWithoutSubshellRedirect='
            copyStdoutStderrToFile () {
                logErrorAndAbort "copyStdoutStderrToFile() : not implemented for shells without subshellRedirect"
            }
            '
        eval "${copyStdoutStderrToFileWithoutSubshellRedirect}"
        copyStdoutStderrToFileFunctionType_global='notImplemented'
    fi

    if [ "${isSubshellRedirectUsable_global}" = 'yes' ]; then
        # Assumption: shells with subshell redirect also have array capabilities.
        # https://www.baeldung.com/linux/reading-output-into-array
        # CAVEAT: linesToArrayR1 and linesToArrayWithStripStandardR1 will strip empty lines
        # However, linesToArrayR1 will preserve lines that do contain whitespace only
        linesToArrayR1WithSubshellRedirect='
            linesToArrayR1 () {
                local returnVarArray_linesToArrayR1="$1"
                local inputLines="$2"

                if [ "${inputLines}" = "" ]; then
                    getVarR1 inputLines "${returnVarArray_linesToArrayR1}"
                fi
                local instanceArray_linesToArrayR1=""
                IFS=$'"'"'\n'"'"' read -r -d "" -a instanceArray_linesToArrayR1 < <( echo "${inputLines}" && printf '"'"'\0'"'"' )
                copyArrayVar instanceArray_linesToArrayR1 "${returnVarArray_linesToArrayR1}"
            }
            linesToArrayWithStripStandardR1 () {
                local returnVarArray_linesToArrayWithStripStandardR1="$1"
                local inputLines="$2"

                if [ "${inputLines}" = "" ]; then
                    getVarR1 inputLines "${returnVarArray_linesToArrayWithStripStandardR1}"
                fi
                local instanceArray_linesToArrayWithStripStandardR1=""
                IFS=$'"'"'\n'"'"' read -r -d "" -a instanceArray_linesToArrayWithStripStandardR1 < <( echo "${inputLines}" | pipeStripStandard && printf '"'"'\0'"'"' )
                copyArrayVar instanceArray_linesToArrayWithStripStandardR1 "${returnVarArray_linesToArrayWithStripStandardR1}"
            }
            copyArrayVar () {
                if [ "$1" = "" ]; then
                    logErrorAndAbort "setVar(): no source variable name specified."
                fi
                if [ "$2" = "" ]; then
                    logErrorAndAbort "setVar(): no target variable name specified."
                fi
                assertValidName "$1"
                assertValidName "$2"
                eval "$2"'"'"'=("${'"'"'"$1"'"'"'[@]}")'"'"'
            }
            '
        eval "${linesToArrayR1WithSubshellRedirect}"
        linesToArrayR1FunctionType_global='subshellRedirect'
    else
        linesToArrayR1WithoutSubshellRedirect='
            linesToArrayR1 () {
                logErrorAndAbort "linesToArrayR1() : not implemented for shells without subshellRedirect"
            }
            linesToArrayWithStripStandardR1 () {
                logErrorAndAbort "linesToArrayWithStripStandardR1() : not implemented for shells without subshellRedirect"
            }
            copyArrayVar () {
                logErrorAndAbort "copyArrayVar() : not implemented for shells without subshellRedirect"
            }
            '
        eval "${linesToArrayR1WithoutSubshellRedirect}"
        linesToArrayR1FunctionType_global='notImplemented'
    fi
}

getTempfileR1 () {
    local returnVar_getTempfileR1="$1"

    assertVariable returnVar_getTempfileR1

    local tempfile_getTempfileR1=`mktemp /tmp/bam_temp_file_${scriptName_global}.XXXXXX` \
                || logErrorAndAbort "Unable to create temp file."

    setVar "${returnVar_getTempfileR1}" "${tempfile_getTempfileR1}"
}

sourceScriptFromVar () {
    local varWithScript="$1"
    if [ "${varWithScript}" = "" ]; then
        # Nothing to do.
        logDebug "sourceScriptFromVar: variable is empty or not defined: ${varWithScript}"
        return
    fi
    sourceScriptFromVar_ "$@"
}

isIntR1 () {
    local returnVar_isIntR1="$1" # Optional.
    local valueToCheck_isIntR1="$2" # Optional. Defaults to value of return var.

    if [ "${returnVar_isIntR1}" != '' -a "${valueToCheck_isIntR1}" = '' ]; then
        getVarR1 valueToCheck_isIntR1 "${returnVar_isIntR1}"
    fi

    assertVar valueToCheck_isIntR1

    # https://unix.stackexchange.com/questions/151654/checking-if-an-input-number-is-an-integer
    if [ "${valueToCheck_isIntR1}" -eq "${valueToCheck_isIntR1}" ] 2>/dev/null; then
        setVarOrLogInfo "${returnVar_isIntR1}" 'yes' "Value is integer: ${valueToCheck_isIntR1}"
    else
        setVarOrLogInfo "${returnVar_isIntR1}" 'no' "Value is NOT integer: ${valueToCheck_isIntR1}"
    fi
}

isFloatR1 () {
    local returnVar_isFloatR1="$1" # Optional.
    local valueToCheck_isFloatR1="$2" # Optional. Defaults to value of return var.

    if [ "${returnVar_isFloatR1}" != '' -a "${valueToCheck_isFloatR1}" = '' ]; then
        getVarR1 valueToCheck_isFloatR1 "${returnVar_isFloatR1}"
    fi

    assertVar valueToCheck_isFloatR1

    # https://stackoverflow.com/questions/12643009/regular-expression-for-floating-point-numbers
    local isFloatTest=`echo "${valueToCheck_isFloatR1}" | grep -E '^[+-]?([0-9]*[.])?[0-9]+$'`
    if [ "${isFloatTest}" != '' ] 2>/dev/null; then
        setVarOrLogInfo "${returnVar_isFloatR1}" 'yes' "Value is float: ${valueToCheck_isFloatR1}"
    else
        setVarOrLogInfo "${returnVar_isFloatR1}" 'no' "Value is NOT float: ${valueToCheck_isFloatR1}"
    fi
}

assertInt () {
    local value_assertInt="$1"
    local message="$2"

    local result=''
    isIntR1 result "${value_assertInt}"
    if [ "${result}" = 'no' ]; then
        setDefault message "Value is not an integer: ${value_assertInt}"
        logErrorAndAbort "${message}"
    fi
}

assertFloat () {
    local value_assertFloat="$1"
    local message="$2"

    local result=''
    isFloatR1 result "${value_assertFloat}"
    if [ "${result}" = 'no' ]; then
        setDefault message "Value is not a float: ${value_assertFloat}"
        logErrorAndAbort "${message}"
    fi
}

isIntInRangeR1 () {
    local returnVar="$1"
    local value="$2"
    local min="$3"
    local max="$4"

    assertVar value
    assertVar min
    assertVar max

    assertInt "${value}"
    assertInt "${min}"
    assertInt "${max}"

    if [ ${value} -lt ${min} -o ${value} -gt ${max} ]; then
        setVarOrLogInfo "${returnVar}" 'no'
        return
    fi
    setVarOrLogInfo "${returnVar}" 'yes'
}

assertIntInRange () {
    local value="$1"
    local min="$2"
    local max="$3"
    local message="$4"

    local result
    isIntInRangeR1 result "$@"

    if [ "${result}" != 'yes' ]; then
        setDefault message "Not in range between ${min} and ${max}: ${value}"
        logErrorAndAbort "${message}"
    fi
}

setVariable () { setVar "$@"; }
getVariableR1 () { getVarR1 "$@"; }
assertVariable () { assertVar "$@"; }

abortLevel_bam000=0
abort () {
    local showStackTrace='yes'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '--noStackTrace' ]; then
            showStackTrace='no'
            shift
        elif [ "$1" = '-w' -o "$1" = '--withStackTrace' ]; then
            showStackTrace='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "abort(): Ignoring unknown option: $1"
            shift
        fi
    done

    let abortLevel_bam000+=1
    # If this is an interactive shell we do not want to exit since
    # this will close the current shell. This is very annoying. To
    # abort execution of the currently running function we can send
    # ourselves SIGINT instead. This is the same as the user hitting
    # ctrl-C on the keyboard and usually yields an exit code of 130.

    logInfo "Aborting."

    if [ "${isBash_global}" = 'yes' -a "${showStackTrace}" = 'yes' ]; then
        if [ "${debugEnabled_global}" = 'yes' ]; then
            getStackTraceR1
        else
            getStackTraceR1
            #getStackTraceShortR1
        fi
    fi

    # Try to send the SIGINT to the subshell itself. We can do that in
    # bash. This will terminate the sub-process, but the parent
    # process will continue to run normally. With this we can nest
    # function calls where we don't care about whether they run or not
    # in a simple sub-process:
    #
    # ( flakeyFunction )
    # echo this will run even if flakeyFunction uses abort

    # We can't use $PPID since it does not get updates with subshell
    # invocation. I.e, these two commands show the same pid:
    #
    # echo $PPID
    # ( echo $PPID )
    #
    # The same goes for echo $$; ( echo $$ )

    # If we do not have the process id of the sub-process, we'll have
    # to pull the brake for everything:
    #
    # ( kill -INT $$ # sends SIGINT to parent process id
    #   echo this will run in the sub-process )
    # echo this will not run because we received SIGINT in this process
    #
    # So the hard brake looks like this
    #
    # ( echo not even trying to send SIGINT
    #   exit 1
    #   echo this will definitely NOT run )
    # echo but this will run

    # The workaround is to set a global variable inside the subshell,
    # which hence does not affect the main shell. We can test for that
    # variable in the abort function.
    #
    # ( SUBSHELL='yes'
    #   flakeyFunction # uses abort
    #   echo this will NOT run )
    # echo this will run
    #
    # $BASHPID is not available on MAC (bash version?), but
    # $BASH_SUBSHELLL is.
    local thisIsASubshellBestGuess='no'

    if [ "${SUBSHELL}" = 'yes' ]; then
        # The user declared that this is a subshell.
        thisIsASubshellBestGuess='yes'
    elif [ "${BASHPID}" != '' -a "${BASHPID}" != "$$" ]; then
        thisIsASubshellBestGuess='yes'
    elif [ "${BASH_SUBSHELL}" != '' ]; then
        if [ "${BASH_SUBSHELL}" -gt 0 ]; then
            thisIsASubshellBestGuess='yes'
        fi
    fi
    if [ "${isBash_global}" = 'yes' ]; then
        if [ "${abortLevel_bam000}" -gt 5 ]; then
            logError "Nested abort level is too high on bash."
            if [ "${isInteractive_global}" = 'yes' ]; then
                logError "Press enter to exit."
                read
            fi
            exit 1
        fi
    else
        # If we forget to set SUBSHELL='yes' on sh, abort seems to
        # call itself recursively until it segfaults.
        logError "Nested abort level is too high on sh."
        logError "Code may be missing SUBSHELL='yes' in a subshell."
        logError "Assuming we are in a subshell at this point."
        thisIsASubshellBestGuess='yes'
    fi
    logDebugVar thisIsASubshellBestGuess
    if [ "${thisIsASubshellBestGuess}" = 'yes' ]; then
        # We are in a subshell. Hopefully.
        if [ "${BAM_ENTER_BEFORE_EXIT}" = 'yes' ]; then
            logInfo "Abort in subshell. Press enter to exit."
            read
        else
            logDebug "Abort in subshell. Using exit."
        fi
        exit 1
        logError "BAD: Still running after subshell 'exit' is BAD."
        logError "Press enter to kill -9 main shell."
        read
        kill -9 $$
    fi

    # We are not in a subshell.
    let abortLevel_bam000-=1

    # Abort by sending ourselves an interrupt signal.
    # Trap handlers may still be in place.
    kill -INT "$$"

    # This still executes if there is a trap handler installed.
    # Remove trap handler first ...
    trap - INT
    # And send another SIGINT
    kill -INT "$$"

    # This should not execute any more. If it does, that is an
    # issue. We'll try to write a trace to disk, because when the
    # shell window closes we don't see what went wrong.

    local traceFile=/tmp/bam_abortFail.${username_global:-unknown_username}
    local stackTrace=''
    getStackTraceR1 stackTrace
    logError "BAD: Abort function still running after SIGINT. Forcing exit."
    echo "BAD: Abort function still running after SIGINT. Forcing exit." \
         >"${traceFile}"
    echo "${stackTrace}" >>"${traceFile}"
    if [ "${isInteractive_global}" = 'yes' ]; then
        echo "Press enter to exit."
        read
    fi
    if [ "${BAM_ENTER_BEFORE_EXIT}" = 'yes' ]; then
        logInfo "Press enter to exit."
        read
    fi
    exit 1
}

# logging functions
logDebugDisable () {
    unset logDebugEnabled_global
    unset logVerboseEnabled_global
    unset logVerboseEnableState_global
    logDebugEnabled_global='no'
    if [ "${logVerboseEnableState_global}" = 'verboseEnabledByDebug' ]; then
        # If logDebugEnable also enable verbose messages turn it back
        # off as well.
        logVerboseEnabled_global='no'
        logVerboseEnableState_global='verboseDisabledByDebug'
    fi
}
logDebugEnable () {
    unset logDebugEnabled_global
    unset logVerboseEnabled_global
    unset logVerboseEnableState_global
    logDebugEnabled_global='yes'
    if [ "${logVerboseEnabled_global}" != 'yes' ]; then
        # logDebugEnable will also enable verbose messages.
        logVerboseEnabled_global='yes'
        logVerboseEnableState_global='verboseEnabledByDebug'
    fi
}
logDebugEnableExport () {
    logDebugEnable
    export logDebugEnabled_global
    export logVerboseEnabled_global
    export logVerboseEnableState_global
}

logVerboseDisable () {
    unset logVerboseEnabled_global
    unset logVerboseEnableState_global
    logVerboseEnabled_global='no'
    logVerboseEnableState_global='verboseDisabledExplicitly'
}

logVerboseEnable () {
    logVerboseEnabled_global='yes'
    logVerboseEnableState_global='verboseEnabledExplicitly'
}

logVerboseEnableExport () {
    logVerboseEnable
    export logVerboseEnabled_global
    export logVerboseEnableState_global
}

logFlashDisable () {
    logFlashClear
    unset logFlashEnabled_global
    logFlashEnabled_global='no'
}

logFlashEnable () {
    unset logFlashEnabled_global
    logFlashEnabled_global='yes'
}

logFlashEnableExport () {
    logFlashEnable
    export logFlashEnabled_global
}


getTimestampR1() {
    local returnVar="$1"

    # Use ISO 8601 for dates and 24h for times
    # see: https://xkcd.com/1179/ and https://xkcd.com/247/ ;-)

    # %Y - 4 digit year
    # %m - 2 digit month
    # %d - 2 digit day
    # %H - 24 hour time
    # %M - 2 digit minute
    # %S - 2 digit second
    # overkill: %N - 9 digit nanoseconds. Does not work on Mac.
    # %Z - variable length timezone (e.g. EDT, AEST)

    # example: 2016-04-01 16:48:22 AEST
    setVar "${returnVar}" "`date +'%Y-%m-%d %H:%M:%S %Z'`"
}

logTimestampEnable () {
    logTimestamp_global='yes'
}
logTimestampDisable () {
    logTimestamp_global='no'
}

logTimerStart_global=''
logTimerStart () {
    logTimerStart_global=`date +%s`
}

logContext_global=''
logContextReset () {
    logContext_global=''
    logDebug "log context has been reset."
}

logContextPush () {
    local newContext="$1"

    assertValidName "${newContext}"
    logContext_global="${logContext_global}${newContext}:"
    logDebug "new log context is: ${logContext_global}"
}

logContextPop () {
    logContext_global=`echo "${logContext_global}" | sed 's/[^:][^:]*:$//'`
    logDebug "new log context is: ${logContext_global}"
}

terminalColumns_global=''
setTerminalColumnsGlobalVar () {
    local previousValue="${terminalColumns_global}"

    # bash and some others set COLUMNS in interactive shells
    terminalColumns_global="${COLUMNS}"
    if [ "${terminalColumns_global}" = '' ]; then
        terminalColumns_global=`stty size 2>/dev/null | awk ' { print $2 } '`
    fi
    if [ "${terminalColumns_global}" = '' ]; then
        terminalColumns_global="${previousValue}"
    fi
    if [ "${terminalColumns_global}" = '' ]; then
        terminalColumns_global=80
    fi
}
setTerminalColumnsGlobalVar

fmt_global=`which fmt 2>/dev/null`
tput_global=`which tput 2>/dev/null`
lastLogVerbosity_global=''
isLogEnabled_global='yes'
logEnable () {
    isLogEnabled_global='yes'
}
logDisable () {
    isLogEnabled_global='no'
}

clearCurrentLine () {
    if [ "${tput_global}" != '' -a "${hostKernelName_global}" != 'FreeBSD' ]; then
        # https://stackoverflow.com/questions/4644350/backspacing-in-bash
        # clear the last line
        # tput not available on busybox / esxi
        # However, if not available it will not clear longer lines, but still work
        printf "\r$(tput el 2>/dev/null)"
    elif [ "${terminalColumns_global}" != '' ]; then
        printf '\r%'"${terminalColumns_global}"'s\r' ' '
    else
        printf '\r'
    fi
}

logClearPendingMessage () {
    logPendingHeaderMessage_global=''
    logPendingHeaderLevel_global=''
    logPendingHeaderWrap_global=''
}
logClearPendingMessage
log_ () {
    if [ "${isLogEnabled_global}" = 'no' ]; then
        return
    fi
    local wrap='no'
    local force='yes'
    local forceTimestamp='no'
    local color='no'
    local pending='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-n' -o "$1" = '-noFmt' -o "$1" = '--noFmt' -o "$1" = '--nowrap' ]; then
            wrap='no'
            shift
        elif [ "$1" = '-w' -o "$1" = '--wrap' ]; then
            wrap='yes'
            shift
        elif [ "$1" = '-f' -o "$1" = '--force' ]; then
            force='yes'
            pending='no'
            shift
        elif [ "$1" = '-p' -o "$1" = '--pending' ]; then
            force='no'
            pending='yes'
            shift
        elif [ "$1" = '-t' -o "$1" = '--timestamp' ]; then
            forceTimestamp='yes'
            shift
        elif [ "$1" = '-c' -o "$1" = '--color' ]; then
            color='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "log_(): Ignoring unknown option: $1"
            shift
        fi
    done
    local logLevel="$1"
    shift
    local logMessage="$*"
    if [ "${logMessage}" = '' ]; then
        return
    fi

    # Flush pending message if the pending message is from a different
    # header level
    if [ "${logLevel:0:6}" = 'header' -a \
         "${logPendingHeaderMessage_global}" != '' -a \
         "${logPendingHeaderLevel_global}" != "${logLevel}" ]; then
        local pendingMessage="${logPendingHeaderMessage_global}"
        local wrapFlag=''
        if [ "${logPendingHeaderWrap_global}" = 'yes' ]; then
            wrapFlag='--wrap'
        fi
        if [ "${logPendingHeaderLevel_global}" = 'header' ]; then
            logHeader1 --force ${wrapFlag} "${pendingMessage}"
        elif [ "${logPendingHeaderLevel_global}" = 'header2' ]; then
            logHeader2 --force ${wrapFlag} "${pendingMessage}"
        elif [ "${logPendingHeaderLevel_global}" = 'header3' ]; then
            logHeader3 --force ${wrapFlag} "${pendingMessage}"
        fi
        logClearPendingMessage
    fi
    if [ "${logLevel:0:6}" = 'header' -a "${force}" = 'no' ]; then
        logPendingHeaderMessage_global="${logMessage}"
        logPendingHeaderLevel_global="${logLevel}"
        logPendingHeaderWrap_global="${wrap}"
        return
    fi
    if [ "${logLevel:0:6}" = 'header' -a "${force}" = 'yes' ]; then
        logClearPendingMessage
    fi
    # Flush pending message if this is not a header message.
    if [ "${logLevel:0:6}" != 'header' -a "${logPendingHeaderMessage_global}" != '' ]; then
        local pendingMessage="${logPendingHeaderMessage_global}"
        local wrapFlag=''
        if [ "${logPendingHeaderWrap_global}" = 'yes' ]; then
            wrapFlag='--wrap'
        fi
        if [ "${logPendingHeaderLevel_global}" = 'header' ]; then
            logHeader1 --force ${wrapFlag} "${pendingMessage}"
        elif [ "${logPendingHeaderLevel_global}" = 'header2' ]; then
            logHeader2 --force ${wrapFlag} "${pendingMessage}"
        elif [ "${logPendingHeaderLevel_global}" = 'header3' ]; then
            logHeader3 --force ${wrapFlag} "${pendingMessage}"
        fi
        logClearPendingMessage
    fi

    if [ "${logContext_global}" != '' -a "${logLevel}" != 'header' ]; then
        logMessage=":${logContext_global} ${logMessage}"
    fi

    if [ "${logTimerStart_global}" != '' ]; then
        local logTimerEnd=`date +%s`
        local elapsed
        let elapsed=${logTimerEnd}-${logTimerStart_global}
        logTimerStart_global=''
        local logLevelSaved="${logLevel}"
        log_ "${lastLogVerbosity_global}" "Elapsed time: ${elapsed} seconds"
        logLevel="${logLevelSaved}"
    fi

    if [ "${logMessage}" = '' ]; then
        return
    fi

    if [ "${lastLogVerbosity_global}" = 'flash' ]; then
        if [ "${logVerboseEnabled_global}" = 'yes' -o "${logDebugEnabled_global}" = 'yes' ]; then
            echo
        else
            if [ -t 1 ]; then
                clearCurrentLine
            fi
        fi
    fi
    logLevelMark=''
    case "${logLevel}" in
        'flash')
            logLevelMark='FLASH'
            wrap='no'
            if [ "${logVerboseEnabled_global}" != 'yes' -o "${logDebugEnabled_global}" != 'yes' ]; then
                if [ ! -t 1 ]; then
                    # Suppress flash messages if stdout is not a terminal
                    return
                fi
            fi
            ;;
        'debug')
            logLevelMark='DEBUG'
            ;;
        'verbose')
            logLevelMark='VERB '
            ;;
        'info')
            logLevelMark='INFO '
            ;;
        'notice')
            logLevelMark='NOTE '
            ;;
        'warning')
            logLevelMark='WARN '
            ;;
        'error')
            logLevelMark='ERROR'
            ;;
        'header')
            logLevelMark='HEAD '
            color='yes'
            ;;
        'header2')
            logLevelMark='HEAD2'
            color='yes'
            ;;
        'header3')
            logLevelMark='HEAD3'
            color='yes'
            ;;
        *)
            logWarning "Unknown log level, using INFO instead: ${logLevel}"
            logLevelMark='INFO '
    esac
    lastLogVerbosity_global="${logLevel}"
    if [ "${logTimestamp_global}" != 'yes' -a "${forceTimestamp}" != 'yes' ]; then
        if [ "${fmt_global}" != '' -a "${wrap}" = 'yes' ]; then
            # Line wrap using fmt keeps long words on a single line,
            # but also merges lines and does not retain existing
            # newlines.
            # Note: we can't use the fmt -p option since it behaves
            # differently on BSD and GNU.
            #
            # When using fold, we can retain newlines, but long words
            # (like a file path longer than 80 chars) will be split.
            logMessage=`echo "${logMessage}" | fmt`
        fi
        if [ "${logLevel:0:6}" = 'header' ]; then
            local separatorHeader=''
            if [ "${logLevel}" = 'header' ]; then
                local separatorHeader='======================================================================'
            elif [ "${logLevel}" = 'header2' ]; then
                local separatorHeader='::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            elif [ "${logLevel}" = 'header3' ]; then
                local separatorHeader='----------------------------------------------------------------------'
            fi
            logMessage="${separatorHeader}
${logMessage}
${separatorHeader}"
        fi
        # add 6 spaces at the beginninng of every line
        logMessage=`echo "${logMessage}" | sed  's/^/      /'`
        # remove 6 spaces at the beginning of the first line
        logMessage="${logMessage:6}"
        # Remove empty lines.
        logMessage=`echo "${logMessage}" | sed  '/^[ ]*$/d'`
        logMessage=`printf "%-5s %s\n" "${logLevelMark}" "${logMessage}"`
    else
        if [ "${logLevel:0:6}" = 'header' ]; then
            local separatorHeader=''
            if [ "${logLevel}" = 'header' ]; then
                separatorHeader='============================================='
            elif [ "${logLevel}" = 'header2' ]; then
                separatorHeader=':::::::::::::::::::::::::::::::::::::::::::::'
            elif [ "${logLevel}" = 'header3' ]; then
                separatorHeader='---------------------------------------------'
            fi
            logMessage="${separatorHeader}
                               ${logMessage}
                               ${separatorHeader}"
        fi
        local timestamp=''
        getTimestampR1 timestamp
        # Remove empty lines.
        logMessage=`echo "${logMessage}" | sed  '/^[ ]*$/d'`
        # Not doing line wrapping, since we have 30 chars before the
        # actual log message.
        logMessage=`printf "%-24s %-5s %s\n" "${timestamp}" "${logLevelMark}" "${logMessage}"`
    fi
    if [ "${logLevel}" = 'flash' ]; then
        # flash does first line only
        logMessage=`printf '%s' "${logMessage}" | head -n 1`
        # and we truncate to the terminal
        setTerminalColumnsGlobalVar
        logMessage="${logMessage:0:${terminalColumns_global}}"
        printf '%s' "${logMessage}"
    else
        if [ "${color}" = 'yes' -a "`which lolcat 2>/dev/null`" != '' -a -t 1 ]; then
            printf '%s\n' "${logMessage}" | lolcat
        else
            printf '%s\n' "${logMessage}"
        fi
    fi
}

logFlash  () {
    local force='no'
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o "$1" = '--force' ]; then
            # Force logging despite debug being disabled.
            force='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    if [ "${logFlashEnabled_global}" = 'yes' -o "${force}" = 'yes' ]; then
        log_ ${passOptions# } 'flash' "$@"
    fi
}

logFlashClear () {
    if [ "${lastLogVerbosity_global}" = 'flash' ]; then
        clearCurrentLine
        lastLogVerbosity_global=''
    fi
}

logFlashPipe () {
    local line=''
    while read line; do
        logFlash -- "${line}"
    done
    logFlashClear
}

logInfoPipe () {
    local line=''
    while read line; do
        logInfo -- "${line}";
    done
    logFlashClear
}

logDebug  () {
    local force='no'
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o "$1" = '--force' ]; then
            # Force logging despite debug being disabled.
            force='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    if [ "${logDebugEnabled_global}" = 'yes' -o "${force}" = 'yes' ]; then
        log_ ${passOptions# } 'debug' "$@"
    fi
}

if [ "${logVerboseFlashDefault_global}" = '' ]; then
    logVerboseFlashDefault_global='no'
fi
logVerboseFlashEnable () {
    logVerboseFlashDefault_global='yes'
}

logVerboseFlashDisable () {
    logVerboseFlashDefault_global='no'
}

logVerbose () {
    local force='no'
    local passOptions=''
    local logFlash='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o "$1" = '--force' ]; then
            # Force logging despite verbose being disabled.
            force='yes'
            shift
        elif [ "$1" = '--flash' ]; then
            logFlash='yes'
            shift
        elif [ "$1" = '--noFlash' ]; then
            logFlash='no'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    if [ "${logVerboseEnabled_global}" = 'yes' -o "${force}" = 'yes' ]; then
        log_  ${passOptions# } 'verbose' "$@"
    else
        if [ "${PS1}" != '' ]; then
            # If we are piping commands through ssh, e.g. for scp or rsync,
            # we do not want to taint stdout. Hence checking for PS1.
            if [ "${logFlash}" = 'yes' -o "${logVerboseFlashDefault_global}" = 'yes' ]; then
                # If we are in a sub-process flash messages might get messy.
                # We will only show flash messages in the main process.
                local isMain=''
                isMainProcessR1 isMain
                if [ "${isMain}" = 'yes' ]; then
                    logFlash ${passOptions# } "$@"
                    return
                fi
            fi
        fi
    fi
}

logVerboseOrFlash () {
    logVerbose --flash "$@"
}

logVerboseOnly () {
    logVerbose --noFlash "$@"
}

logInfo () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    log_ ${passOptions# } 'info' "$@"
}

logNotice () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    log_ ${passOptions# } 'notice' "$@"
}

logWarning () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    log_ ${passOptions# } 'warning' "$@"
}

logError  () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    log_ ${passOptions# } 'error' "$@"
}

logErrorAndAbort () {
    local graceful='no'
    local passOptions=''
    local abortStackTraceOption=''

    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-g' -o "$1" = '-graceful' -o "$1" = '--graceful' ]; then
            graceful='yes'
            shift
        elif [ "$1" = '-n' -o "$1" = '--noStackTrace' ]; then
            abortStackTraceOption='--noStackTrace'
            shift
        elif [ "$1" = '-w' -o "$1" = '--withStackTrace' ]; then
            abortStackTraceOption='--withStackTrace'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    logError ${passOptions# } "$@"

    # Graceful: log the error, but do not abort. Important e.g. of
    # trap handling where you want to do a best effort clean-up.
    if [ "${graceful}" != 'yes' ]; then
        abort ${abortStackTraceOption}
    fi
}

logErrorAndAbortNoStackTrace () {
    logErrorAndAbort --noStackTrace "$@"
}

logErrorAndAbortWithStackTrace () {
    logErrorAndAbort --withStackTrace "$@"
}

logDebugVar () {
    local forceFlag=''
    if [ "$1" = '-f' ]; then
        forceFlag='-f'
        shift
    fi
    logVar_ ${forceFlag} 'debug' "$@"
}
logVerboseVar () {
    local forceFlag=''
    if [ "$1" = '-f' ]; then
        forceFlag='-f'
        shift
    fi
    logVar_ ${forceFlag} 'verbose' "$@"
}

logInfoVar () {
    logVar_ 'info' "$@"
}

logVarDebug () {
    logDebugVar "$@"
}
logVarInfo () {
    logInfoVar "$@"
}
logVarVerbose () {
    logVerboseVar "$@"
}

logVar_ () {
    local force='no'
    if [ "$1" = '-f' ]; then
        force='yes'
        shift
    fi

    local logLevel="$1"
    shift

    if [ "${logLevel}" = 'debug' ]; then
        if [ "${logDebugEnabled_global}" != 'yes' \
             -a "${force}" != 'yes' ]; then
            return
        fi
    fi

    if [ "${logLevel}" = 'verbose' ]; then
        if [ "${logVerboseEnabled_global}" != 'yes' \
             -a "${force}" != 'yes' ]; then
            return
        fi
    fi

    local varName=''
    local varValue=''
    for varName in $*; do
        getVarR1 varValue "${varName}"
        log_ "${logLevel}" "${varName}=${varValue}"
    done
}

logHeader1 () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    log_ ${passOptions# } 'header' "$@"
}

logHeader2 () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    log_ ${passOptions# } 'header2' "$@"
}

logHeader3 () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    log_ ${passOptions# } 'header3' "$@"
}

getStackTraceR1 () {
    local returnVar="$1"
    local skip="$2"
    local limit="$3"

    # caller is not available on standard shell.
    # (it is on bash simulating sh)
    assertBash

    setDefault skip  0  # no skip at the beginning.
    setDefault limit 0 # no limit

    local stackTrace_getStackTraceR1='Stack trace:'
    local count="${skip}"
    local caller=`caller ${count}`
    while [ "${caller}" != '' ]; do
        stackTrace_getStackTraceR1="${stackTrace_getStackTraceR1}
${caller}"
        let "count += 1"
        if [ "${limit}" -gt 0 -a "${count}" -ge "${limit}" ]; then
            break
        fi
        caller=`caller ${count}`
    done
    if [ "${returnVar}" = '' ]; then
        logInfo "${stackTrace_getStackTraceR1}"
    else
        setVar "${returnVar}" "${stackTrace_getStackTraceR1}"
    fi
}

getStackTraceShortR1 () {
    local skip=1
    local limit=5

    # skip=1 hides the entry for getStackTraceShortR1 itself
    getStackTraceR1 "$1" "${skip}" "${limit}"
}

logDebugInteractive () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logDebug "$@"
    fi
}
logDebugInteractiveVar () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logDebugVar "$@"
    fi
}
logFlashInteractive () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logFlash "$@"
    fi
}
logInfoInteractive () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logInfo "$@"
    fi
}
logInfoInteractiveVar () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logVar_ 'info' "$@"
    fi
}
logVerboseInteractive () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logVerbose "$@"
    fi
}
logVerboseInteractiveVar () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logVerboseVar "$@"
    fi
}
logWarningInteractive () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logWarning "$@"
    fi
}
logErrorInteractive () {
    if [ -t 0 -a -t 1 -a "${isInteractive_global}" = 'yes' ]; then
        logError "$@"
    fi
}

assertNoTics () {
    # Check a variable value for single quote aka tick (')
    # $1 variable name
    local value=''
    eval value="\"\$$1\""
    local tickTest="${value#*\'}"
    # comment to fix gitlab syntax highlighting "${2#*\'}"
    if [ "$value" != "${tickTest}" ]; then
        logErrorAndAbort "Tick found in variable ${1} value: ${value}"
    fi
}

assertInteractive () {
    # Can be used if a function requires interactive user feedback
    # that there actually is a user. This avoids stalling scripts run
    # in batch mode.
    if [ ! -t 0 -a -t 1 -o "${isInteractive_global}" != 'yes' ]; then
        logErrorAndAbort "This is not an interactive shell."
    fi
}

libraryUsage () {
    echo "
This is the bam core library file. You can source it in other
scripts and use the functions lested below.

The primary focus of this library is for bash usage, but it also has
limited sh and busybox usability.

If you have wget, you can bootstrap a local instal using this:
wget -qO- https://gitlab.com/astenger/bam/raw/master/bam.sh | sh -s - boot

Or with curl:
curl -s https://gitlab.com/astenger/bam/raw/master/bam.sh | sh -s - boot

Which will either do a git clone in the current directory or download
and extract the tarfile bundle.  "

    echo "
The following global variables are defined:
"
    showGlobalVars
    echo
}

libraryUsageGeneric () {
    if [ "${scriptNameQualified_global}" = '' ]; then
        logError "Unable to determine current script."
        return
    fi
    echo "Library file:
${scriptNameQualified_global}

This library defines the following functions:
"
    # Function names starting or ending with _ are considered hidden.
    cat "${scriptNameQualified_global}" \
        | egrep '^[a-zA-Z][a-zA-Z0-9_]* \(\) \{' \
        | egrep -v '_ \(\) \{' \
        | sed 's/ {.*$//' \
        | sort \
        | uniq

    echo
}

pipeStripComments () {
    local commentSymbol="$1"
    setDefault commentSymbol '#'
    # Delete lines with comments only and strip comments anywhere if
    # they have leading whitespace. E.g. ${#variable} is not a comment.
    local tab=$'\t'
    sed "/^[ ${tab}]*${commentSymbol}.*\$/d" \
        | sed "s/[ ${tab}][ ${tab}]*${commentSymbol}.*//"
}

stripCommentsR1 () {
    # NOTE: the shell looses trailing blank lines.
    local returnVar="$1"
    local value_stripCommentsR1="$2" # Optional.
    local commentSymbol="$3" # Optional.

    if [ "${value_stripCommentsR1}" = '' ]; then
        getVarR1 value_stripCommentsR1 "${returnVar}"
    fi
    local stripped_stripCommentsR1=`
        echo "${value_stripCommentsR1}" \
            | pipeStripComments "${commentSymbol}"`
    setVar "${returnVar}" "${stripped_stripCommentsR1}"
}

pipeStripLeadingWhitespace () {
    # Strip whitespace from the beginning of the line.
    local tab=$'\t'
    sed "s/^[ ${tab}]*//"
}

stripLeadingWhitespaceR1 () {
    # NOTE: the shell looses trailing blank lines.
    local returnVar="$1"
    local value_stripLeadingWhitespaceR1="$2" # Optional.
    if [ "${value_stripLeadingWhitespaceR1}" = '' ]; then
        getVarR1 value_stripLeadingWhitespaceR1 "${returnVar}"
    fi
    local stripped_stripLeadingWhitespaceR1=`
        echo "${value_stripLeadingWhitespaceR1}" \
            | pipeStripLeadingWhitespace`
    setVar "${returnVar}" "${stripped_stripLeadingWhitespaceR1}"
}

pipeStripTrailingWhitespace () {
    # Strip whitespace from the end of the line.
    local tab=$'\t'
    sed "s/[ ${tab}]*\$//"
}

stripTrailingWhitespaceR1 () {
    # NOTE: the shell looses trailing blank lines.
    local returnVar="$1"
    local value_stripTrailingWhitespaceR1="$2" # Optional.
    if [ "${value_stripTrailingWhitespaceR1}" = '' ]; then
        getVarR1 value_stripTrailingWhitespaceR1 "${returnVar}"
    fi
    local stripped_stripTrailingWhitespaceR1="`
        echo "${value_stripTrailingWhitespaceR1}" \
            | pipeStripTrailingWhitespace`"
    setVar "${returnVar}" "${stripped_stripTrailingWhitespaceR1}"
}

pipeStripEmptyLines () {
    # Lines with whitespace only are considrered empty.
    local tab=$'\t'
    sed "/^[ ${tab}]*\$/d"
}

stripEmptyLinesR1 () {
    local returnVar="$1"
    local value_stripEmptyLinesR1="$2" # Optional.
    if [ "${value_stripEmptyLinesR1}" = '' ]; then
        getVarR1 value_stripEmptyLinesR1 "${returnVar}"
    fi
    local stripped_stripEmptyLinesR1="`
        echo "${value_stripEmptyLinesR1}" \
            | pipeStripEmptyLines`"
    setVar "${returnVar}" "${stripped_stripEmptyLinesR1}"
}

pipeStripStandard () {
    pipeStripComments \
        | pipeStripLeadingWhitespace \
        | pipeStripTrailingWhitespace \
        | pipeStripEmptyLines
}

stripStandardR1 () {
    # NOTE: the shell looses trailing blank lines.
    local returnVar="$1"
    local value_stripStandardR1="$2" # Optional.
    if [ "${value_stripStandardR1}" = '' ]; then
        getVarR1 value_stripStandardR1 "${returnVar}"
    fi
    local stripped_stripStandardR1="`
        echo "${value_stripStandardR1}" \
            | pipeStripStandard`"
    setVar "${returnVar}" "${stripped_stripStandardR1}"
}

pipeStripLeadingBlankLines () {
    # see http://sed.sourceforge.net/sed1line.txt
    sed '/./,$!d'
}

stripLeadingBlankLinesR1 () {
    # NOTE: the shell looses trailing blank lines.
    local returnVar="$1"
    local value_stripLeadingBlankLinesR1="$2" # Optional.
    if [ "${value_stripLeadingBlankLinesR1}" = '' ]; then
        getVarR1 value_stripLeadingBlankLinesR1 "${returnVar}"
    fi
    local stripped_stripLeadingBlankLinesR1="`
        echo "${value_stripLeadingBlankLinesR1}" \
            | pipeStripLeadingBlankLines`"
    setVar "${returnVar}" "${stripped_stripLeadingBlankLinesR1}"
}

pipeStripTrailingBlankLines () {
    # see http://sed.sourceforge.net/sed1line.txt
    sed -e :a -e '/^\n*$/{$d;N;ba' -e '}'
}

stripTrailingBlankLinesR1 () {
    # NOTE: the shell looses trailing blank lines.
    local returnVar="$1"
    local value_stripTrailingBlankLinesR1="$2" # Optional.
    if [ "${value_stripTrailingBlankLinesR1}" = '' ]; then
        getVarR1 value_stripTrailingBlankLinesR1 "${returnVar}"
    fi
    local stripped_stripTrailingBlankLinesR1="`
        echo "${value_stripTrailingBlankLinesR1}" \
            | pipeStripTrailingBlankLines`"
    setVar "${returnVar}" "${stripped_stripTrailingBlankLinesR1}"
}

stripR1 () {
    # Remove leading and trailing whitespace.
    local returnVar="$1"
    local value_stripR1="$2" # Optional. Defaults to value of returnVar.

    if [ "${value_stripR1}" = '' ]; then
        getVarR1 value_stripR1 "${returnVar}"
    fi
    local stripped_stripR1=`
        echo "${value_stripR1}" \
            | pipeStripLeadingWhitespace \
            | pipeStripTrailingWhitespace \
            | pipeStripLeadingBlankLines \
            | pipeStripTrailingBlankLines`
    setVar "${returnVar}" "${stripped_stripR1}"
}

getSectionR1 () {
    local returnVar="$1"
    local value_getSectionR1="$2" # Optional. Defaults to value of returnVar.

    if [ "${value_getSectionR1}" = '' ]; then
        getVarR1 value_getSectionR1 "${returnVar}"
    fi
    local section_getSectionR1=`
        echo "${value_getSectionR1}" \
            | pipeGetSection "${startEgrep}" "${endEgrep}"`
    setVar "${returnVar}" "${stripped_stripR1}"
}

pipeStripFirstLine () {
    # Remove the first line.
    sed '1d'
}

pipeStripLastLine () {
    # Remove the last line
    sed '$d'
}

pipeReverseLines () {
    # see: https://stackoverflow.com/questions/742466/how-can-i-reverse-the-order-of-lines-in-a-file
    awk '{a[i++]=$0} END {for (j=i-1; j>=0;) print a[j--] }'
}

pipeGetSection () {
    local startEgrep="$1"
    local endEgrep="$2"

    assertVar startEgrep

    local state='inactive'
    while read line; do
        #logInfoVar line state

        if [ "${state}" = 'inactive' ]; then
            local startTest=`echo "${line}" | grep -E "${startEgrep}"`
            #logInfoVar startTest
            if [ "${startTest}" != '' ]; then
                state='active'
                echo "${line}"
            fi
        elif [ "${state}" = 'active' ]; then
            if [ "${endEgrep}" != '' ]; then
                local endTest=`echo "${line}" | grep -E "${endEgrep}"`
                if [ "${endTest}" != '' ]; then
                    break
                fi
            fi
            echo "${line}"
        fi
    done
}

varValueAppendLine () {
    local varName_appendLineToVarValue="$1"
    local appendValue_appendLineToVarValue="$2"

    local oldValue_appendLineToVarValue=''
    local newValue_appendLineToVarValue=''

    getVarR1 oldValue_appendLineToVarValue "${varName_appendLineToVarValue}"
    if [ "${oldValue_appendLineToVarValue}" = '' ]; then
        newValue_appendLineToVarValue="${appendValue_appendLineToVarValue}"
    else
        newValue_appendLineToVarValue="${oldValue_appendLineToVarValue}
${appendData_appendLineToVarValue}"
    fi
    setVar "${varName_appendLineToVarValue}" "${newValue_appendLineToVarValue}"
}

varValueReverseLines () {
    local varName_reverseLinesOfVarValue="$1"

    local oldValue_reverseLinesOfVarValue=''
    local newValue_reverseLinesOfVarValue=''

    getVarR1 oldValue_appendLineToVarValue "${varName_reverseLinesOfVarValue}"
    newValue_reverseLinesOfVarValue=`
        echo "${oldValue_appendLineToVarValue}" \
            | pipeReverseLines`
    setVar "${varName_reverseLinesOfVarValue}" "${newValue_reverseLinesOfVarValue}"
}

assertDir () {
    local dir="$1"
    local errorMessage="$2"

    assertVar dir
    if [ -d "${dir}" ]; then
        return
    fi
    if [ -e "${dir}" ]; then
        setDefault errorMessage "This is not a directory: ${dir}"
        logErrorAndAbort "${errorMessage}"
    fi
    setDefault errorMessage "Directory does not exist: ${dir}"
    logErrorAndAbort "${errorMessage}"
}

assertDirectory () { assertDir "$@"; }

createDir () {
    local mkdirOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-p' ]; then
            mkdirOptions='-p'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "createDir(): Ignoring unknown option: $1"
            shift
        fi
    done

    local dir_createDir="$1"
    local errorMessage="$2"

    assertVar dir_createDir
    if [ -d "${dir_createDir}" ]; then
        return
    fi
    qualifyPathR1 dir_createDir
    setDefault errorMessage "Unable to create dir: ${dir_createDir}"
    mkdir ${mkdirOptions} "${dir_createDir}"
    assertDir "${dir_createDir}" "${errorMessage}"
    logInfo "Created dir: ${dir_createDir}"
}

assertFile () {
    local file="$1"
    local errorMessage="$2"

    assertVar file

    if [ -e "${file}" -a ! -f "${file}" ]; then
        setDefault errorMessage "Not a regular file: ${file}"
        logErrorAndAbort "${errorMessage}"
    fi
    if [ ! -f "${file}" ]; then
        setDefault errorMessage "File does not exist: ${file}"
        logErrorAndAbort "${errorMessage}"
    fi
}

assertFileOrDir () {
    local fileOrDir="$1"
    local errorMessage="$2"

    assertVar fileOrDir

    if [ -e "${file}" ]; then
        if [ -f "${fileOrDir}" -o -d "${fileOrDir}" ]; then
            return
        fi
        setDefault errorMessage "Not a regular file or directory: ${fileOrDir}"
        logErrorAndAbort "${errorMessage}"
    fi

    if [ ! -f "${fileOrDir}" -a ! -d "${fileOrDir}" ]; then
        setDefault errorMessage "File or dir does not exist: ${fileOrDir}"
        logErrorAndAbort "${errorMessage}"
    fi
}

assertExists () {
    local path="$1"
    local errorMessage="$2"

    if [ ! -e "${path}" ]; then
        setDefault errorMessage "Does not exist: ${path}"
        logErrorAndAbort "${errorMessage}"
    fi
}

assertDoesNotExist () {
    local path="$1"
    local errorMessage="$2"

    if [ -e "${path}" ]; then
        qualifyPathR1 path
        setDefault errorMessage "Does exist: ${path}"
        logErrorAndAbort "${errorMessage}"
    fi
}

qualifyPathR1 () {
    # Expand directory name to fully qualified name.
    #
    # myBinDir=~/bin
    # qualifyPathR1 myBinDir
    #
    # Note that this uses bash tilde expansion:
    # myBinDir=~/bin
    # Whereas this does not and thus has a different meaning:
    # myBinDir="~/bin"

    local returnVar_qualifyPathR1="$1"
    local pathToQualify_qualifyPathR1="$2" # Optional. Use returnVar_qualifyPathR1 if not set.

    if [ "${pathToQualify_qualifyPathR1}" = '' ]; then
        assertVar \
            returnVar_qualifyPathR1 \
            "qualifyPathR1: neither returnVar_qualifyPathR1 nor pathToQualify_qualifyPathR1 specified."
        getVarR1 pathToQualify_qualifyPathR1 "${returnVar_qualifyPathR1}"
    fi

    assertVar pathToQualify_qualifyPathR1
    # Special case: soft link to a file [ to a file ...]
    local basedirForSymlinkResolution=`pwd`
    while [ -L "${pathToQualify_qualifyPathR1}" ]; do
        if [ "${pathToQualify_qualifyPathR1:0:1}" = '/' ]; then
            basedirForSymlinkResolution="${pathToQualify_qualifyPathR1%/*}"
        else
            basedirForSymlinkResolution="${basedirForSymlinkResolution}/${pathToQualify_qualifyPathR1%/*}"
        fi
        pathToQualify_qualifyPathR1="`readlink \"${pathToQualify_qualifyPathR1}\"`"
        if [ "${pathToQualify_qualifyPathR1:0:1}" != '/' ]; then
            # relative symlink
            pathToQualify_qualifyPathR1="${basedirForSymlinkResolution}/${pathToQualify_qualifyPathR1}"
            basedirForSymlinkResolution="${pathToQualify_qualifyPathR1%/*}"
        fi
    done

    local isAbsolutePath='no'
    if [ "${pathToQualify_qualifyPathR1:0:1}" = '/' ]; then
        isAbsolutePath='yes'
    fi
    local existingPath="${pathToQualify_qualifyPathR1}"
    local remainingPath=''
    while [ ! -d "${existingPath}" ]; do
        if [ "${existingPath}" = "${existingPath%/*}" ]; then
            remainingPath="${existingPath}/${remainingPath}"
            existingPath=''
            break
        fi
        remainingPath="${existingPath##*/}/${remainingPath}"
        existingPath="${existingPath%/*}"
    done
    # remainingPath always has a / at the end ...
    remainingPath="${remainingPath%/}"
    if [ "${existingPath}" = '' ]; then
        if [ "${isAbsolutePath}" = 'yes' ]; then
            existingPath='/'
        else
            existingPath='.'
        fi
    fi
    local qualifiedDir_qualifyPathR1=''
    qualifiedDir_qualifyPathR1="`(cd \"${existingPath}\"; pwd -P)`"
    qualifiedDir_qualifyPathR1="${qualifiedDir_qualifyPathR1}/${remainingPath}"
    # If remainingPath was empty, qualifiedDir_qualifyPathR1 has a / at the end ...
    qualifiedDir_qualifyPathR1="${qualifiedDir_qualifyPathR1%/}"
    # Remove duplicate // if we have any
    qualifiedDir_qualifyPathR1=`echo "${qualifiedDir_qualifyPathR1}" | sed 's/\/[\/]*/\//g'`
    setVarOrLogInfo "${returnVar_qualifyPathR1}" "${qualifiedDir_qualifyPathR1}" "${qualifiedDir_qualifyPathR1}"
}

splitPathR2 () {
    local returnVarDirname="$1"  # Optional.
    local returnVarBasename="$2" # Optional.
    local pathToSplit="$3"

    local dirname_splitPathR2="`dirname \"${pathToSplit}\"`"
    local basename_splitPathR2="`basename \"${pathToSplit}\"`"

    qualifyPathR1 dirname_splitPathR2

    setVarOrLogInfo "${returnVarDirname}" "${dirname_splitPathR2}"
    setVarOrLogInfo "${returnVarBasename}" "${basename_splitPathR2}"
}

splitPathR3 () {
    local returnVarDirname="$1"       # Optional.
    local returnVarBasenameNoExt="$2" # Optional.
    local returnVarExt="$3"           # Optional.
    local pathToSplit="$4"

    local basenameWithExt=''
    splitPathR2 "$1" basenameWithExt "$4"

    local basenameNoExt="${basenameWithExt%.*}"
    local basenameJustExt="${basenameWithExt##*.}"
    # Special case if there is no . in the file name
    if [ "${basenameNoExt}" = "${basenameJustExt}" ]; then
        basenameJustExt=''
    fi
    setVarOrLogInfo "${returnVarBasenameNoExt}" "${basenameNoExt}"
    setVarOrLogInfo "${returnVarExt}" "${basenameJustExt}"
}

cdf () {
    # cd into directory of a file or a directory directly
    # renamed from cdi to cdf to avoid conflict with zoxide
    local fileOrDir="$1"

    if [ -d "${fileOrDir}" ]; then
        spushd "${fileOrDir}"
        return
    fi
    local dirName=''
    local fileName=''
    # File does not necessarily need to exist yet.
    splitPathR2 dirName fileName "${fileOrDir}"
    if [ ! -d "${dirName}" ]; then
        logErrorAndAbort "Neither is a directory: ${fileOrDir} ${dirName}"
        return
    fi
    spushd "${dirName}"
}

cdo () {
    # cd out
    spopd
}

logCurrentScript () {
    logTimestampEnable
    if [ "${scriptName_global}" = '' ]; then
        logInfo "Current script: [interactive shell]"
    else
        logInfo "Current script: ${scriptName_global}"
    fi
    logInfo "Running on:     ${hostname_global}"
    if [ "${username_global}" != '' ]; then
        logInfo "Running as:     ${username_global}"
    fi
}

assertValidName () {
    local name="$1"
    local errorMessage="$2"

    local isValid=`echo "${name}" | egrep '^[a-zA-Z_][a-zA-Z0-9_]*$'`
    if [ "${isValid}" = '' ]; then
        setDefault errorMessage "name is invalid: ${name}"
        logErrorAndAbort "${errorMessage}"
    fi
}

moduleLoadHistory_bam000=''
getModuleLoadHistoryMostRecentLastR1 () {
    local style=''
    local unique='no'

    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-s' -o  "$1" = '--short' ]; then
            style='short'
            shift
        elif [ "$1" = '-p' -o "$1" = '--path' ]; then
            style='path'
            shift
        elif [ "$1" = '-e' -o "$1" = '--extended' ]; then
            style='extended'
            shift
        elif [ "$1" = '-u' -o "$1" = '--uniq' ]; then
            uniq='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "getModuleLoadHistoryMostRecentLastR1(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVar_getModuleLoadHistoryMostRecentLastR1="$1"
    local returnValue_getModuleLoadHistoryMostRecentLastR1="${moduleLoadHistory_bam000}"
    if [ "${style}" = 'short' ]; then
        returnValue_getModuleLoadHistoryMostRecentLastR1=`
            echo "${moduleLoadHistory_bam000}" \
                | pipeStripEmptyLines \
                | awk ' { print $1 } '
            `
    elif [ "${style}" = 'path' ]; then
        returnValue_getModuleLoadHistoryMostRecentLastR1=`
            echo "${moduleLoadHistory_bam000}" \
                | pipeStripEmptyLines \
                | awk ' { print $2 } '
            `
    elif [ "${style}" = 'extended' ]; then
        returnValue_getModuleLoadHistoryMostRecentLastR1=`
            echo "${moduleLoadHistory_bam000}" \
                | pipeStripEmptyLines \
                | awk ' { printf "%-12s from %s\n", $1, $2 } '
            `
    fi

    if [ "${uniq}" = 'yes' ]; then
        returnValue_getModuleLoadHistoryMostRecentLastR1=`
            echo "${returnValue_getModuleLoadHistoryMostRecentLastR1}" \
                | uniq
            `
    fi
    setVarOrLogInfo "${returnVar_getModuleLoadHistoryMostRecentLastR1}" "${returnValue_getModuleLoadHistoryMostRecentLastR1}"
}

useGetModulePathR1 () {
    local graceful='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-g' -o "$1" = '-graceful' -o "$1" = '--graceful' ]; then
            # It's totally fine if the module cannot be found
            # on the file system.
            graceful='yes'
            shift
        elif [ "$1" = '-v' -o "$1" = '-verbose' -o "$1" = '--verbose' ]; then
            forceVerbose='-f'
            shift
        elif [ "$1" = '-d' -o "$1" = '-debug' -o "$1" = '--debug' ]; then
            forceDebug='-f'
            forceVerbose='-f'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            # ${FUNCNAME[0]} is not available in sh
            logWarning "useGetModulePathR1(): Ignoring unknown option: $1"
            shift
        fi
    done

    local returnVar_useGetValidModuleNamesR1="$1"
    local moduleName="$2"

    local modulePath_useGetModulePathR1
    assertDirectory "${bamDir_global}"
    if [ "${isBash_global}" = 'yes' -o "${isZsh_global}" = 'yes' ]; then
        extensions=".bash .sh"
    fi

    for dir in \
        "${bamDir_global}" \
        "${bamDir_global}/lib" \
        "${bamDir_global}/../bam_"*"/lib" \
        "${bamDir_global}/../bam_projects/"*"/lib" \
        "~/bam" \
        "~/bam/lib" \
        "~/bam_"*"/lib" \
        "~/bam_projects/"*"/lib" \
        "/root/bam" \
        "/root/bam/lib" \
        "/root/bam_"*"/lib" \
        "/root/bam_projects/"*"/lib" \
        ; do
        if [ "${modulePath_useGetModulePathR1}" != '' ]; then
            # This does not work in sh: break 2
            # So we'll do a separate check here.
            break
        fi
        for extension in ${extensions}; do
            filename="${dir}/${moduleName}${extension}"
            logDebug ${forceDebug} "checking for: ${filename}"
            if [ -f "${filename}" ]; then
                modulePath_useGetModulePathR1="${filename}"
                break
            fi
        done
    done
    if [ "${modulePath_useGetModulePathR1}" = '' ]; then
        if [ "${graceful}" = 'yes' ]; then
            logVerboseOnly ${forceVerbose} "Unable to find module: ${moduleName}"
            return
        fi
        logErrorAndAbort "Unable to find module: ${moduleName}"
        return
    fi
    qualifyPathR1 modulePath_useGetModulePathR1

    setVarOrLogInfo "${returnVar_useGetValidModuleNamesR1}" "${modulePath_useGetModulePathR1}"
}

use () {
    local force='no'
    local graceful='no'
    local forceDebug=''
    local forceVerbose=''
    local url=''
    local listModules='no'
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-f' -o  "$1" = '-force' -o  "$1" = '--force' ]; then
            # Force reload even if it has been loaded before.
            force='yes'
            shift
        elif [ "$1" = '-g' -o "$1" = '-graceful' -o "$1" = '--graceful' ]; then
            # It's totally fine if the module cannot be found
            # on the file system.
            graceful='yes'
            passOptions="${passOptions} --graceful"
            shift
        elif [ "$1" = '-v' -o "$1" = '-verbose' -o "$1" = '--verbose' ]; then
            forceVerbose='-f'
            passOptions="${passOptions} --verbose"
            shift
        elif [ "$1" = '-d' -o "$1" = '-debug' -o "$1" = '--debug' ]; then
            forceDebug='-f'
            forceVerbose='-f'
            passOptions="${passOptions} --debug"
            shift
        elif [ "$1" = '-u' -o "$1" = '-url' -o "$1" = '--url' ]; then
            # Load module from URL. Experimental.
            shift
            url="$1"
            shift
        elif [ "$1" = '-l' -o "$1" = '-list' -o "$1" = '--list' ]; then
            shift
            listModules='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "use(): Ignoring unknown option: $1"
            shift
        fi
    done
    local moduleName="$1"

    local dir=''
    local extension=''
    local filename=''
    local modulePath=''
    local extensions=".sh"

    if [ "${moduleName}" = '' -a "${graceful}" = 'no' ]; then
        listModules='yes'
    fi

    if [ "${listModules}" = 'yes' ]; then
        local loadHistory=''
        getModuleLoadHistoryMostRecentLastR1 loadHistory
        if [ "${loadHistory}" = '' ]; then
            logInfo "No modules have been loaded."
        else
            logInfo "The following modules have been loaded
(most recent last):
${loadHistory}"
        fi
        local moduleList=''
        for dir in \
            "${bamDir_global}/lib" \
            "${bamDir_global}/../bam_"*"/lib" \
            "${bamDir_global}/../bam_projects/"*"/lib" \
            "~/bam/lib" \
            "~/bam_"*"/lib" \
            "~/bam_projects/"*"/lib" \
            "/root/bam/lib" \
            "/root/bam_"*"/lib" \
            "/root/bam_projects/"*"/lib" \
            ; do

            if [ ! -d "${dir}" ]; then
                continue
            fi
            qualifyPathR1 dir
            if [ "${isBash_global}" = 'yes' -o "${isZsh_global}" = 'yes' ]; then
                moduleList="${moduleList}
`find \"${dir}\" -type f -name '*.bash' 2>/dev/null`"
            fi
            moduleList="${moduleList}
`find \"${dir}\" -type f -name '*.sh' 2>/dev/null`"
        done

        moduleList=`echo "${moduleList}" | pipeStripEmptyLines`

        logInfo "Available modules:"
        if [ "${logVerboseEnabled_global}" = 'yes' -o "${forceVerbose}" = '-f' ]; then
            moduleList=`echo "${moduleList}" | sort | uniq`
            logInfo "${moduleList}"
        else
        moduleList=`echo "${moduleList}" | egrep -v '^bam.sh$' | sort | uniq | pipeStripEmptyLines`
            moduleList=`echo "${moduleList}" | sed 's/.*\///' | sort | uniq`
        logInfo "${moduleList}"
        fi
        return
    fi
    assertValidName moduleName "use: invalid module name: ${moduleName}"
    local isModuleAlreadyLoaded=`
              echo "${moduleLoadHistory_bam000}" | egrep "^${moduleName} "`
    if [ "${isModuleAlreadyLoaded}" != '' ]; then
        if [ "${force}" = 'yes' ]; then
            logWarning "Re-loading module: ${moduleName}"
        else
            logDebug ${forceDebug} "Module has already been loaded:
${isModuleAlreadyLoaded}"
            return
        fi
    fi
    if [ "${url}" != '' ]; then
        local moduleCode=''
        getScriptFromUrlR1 moduleCode "${url}"
        logVerboseOnly ${forceVerbose} "Loading module: ${moduleName} from ${url}"
        modulePath="${url}"
        moduleLoadHistory_bam000="${moduleLoadHistory_bam000}
${moduleName} ${modulePath}"
        sourceScriptFromVar moduleCode
    else
        useGetModulePathR1 ${passOptions} modulePath "${moduleName}"

        if [ "${modulePath}" = '' ]; then
            if [ "${graceful}" = 'yes' ]; then
                logVerboseOnly ${forceVerbose} "Unable to find module: ${moduleName}"
                return
            fi
            logErrorAndAbort "Unable to find module: ${moduleName}"
            return
        fi

        logVerboseOnly ${forceVerbose} "Loading module: ${moduleName} from ${filename}"
        if [ "${moduleLoadHistory_bam000}" = '' ]; then
            moduleLoadHistory_bam000="${moduleName} ${modulePath}"
        else
            moduleLoadHistory_bam000="${moduleLoadHistory_bam000}
${moduleName} ${modulePath}"
        fi
        source "${modulePath}"
    fi
}

setVarOrLogInfo () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    local msg="$3"
    setDefault msg "$2"
    if [ "$1" != '' ]; then
        # setVar simply ignores the 3rd argument. Perfect.
        setVar "$@"
    else
        logInfo ${passOptions# } -- "${msg}"
    fi
}

setVarOrLogWarning () {
    local passOptions=''
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--' ]; then
            shift
            break
        else
            passOptions="${passOptions} $1"
            shift
        fi
    done

    local msg="$3"
    setDefault msg "$2"
    if [ "$1" != '' ]; then
        # setVar simply ignores the 3rd argument. Perfect.
        setVar "$@"
    else
        logWarning ${passOptions# } -- "${msg}"
    fi
}

assertValue () {
    local value="$1"
    local errorMessage="$2" # Optional.


    if [ "${value}" = '' ]; then
        setDefault errorMessage "Value is empty."
        logErrorAndAbort "${errorMessage}"
    fi
}

assertVariableValue () {
    local variable="$1"
    local expectedValue="$2"
    local errorMessage="$3"

    assertVar variable
    local currentValue=''
    getVarR1 currentValue "${variable}"

    if [ "${currentValue}" != "${expectedValue}" ]; then
        setDefault \
            errorMessage \
            "Value for variable ${variable} is not as expected:
'${currentValue}' != '${expectedValue}'"
        logErrorAndAbort "${errorMessage}"
    fi
}

setGlobalVarsBam () {
    if [ "${isBash_global}" = 'yes' -o "${isBashSimulatingSh}" = 'yes' ]; then
        eval 'bam_global="${BASH_SOURCE[0]}"'
        if [ "${bam_global}" = 'main' ]; then
            # We are being piped into a bash.
            bam_global='-'
            bamDir_global=''
        else
            if [ "${bam_global}" = '' ]; then
                bam_global='?'
                bamDir_global=''
            else
                qualifyPathR1 bam_global
                bamDir_global=`dirname "${bam_global}"`
            fi
        fi
    else
        # If we are on a plain sh we can't trace ourselves when we are
        # being sourced. ${BASH_SOURCE[0]} is not available and I was
        # unable to find an alternative.
        bam_global='?'
        bamDir_global=''
        # However, we can look in the usual places :-)
        local bam=''
        for bam in \
            "`dirname -- $0`/bam.sh" \
            "`dirname -- $0`/../bam.sh" \
            ~/bam/bam.sh \
            /root/bam/bam.sh \
            ; do
            if [ -f "${bam}" ]; then
                # This may not be the bam that is running right now,
                # but it's better than nothing.
                logDebugInteractive "Found a bam.sh at: ${bam}"
                bam_global="${bam}"
                qualifyPathR1 bam_global
                bamDir_global=`dirname "${bam_global}"`
                break
            fi
        done
    fi
    logDebugVar bam_global bamDir_global

    # scriptNameRawBam_global is initially set in determineShellCapabilities.
    # So are isInteractive_global and isInteractiveShell_global

    if [ "${isInteractiveShell_global}" = 'yes' ]; then
        scriptName_global=''
        scriptNameNoExt_global=''
        scriptDir_global=''
        scriptNameQualified_global=''
    else
        scriptName_global="`basename -- \"$0\"`"
        scriptNameNoExt_global="${scriptName_global%.*}"
        scriptDir_global="`dirname -- \"${scriptNameRawBam_global}\"`"
        qualifyPathR1 scriptDir_global
        scriptNameQualified_global="${scriptDir_global}/${scriptName_global}"
    fi

    local hostnameSimple=`hostname`
    local hostnameFullyQualified=`hostname -f 2>/dev/null`
    # Some older implementations of hostname do not support any
    # parameters. On these platforms `hostname -f` sill SET the
    # hostname to '-f'.
    # Some systems (e.g. mingw) just error out on the -f option.
    local hostnameCheck=`hostname`
    if [ "${hostnameCheck}" = '-f' ]; then
        # Ooops, this went the wrong way.
        hostname "${hostnameSimple}"
        hostname_global="${hostnameSimple}"
    else
        hostname_global="${hostnameFullyQualified}"
    fi
    setDefault hostname_global "${hostnameSimple}"

    # Strip everything after the first dot.
    hostnameShort_global="${hostname_global%%.*}"

    hostKernelName_global=`uname -s`

    # Different implementations of id use different options. Get the
    # full output and chop it up.
    local id=`id 2>/dev/null`
    if [ "${id}" != '' ]; then
        # id is something like: uid=501(andy) gid=20(staff) groups=20(staff),...
        local user=''
        user="${id#*uid=}"            # 501(andy) gid=20(staff) groups=20(staff),...
        user="${user%% *}"            # 501(andy)
        user="${user%%\)}"             # 501(andy
        username_global="${user##*\(}" # andy
        userid_global="${user%%\(*}"   # 501

        local group=''
        group="${id#*gid=}"
        group="${group%% *}"
        group="${group%%\)}"
        groupname_global="${group##*\(}"
        groupid_global="${group%%\(*}"
    fi
    # id command is not available on older busybox
    # Let's try a couple of other things.
    setDefault username_global "${USER}"
    setDefault username_global "${LOGNAME}"
    if [ "${userid_global}" = '' -a "${username_global}" != '' -a -f /etc/passwd ]; then
        userid_global=`cat /etc/passwd | grep -E "${username_global}:" | awk -F ':' ' { print $3 } '`
    fi
    if [ "${groupid_global}" = '' -a "${username_global}" != '' -a -f /etc/passwd ]; then
        groupid_global=`cat /etc/passwd | grep -E "${username_global}:" | awk -F ':' ' { print $4 } '`
    fi
    if [ "${groupname_global}" = '' -a "${groupid_global}" != '' -a -f /etc/group ]; then
        groupname_global=`cat /etc/group | grep -E ":0:[^:]*"'$' | awk -F ':' ' { print $0 } '`
    fi

    tab_global="`echo -n -e '\t'`"

    # Other global variables sane default state.
    setDefault envVarDir_global ~/.bam/envVar
    setDefault abortLevel_bam000 0
    setDefault fmt_global `which fmt 2>/dev/null`

    if [ "${bamMainPid_global}" = '' ]; then
        bamMainPid_global=$$
    fi
}

isMainProcessR1 () {
    returnVar_isMainProcessR1="$1"

    # BASHPID requires bash 4 or later
    if [ "${BASHPID}" = '' ]; then
        setVarOrLogInfo "${returnVar_isMainProcessR1}" 'unknown'
        return
    fi
    if [ "${bamMainPid_global}" = '' ]; then
        setVarOrLogInfo "${returnVar_isMainProcessR1}" 'unknown'
        return
    fi
    if [ "${bamMainPid_global}" = "${BASHPID}" ]; then
        setVarOrLogInfo "${returnVar_isMainProcessR1}" 'yes'
        return
    fi
    setVarOrLogInfo "${returnVar_isMainProcessR1}" 'no'
}

showGlobalVars () {
    local forceVerbosityFlag=''
    if [ "$1" = '-v' -o "$1" = '-f' ]; then
        forceVerbosityFlag='-f'
        shift
    fi

    # This will only show the first line of multi-line values.
    local primaryGlobals=`
              set | egrep '^[a-zA-Z_][a-zA-Z0-9_]*_global=' \
                  | sort`
    local globalVars=`
              set | egrep '^[a-zA-Z_][a-zA-Z0-9_]*_global=' \
                  | sed 's/=.*//' \
                  | sort \
                  | uniq `
    logInfo "Primary global variables:
${primaryGlobals}"

    local additionalGlobals=`
              set | egrep '^[a-zA-Z_][a-zA-Z0-9_]*_[a-z][a-z][a-z]000=' \
                  | sort`
    logVerbose \
        ${forceVerbosityFlag} \
        "Additional global variables:
${additionalGlobals}"
}

showEnvVars () {
    logInfo "Environment variables:
`env`"
}

pipeDownloadFile () {
    local url="$1"

    assertVar url

    local isCurlInPath=`which curl 2>/dev/null`
    local isWgetInPath=`which wget 2>/dev/null`

    local content=''
    if [ "${isCurlInPath}" != '' ]; then
        curl -s "${url}"
    elif [ "${isWgetInPath}" != '' ]; then
        wget -qO- "${url}"
    else
        logErrorAndAbort "Neither curl nor wget found in path." 1>&2
    fi
}

getUrlContentR1 () {
    local returnVar="$1"
    local url="$2"

    assertVar returnVar
    assertVar url

    local isCurlInPath=`which curl 2>/dev/null`
    local isWgetInPath=`which wget 2>/dev/null`

    local content=''
    if [ "${isCurlInPath}" != '' ]; then
        logInfo "Fetching with curl: ${url}"
        content="`curl -s \"${url}\"`"
    elif [ "${isWgetInPath}" != '' ]; then
        logInfo "Fetching with wget: ${url}"
        content="`wget -qO- \"${url}\"`"
    else
        logErrorAndAbort "Neither curl nor wget found in path."
    fi
    setVar "${returnVar}" "${content}"
}

getScriptFromUrlR1 () {
    local returnVar="$1"
    local url="$2"

    assertVar returnVar
    assertVar url
    local script=''
    getUrlContentR1 script "${url}"
    local scriptFirstLine=`echo "${script}"  | head -n 1`
    if [ "${scriptFirstLine:0:3}" != '#!/' ]; then
        logErrorAndAbort "Download does not look like a script.
${scriptFirstLine}"
    fi
    setVar "${returnVar}" "${script}"
}

boot () {
    logInfo "Fetching bootstrap."
    local url='https://gitlab.com/astenger/bam/raw/master/bin/bootstrap.sh'
    use -u "${url}" bootstrap
}

assertIamRoot () {
    # Note that USER is not set when a cron job is executing.
    if [ "${userid_global}" = '0' ]; then
        return
    fi
    logErrorAndAbort "I am not root but: `id`"
}

assertIamRootOrInteractive () {
    # Good for functions that use sudo.
    local logSudoWarning='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-w' -o "$1" = '--logSudoWarning' ]; then
            logSudoWarning='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            # ${FUNCNAME[0]} is not available in sh
            logWarning "assertIamRootOrInteractive(): Ignoring unknown option: $1"
            shift
        fi
    done
    if [ "${userid_global}" = '0' ]; then
        return
    fi
    if [ "${isInteractive_global}" = 'yes' ]; then
        if [ "${logSudoWarning}" = 'yes' ]; then
            logWarning "This may ask for your sudo password."
        fi
        return
    fi
    logErrorAndAbort "I am not root and this is not an interactive shell."
}

bam () {
    # Relaunch a clean bam shell. Helps with development.
    exec bash --rcfile "${bam_global}"
}

bamUpdate () {
    bamUpdateR1
}
bamUpdateR1 () {
    local returnVar_bamUpdateR1="$1"

    local hadUpdates_bamUpdateR1='no'

    local isGitInPath=`which git 2>/dev/null`
    assertVar isGitInPath "git is not in PATH."

    local dirToUpdate=''
    for dirToUpdate in \
        "${bamDir_global}" \
        "${bamDir_global}/../bam_projects/"* \
        ; do

        if [ ! -d "${dirToUpdate}" ]; then
            continue
        fi
        if [ ! -d "${dirToUpdate}/.git" ]; then
            logVerboseOrFlash "Skipping: Does not appear to be a git folder:
${dirToUpdate}"
            continue
        fi
        cdf "${dirToUpdate}"
        local trace=''
        if [ "${isInteractive_global}" = 'yes' ]; then
            logFlash "Updating: ${dirToUpdate}"
            #trace=$(git -c color.ui=always pull | tee /dev/tty)
            trace=$(git -c color.ui=always pull 2>&1)
            if [ "${trace}" = 'Already up to date.' ]; then
                logFlash "${trace}"
            else
                logInfo "Update for: ${dirToUpdate}
${trace}"
            fi
        else
            logInfo "Updating: ${dirToUpdate}"
            trace=$(git pull)
            logInfo "${trace}"
        fi
        local alreadyUpToDateTest=`echo "${trace}" | grep 'Already up to date.'`
        if [ "${alreadyUpToDateTest}" = '' ]; then
            hadUpdates_bamUpdateR1='yes'
        fi
        cdo
    done
    logFlashClear

    if [ "${returnVar_bamUpdateR1}" != '' ]; then
        setVar "${returnVar_bamUpdateR1}" "${hadUpdates_bamUpdateR1}"
    fi
}

removeFileIfExists () {
    local verbosity='logRemovalOnly'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '--logRemovalOnly' ]; then
            verbosity='logRemovalOnly'
            shift
        elif [ "$1" = '--silent' ]; then
            verbosity='silent'
            shift
        elif [ "$1" = '--verbose' ]; then
            verbosity='verbose'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            # ${FUNCNAME[0]} is not available in sh
            logWarning "removeFileIfExists(): Ignoring unknown option: $1"
            shift
        fi
    done

    local fileToRemove="$1"

    assertVariable fileToRemove
    qualifyPathR1 fileToRemove

    if [ ! -e "${fileToRemove}" ]; then
        if [ "${verbosity}" = 'verbose' ]; then
            logInfo "File does not exist: ${fileToRemove}"
        elif [ "${verbosity}" != 'silent' ]; then
            logFlash "File does not exist: ${fileToRemove}"
        fi
        return
    fi
    if [ ! -f "${fileToRemove}" ]; then
        logErrorAndAbort "File is not a regular file: ${fileToRemove}"
    fi
    if [ "${verbosity}" = 'verbose' -o "${verbosity}" = 'logRemovalOnly' ]; then
        logInfo "Removing file: ${fileToRemove}"
    else
        logDebug "Removing file: ${fileToRemove}"
    fi
    rm -f "${fileToRemove}"
    assertDoesNotExist "${fileToRemove}" "Unable to remove file: ${fileToRemove}"
}

isProcessIdRunningR1 () {
    local returnVarIsRunning="$1"
    local pidToCheck="$2"

    local pidCheck=''

    # kill system command special: signal 0 does not send a
    # signal. Can be used to check whether a process is alive or not.
    kill -0 "${pidToCheck}" 2>/dev/null; pidCheck=$?

    if [ "${pidCheck}" != '0' ]; then
        setVarOrLogInfo "${returnVarIsRunning}" 'no'
    else
        setVarOrLogInfo "${returnVarIsRunning}" 'yes'
    fi
}

downcaseR1 () {
    local returnVar_downcaseR1="$1"
    local valueToDowncase_downcaseR1="$2" # Optional. Defaults to value of return var.

    if [ "${returnVar_downcaseR1}" != '' -a \
         "${valueToDowncase_downcaseR1}" = '' ]; then
        getVarR1 valueToDowncase_downcaseR1 "${returnVar_downcaseR1}"
    fi

    # https://stackoverflow.com/questions/2264428/how-to-convert-a-string-to-lower-case-in-bash
    local down_downcaseR1=`echo "${valueToDowncase_downcaseR1}" | tr '[:upper:]' '[:lower:]'`
    setVarOrLogInfo "${returnVar_downcaseR1}" "${down_downcaseR1}"
}

castYesNoR1 () {
    local returnVar_castYesNoR1="$1"
    local valueToCast_castYesNoR1="$2" # Optional. Defaults to value of return var.

    if [ "${returnVar_castYesNoR1}" != '' -a \
         "${valueToCast_castYesNoR1}" = '' ]; then
        getVarR1 valueToCast_castYesNoR1 "${returnVar_castYesNoR1}"
    fi

    local yesOrNo_castYesNoR1=''
    downcaseR1 valueToCast_castYesNoR1
    case "${valueToCast_castYesNoR1}" in
        'yes' | 'true' | '1' | 'positive' | 'affirmative' | 'y' | 'enabled' )
            yesOrNo_castYesNoR1='yes'
            ;;
        'no' | 'false' | '0' | 'negative' | 'n' | 'disabled' )
            yesOrNo_castYesNoR1='no'
            ;;
        *)
            logWarning "Interpreting value '${valueToCast_castYesNoR1}' 'no'."
            yesOrNo_castYesNoR1='no'
    esac
    setVarOrLogInfo "${returnVar_castYesNoR1}" "${yesOrNo_castYesNoR1}"
}

spushd () {
    # Silent version of pushd
    assertDir "$1"
    pushd "$1" 1>/dev/null 2>&1
}

spopd () {
    # Silent version of popd
    popd 1>/dev/null 2>&1
}

installSoftlink () {
    local softlink="$1"
    local softlinkTarget="$2"
    local backupFile="$3"

    if [ -L "${softlink}" ]; then
        local currentTarget=`readlink "${softlink}"`
        if [ "${currentTarget}" = "${softlinkTarget}" ]; then
            logVerboseOnly "Softlink is already in place: (pwd=`pwd`) ${softlink} -> ${softlinkTarget}"
            return
        fi
    elif [ -e "${softlink}" -a ! -f "${softlink}" ]; then
        logErrorAndAbort "Does exist and is not a regular file or softlink: (pwd=`pwd`) ${softlink}"
    fi

    if [ -e "${softlink}" ]; then
        if [ -e "${backupFile}" ]; then
            logVerboseOnly "Previous backup exists: ${backupFile}"
            logVerboseOnly "Removing existing:      (pwd=`pwd`) ${softlink}"
            rm "${softlink}"
        else
            logVerboseOnly "backing up (pwd=`pwd`) ${softlink} to ${backupFile}"
            mv "${softlink}" "${backupFile}"
            assertExists "${backupFile}" "Unable to relocate (pwd=`pwd`) ${softlink} to ${backupFile}"
        fi
    fi
    assertDoesNotExist "${softlink}"
    # I never remember this correctly. ln arguments are like copy,
    # what already exists comes first, the softlink to be created
    # second.
    local trace=`ln -s "${softlinkTarget}" "${softlink}" 2>&1`
    if [ ! -L "${softlink}" ]; then
        logErrorAndAbort "Unable to create softlink: (pwd=`pwd`) ${softlink} -> ${softlinkTarget}
${trace}"
    fi
    logInfo "Installed softlink: (pwd=`pwd`) ${softlink} -> ${softlinkTarget}"

}

installInputrc () {
    local movedExistingInputrc='no'
    if [ -f ~/.inputrc ]; then
        logInfo "Moving ~/.inputrc to ~/.inputrc.old"
        mv ~/.inputrc ~/.inputrc.old
        movedExistingInputrc='yes'
    fi
    logInfo "Installing ~/.inputrc"

    echo '"\e[A": history-search-backward
"\e[B": history-search-forward
"\e[C": forward-char
"\e[D": backward-char
$if Bash
  Space: magic-space
$endif
set completion-ignore-case on
set mark-symlinked-directories on
set show-all-if-ambiguous on
"\M-s": menu-complete' > ~/.inputrc
    assertFile  ~/.inputrc
    if [ "${movedExistingInputrc}" = 'yes' ]; then
        local isDiffAvailable=`which diff 2>/dev/null`
        if [ "${isDiffAvailable}" != '' ]; then
            local diff="`diff ~/.inputrc.old  ~/.inputrc`"
            if [ "${diff}" = '' ]; then
                logInfo "No update."
            else
                logInfo "diff ~/.inputrc.old  ~/.inputrc
${diff}"
            fi
        fi
    fi
}

bamInstall () {
    # Not taking bash initialization overrides into account (--login,
    # --noprofile, --rcfile, --norc), these are the main cases:
    # Interactive login shell: ~/.bash_profile
    # Interactive but not login shell: ~/.bashrc
    # Not interactive and not login shell: ${BASH_ENV}
    # See:
    # https://www.solipsys.co.uk/new/BashInitialisationFiles.html

    if [ "${isBash_global}" = 'yes' ]; then
        (
        cd

        # .bash_profile is executed for login shells, but does not always exist.
        #     console or ssh
        # .bashrc is executed for interactive non-login shells
        #     new terminal or new shell when already logged in

        installSoftlink '.bashrc' "${bamDir_global}/bam.sh" '.bashrc_bamInstall_backup'

        # A fresh mac os Ventura install does not have a .bash_profile and it
        # is required to start bam in a new iterm
        installSoftlink '.bash_profile' '.bashrc' '.bash_profile_bamInstall_backup'

        # we're leaving .profile alone for now. This usually sources .bashrc anyways.
        # https://unix.stackexchange.com/questions/3052/is-there-a-bashrc-equivalent-file-read-by-all-shells
        # bash interactive login shell loads the following files:
        #     /etc/profile
        # and the first one existing and readable of:
        #     ~/.bash_profile
        #     ~/.bash_login
        #     ~/.profile
        # bash non-interactive reads:
        #    ~/.bashrc
        )
    elif [ "${isZsh_global}" = 'yes' ]; then
        logErrorAndAbort 'bamInstall is not yet supported on zsh'
    else
        (
        cd
        # Assuming sh at this point.

        # sh interactive LOGIN shells use /etc/profile and
        # ~/.profile. There is nothing for interactive non-login
        # shells. The idea is that if you export your variables in a
        # login shell all sub-shells inherit the exports so there is
        # no need to run it again. Right.
        installSoftlink '.profile' "${bamDir_global}/bam.sh" '.profile_bamInstall_backup'

        logInfo "Please use 'sh -l' to test."
        )
    fi
    if [ ! -f ~/.inputrc ]; then
        installInputrc
    fi
}

getDateForFilenameR1 () {
    local returnVarDate_getDateForFilenameR1="$1"

    local date_getDateForFilenameR1=`date +'%Y-%m-%d_%H%M%S'`
    setVarOrLogInfo "${returnVarDate_getDateForFilenameR1}" "${date_getDateForFilenameR1}"
}

getRandomIntR1 () {
    # Somewhat random anyways. Do not use for serious stuff, e.g. crypto.
    local returnVarRandom="$1"
    local min="$2"
    local max="$3"

    assertBash

    assertVar min
    assertVar max
    assertInt "${min}"
    assertInt "${max}"
    if [ "${min}" -ge "${max}" ]; then
        logErrorAndAbort "max needs to be greater than min."
    fi
    # https://tldp.org/LDP/abs/html/randomvar.html
    local randomNumber=${RANDOM}
    assertVar randomNumber
    local range
    let "range = ${max} - ${min} + 1"
    let "randomNumber %= ${range}"
    let "randomNumber += ${min}"
    setVarOrLogInfo "${returnVarRandom}" "${randomNumber}"

    # This is way too paranoid ;-)
    #assertIntInRange "${randomNumber}" "${min}" "${max}"
}

getPropertyFromConfigFileR1 () {
    local returnVarProperty="$1"
    local propertyName="$2"
    local propertyFile="$3"

    if [ "${propertyName}" = '' ]; then
        getVarR1 propertyName "${returnVarProperty}"
    fi
    assertVar propertyName "Please specify property name."

    if [ "${propertyFile}" = '' ]; then
        if [ "${scriptName_global}" = '' ]; then
            logErrorAndAbort "Please specify property file."
        fi
        assertVar scriptDir_global
        propertyFile="${scriptDir_global}/../etc/${scriptName_global}.conf"
        qualifyPathR1 propertyFile
        logDebug "Using default config file: ${propertyFile}"
    fi
    assertFile "${propertyFile}"
    # Allows for spaces around =, but not tab
    local value_getPropertyFromConfigFileR1=`egrep "^${propertyName}[ ${tab_global}]*=" "${propertyFile}" | tail -n 1 | sed 's/^[^=][^=]*=[ '"${tab_global}"']*//'`
    setVarOrLogInfo "${returnVarProperty}" "${value_getPropertyFromConfigFileR1}"
}

assertExecutableInPath () {
    local executableName="$1"
    local executableTest=`which "${executableName}" 2>/dev/null`
    assertVar executableTest "Executable not in PATH: ${executableName}"
}

envVarDir_global=~/.bam/envVar
envVarSave () {
    local envVar=''
    setDefault envVarDir_global ~/.bam/envVar
    qualifyPathR1 envVarDir_global
    if [ "$*" = '' ]; then
        logDebug "envVarSave: no variable names passed, nothing to do"
        return
    fi

    createDir -p "${envVarDir_global}"

    for envVar in "$@"; do
        assertValidName "${envVar}"
        if [ "${envVar}" = 'SSHPASS' ]; then
            logWarning "Cowardly refusing to save environment variable: ${envVar}"
            continue
        fi
        local envVarFileName="${envVarDir_global}/${envVar}"
        local currentValue=''
        getVarR1 currentValue "${envVar}"
        if [ -f "${envVarFileName}" ]; then
            local oldValue=`cat "${envVarFileName}"`
            if [ "${currentValue}" = "${oldValue}" ]; then
                logVerboseOnly "Variable unchanged since last save: ${envVar}"
                # We will touch the file to reflect last save.
                touch "${envVarFileName}"
                continue
            fi
            logDebug "Updating saved variable: ${envVar}"
        else
            logDebug "Saving new variable: ${envVar}"
        fi
        touch "${envVarFileName}"
        assertFile "${envVarFileName}"
        chmod u+w,o-w,g-w "${envVarFileName}"
        echo "${currentValue}" > "${envVarFileName}"
        local writtenValue=`cat "${envVarFileName}"`
        if [ "${writtenValue}" != "${currentValue}" ]; then
            logErrorAndAbort "Unable to save value for ${envVar} correctly: ${writtenValue} != ${currentValue}"
        fi
        logVerboseOnly "Variable ${envVar} saved to: ${envVarFileName}"
        export "${envVar}"
        logVerboseOnly "Variable has been set as environment variable/exported: ${envVar}"
    done
}

envVarShowStatus () {
    local envVar="$1"

    assertVariable envVar
    assertValidName "${envVar}"

    setDefault envVarDir_global ~/.bam/envVar
    qualifyPathR1 envVarDir_global

    local envVarFileName="${envVarDir_global}/${envVar}"
    local currentValue=''
    getVarR1 currentValue "${envVar}"
    if [ "${currentValue}" = '' ]; then
        logInfo "No current value set for: ${envVar}"
    else
        logInfo "Current value for ${envVar} is: ${currentValue}"
    fi
    local exportTest=`export | grep " ${envVar}="`
    if [ "${exportTest}" = '' ]; then
        logInfo "Variable is not being exported from this process."
    else
        logInfo "Variable is being exported from this process."
    fi
    if [ -f "${envVarFileName}" ]; then
        logInfo "Env var safe file exists: ${envVarFileName}"
        local valueFromFile=`cat "${envVarFileName}"`
        if [ "${valueFromFile}" = "${currentValue}" ]; then
            logInfo "Saved value is the same as current value."
        else
            logInfo "Saved value is different from current value: ${valueFromFile}"
        fi
    else
        logInfo "Env var safe file does not exist: ${envVarFileName}"
    fi
}

envVarLoad () {
    local envVar=''
    setDefault envVarDir_global ~/.bam/envVar
    qualifyPathR1 envVarDir_global
    if [ ! -d "${envVarDir_global}" ]; then
        logVerboseOnly "There are no saved variables at all."
        return
    fi
    if [ "$*" = '' ]; then
        logDebug "envVarLoad: no variable names passed, nothing to do"
        return
    fi

    for envVar in "$@"; do
        assertValidName "${envVar}"
        local envVarFileName="${envVarDir_global}/${envVar}"
        if [ ! -f "${envVarFileName}" ]; then
            logVerboseOnly "Variable has never been saved: ${envVar}"
            continue
        fi
        local currentValue=''
        getVarR1 currentValue "${envVar}"
        local valueFromFile=`cat "${envVarFileName}"`
        if [ "${currentValue}" = "${valueFromFile}" ]; then
            logVerboseOnly "Variable unchanged remains unchanged comapred to: ${envVarFileName}"
            continue
        fi
        logVerboseOnly "Updating variable ${envVar} with value from file: ${envVarFileName}"
        setVar "${envVar}" "${valueFromFile}"
        export "${envVar}"
    done
}

envVarDelete () {
    local envVar=''
    setDefault envVarDir_global ~/.bam/envVar
    qualifyPathR1 envVarDir_global
    if [ ! -d "${envVarDir_global}" ]; then
        logVerboseOnly "There are no saved variables at all."
        return
    fi
    if [ "$*" = '' ]; then
        logDebug "envVarDelete: no variable names passed, nothing to do"
        return
    fi

    for envVar in "$@"; do
        assertValidName "${envVar}"
        local envVarFileName="${envVarDir_global}/${envVar}"
        if [ ! -f "${envVarFileName}" ]; then
            logVerboseOnly "Variable has never been saved: ${envVar}"
            continue
        fi
        rm -f "${envVarFileName}"
        assertDoesNotExist "${envVarFileName}"
        logVerboseOnly "Saved variable ${envVar} file has been deleted: ${envVarFileName}"
        unset "${envVar}"
        logVerboseOnly "In memory variable has been cleared/unset: ${envVar}"
    done
}

envVarLoadAll () {
    setDefault envVarDir_global ~/.bam/envVar
    qualifyPathR1 envVarDir_global
    if [ ! -d "${envVarDir_global}" ]; then
        logVerboseOnly "There are no saved variables."
        return
    fi
    local savedVarNames=`find "${envVarDir_global}" -type f | sed 's/.*\///'`
    if [ "${savedVarNames}" = '' ]; then
        logVerboseOnly "There are no saved variables."
        return
    fi
    local savedVarName=''
    for savedVarName in ${savedVarNames}; do
        envVarLoad "${savedVarName}"
    done
}

envVarListSaved () {
    setDefault envVarDir_global ~/.bam/envVar
    qualifyPathR1 envVarDir_global
    if [ ! -d "${envVarDir_global}" ]; then
        logInfo "There are no saved variables at all."
        return
    fi
    local savedVarFiles=`find "${envVarDir_global}" -type f`
    if [ "${savedVarFiles}" = '' ]; then
        logInfo "There are no saved variables."
        return
    fi

    logInfo "The following variables have saved values:
${savedVarFiles}"
}

showBootstrap () {
    local bamBootstrapScriptUrl='https://gitlab.com/astenger/bam/-/raw/master/bin/bootstrap.sh'
    cat <<EOF
if [ "`which curl 2>/dev/null`" != '' ]; then
    curl -s '${bamBootstrapScriptUrl}' | sh
elif [ "`which wget 2>/dev/null`" != '' ]; then
    wget -qO- '${bamBootstrapScriptUrl}' | sh
else
    echo "ERROR neither curl nor wget in PATH."
fi
EOF
}

redirectStdoutStderrToFile () {
    local append='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = '-a' -o "$1" = '--append' ]; then
            append='yes'
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "redirectStdoutStderrToFile(): Ignoring unknown option: $1"
            shift
        fi
    done

    local logFile="$1"
    assertVar logFile
    if [ "${append}" = 'yes' ]; then
        >>"${logFile}"
        assertFile "${logFile}"
        exec >> "${logFile}" 2>&1
    else
        >"${logFile}"
        assertFile "${logFile}"
        exec > "${logFile}" 2>&1
    fi
}

# copyStdoutStderrToFile is defined at runtime in defineShellFeatureDependentFunctions

fixme () {
    logErrorAndAbort "FIXME $@"
}

FIXME () {
    fixme "$@"
}

pathDedupe () {
    # Adapted from:
    # https://unix.stackexchange.com/questions/40749/remove-duplicate-path-entries-with-awk-command
    if [ -n "$PATH" ]; then
        old_PATH=$PATH:; PATH=
        while [ -n "$old_PATH" ]; do
            x=${old_PATH%%:*}       # the first remaining entry
            if [ "${x}" = '' ]; then
                continue
            fi
            case $PATH: in
                *:"$x":*) # already there
                    ;;
                *) # not there yet
                    if [ -d "${x}" ]; then
                        PATH=$PATH:$x
                    fi
                    ;;
            esac
            old_PATH=${old_PATH#*:}
        done
        PATH=${PATH#:}
        unset old_PATH x
    fi
    export PATH
}

pathAddOverride () {
    local newPath=''
    for newPath in "$@"; do
        if [ "${newPath}" = '' ]; then
            continue
        fi
        if [ -d "${newPath}" ]; then
            PATH="${newPath}:${PATH}"
        fi
    done
    PATH="${PATH#:}"
    PATH="${PATH%:}"
    pathDedupe
}

pathAddExtend () {
    local newPath=''
    for newPath in "$@"; do
        if [ "${newPath}" = '' ]; then
            continue
        fi
        if [ -d "${newPath}" ]; then
            PATH="${PATH}:${newPath}"
        fi
    done
    PATH="${PATH#:}"
    PATH="${PATH%:}"
    pathDedupe
}

modulePathAddOverride() {
    if [ "${modulePath}" = '' ]; then
        logErrorInteractive "modulePathAddOverride() : can only be used during module loading"
        return
    fi
    assertVar modulePath
    modulePathBin="${modulePath%/*}/../bin"
    qualifyPathR1 modulePathBin

    pathAddOverride "${modulePathBin}"
}

modulePathAddExtend() {
    if [ "${modulePath}" = '' ]; then
        logErrorInteractive "modulePathAddExtend() : can only be used during module loading"
        return
    fi
    assertVar modulePath
    modulePathBin="${modulePath%/*}/../bin"
    qualifyPathR1 modulePathBin

    pathAddExtend "${modulePathBin}"
}

isDirEmptyR1 () {
    local returnVar_isDirEmptyR1="$1"
    local dir="$2"
    assertDir "${dir}"
    # symlinks pointing to dirs are dirs
    # but find won't travel symlinks
    local qualifiedDir=''
    qualifyPathR1 qualifiedDir "${dir}"
    local findResults_isDirEmptyR1=`find "${qualifiedDir}" | head -n 2 | grep -v -E '^'"${qualifiedDir}"'$'`
    if [ "${findResults_isDirEmptyR1}" != '' ]; then
        setVarOrLogInfo "${returnVar_isDirEmptyR1}" 'no'
    else
        setVarOrLogInfo "${returnVar_isDirEmptyR1}" 'yes'
    fi
}
assertDirIsEmpty () {
    local dir="$1"
    local isDirEmpty=''
    isDirEmptyR1 isDirEmpty "${dir}"
    if [ "${isDirEmpty}" != 'yes' ]; then
        logErrorAndAbort "Dir is not empty: ${dir}"
    fi
}
assertDirIsNotEmpty () {
    local dir="$1"
    local isDirEmpty=''
    isDirEmptyR1 isDirEmpty "${dir}"
    if [ "${isDirEmpty}" = 'yes' ]; then
        logErrorAndAbort "Dir is empty: ${dir}"
    fi
}

### MAIN ###

main1 () {
    # determineShellCapabilities needs to happen right away - before we
    # establish better getVarR1 and setVar functions.
    determineShellCapabilities

    defineShellFeatureDependentFunctions

    setDefault logDebugEnabled_global 'no'
    setDefault logVerboseEnabled_global 'no'
    setDefault logFlashEnabled_global 'yes'
    setDefault SUBSHELL 'no'

    # MacOS now defaults to zsh and has an annoying warning.
    export BASH_SILENCE_DEPRECATION_WARNING=1

    if [ "${isInteractive_global}" = 'no' ]; then
        # Enable log timestamp on non-interactive shells by default.
        logTimestampEnable
    fi

    setGlobalVarsBam

    if [ "${debugEnabled_global}" = 'yes' ]; then
        showShellCapabilities
    fi
}

main2 () {
    # Load saved environment variables (if any)
    envVarLoadAll

    if [ "${isInteractiveShell_global}" = 'yes' ]; then
        if [ "${bamDir_global}" != '' ]; then
            if [ "${BAM_AUTO_LOAD_MODULES_SKIP}" != 'yes' ]; then
                use -g interactive
            fi
        fi
    fi

    # iterm shell integration
    if [ -f '~/.iterm2_shell_integration.bash' ]; then
        source ~/.iterm2_shell_integration.bash
    fi
    logFlashClear
}

assertEnvVar () {
    local assertNotEmpty='no'
    while [ "${1:0:1}" = '-' ]; do
        if [ "$1" = "-n" -o "$1" = "--assertNotEmpty" ]; then
            assertNotEmpty="yes"
            shift
        elif [ "$1" = '--' ]; then
            shift
            break
        else
            logWarning "assertEnvVar(): Ignoring unknown option: $1"
            shift
        fi
    done

    local envVarToCheck="$1"
    local message="$2"
    local envVarIsDefinedTest=`env | grep -E "^${envVarToCheck}="`
    local envVarToCheckValue=''

    getVarR1 envVarToCheckValue "${envVarToCheck}"
    if [ "${envVarToCheckValue}" != '' ]; then
        setDefault message "Variable is set but has not been exported: ${envVarToCheck}"
        assertVar envVarIsDefinedTest "${message}"
    else
        setDefault message "Environment variable is not defined: ${envVarToCheck}"
        assertVar envVarIsDefinedTest "${message}"
        if [ "${assertNotEmpty}" = 'yes' ]; then
            setDefault message "Environment variable is defined with empty value: ${envVarToCheck}"
            logErrorAndAbort "${message}"
        else
            setDefault message "Environment variable is defined with empty value: ${envVarToCheck}"
            logWarningInteractive "${message}"
        fi
    fi
}

assertEnvVarWithValue () {
    assertEnvVar --assertNotEmpty "$@"
}

# Check if we are being sourced or not.  This is not pretty and not
# nicely wrapped since it has to run on the top level of the script,
# but the best I found so far.
#
# see https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced
# Above has a good solution for bash, but there is no soluton for
# busybox/esxi.

# New style is determineIsSourcedCode.

determineIsSourcedCode='
isSourced_global=''
if [ "${isBash_global}" = "yes" -o "${isSimulatedSh_global}" = "yes" ]; then
    (return 0 2>/dev/null) \
        && isSourced_global="yes" \
        || isSourced_global="no"
elif [ "${moduleLoading}" = "" ]; then
    isSourced_global="unknown"
elif [ "${moduleLoading}" = "${scriptName_global}" ]; then
    isSourced_global="no"
fi
'

# Keeping showLibraryUsageIfNotSourced for legacy reasons for now.
showLibraryUsageIfNotSourced='
if [ "${isBash_global}" = "yes" -o "${isSimulatedSh_global}" = "yes" ]; then
    (return 0 2>/dev/null) \
        && isSourced_global="yes" \
        || isSourced_global="no"
elif [ "${moduleLoading}" = "" ]; then
    isSourced_global="unknown"
elif [ "${moduleLoading}" = "${scriptName_global}" ]; then
    isSourced_global="no"
fi
if [ "${isSourced_global}" = "no" ]; then
    libraryUsage
    libraryUsageGeneric
    exit
fi
'

runFunctionIfNotSourced () {
    # ATTENTION: you need to run this BEFORE calling this function:
    # eval "${determineIsSourcedCode}"
    local functionToCall="$1"
    shift
    assertValidName "${functionToCall}"

    assertVar isSourced_global
    if [ "${isSourced_global}" = "no" ]; then
        ${functionToCall} "$@"
    else
        if [ "${BAM_SUPPRESS_INFO_MESSAGE_IF_SOURCED}" != 'yes' -a \
             "${functionToCall}" != 'showLibraryUsageAndExit' ]; then
            logInfoInteractive "Script has been sourced. Skipping execution of main function:
${functionToCall} $@"
        fi
    fi
}

showLibraryUsageAndExit () {
    libraryUsage
    libraryUsageGeneric
    exit
}

main1

# These sections need to run NOT in a function. While there might be a way
# to get this handled in bash, this is much trickier in sh.

if [ "$1" = 'boot' -o "${BAM_BOOT}" = 'yes' ]; then
    unset BAM_BOOT
    boot
    # If we have been sourced this will end execution of the source command.
    return 2>/dev/null
    # If we have not been sourced simply exit.
    exit 0
fi

# In other library files, define libraryUsage and copy/paste
# the following line:
# new style:
# eval "${determineIsSourcedCode}"
# runFunctionIfNotSourced showLibraryUsageAndExit
# old style:
# eval "${showLibraryUsageIfNotSourced}"

eval "${determineIsSourcedCode}"
runFunctionIfNotSourced showLibraryUsageAndExit

main2

# Guard against Log4Shell (CVE-2021-44228)
export JAVA_TOOLS_OPTIONS="-Dlog4j2.formatMsgNoLookups=true"

if [ "${HOME}" != '' -a -d "${HOME}/bin" ]; then
    pathAddExtend "${HOME}/bin"
fi
